module Action {
   
    /**
     * 计算两个时间戳的时间间隔
     * 一天以内返回小时差，一天以外显示天数差
     * @param faultDate 传过来的时间
     * @param completeTime 当前时间
     */
    export function countTime(faultDate, completeTime) {

        var usedTime = completeTime - faultDate;  //两个时间戳相差的毫秒数  
        var days = Math.floor(usedTime / (24 * 3600 * 1000));
        //计算出小时数  
        var leave1 = usedTime % (24 * 3600 * 1000);    //计算天数后剩余的毫秒数  
        var hours = Math.floor(leave1 / (3600 * 1000));
        //计算相差分钟数  
        var leave2 = leave1 % (3600 * 1000);        //计算小时数后剩余的毫秒数  
        var minutes = Math.floor(leave2 / (60 * 1000));
        var time = days + "天" + hours + "时" + minutes + "分";
        if (hours < 1) {
            return '刚刚'
        }
        if (days > 0) {
            return days + '天前'
        } else {
            return hours + '小时前'
        }
    }
    /**
     * 根据传来的秒数 转化成hh:mm:ss的格式
     */
    export function turnSecond(second: number) {
        if(second / (3600 * 24) > 1){
            let d = Math.floor(second / (3600 * 24))
            let h: number | string = Math.floor(second % (3600 * 24) / 3600)
            h = h < 10 ? '0'+ h : h
            let m: number | string = Math.floor(second % (3600 * 24) % 3600 / 60)
            m = m < 10 ? '0' + m : m
            let s: number | string = Math.floor((second % (3600 * 24) % 3600 % 60))
            s = s < 10 ? '0' + s : s
            return d+ "天" + h + ":" + m + ":" + s
        }
        if (second / 3600 > 1) {
            let h: number | string = Math.floor(second / 3600)
            h = h < 10 ? '0'+ h : h
            let m: number | string = Math.floor((second % 3600) / 60)
            m = m < 10 ? '0' + m : m
            let s: number | string = Math.floor((second % 3600) % 60)
            s = s < 10 ? '0' + s : s
            return h + ":" + m + ":" + s
        } else if (second / 60 > 1) {
            let m: number | string = Math.floor(second / 60)
            m = m < 10 ? '0' + m : m
            let s: number | string = Math.floor((second % 60))
            s = s < 10 ? '0' + s : s
            return '00:' + m + ":" + s
        } else {
            let s: number | string = second
            s = s < 10 ? '0' + s : s
            return "00:00:" + s
        }
    }
    /**
     * 对象克隆
     */
    export function clone(obj) {

        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            let copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            let copy = [];
            for (var i = 0; i < obj.length; ++i) {
                copy[i] = Action.clone(obj[i]);
            }
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            let copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = Action.clone(obj[attr]);
            }
            return copy;
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
    }

  

    // **
    // 	 * 加载资源
    // 	 * @param url 资源地址
    // 	 * @param call 回调
    // 	 * @param thisObj 回调所属对象
    // 	 * @param param 回调参数，注意第一个默认是纹理，因此param会在第二个参数
    // 	 */
    export function loadUrl(url: string, call: (t: egret.Texture, url: string) => void, thisObj?: any, param?: any): void {
        let self = this
        //暂时不用缓存
        // let texture = self.picCache[url]
        let texture = null
        let endCall = () => {
            return call.call(thisObj, texture, param)
        }

        let loader = new egret.ImageLoader
        loader.addEventListener(egret.Event.COMPLETE, (evt: egret.Event) => {
            let imageLoader = <egret.ImageLoader>evt.currentTarget
            texture = new egret.Texture
            texture._setBitmapData(imageLoader.data)
            endCall()
        }, self)
        loader.load(url)
    }

    /**
     * 取随机数整数(获取数组随机下标用)
     * @prama num 随机数的最大值+1 (可以传数组的length)
     */
    export function shuffle(num: number) {
        return Math.floor((Math.random() * num))
    }
    
    /**
     * 加载提示
     */
    export function showLoading(words: string = '正在加载') {
        wx.showLoading({
            title: words
        })
    }
    /**
     * 加载资源
     */
    export function loadResByUrl(url: string, type: string = 'image'): Promise<egret.Texture> {
        return new Promise((resolve, reject) => {
            let loader = new egret.ImageLoader
            loader.addEventListener(egret.Event.COMPLETE, (event: egret.Event) => {
                let imageLoader = <egret.ImageLoader>event.currentTarget
                let texture = new egret.Texture
                texture._setBitmapData(imageLoader.data);
                resolve(texture)
            }, null)
            loader.load(url)
        })
    }
    /**
     * 移除加载提示
     */
    export function hideLoading() {
        wx.hideLoading()
    }

}
