interface ITFUpdataData{
    time: number,
    score: number
}

interface ITFShareInfo{
    title: string,
    imageUrl: string
}