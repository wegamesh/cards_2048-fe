class Ajax {
    public static ajax: Ajax;
    public timeout: any
    public timeout2: any
    // private api: Api = Api.getInstance()

    private constructor() {

    }

    public static getInstance(): Ajax {
        if (!Ajax.ajax) {
            Ajax.ajax = new Ajax();
        }
        return Ajax.ajax;
    }

    public get(url: string, data?: any) {
        var _self = this
        return new Promise((resolve, reject) => {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            var param = '';
            if (data) {
                if (data.__proto__ === Object.prototype) {
                    for (let key in data) {
                        param += '/' + data[key]
                    }
                } else {
                    param = '/' + data
                }
            }
            request.open(url + param, egret.HttpMethod.GET);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send();
            request.addEventListener(egret.Event.COMPLETE, onGetComplete, this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onGetIOError, this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onGetProgress, this);

            if (_self.timeout) {
                clearTimeout(_self.timeout)
            }
            if (_self.timeout2) {
                clearTimeout(_self.timeout2)
            }
            _self.timeout = setTimeout(() => {
                console.error(url)
                wx.showToast({
                    title: '连接超时',
                    icon: 'none'
                })
                clear()
                reject()
            }, 5000);

            _self.timeout2 = setTimeout(() => {
                Action.showLoading()
            }, 800);

            function clear(){
                clearTimeout(_self.timeout)
                clearTimeout(_self.timeout2)
                _self.timeout = null
                _self.timeout2 = null
                Action.hideLoading()
            }

            function onGetComplete(e) {
                // _self.sendDot('requestSendComplete')
                resolve(request.response);
                clear()
            }
            function onGetIOError(e) {
                wx.showToast({
                    title: '请求失败',
                    icon: 'none'
                });
                // _self.sendDot('requestSendError', e)
                clear();
                reject();
            }
            function onGetProgress(e) {
                // _self.sendDot('requestSendSuccess')
                resolve(JSON.parse(request.response));
                clear()
            }
        })
    }

    public post(url: string, data: any = {}) {
        var _self = this
        return new Promise((resolve, reject) => {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            //设置为 POST 请求
            request.open(url + '?', egret.HttpMethod.POST);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(json2form(data));
            request.addEventListener(egret.Event.COMPLETE, onPostComplete, this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onPostProgress, this);

            if (_self.timeout) {
                clearTimeout(_self.timeout)
            }
            if (_self.timeout2) {
                clearTimeout(_self.timeout2)
            }
            _self.timeout = setTimeout(() => {
                console.error(url)
                wx.showToast({
                    title: '连接超时',
                    icon: 'none'
                })
                clear()
                reject()
            }, 5000);

            _self.timeout2 = setTimeout(() => {
                Action.showLoading()
            }, 800);

            function clear(){
                clearTimeout(_self.timeout)
                clearTimeout(_self.timeout2)
                _self.timeout = null
                _self.timeout2 = null
                Action.hideLoading()
            }

            function onPostComplete(e) {
                clear();
                // _self.sendDot('requestSendComplete')
                resolve(request.response);
            }
            function onPostIOError(e) {
                wx.showToast({
                    title: '请求失败',
                    icon: 'none'
                });
                // _self.sendDot('requestSendError', e)
                clear();
                reject();
            }
            function onPostProgress(e) {
                clear();
                // _self.sendDot('requestSendSuccess')
                resolve(JSON.parse(request.response));
            }
        })
    }

    /**
     * 发送打点信息
     * @param Action 打点信息
     */
    public sendDot(Action: string, error? : string){
        //打点
        XG_STATISTICS.send('pocket_' + Action || "wx")
        let myInfo = AppConfig.myInfo
        let userId = 0
        try{
            if(myInfo.sid){
                userId = myInfo.sid
            }
            let data = {
                userId: userId,
                appId: Api.appId,
                Action: 'pocket_' + Action || "wx",
                info: error
            }
            wx.request({
                url: Api.hostDot + '/Action',
                data: data,
                header: {
                },
                method: "POST",   //后续会改
                success: function (t) {
                }
            })
        }catch(error){
            console.error(error)
        }
        
    }


    public dotPost(url: string, data: any = {}) {
        return new Promise((resolve, reject) => {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            //设置为 POST 请求
            request.open(url + '?', egret.HttpMethod.POST);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(json2form(data));
            request.addEventListener(egret.Event.COMPLETE, onPostComplete, this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onPostProgress, this);

            function onPostComplete(e) {
                resolve(request.response);
            }
            function onPostIOError(e) {
                reject();
            }
            function onPostProgress(e) {
                resolve(JSON.parse(request.response));
                
            }
        })
    }

}

function addItemsToForm(form: any, names: any, obj: any) {
    // eslint-disable-next-line no-use-before-define
    if (obj === undefined || obj === "" || obj === null)
        return addItemToForm(form, names, "");

    if (
        typeof obj === "string" ||
        typeof obj === "number" ||
        obj === true ||
        obj === false
    ) {
        // eslint-disable-next-line no-use-before-define
        return addItemToForm(form, names, obj);
    }

    // eslint-disable-next-line no-use-before-define
    if (obj instanceof Date) return addItemToForm(form, names, obj.toJSON());

    // array or otherwise array-like
    if (obj instanceof Array) {
        return obj.forEach((v, i) => {
            names.push(`[${i}]`);
            addItemsToForm(form, names, v);
            names.pop();
        });
    }

    if (typeof obj === "object") {
        return Object.keys(obj).forEach(k => {
            names.push(k);
            addItemsToForm(form, names, obj[k]);
            names.pop();
        });
    }
    return null;
}
function addItemToForm(form: any, names: any, value: any) {
    let name = encodeURIComponent(names.join(".").replace(/\.\[/g, "["));
    value = encodeURIComponent(value.toString());
    form.push(`${name}=${value}`);
}
function json2form(data: any) {
    let form = [];
    addItemsToForm(form, [], data);
    const body = form.join("&");
    return body;
}
