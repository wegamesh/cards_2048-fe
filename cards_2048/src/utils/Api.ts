class Api {
    public static httpUrl: string = 'https://wxgame.xunguanggame.com/cardgame-go-qa'
    // public static httpUrl: string = 'https://wxgame.xunguanggame.com/cardgame-go'
    public static readonly hostDot: string = 'https://pkwegame.xunguanggame.com/platform-go'
    // 发送打点数据的host
    public static readonly appId: string = 'wxd809893d56d16080'
    public static readonly staticHost: string = 'https://static.xunguanggame.com/pocket'

    private static api: Api;
    private static ajax: Ajax = Ajax.getInstance();

    private constructor() {

    }

    public static getInstance(): Api {
        if (!Api.api) {
            Api.api = new Api();
        }
        return Api.api;
    }

    public static get(url: string, para?: any): Promise<any> {
        
        return new Promise<any>((resolve, reject) => {
            Api.ajax.get(`${Api.httpUrl}` + url, para).then(res => {
                if (typeof res === 'string') {
                    let json = JSON.parse(res)
                    if (json['Code'] === 0) {
                        resolve(json['Data'])
                    } else {
                        console.error(res['Msg'])

                        if (json['Code'] === 1) {
                            reject('tokenError')
                        } else {
                            reject(json['Msg'])
                        }
                    }
                } else {
                    if (res['Code'] === 0) {
                        resolve(res['Data'])
                    } else {
                        console.error(res['Msg'])
                        if (res['Code'] === 1) {
                            reject('tokenError')
                        } else {
                            reject(res['Msg'])
                        }
                    }
                }
            })
        })
    }

    /**
     * 发起post请求，请求异常reject空
     */
    public static post(url: string, data: Object): Promise<any> {
        console.log(`${Api.httpUrl}` + url, data)
        return new Promise<any>((resolve, reject) => {

            Api.ajax.post(`${Api.httpUrl}` + url, data).then(res => {
                if (typeof res === 'string') {
                    let json = JSON.parse(res)
                    if (json['Code'] === 0) {
                        resolve(json['Data'])
                    } else {
                        console.error('Msg', json['Msg'])
                        reject(json['Msg'])
                    }
                } else {
                    if (res['Code'] === 0) {
                        resolve(res['Data'])
                    } else {
                        console.error('Msg', res['Msg'])
                        reject(res['Msg'])
                    }
                }
            }).catch(reject)
        })
    }

    /**
     * 客服消息发送
     */
    public kfPost(url, data: Object): Promise<any>{
        return new Promise( (resolve, reject ) => {
            Api.ajax.post(`https://pkwegame.xunguanggame.com` + url, data).then(res => {
                if (typeof res === 'string') {
                    let json = JSON.parse(res)
                    if (json['Code'] === 0) {
                        resolve(json['Data'])
                    } else {
                        console.error(json['Msg'])
                        reject(json['Msg'])
                    }
                } else {
                    if (res['Code'] === 0) {
                        resolve(res['Data'])
                    } else {
                        console.error(res['Msg'])
                        reject(res['Msg'])
                    }
                }
            })
        })
    }

    /**
     * 发送渠道打点
     */
    protected sendChannelDot(channel: string): void {
        var data = {
            userId: AppConfig.myInfo.sid,
            appId: Api.appId,
            channel: channel
        }
        Api.ajax.dotPost(`${Api.hostDot}` + '/channel', data).then(res => {
            console.log(`渠道数据${data.channel}发送成功 ,${res}`)
        }).catch(err => {
            console.error(`渠道数据${data.channel}发送失败 ,${err}`)
        })
    }

    /**
     * 发起post请求，请求异常reject空
     */
    public dotPost(url: string, data: Object) {
        return new Promise((resolve, reject) => {
            Api.ajax.dotPost(`${Api.httpUrl}` + url, data).then(res => {
                if (typeof res === 'string') {
                    let json = JSON.parse(res)
                    if (json['Code'] === 0) {
                        resolve(json['Data'])
                    } else {
                        console.error('Msg', json['Msg'])
                        reject(json['Msg'])
                    }
                } else {
                    if (res['Code'] === 0) {
                        resolve(res['Data'])
                    } else {
                        console.error('Msg', res['Msg'])
                        reject(res['Msg'])
                    }
                }
            }).catch(reject)
        })
    }


    /**
     *登陆游戏上报
     **/
    public sendloginChannelDot(): void {
        let option = wx.getLaunchOptionsSync() || {}
        console.log("启动参数 option", option)
        //有scene再上报
        var query = option.query;
        var scene = query.scene;
        scene && this.sendChannelDot(scene);
        
        // 广告主
        var adInfo = query.weixinadinfo
        if (adInfo) {
            let attr = adInfo.split('.');
            let adtId = attr[0];
            let gdtVid = query.gdt_vid;
            console.log('weixinadinfo:', adtId, gdtVid);
            this.sendChannelDot(['wxad', adtId, gdtVid].join('_'));
        }
    }

    /**
     *游戏中上报游戏操作
     **/
    public sendGameActionDot(action: string): void {
        let myInfo = AppConfig.myInfo
        let data = {
            userId: myInfo.sid,
            appId: Api.appId,
            action: 'pocket_' + action || "wx"
        }
        //打点
        XG_STATISTICS.send('pocket_' + action || "wx")
        console.log('游戏中上报游戏操作!', data)
        Api.ajax.post(`${Api.hostDot}` + '/action', data).then(res => {
            console.log(`上报游戏操作${data.action}发送成功 ,${res}`)
        }).catch(err => {
            console.error(`上报游戏操作${data.action}发送失败 ,${err}`)
        })
    }
    
    /**
     * 登录
     * [post] code userId
     */
    public static readonly login = '/login'
}


