module lie {
	/**
	 * 监听对象
	 */
	interface TargetEvent {
		type: string;
		listener: Function;
		thisObject: any;
		useCapture: boolean;
		priority: number;
	}

	interface ScaleTarget extends egret.DisplayObject {
		$bgScaleX?: number;	// 初始缩放值
		$bgScaleY?: number;
		$evtScale?: number;	// 缩放倍数
	}

	/**
	 * 控件监听工具类
	 */
	export class EventUtils {

		protected static $scaleTime: number = 100;	// 缩放动画时间

		/**
		 * 移除控件上的所有监听，该方法也适用于没有通过addEventListener来添加的控件
		 */
		public static removeEventListener(target: egret.EventDispatcher): void {
			var value = target.$EventDispatcher;
			var list = [].concat(value[1] || [], value[2] || []);
			var events = <TargetEvent[]>[];
			for (let i in list) {
				let item = list[i];
				for (let j in item) {
					let datas = <TargetEvent[]>item[j];
					for (let k in datas) {
						let event = datas[k];
						target.removeEventListener(event.type, event.listener, event.thisObject, event.useCapture);
					}
				}
			}
		}

		/**
		 * 移除root往下所有的点击事件
		 */
		public static removeEventListeners(root: egret.EventDispatcher): void {
			var self = EventUtils;
			if (root instanceof egret.DisplayObjectContainer)
				for (let i = 0, num = root.numChildren; i < num; i++)
					self.removeEventListeners(root.getChildAt(i));
			else
				self.removeEventListener(root);
		}

		/**
		 * 添加缩放监听，记得用removeEventListener来移除这个监听
		 */
		public static addScaleListener(target: ScaleTarget, scale: number = 0.95): void {
			var addE = function (type, call, thisObj) {
				target.addEventListener(type, call, thisObj);
			};
			var self = EventUtils;
			var clzz = egret.TouchEvent;
			target.$evtScale = scale;
			target.$bgScaleX = target.scaleX;
			target.$bgScaleY = target.scaleY;
			addE(clzz.TOUCH_BEGIN, self.onScleBegin, self);
			addE(clzz.TOUCH_END, self.onScaleEnd, self);
			addE(clzz.TOUCH_CANCEL, self.onScaleEnd, self);
			addE(clzz.TOUCH_RELEASE_OUTSIDE, self.onScaleEnd, self);
		}

		/**
		 * 缩放开始
		 */
		protected static onScleBegin(event: egret.TouchEvent): void {
			var target = <ScaleTarget>event.currentTarget;
			var tween = egret.Tween;
			var scale = target.$evtScale;
			var scaleX = target.scaleX * scale;
			var scaleY = target.scaleY * scale;
			tween.removeTweens(target);
			tween.get(target).to({ scaleX: scaleX, scaleY: scaleY }, EventUtils.$scaleTime);
		}

		/**
		 * 缩放结束
		 */
		protected static onScaleEnd(event: egret.TouchEvent): void {
			var target = <ScaleTarget>event.currentTarget;
			var time = EventUtils.$scaleTime;
			var tween = egret.Tween;
			var scaleX = target.$bgScaleX;
			var scaleY = target.$bgScaleY;
			var bScaleX = scaleX * 1.1;
			var bScaleY = scaleY * 1.1;
			tween.removeTweens(target);
			tween.get(target).to({ scaleX: bScaleX, scaleY: bScaleY }, time).to({ scaleX: scaleX, scaleY: scaleY }, time);
		}

		// 常用的监听类型归类

		/**
		 * 添加TouchTap监听
		 */
		public static addTouchTapListener(target: egret.EventDispatcher, call: Function, thisObj?: any, useCapture?: boolean): void {
			target.addEventListener(egret.TouchEvent.TOUCH_TAP, call, thisObj, useCapture);
		}

		/**
		 * 在TouchTap的基础上进行缩放
		 */
		public static addTouchTapScaleListener(target: ScaleTarget, call: Function, thisObj?: any, scale?: number, useCapture?: boolean): void {
			var self = EventUtils;
			self.addScaleListener(target, scale);
			self.addTouchTapListener(target, call, thisObj, useCapture);
		}

		/**
		 * 添加按住监听
		 * @param target
		 * @param begin 按住时的回调
		 * @param end 松手时的回调，会调用多次，请自己在end里判断
		 */
		public static addTouchingListener(target: egret.EventDispatcher, begin: Function, end: Function, thisObj?: any): void {
			var event = egret.TouchEvent;
			target.addEventListener(event.TOUCH_BEGIN, begin, thisObj);
			target.addEventListener(event.TOUCH_END, end, thisObj);
			target.addEventListener(event.TOUCH_CANCEL, end, thisObj);
			target.addEventListener(event.TOUCH_RELEASE_OUTSIDE, end, thisObj);
		}

		/**
		 * 添加移动监听
		 */
		public static addTouchMoveListener(target: egret.DisplayObject): void {
			var event = egret.TouchEvent;
			var touchX, touchY;
			target.addEventListener(event.TOUCH_BEGIN, function (e) {
				e.stopImmediatePropagation();
				touchX = e.stageX;
				touchY = e.stageY;
			}, null);
			target.addEventListener(event.TOUCH_MOVE, function (e) {
				e.stopImmediatePropagation();
				let newX = e.stageX, newY = e.stageY;
				target.x += newX - touchX;
				target.y += newY - touchY;
				touchX = newX;
				touchY = newY;
			}, null);
		}

		/**
		 * 添加list的选项监听
		 */
		public static addItemTapListener(list: eui.List, call: Function, thisObj?: any): void {
			list.addEventListener(eui.ItemTapEvent.ITEM_TAP, call, thisObj);
		}
	}
}