module lie {

	/**
	 * 时间工具类
	 */
	export class TimeUtils {

        /**
         * 获取天数
         * @param second
         */
		public static getDay(second: number): number {
			return (second + 8 * 3600) / 86400 | 0;
		}

        /**
         * 检测两个秒数是否同一天
         */
		public static isSameDay(second0: number, second1: number): boolean {
			var get = TimeUtils.getDay;
			return get(second0) == get(second1);
		}

		/**
		 * 检测日期是否是同一天
		 */
		public static isSameDayDate(date0: Date, date1: Date): boolean {
			var g = function (date: Date) {
				return Math.floor(date.getTime() / 1000);
			};
			return TimeUtils.isSameDay(g(date0), g(date1));
		}
	}
}