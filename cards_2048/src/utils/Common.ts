module Common {
    export class CommonUtils {
        /**
             * 设置点击转发按钮
             */
        public static setGlobalShare(userId?: any): void {
            wx.onShareAppMessage(function () {
                // 用户点击了“转发”按钮
                var shareInfo: ITFShareInfo = CommonUtils.getShareInfo()
                return CommonUtils.getGlobalShareInfo(shareInfo.title, shareInfo.imageUrl)
            });
            wx.showShareMenu();	//自动显示
        }

        /**
         * 获取全局分享数据
         */
        public static getGlobalShareInfo(title?: string, imageUrl?: string, inviteType?: string, category?: string): any {
            // var query = `share=true&userId=${AppConfig.myInfo.sid}&userName=${AppConfig.myInfo.nickname}`;
            // inviteType && (query += `&inviteType=${inviteType}`)
            // category && (query += `&category=${category}`)
           
            return {
                title: title ? title : '卡牌2048了解一下!!!',
                imageUrl: imageUrl ? imageUrl : 'https://static.xunguanggame.com/card2048/sharePics/cards_logo.png'
            }
        }

        public static getShareInfo(): ITFShareInfo{
            const shareTexts: Array<any> = this.shareTexts,
                  shareImgs: Array<any> = this.shareImgs

            let title = shareTexts[Action.shuffle(shareTexts.length)]
            let imageUrl = shareImgs[Action.shuffle(shareImgs.length)]
            return {
                title,
                imageUrl
            }
        }

        /**
		 * 主动拉起转发
		 */
		public static showShare(title: any, imageUrl?: string, inviteType?: string, category?: string): Promise<any> {
			return new Promise<any>(function (resolve, reject) {
				let obj = CommonUtils.getGlobalShareInfo(title, imageUrl, inviteType, category)
				obj.success = function (res) {
					console.log(res)
					resolve(res)
				};
				obj.fail = function (res) {
					console.log(res)
					reject(res)
				};
				wx.shareAppMessage(obj);
			});
		}

        /**
         * 分享的文案
         */
        public static shareTexts = [
            '超魔性的卡牌2048了解一下～'
        ]

        /**
         * 分享的图片
         */
        public static shareImgs = [
            'https://static.xunguanggame.com/card2048/sharePics/cards_logo.png'
        ]
    }

    

}
