module lie {

	var app: AppViews;	// 存放单例

	/**
	 * 层级容器
	 */
	class Container extends egret.DisplayObjectContainer {

		/**
		 * 移除子控件触发
		 */
		public $doRemoveChild(index: number, notifyListeners?: boolean): egret.DisplayObject {
			var target = super.$doRemoveChild(index, notifyListeners);
			if (target) {
				// 当前层级最顶层
				let last = AppViews.getTopComponent(this);
				last && last.onShow();
			}
			return target;
		}
	}

	/**
	 * 应用的视图管理类
	 */
	export class AppViews {

		private $panelLevel: Container;		// 面板层
		private $dialogLevel: Container;	// 对话框层
		private $topLevel: Container;	// 顶层

		private static loginData: any[]  //用来不同途径的登录信息

		protected constructor() {

		}

		/**
		 * 初始化
		 */
		private init(): void {
			var self = this;
			AppViews.stage.removeChildren();
			self.$panelLevel = self.addLevel('panel')
			self.$dialogLevel = self.addLevel('dialog')
			self.$topLevel = self.addLevel('top', true)
			//如果从其他途径登陆,根据业务显示页面(暂时设置延迟0.5秒,后续根据情况再调整)
			setTimeout(function () {
				let datas = AppViews.loginData
				if (datas) {
					// if (global.guideInfo.isEnd) {

					// } else {
					// 	wx.showToast({
					// 		title: '请先完成新手引导再赠送食物哦',
					// 		icon: 'none'
					// 	})
					// }
					self.excuteCall(datas[0], datas[1]);
					AppViews.loginData = null;
				}
			}, 500)
		}

		/**
		 * 获取面板层
		 */
		public get panelLevel(): Container {
			return this.$panelLevel;
		}

		/**
		 * 获取对话框层
		 */
		public get dialogLevel(): Container {
			return this.$dialogLevel;
		}

		/**
		 * 获取顶层
		 */
		public get topLevel(): Container {
			return this.$topLevel;
		}

		/**
		 * 添加层
		 * @param name 名称
		 */
		public addLevel(name?: string, isOld?: boolean): Container {
			var container = isOld ? new Container() : new Container();
			container.name = name;
			AppViews.stage.addChild(container);
			return container;
		}

		/**
		 * 获取层
		 */
		public getLevel(name?: string): Container {
			return <Container>AppViews.stage.getChildByName(name);
		}

		/**
		 * 获取最顶层
		 */
		public get curPanel(): UIComponent {
			var panel = this.panelLevel;
			return <UIComponent>panel.$children[panel.numChildren - 1];
		}

		/**
		 * 获取最顶对话框
		 */
		public get curDialog(): Dialog {
			var panel = this.dialogLevel;
			return <Dialog>panel.$children[panel.numChildren - 1];
		}

		/**
		 * 获取视图管理类单例
		 */
		public static app(): AppViews {
			if (!app) {
				app = new AppViews;
				app.init();
			}
			return app;
		}

		/**
		 * 获取舞台
		 */
		public static get stage(): egret.Stage {
			return egret.sys.$TempStage;	// egret.MainContext.instance.stage
		}

		// 重点，层及控制管理体现

		/**
		 * 获取当前面板的最顶元素（该面板没有则取下一面板）
		 */
		public static getTopComponent(cont: Container): UIComponent {
			if (cont.numChildren == 0) {
				let parent = cont.parent;
				let index = parent.getChildIndex(cont);
				cont = null;
				for (let i = index - 1; i >= 0; i--) {
					cont = <any>parent.getChildAt(i);
					if (cont.numChildren)
						break;
				}
				if (!cont)
					return null;
			}
			return <UIComponent>cont.$children[cont.numChildren - 1];
		}

		/**
		 * 获取最顶的控件——对话框之下
		 */
		public static getTopCommont(): lie.UIComponent {
			return AppViews.getTopComponent(AppViews.app().dialogLevel);
		}

		/**
		 * 获取当前面板
		 */
		public static get curPanel(): UIComponent {
			return app && app.curPanel;
		}

		/**
		 * 获取当前对话框
		 */
		public static get curDialog(): Dialog {
			return app && app.curDialog;
		}

		/**
		 * 插入一个控件，并居中
		 * @param attr AppViews对象的属性名
		 * @param clzz 需要添加的对象类
		 */
		private static pushChild<T extends UIComponent>(attr: string, clzz: { new (...args: any[]): T }, para1?: any, para2?: any, para3?: any): T {
			var self = AppViews;
			var child = new clzz(para1, para2, para3);
			var container = <Container>self.app()[attr];
			var last = self.getTopComponent(container);	// 当前层级最顶层
			// 隐藏
			last && last.onHide();
			// 控件动画
			// child.scaleX = 0.6
			// child.scaleY = 0.6
			// child.alpha = 0
			// child.an
			child.anchorOffsetX = child.width / 2
			child.anchorOffsetY = child.height / 2

			// var tw = egret.Tween.get(child)
			// tw.to({
			// 	scaleX: 1,
			// 	scaleY: 1,
			// 	alpha: 1
			// }, 300, egret.Ease.backOut).call(() => {
			// 	// if(child.setObscureAlpha){
			// 	// 	child.setObscureAlpha(1)
			// 	// }

			// })

			container.addChild(child);
			self.setInCenter(child);
			return child;
		}

		/**
		 * 移除层里的控件
		 * @param attr AppViews对象的属性名
		 * @param clzz 需要移除的对象类
		 */
		private static removeChild(attr: string, clzz: { new (...args: any[]): UIComponent }): void {
			var panel = AppViews.app()[attr];
			// for (let i = 0, num = panel.numChildren; i < num; i++) {
			// 	let child = panel.getChildAt(i);

			// 	if (child instanceof clzz){
			// 		// let tw = egret.Tween.get(child)
			// 		// tw.to({
			// 		// 	scaleX: 0.5,
			// 		// 	scaleY: 0.5,
			// 		// 	alpha: 0
			// 		// },100).call(()=>{
			// 		panel.removeChildAt(i);
			// 		// })

			// 	}
			// }
			var child = panel.$children.find(item => {
				return item instanceof clzz
			})
			if (child) {
				panel.removeChild(child)
			}
			// panel.$children.forEach( item => {
			// 	if(item instanceof clzz){
			// 		panel.removeChild(item)
			// 	}
			// })
		}

		/**
		 * 将子控件设置在父控件中心点
		 */
		public static setInCenter(child: egret.DisplayObject): void {
			var stage = AppViews.stage;
			child.x = stage.stageWidth / 2;
			child.y = stage.stageHeight / 2;
		}

		/**
		 * 新建一个面板并放入
		 */
		public static pushPanel<T extends UIComponent>(clzz: { new (...args: any[]): T }, para1?: any, para2?: any): T {
			return AppViews.pushChild('panelLevel', clzz, para1, para2);
		}

		/**
		 * 新建一个对话框并放入
		 */
		public static pushDialog<T extends Dialog>(clzz: { new (...args: any[]): T }, para1?: any, para2?: any, para3?: any): T {
			return AppViews.pushChild('dialogLevel', clzz, para1, para2, para3);
		}

		/**
		 * 新建一个对话框并放入
		 */
		public static pushTop<T extends UIComponent>(clzz: { new (...args: any[]): T }, para1?: any, para2?: any): T {
			return AppViews.pushChild('topLevel', clzz, para1, para2);
		}

		/**
		 * 移除面板
		 */
		public static removePanel(clzz: { new (...args: any[]): UIComponent }): void {
			AppViews.removeChild('panelLevel', clzz);
		}

		/**
		 * 移除对话框
		 */
		public static removeDialog(clzz: { new (...args: any[]): Dialog }): void {
			AppViews.removeChild('dialogLevel', clzz);
		}

		private excuteCall(call: Function, thisObj?: any) {
			call && call.call(thisObj)
		}

		/**
		 * 添加初始化后回调，如果已经初始化，则立即执行
		 */
		public static addInitCall(call: Function, thisObj?: any): void {
			if (app) {
				AppViews.removePanelAndDialog().then(res => {
					app.excuteCall(call, thisObj)
				})
			} else {
				AppViews.loginData = [call, thisObj]
			}
		}

		/**
		 * 进入游戏移除除主界面外的其他页面
		 */
		private static removePanelAndDialog() {
			return new Promise((resolve, reject) => {
				let panelNum = AppViews.app().panelLevel.numChildren
				let dialogNum = AppViews.app().dialogLevel.numChildren
				//移除所有dialog
				for (let i = 0; i < dialogNum; i++) {
					let dialog = AppViews.app().dialogLevel.getChildAt(i)
					AppViews.app().dialogLevel.removeChild(dialog)
				}
				//移除除主界面以外的panel,从1开始
				if (panelNum > 1) {
					for (let i = 1; i < panelNum; i++) {
						let panel = AppViews.app().panelLevel.getChildAt(i)
						AppViews.app().panelLevel.removeChild(panel)
					}
				}
				console.log('remove all child')
				resolve()
			})
		}

		/**
		 * 界面上是否拥有该类
		 */
		public static hasView(clzz: any, cont: Container): boolean {
			for (let i = 0, num = cont.numChildren; i < num; i++) {
				let child = cont.getChildAt(i);
				if (child instanceof clzz)
					return true;
			}
			return false;
		}
	}
}