/**
 * 声音控制类
 */
class Sound {

    private static music: any = {};
    /**
     * 初始化音效并播放
     * name: 名称
     * loop？: 是否循环播放
     * volume? : 音量
     * isPreLoad? : 是否是预加载
     */
    public static initPlay(name: string, loop?: boolean, volume?: number, isPreLoad?: boolean) {
        let volume1 = (volume == undefined) ? 0.6 : volume
        if (!Sound.music[`${name}`]) {
            var url = `https://static.xunguanggame.com/card2048/sounds/${name}`
            var innerAudioContext = wx.createInnerAudioContext()
            innerAudioContext.src = url
            //系统静音开关
            innerAudioContext.obeyMuteSwitch = true
            innerAudioContext.volume = volume1
            //是否循环播放
            if (loop) {
                innerAudioContext.loop = true
            }
            Sound.music[`${name}`] = innerAudioContext
            //播放音乐
            Sound.play(name, volume1, isPreLoad)
        } else {
            Sound.play(name, volume1, isPreLoad)
        }
    }

    /**
     * 暂停
     * pramas:
     * name: 名称
     */
    public static pause(name: string) {
        try {
            let music = Sound.music[`${name}`]
            music && music.pause()
            // global.userGameInfo.soundOn && music && music.pause()
        } catch (error) {
            console.log(error)
        }
    }

    /**
     * 播放
     * pramas：
     * name: 名称
     */
    public static play(name: string, volume: number = 0.6, isPreLoad?: boolean) {
        try {
            let music = Sound.music[`${name}`]
            music && (music.volume = volume)
            //预加载先静音播放一遍
            if (isPreLoad) {
                music.volume = 0
                music.play()
                return
            }
            music && music.play()
            //记录当前播放音乐名(过滤掉音效)
            
        } catch (error) {
            console.log(error)
        }
    }

    /**
     * 静音
     */
    public static soundOff() {
        for (let key in Sound.music) {
            Sound.music[key].stop()
        }
    }
}       