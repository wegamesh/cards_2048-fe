module lie {
	/**
	 * 游戏生命周期管理类
	 * 注：调试模式不要开启
	 */
	export class LifeCycle {

		/**
		 * 游戏生命周期初始化
		 */
		public static init(): void {
			var self = LifeCycle;
			var lifecycle = egret.lifecycle;
			lifecycle.addLifecycleListener((context) => {
				context.onUpdate = self.update;
			});
			lifecycle.onPause = self.pause;
			lifecycle.onResume = self.resume;
		}

		/**
		 * 全局游戏更新函数
		 */
		public static update(): void {

		}

		/**
		 * 全局游戏暂停函数
		 */
		public static pause(): void {
			egret.ticker.pause();
		}

		/**
		 * 全局游戏恢复函数
		 */
		public static resume(): void {
			egret.ticker.resume();
		}
	}
}