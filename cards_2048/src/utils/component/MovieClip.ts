module lie {
	/**
	 * 影片剪辑，实现可于皮肤创建
	 */
	export class MovieClip extends UIComponent {

		private $factory: egret.MovieClipDataFactory;
		private $movieClip: egret.MovieClip;

		private $image: string;
		private $frame: string;
		private $action: string;

		private m_pClipCall: Function;
		private m_pThisObj: any;

		protected childrenCreated(): void {
			var self = this;
			var getr = RES.getResAsync;
			// 这里就不检测资源的正确性了，调用的时候注意即可
			getr(self.image, function (texture) {
				if (!self.isDestroy) {
					getr(self.frame, function () {
						if (!self.isDestroy)
							self.initMovieClip();
					}, null);
				}
			}, null);
		}

		protected onDestroy(): void {
			var self = this;
			var clip = self.$movieClip;
			self.clearMovieCall();
			self.$factory = null;
			// 移除动画
			if (clip) {
				clip.stop();
				self.$movieClip = null;
			}
		}

		/**
		 * 初始化数据
		 */
		private initMovieClip(): void {
			var self = this;
			var getr = RES.getRes;
			self.$factory = new egret.MovieClipDataFactory(getr(self.frame), getr(self.image))
			self.addChild(self.$movieClip = new egret.MovieClip);
			self.carryMovieCall();
		}

		/**
		 * 执行回调
		 */
		private carryMovieCall(): void {
			var self = this;
			var call = self.m_pClipCall;
			if (call) {
				let clip = self.$movieClip;
				// 初始化数据
				clip.movieClipData = self.$factory.generateMovieClipData(self.action);
				call.call(self.m_pThisObj, clip);
				self.clearMovieCall();	// 执行完销毁
			}
		}

		/**
		 * 清空回调
		 */
		private clearMovieCall(): void {
			var self = this;
			self.m_pClipCall = self.m_pThisObj = null;
		}

		/**
		 * 若命名一致，可取其前缀
		 */
		public set res(value: string) {
			this.image = value + '_png';
			this.frame = value + '_json';
		}

		/**
		 * 设置帧动画合图资源名
		 */
		public set image(value: string) {
			this.$image = String(value);
		}

		/**
		 * 获取帧动画合图资源名
		 */
		public get image(): string {
			return this.$image;
		}

		/**
		 * 设置帧动画数据资源名
		 */
		public set frame(value: string) {
			this.$frame = String(value);
		}

		/**
		 * 获取帧动画数据资源名
		 */
		public get frame(): string {
			return this.$frame;
		}

		/**
		 * 设置当前动画名称
		 */
		public set action(value: string) {
			this.$action = String(value);
		}

		/**
		 * 获取当前动画名称
		 */
		public get action(): string {
			return this.$action;
		}

		// 供外部使用的方法

		/**
		 * 只能通过该方法来获取MovieClip对象
		 */
		public addMovieCall(call: (clip: egret.MovieClip) => void, thisObj?: any): void {
			var self = this;
			self.m_pClipCall = call;
			self.m_pThisObj = thisObj;
			self.$factory && self.carryMovieCall();	// 存在则立马执行
		}

		/**
		 * 播放
		 */
		public play(playTimes?: number, cb?: (clip: egret.MovieClip) => void, thisObj?: any): void {
			this.addMovieCall(function (clip) {
				clip.gotoAndPlay(0, playTimes);
				cb && clip.once(egret.Event.COMPLETE, cb, thisObj);
			}, null);
		}

		/**
		 * 暂停
		 */
		public stop(cb?: (clip: egret.MovieClip) => void, thisObj?: any): void {
			this.addMovieCall(function (clip) {
				clip.stop()
			}, null);
		}
	}
}