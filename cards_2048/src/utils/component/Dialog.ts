module lie {
	var single: SignleDialog;

	/**
	 * 对话框，注意onDestroy加了点东西
	 */
	export class Dialog extends AppComponent {

		protected obscure: eui.Rect | egret.Shape;		// 背景朦层

		private m_pCall: Function;		// 关闭回调
		private m_pThis: any;

		protected onCreate(): void {
			this.initObscure();
		}

		public $onRemoveFromStage(): void {
			super.$onRemoveFromStage();
			var self = this;
			var call = self.m_pCall;
			if (call) {
				call(self.m_pThis);
				self.m_pCall = self.m_pThis = null;
			}
		}

		/**
		 * 初始化朦层
		 */
		private initObscure(): void {
			var self = this;
			var stage = self.stage;
			var rect = self.obscure = new eui.Rect;
			rect.width = stage.stageWidth;
			rect.height = stage.stageHeight;
			rect.horizontalCenter = rect.verticalCenter = 0;
			// rect.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
			// 	this.removeFromParent()
			// }, this)
			self.addChildAt(rect, 0);
			self.setObscureAlpha(0.5);
			rect.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
				this.removeFromParent()
			}, this)
		}

		/**
		 * 设置朦层的透明度
		 */
		public setObscureAlpha(alpha: number): void {
			this.obscure.alpha = alpha;
		}

		/**
		 * 添加朦层监听，点击关闭
		 */
		public addOCloseEvent(): void {
			var self = this;
			EventUtils.addTouchTapListener(self.obscure, self.onClose, self);
		}

		/**
		 * 添加关闭回调
		 */
		public addCloseCall(call: Function, thisObj?: any): void {
			var self = this;
			self.m_pCall = call;
			self.m_pThis = thisObj;
		}

		/**
		 * 关闭窗口
		 */
		protected onClose(event: egret.Event): void {
			event.stopImmediatePropagation();
			this.removeFromParent();
		}

		/**
		 * 显示单一的对话框，对话框样式固定
		 * @param 内容
		 * @param 点击确定的回调
		 * @param 回调所属对象
		 */
		public static showSingleDialog(content: string, call: Function, thisObj?: any): SignleDialog {
			if (!single) {
				single = AppViews.pushDialog(SignleDialog);
				single.skinName = AppConfig.getSkin('SingleDialogSkin');
			}
			single.setText(content);
			single.addCall(call, thisObj);
			return single;
		}

		/**
		 * 隐藏单一对话框
		 */
		public static hideSingleDialog(): void {
			if (single) {
				single.visible = false;
				single.clearCall();
			}
		}

		/**
		 * 移除单一对话框
		 */
		public static removeSingDialog(): void {
			var r = single;
			if (r) {
				single = null;
				r.removeFromParent();
			}
		}

		//// 轮流对话框

		private static $turnDialogs: TDialog[] = [];
		private static $turnParams: any[] = [];
		private static $isShowTurn: boolean;

		/**
		 * 运行循环
		 */
		protected static runTurn(): void {
			var self = Dialog;
			let clzz = self.$turnDialogs.shift();
			if (clzz) {
				let param = self.$turnParams.shift();
				AppViews.pushDialog(clzz, param).
					addCloseCall(self.runTurn);
			}
			else
				self.$isShowTurn = false;
		}

		/**
		 * 轮流显示对话框
		 * @param dialogs 需要现实的对话框类
		 * @param params 对话框类构造函数参数，注意长度一致，注意每隔类的参数只支持一个，多个参数的请修改该类
		 */
		public static showDialogsInTurn(dialogs: TDialog[], params?: any[]): void {
			var length = dialogs.length;
			if (length) {
				let self = Dialog;
				let turns = self.$turnDialogs;
				if (params == void 0)
					params = Utils.memset(length, void 0);
				else
					params.length = length;
				self.$turnDialogs = turns.concat(dialogs);
				self.$turnParams = self.$turnParams.concat(params);
				if (!self.$isShowTurn) {
					self.$isShowTurn = true;
					self.runTurn();
				}
			}
		}

		/**
		 * 放对话框进入轮流
		 * @param dialog
		 * @param param 构造参数，仅支持一个
		 * @param endDialog 将dialog放在endDialog的后面，不存在则默认最后
		 */
		public static pushDialogInTurn(dialog: TDialog, param?: any, endDialog?: TDialog): void {
			var self = Dialog;
			var turns = self.$turnDialogs;
			var index = turns.length;
			if (endDialog) {
				for (let i = 0, len = turns.length; i < len; i++) {
					if (endDialog == turns[i]) {
						index = i;
						break;
					}
				}
			}
			turns.splice(index, 0, dialog);
			self.$turnParams.splice(index, 0, param);
		}

		/**
		 * 在固定位置插入对话框
		 * @param dialog
		 * @param param
		 * @param index 下标
		 */
		public static insertDialogInTuen(dialog: TDialog, param: any, index?: number): void {
			var self = Dialog;
			var turns = self.$turnDialogs;
			if (isNaN(index))
				index = turns.length;
			self.$turnDialogs.splice(index, 0, dialog);
			self.$turnParams.splice(index, 0, param);
			!self.$isShowTurn && self.runTurn();
		}
	}

	/**
	 * 只允许弹出一个的对话框，也可以称为通用对话框
	 */
	class SignleDialog extends Dialog {

		private m_lblText: eui.Label;
		private m_btnOk: eui.Image;

		private $call: Function;
		private $thisObje: any;

		protected onCreate(): void {
			super.onCreate();
			var self = this;
			EventUtils.addTouchTapScaleListener(self.m_btnOk, self.removeFromParent, self);
		}

		protected onDestroy(): void {
			super.onDestroy();
			var self = this;
			EventUtils.removeEventListener(self.m_btnOk);
			// 执行回调
			var call = self.$call;
			if (call) {
				call.call(self.$thisObje);
				self.clearCall();
			}
		}

		/**
		 * 设置文本
		 */
		public setText(text: string): void {
			this.m_lblText.text = text;
		}

		/**
		 * 添加监听
		 */
		public addCall(call: Function, thisObje?: any): void {
			var self = this;
			self.$call = call;
			self.$thisObje = thisObje;
		}

		/**
		 * 清除回调
		 */
		public clearCall(): void {
			var self = this;
			self.$call = self.$thisObje = null;
		}
	}
}