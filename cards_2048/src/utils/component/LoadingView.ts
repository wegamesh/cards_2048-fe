module lie {

	var inInit = false;
	var flag = RES.FEATURE_FLAG;
	/**
	 * 重写
	 */
	var init = function () {
		if (!inInit) {
			inInit = true;
			flag.FIX_DUPLICATE_LOAD = 0;	// 允许重复加载，针对合图、位图等
			// 重写，注意不需要该文件请不要加入工程
			let prototype = <any>RES.ResourceLoader.prototype;
			let next = prototype.next;
			// 返回进度
			let curProgress = 0;
			let onProgress = function (reporter, current, total) {
				let onProgress = reporter && reporter.onProgress;
				if (onProgress) {
					onProgress(current + curProgress, total + curProgress);
				}
			};
			prototype.next = function () {
				let self = this;
				// 获取错误次数
				let getECount = function (groupName) {
					let array = self.loadItemErrorDic[groupName];
					return array ? array.length : 0;
				};
				let _loop_1 = function () {
					let r = self.getOneResourceInfo();
					if (!r)
						return "break";
					self.loadingCount++;
					self.loadResource(r)
						.then(function (response) {
							self.loadingCount--;
							RES.host.save(r, response);
							let groupName = r.groupNames.shift();
							if (r.groupNames.length == 0) {
								r.groupNames = undefined;
							}
							let reporter = self.reporterDic[groupName];
							self.numLoadedDic[groupName]++;
							let current = self.numLoadedDic[groupName];
							let total = self.groupTotalDic[groupName];
							let success = current - getECount(groupName);
							onProgress(reporter, success, total);
							if (current == total) {
								let groupError = self.groupErrorDic[groupName];
								self.removeGroupName(groupName);
								delete self.groupTotalDic[groupName];
								delete self.numLoadedDic[groupName];
								delete self.itemListDic[groupName];
								delete self.groupErrorDic[groupName];
								let dispatcher = self.dispatcherDic[groupName];
								if (groupError) {
									let itemList = self.loadItemErrorDic[groupName];
									delete self.loadItemErrorDic[groupName];
									let error = self.errorDic[groupName];
									delete self.errorDic[groupName];
									dispatcher.dispatchEventWith("error", false, { itemList: itemList, error: error });
								}
								else {
									dispatcher.dispatchEventWith("complete");
								}
								curProgress += success;
							}
							self.next();
						}).catch(function (error) {
							if (!error.__resource_manager_error__) {
								// throw error;
								console.error('资源重大bug，请维护：', error);
							}
							self.loadingCount--;
							delete RES.host.state[r.root + r.name];
							let times = self.retryTimesDic[r.name] || 1;
							if (times > self.maxRetryTimes) {
								delete self.retryTimesDic[r.name];
								let groupName = r.groupNames.shift();
								if (r.groupNames.length == 0) {
									delete r.groupNames;
								}
								if (!self.loadItemErrorDic[groupName]) {
									self.loadItemErrorDic[groupName] = [];
								}
								if (self.loadItemErrorDic[groupName].indexOf(r) == -1) {
									self.loadItemErrorDic[groupName].push(r);
								}
								self.groupErrorDic[groupName] = true;
								let reporter = self.reporterDic[groupName];
								self.numLoadedDic[groupName]++;
								let current = self.numLoadedDic[groupName];
								let total = self.groupTotalDic[groupName];
								let success = current - getECount(groupName);
								onProgress(reporter, success, total);
								if (current == total) {
									let groupError = self.groupErrorDic[groupName];
									self.removeGroupName(groupName);
									delete self.groupTotalDic[groupName];
									delete self.numLoadedDic[groupName];
									delete self.itemListDic[groupName];
									delete self.groupErrorDic[groupName];
									let itemList = self.loadItemErrorDic[groupName];
									delete self.loadItemErrorDic[groupName];
									let dispatcher = self.dispatcherDic[groupName];
									dispatcher.dispatchEventWith("error", false, { itemList: itemList, error: error });
									curProgress += success;
								}
								else {
									self.errorDic[groupName] = error;
								}
								self.next();
							}
							else {
								self.retryTimesDic[r.name] = times + 1;
								self.failedList.push(r);
								self.next();
								return;
							}
						});
				};
				while (self.loadingCount < self.thread) {
					let state_1 = _loop_1();
					if (state_1 === "break")
						break;
				}
			};
		}
	}
	/**
	 * 游戏开始页面
	 */
	export class LoadingView extends egret.DisplayObjectContainer {

		private $groupName: string;         // 预加载的组名
		private $timeout: number;           // 定时器
		private $errorKeys: string[];       // 加载错误的key
		private $errorNType: NetworkType;   // 错误加载项时的网络状态
		private $isFinish: boolean;         // 是否结束

		protected eGroupName: string = '$errorGroup';   // 重加载的错误组名，可重写
		protected eTimeOut: number = 5000;              // 超时检测时间
		protected isLoading: boolean;					// 是否加载中，不能重写

		/**
		 * @param groupName 预加载组名
		 * @param bgUrl 背景图，若存在则会先加入loading
		 */
		public constructor(groupName: string) {
			super();
			this.$groupName = groupName;
			init();
		}

		/**
		 * 开始加载
		 */
		protected onLoadStart(): void {
			var self = this;
			if (!self.isLoading) {
				let event = RES.ResourceEvent.ITEM_LOAD_ERROR;
				let gName = self.$groupName;
				let remove = function () {
					self.isLoading = false;
					gName == self.eGroupName &&
						delete RES.config.config.groups[gName]; // 删除组名
					RES.removeEventListener(event, self.onItemError, self);
				};
				self.isLoading = true;
				self.$errorKeys = [];
				RES.loadGroup(gName, 0, <any>self).then(function () {
					remove();
					flag.FIX_DUPLICATE_LOAD = 1;
					self.onSuccess();
				}).catch(function () {
					remove();
					self.onReload();
				});
				RES.addEventListener(event, self.onItemError, self);
			}
		}

		/**
		 * 加载错误的key值
		 */
		protected onItemError(event: RES.ResourceEvent): void {
			this.$errorKeys.push(event.resItem.name);
		}

		/**
		 * 移除监听
		 */
		private $removeListener(): void {
			var self = this;
			egret.clearTimeout(self.$timeout);
			NetworkUtils.removeNetworkStatusChange(self.onStatusChange, self);
		}

		/**
		 * 重新加载判断
		 */
		protected onReload(): void {
			var self = this;
			var utils = NetworkUtils;
			utils.getNetworkType().then(function (type) {
				let eGroupName = self.eGroupName;
				let createGroup = eGroupName && RES.createGroup(eGroupName, self.$errorKeys);
				// 创建新组成功
				if (createGroup) {
					// 修改错误组
					self.$groupName = eGroupName;
					// 当前网络状态已是最佳状态
					if (utils.isBestNetworkType(type))
						self.onError(1);
					else {
						self.$errorNType = type;
						// 超时检测
						self.$timeout = egret.setTimeout(self.onError, self, self.eTimeOut, 0);
						// 状态变化
						utils.addNetworkStatusChange(self.onStatusChange, self);
					}
				}
				else {
					self.onError(-1);
				}
			});
		}

		/**
		 * 监听网络状态变化回调
		 */
		protected onStatusChange(type: NetworkType): void {
			var self = this;
			// 网络状态升高，自动重新加载
			if (NetworkUtils.networkUpgrade(self.$errorNType, type)) {
				self.$removeListener();
				self.onLoadStart();
			}
		}

		/////////////以下方法供子类修改，记得加上super，上面的方法看不懂则请勿乱改

		/**
		 * 进度更新
		 */
		protected onProgress(current: number, total: number): void {
			// 加载结束
			if (current == total) {
				let self = this;
				// 进度条卡100%情况处理
				egret.setTimeout(function () {
					!self.$isFinish && self.onSuccess();
				}, null, 1000);
			}
		}

		/**
		 * 加载结束
		 */
		protected onSuccess(): void {
			this.$isFinish = true;
			this.$removeListener();
		}

		/**
		 * 加载错误
		 * @param errorCode 错误编码：-1组名错误，请前端同志修改eGroupName，0正常错误，1资源路径错误
		 */
		protected onError(errorCode: number): void {
			this.$removeListener();
		}
	}
}