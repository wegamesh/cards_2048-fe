declare const wx: any;

module lie {
    /**
 * 网络状态类型
 */
    export type NetworkType = '2g' | '3g' | '4g' | 'wifi' | 'none' | 'unknown';

    /**
     * 网络状态工具——基于微信
     */
    export class NetworkUtils {

        private static $netCall: [[(type: NetworkType) => void, any, boolean]] = <any>[];

        /**
         * 获取当前网络状态
         */
        public static getNetworkType(): Promise<NetworkType> {
            return new Promise<NetworkType>(function (resolve) {
                wx.getNetworkType({
                    success: function (res) {
                        resolve(res.networkTyp);
                    }
                });
            });
        }

        /**
         * 判断类型是否是最佳网络状态
         */
        public static isBestNetworkType(type: NetworkType): boolean {
            return type == '4g' || type == 'wifi';
        }

        /**
         * 判断网络状态是否提升
         * @param oldType 旧类型
         * @param newType 新类型
         */
        public static networkUpgrade(oldType: NetworkType, newType: NetworkType): boolean {
            var types = ['none', 'unknown', '2g', '3g', '4g', 'wifi'];
            var index0 = types.indexOf(oldType);
            var index1 = types.indexOf(newType);
            return index1 > index0;
        }


        /**
         * 添加网络变化回调
         * @param call 回调，参数是当前的网络状态
         * @param thisObj 回调对象
         * @param isOnce 是否是一次性的回调，即回调结束下次网络变化不再接受
         */
        public static addNetworkStatusChange(call: (type: NetworkType) => void, thisObj?: any, isOnce?: boolean): void {
            NetworkUtils.$netCall.push([call, thisObj, isOnce]);
        }

        /**
         * 移除网络变化回调
         * @param call 回调，参数是当前的网络状态
         * @param thisObj 回调对象
         * @returns 返回移除结果
         */
        public static removeNetworkStatusChange(call: (type: NetworkType) => void, thisObj?: any): boolean {
            var netCall = NetworkUtils.$netCall;
            for (let i = 0, len = netCall.length; i < len; i++) {
                let item = netCall[i];
                if (item[0] == call && item[1] == thisObj) {
                    netCall.splice(i, 1);
                    return true;
                }
            }
            return false;
        }

        /**
         * 执行网络变化回调——请勿手动调用
         */
        public static excuteStatusChange(type: NetworkType): void {
            var netCall = NetworkUtils.$netCall;
            for (let i = 0, len = netCall.length; i < len; i++) {
                let item = netCall[i];
                item[0].call(item[1], type);
                // 删除一次性回调
                if (item[2]) {
                    netCall.splice(i, 1);
                    i--;
                }
            }
        }

    }

    export class PlatformUtils {
        //登录
        public static login() {
            return new Promise((resolve, reject) => {
                wx.login({
                    success: res => {
                        console.log("weixin返回的登录数据", res)
                        let data = { "code": res.code };
                        Api.post(Api.login, data).then((loginRes: any) => {
                            console.log("后端返回的登录数据", loginRes)
                            AppConfig.gameInfo = {
                                score: loginRes.UserInfo.Scroe,
                                userId: loginRes.UserInfo.UserId
                            }
                            var score = LocalData.getItem('highScore')
                            score && (AppConfig.gameInfo.score = Number(score))
                        })

                        
                    }
                })

            })
        }

    }
}
