module lie {
	/**
	 * 存放和获取一些本地变量名，属于应用级别上的存储
	 */
	export class LocalData {

		/**
		 * 设置Item
		 */
		public static setItem(key: string, value: string): boolean {
			return egret.localStorage.setItem(key, value);
		}

		/**
		 * 获取Item的值
		 */
		public static getItem(key: string): string {
			return egret.localStorage.getItem(key);
		}

		/**
		 * 注意value必须是对象，否则会出现奇怪的现象
		 */
		public static setObject(key: string, value: any): boolean {
			return LocalData.setItem(key, JSON.stringify(value));
		}

		/**
		 * 获取一个对象
		 */
		public static getObject(key: string): any {
			try {
				return JSON.parse(LocalData.getItem(key));
			} catch (e) { }
		}
	}
}