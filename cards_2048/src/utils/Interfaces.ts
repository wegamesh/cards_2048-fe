// 存放不好归类的接口

/**
 * 好友信息
 */
interface ITFFriendInfo {
	Id: number;
	Name: string;
	AvatarUrl: string;
	Gender: number;
	City: string;
	Province: string;
	PicNum: number;
	RankNum: number;
	CityNum: number;
	Title: string;
	TitleId: number;
	LastLogin: string;
}

/**
 * 仓库信息
 */
interface ITFRepertory {
	ID: number;
	Uid: number;
	Repertory: string;
	FoodGridCount: number;
	ArticlesGridCount: number;
	TreasureGridCount: number;
	Backpack: string;
	Donate: string;
	CreatedAt: string;
	DeletedAt: string;
	UpdatedAt: string;
}

/**
 * 仓库物品信息
 */
interface ITFRepertoryItem {
	Id: number;
	Type: number;		// 类型0-3：食物、用品、宝贝、特产
	Name: string;
	Introduce: string;
	Category: number;
	Count: number;
	Diamond: number;
	Drop: number;
	ForSale: number;
	GoldCoin: number;
	Level: number;
	SpProper1: number;
	SpProper2: number;
	State: number;
	Unusual: number;
	Weight: number;
}

/**
 * 邀请有礼活动数据
 */
interface ITFInviteData {
	Id: number; 				// 活动序号
	Reward: string; 			// 奖励
	AchieveCondition1: number; 	// 成就条件
	AchieveCondition2: number; 	// 达成目标人数
	ConditionNum: number;		// 目标完成人数 
	IsAchieve: boolean; 		// 是否达成
	IsTake: boolean;			// 是否领取
}

/**
 * 称号
 */
interface ITFTitle {
	TitleId: number;
	Title: string;
	TitleCount: number;
	TitleNum: number;
	TitleTotalCount: number;
	TitleType: number;
}

/**
 * 红点
 */
enum RedInfo {
	T_PARK,         //游乐园红点
	T_ATTENTION,	//关注领取铜钱红点
	T_GETSKIN,		//关注领皮肤红点
	T_DAILYTASK		//日常任务红点
}

/**
 * 对话框类
 */
type TDialog = { new (...arg: any[]): lie.Dialog };

