module lie {
    /**
     * 工具类，存放一些常用的且不好归类的方法
     */
    export class Utils {

        // 工具私有变量，请勿乱动

        /**
         * 初始化一个长度为length，值全为value的数组
         * 注意，由于数组公用一个value，因此，value适用于基础类型，对于对象类的，请用memset2方法
         */
        public static memset<T>(length: number, value: T): T[] {
            return Array.apply(null, Array(length)).map(function () { return value; });
        }

        /**
         * 初始化一个长度为length，值通过getValue函数获取，注意getValue第一个参数是没用，第二个参数是当前数组的下标，即你返回的数据将存放在数组的该下标
         */
        public static memset2<T>(length: number, getValue: (value?: T, index?: number) => T): T[] {
            return Array.apply(null, Array(length)).map(getValue);
        }

        /**
         * 创建一个定时器
         * @param listener 定时回调
         * @param thisObject 回调所属对象
         * @param second 回调间隔，单位秒，默认一秒
         * @param repeat 循环次数，默认一直重复
         */
        public static createTimer(listener: Function, thisObject: any, second: number = 1, repeat?: number): egret.Timer {
            var timer = new egret.Timer(second * 1000, repeat);
            timer.addEventListener(egret.TimerEvent.TIMER, listener, thisObject);
            timer.start();
            return timer;
        }

        /**
         * 移除一个定时器
         * @param timer 被移除的定时器
         * @param listener 监听函数
         * @param thisObject 函数所属对象
         */
        public static removeTimer(timer: egret.Timer, listener: Function, thisObject?: any): void {
            timer.stop();
            timer.removeEventListener(egret.TimerEvent.TIMER, listener, thisObject);
        }

        /**
         * 获取网页参数
         */
        public static getQueryString(name: string): string {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var ret = window.location.search.substr(1).match(reg);
            return ret ? decodeURIComponent(ret[2]) : '';
        }

        /**
         * 获取cr1位于cr0的方向
         * 注：0为上，顺时针开始数，共八个方向
         */
        public static getDistance(col0: number, row0: number, col1: number, row1: number): number {
            var row, col, c = 2, xs = 1;
            if (row0 < row1)
                row = -1;
            else if (row0 > row1)
                row = 1
            else
                row = 0;
            if (col0 > col1) {
                c = 6;
                xs = -1;
            }
            else if (col0 == col1)
                xs = 2;
            return c - row * xs;
        }

        /**
         * 自定义格式化字符串
         * @param reg 正则
         * @param str 字符串
         * @param args 填充参数
         */
        public static formatStringReg(reg: RegExp, str: string, args: any[]): string {
            for (let i in args) {
                let arg = args[i];
                if (reg.test(str))
                    str = str.replace(reg, args[i]);
                else
                    break;
            }
            return str;
        }

        /**
         * 格式化字符串
         */
        public static formatString(str: string, ...args: any[]): string {
            str = str.replace(/%%/g, '%');  // 有些识别问题出现两个%号
            return Utils.formatStringReg(/%d|%s/i, str, args);  // 忽略大小写
        }

        /**
         * 偏移整型数组
         * @param array 需要偏移的数组
         * @param offset 偏移量，对应array长度，没有值时默认0
         */
        public static offsetArray(array: number[], ...offset: number[]): number[] {
            for (let i in array)
                array[i] += (offset[i] || 0);
            return array;
        }

        /**
         *为组件添加朦层
         *
        */
        public static setObscure(width: number, height: number): eui.Rect {
            let rect = new eui.Rect
            rect.name = 'obscure'
            rect.x = 0
            rect.y = 0
            rect.width = width
            rect.height = height - 3
            rect.fillColor = 0x000000
            rect.alpha = 0.3
            rect.strokeAlpha = 0
            rect.ellipseHeight = 44
            rect.ellipseWidth = 44
            return rect
        }

        /**
		 * 添加矩形遮罩
		 */
        public static addRectMask(bitmap: egret.Bitmap): void {
            let shape = new egret.Shape
            let graphics = shape.graphics
            shape.x = bitmap.x + 2
            shape.y = bitmap.y + 2
            bitmap.parent.addChild(shape)
            graphics.beginFill(0)
            graphics.drawRoundRect(0, 0, bitmap.width - 4, bitmap.height - 4, 10)
            graphics.endFill()
            bitmap.mask = shape
        }

        /**
         * 设置纹理的九宫格
         */
        public static setScale9(b: egret.Bitmap, x: number, y: number, w: number, h: number): void {
            var s = egret.Rectangle.create();
            s.x = x;
            s.y = y;
            s.width = w;
            s.height = h;
            b.scale9Grid = s;
        }

        /**
         * 二次延迟
         */
        public static callLater(call: Function, thisObj?: any, ...params: any[]): void {
            var later = egret.callLater;
            later(function () {
                later(function () {
                    call.apply(thisObj, params);
                }, null);
            }, null);
        }

        /**
         * 检测点是否在矩形上
         */
        public static pointInRect(x: number, y: number, rect: { x: number, y: number, width: number, height: number }): boolean {
            x -= rect.x;
            y -= rect.y;
            return x >= 0 && x <= rect.width && y >= 0 && y <= rect.height;
        }

        /**
         * 控件是否显示
         */
        public static isVisible(display: egret.DisplayObject): boolean {
            var stage = display.stage;
            var visible;
            do {
                visible = display.visible;
                display = display.parent;
            } while (visible && (visible = !!display) && display != stage);
            return visible;
        }

        /**
         * 移除List的数据
         * @param list
         * @param isCache 是否是缓存数据，是的话不清除
         */
        public static removeListData(list: eui.List, isCache?: boolean): void {
            var datas = <eui.ArrayCollection>list.dataProvider;
            datas && !isCache && datas.removeAll();
            list.dataProvider = null;
        }

        /**
         * 更新列表数据
         * @param list
         * @param datas 新数据源
         */
        public static updateListData(list: eui.List, datas: any[]): void {
            var array = <eui.ArrayCollection>list.dataProvider;
            if (array)
                array.replaceAll(datas);
            else
                list.dataProvider = new eui.ArrayCollection(datas);
        }

        /**
         * 添加滤镜
         */
        public static setColorMatrixFilter(image: any, colorMatrix: any[]): void {
            let colorFlilter = new egret.ColorMatrixFilter(colorMatrix)
            if (image.filters && image.filters.length) {
                image.filters[0] = colorFlilter
            } else {
                image.filters = [colorFlilter]

            }
        }

        /**
         * 复制一个对象
         * @param obj 需要复制的对象
         * @param copy 被复制对象，不存在则创建
         * @returns 返回copy
         */
        public static copyObj<T>(obj: T, copy: T = Object.create(null)): T {
            for (let i in obj) {
                copy[i] = obj[i];
            }
            return copy;
        }
    }
}