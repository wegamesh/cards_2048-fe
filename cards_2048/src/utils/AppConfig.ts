/*
这个类里面的东西基本都需要玩家根据当前游戏
来修改，下面标※的都是要改的，不标的话可不改
*/

/**
 * 玩家的信息
 */
interface ITFPlayInfo {
	sid: number			// 玩家ID
	nickname: string	// 昵称
	avatar_url: string	// 头像URL
	gender: number		//性别
	province: string	//省
	city: string		//市
	country: string		//国家
	appleNum: number 	//已有苹果数量
	coinNum: number 	//已有金币数量
	LevelId: number  	//成就等级
	LevelName: string 	//成就名称
	LevelNum: number	//经验数
	CityNum: number		//经过城市数
	PicNum: number		//收集图片数
	GoldCoin?: number	//苹果(关系链用其它时候不用)
	Diamond?: number	//铜钱(关系链用其它时候不用)
	LevelPoint?: number //当前所获的成就点数
	SpeedCard?: number  //当前拥有的加速卡的数量
	Unionid?: string    //用户的unionid
}

/**
 * 宠物猪信息
 */
interface ITFPetInfo {
	Uid: number		//用户id
	Name?: string	//宠物名字
	State: number	//游学状态 0:在家 1:不在家
	Speed: number	//游学初始速度
	Skin?: number	//宠物皮肤
	Type?: number
}

/**
 * 游戏信息
 */
interface ITFGameInfo{
	score: number //分数
	userId: number  
}

/**
 * 游戏配置，由每个游戏自己定义
 */
class AppConfig {

	public static myInfo: ITFPlayInfo;			// 玩家的信息
	public static gameInfo: ITFGameInfo			// 游戏信息
	// 其他应用方法
	public static handGuides: number[];
	public static roleGuides: number[];
	public static isCheckIosPay: boolean = false  //标记是否检测过ios充值了

	/**
	 * 获取skins下的皮肤
	 */
	public static getSkin(name: string): string {
		return 'resource/scene/' + name + '.exml';
	}

	/**
	 * 获取引导数据
	 */
	// public static setHandGuide(guides: string): void {
	// 	var array = AppConfig.handGuides = guides.split(',').map(function (v) { return Number(v) });
	// 	for (let i = array.length, len = HandGuide.getConfigType(); i < len; i++)
	// 		array[i] = 0;
	// }

	/**
	 * 更新类型
	 * @param type 类型
	 * @param bool 状态，是否已显示
	 */
	// public static updateHandGuide(type: number, bool: number): void {
	// 	var states = AppConfig.handGuides;
	// 	if (type < states.length && states[type] != bool) {
	// 		states[type] = bool;
	// 		Api.getInstance().post(common.Address.userguide, {
	// 			token: global.token, guide1: states.join(',')
	// 		});
	// 	}
	// }

	/**
	 * 获取引导数据
	 */
	// public static setRoleGuide(guides: string): void {
	// 	var array = AppConfig.roleGuides = guides.split(',').map(function (v) { return Number(v) });
	// 	for (let i = array.length, len = RoleGuide.getConfigType(); i < len; i++)
	// 		array[i] = 0;
	// }

	/**
	 * 更新类型
	 * @param type 类型
	 * @param bool 状态，是否已显示
	 */
	// public static updateRoleGuide(type: number, bool: number): void {
	// 	var self = AppConfig;
	// 	var states = AppConfig.roleGuides;
	// 	if (type < states.length && states[type] != bool) {
	// 		states[type] = bool;
	// 		Api.getInstance().post(common.Address.userguide, {
	// 			token: global.token, guide2: states.join(',')
	// 		});
	// 	}
	// }

	/**
	 * 更新我的数据
	 */
	// public static updateMyInfo(awards: ITFAwardData[]): void {
	// 	var myInfo = AppConfig.myInfo;
	// 	for (let i in awards) {
	// 		let award = awards[i];
	// 		let type = award.Type;
	// 		let count = award.Count;
	// 		if (type == 0)
	// 			myInfo.appleNum += count;
	// 		else if (type == 1)
	// 			myInfo.coinNum += count;
	// 	}
	// 	ResourceGroup.refresh();
	// }

	/**
	 * 获取奖励的物品icon
	 */
	// public static getAwardIcon(data: ITFAwardData): string {
	// 	var type = data.Type, id = data.Id;
	// 	var icon = '';
	// 	switch (type) {
	// 		case 0:
	// 			icon = 'icon_good_apple_png';
	// 			break
	// 		case 1:
	// 			icon = 'icon_good_coin_png';
	// 			break;
	// 		case 2:
	// 			icon = `icon_good_${id}_png`;
	// 			break;
	// 		case 3:
	// 			icon = `icon_good_${id}_png`;
	// 			break;
	// 		case 4:
	// 			icon = `icon_${data.Name}_${data.Category}_png`;
	// 			break;
	// 		case 5:
	// 			icon = 'icon_fish_' + id + '_png';
	// 			break;
	// 		case 6:
	// 			icon = 'pic_bag_0' + id + '_fish_png';
	// 			break;
	// 		case 7:
	// 			icon = 'icon_shell_png';
	// 			break;
	// 		case 8:
	// 			icon = 'icon_bait_fish_png';
	// 			break;
	// 		default:
	// 			icon = `icon_good_${id}_png`;
	// 			break;
	// 	}
	// 	return icon;
	// }

	/**
	 * 获取物品详情数据
	 */
	// public static getAwardDetailData(type: number, source: any): ITFAwardDetailData {
	// 	var data = <ITFAwardDetailData>Object.create(null);
	// 	data.Type = type;
	// 	data.Id = source.Id;
	// 	data.Name = source.Name;
	// 	data.Introduce = source.Introduce;
	// 	data.Category = source.Category;
	// 	return data;
	// }
}