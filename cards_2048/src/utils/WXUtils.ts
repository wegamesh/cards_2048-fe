module lie {
	
	declare var wx;

	var $canvas;	// 不存在的变量别用var

	interface ITFShareInfo {
		title: string;
		imageUrl: string;
		query?: string;
		success?: (a) => void;
		fail?: (a) => void;
	}

	interface KVData {
		key: string;
		value: string;
	}

	/**
	 * 微信平台工具类——微信工具除登录和获取用户信息
	 */
	export class WXUtils implements ITFPFUtils {

		public type: string = 'wxgame';
		protected isShowAds: boolean;		// 是否显示广告视频
		protected bannerAd: any; 			// banner广告

		public constructor() {
		}

		/**
		 * 游戏进入初始化——调用一次即可
		 */
		public init(): void {
			console.log('初始化')
			var self = this;
			// 分享携带ShareTicket
			wx.updateShareMenu({
				withShareTicket: true
			});
			wx.onShow(self.onShow);
			wx.onHide(self.onHide);
			// 分享
			wx.onShareAppMessage(function () {
				// 用户点击了“转发”按钮
				return self.getGlobalShareInfo();
			});
			wx.showShareMenu();	// 自动显示
			;	// 不需要任何操作
			PlatformUtils.login()
		}

		/**
		 * 微信onShow
		 */
		protected onShow(res: any): void {
			// 分享打开的链接
			let isShare = res.query.share == 'true';
			let type = res.query.type || 0;
			let ticket = isShare ? (res.shareTicket || '1') : void 0;
			//AppMsg.notice(MsgId.onShow, ticket);
		}

		/**
		 * 显示广告
		 */
		public showAds(type: number = 0): Promise<boolean> {
			var self = this;
			// let clzz = UserData.gameInfo;

			return new Promise<boolean>(function (resolve, reject) {
				// reject()
				if (!self.isShowAds) {
					self.isShowAds = true;
					// let turn = !clzz.Location.IsCore;
					let sdk = self.getSystemInfoSync().SDKVersion;
					// 转换回调
					let turnCall = function (str) {
						self.isShowAds = false;
						// if (turn)
						// 	reject(str);
						// else {
							self.showToast(str);
							resolve(false);
						// }
					};
					// 字符串
					if (sdk >= '2.0.4') {
						let videoAd = wx.createRewardedVideoAd({
							adUnitId: adUnitIds[type]
						});
						videoAd.load().then(() => {
							wx.hideLoading()
							// 监听回调
							let call = videoAd.onClose(function (res) {
								self.isShowAds = false;
								videoAd.offClose(call);
								console.log('sdk: ', sdk)
								// if (clzz.Location && clzz.Location.IsCore && sdk >= '2.1.0' && res) {
								// 	console.log('isEnded', res.isEnded)
								// 	if (res.isEnded) {
								// 		resolve(true);
								// 	} else {
								// 		self.showToast('广告未看完无法获得奖励');
								// 		resolve(false);
								// 	}
								// } else {
								if (res.isEnded) {
									resolve(true);
								}
								// }
							});
							videoAd.show();
						}).catch(err => {
							turnCall('广告播放异常');
						});
					}
					else {
						turnCall('该微信版本暂不支持广告播放，请升级！');
					}
				}
			});
		}

		/**
		 * 显示banner广告
		 */
		public showBannerAds(): Promise<void> {
			var self = this;
			return new Promise<void>(function (resolve, reject) {
				let info = self.getSystemInfoSync();
				let sdk = info.SDKVersion;
				// 字符串
				if (sdk >= '2.0.4') {
					let stage = AppViews.stage;
					let sWidth = stage.stageWidth;
					let floor = Math.floor;
					let width = 750, height = 250;
					let x = (sWidth - width) / 2;
					let y = stage.stageHeight - height;
					let scaleWidth = info.screenWidth / sWidth;
					// let style = {
					// 	left: floor(x * scaleWidth),
					// 	top: floor(y * scaleWidth)
					// 	// height: floor(height * scaleWidth)
					// };
					let style = {
						left: 0,
						top: floor(y * scaleWidth),
						width: floor(width * scaleWidth),
						height: floor(height * scaleWidth)
					};
					let bannerAd = wx.createBannerAd({
						adUnitId: adUnitIds[1],
						style: style
					});
					
					bannerAd.onResize && bannerAd.onResize( (e) => {
						bannerAd.style && (bannerAd.style.top = wx.getSystemInfoSync().windowHeight - e.height);
					})
					bannerAd.show()
					self.bannerAd = bannerAd;
				}
			});
		}

		/**
		 * 销毁banner广告
		 */
		public hideBannerAds(): Promise<void> {
			var self = this;
			return new Promise<void>(function (resolve, reject) {
				let sdk = self.getSystemInfoSync().SDKVersion;
				let show = Dialog.showSingleDialog;
				// 字符串
				if (sdk >= '2.0.4') {
					self.bannerAd.destroy();
				}
			});
		}

		/**
		 * 微信onHide
		 */
		protected onHide(res: any): void {
			//AppMsg.notice(MsgId.onHide);
		}

		/**
		 * 获取开放域
		 */
		public getShareCanvas(): egret.Bitmap {
			var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
			var texture = new egret.Texture();
			bitmapdata.$deleteSource = false;
			texture.bitmapData = bitmapdata;
			return new egret.Bitmap(texture);
		}

		/**
		 * 获取全局分享数据
		 */
		public getGlobalShareInfo(): ITFShareInfo {
			return {
				title: '测试',
				imageUrl: 'resource/assets/share/wx_share_join.png',
				query: 'share=true'
			}
		}

		/**
		 * 系统信息
		 */
		public getSystemInfoSync(): any {
			return wx.getSystemInfoSync();
		}

		/**
		 * 直接弹出分享
		 */
		public showShare(): Promise<boolean> {
			var self = this;
			return new Promise<boolean>(function (resolve, reject) {
				let obj = self.getGlobalShareInfo();
				obj.success = function (res) {
					resolve(true);
				};
				obj.fail = function (res) {
					resolve(false);
				};
				wx.shareAppMessage(obj);
			});
		}

		/**
		 * app文字提示
		 */
		public showToast(msg: string): void {
			wx.showToast({
				title: msg,
				icon: 'none'
			});
		}

		/**
		 * 获取系统描述
		 */
		public getSystemInfo(): string {
			return wx.getSystemInfoSync().system;
		}

		/**
		 * 设置用户数据
		 */
		public async setUserInfo(map: { [key: string]: any }): Promise<boolean> {
			return new Promise<boolean>(function (resolve, reject) {
				//let clzz = AppConfig;
				let clzz;
				let datas = <KVData[]>[];
				let setCS = wx.setUserCloudStorage;
				let isObj = function (v) {
					return typeof v === 'object';
				};
				for (let i in map) {
					let v = map[i];
					if (isObj(v))
						v = JSON.parse(v);
					datas.push({ key: i, value: v + '' });
				}
				// 低版本不支持
				setCS ? setCS({
					KVDataList: datas,
					success: function () {
						resolve(true);
					},
					fail: function () {
						resolve(false);
					}
				}) : reject();
			})
		}

		/**
		 * 由主域向开放域推送消息
		 * @param action 消息标志
		 * @param data 消息参数
		 */
		public postMessage(action: string, data?: any): void {
			let msg = <ITFWXMessage>{};
			msg.action = action;
			msg.data = data;
			// console.log("抛消息");
			console.log("发送消息")
			console.log(msg)
			wx.postMessage(msg);
		}

		/**
		 * 获取玩家信息
		 */
		public getUserInfo(): Promise<any> {
			return new Promise<any>(function (resolve) {
				// let clzz = AppConfig;
				// let info = clzz.userInfo;
				// info ? resolve(info) :
				// 	platform.getUserInfo().then(function (res) {
				// 		clzz.userInfo = res;
				// 		resolve(res);
				// 	});
			});
		}

		/**
		 * loading
		 */
		public showLoading(title?: string, mask: boolean = false){
			wx.showLoading({
				title: title || '',
				mask: mask
			})
		}

		public hideLoading(){
			wx.hideLoading()
		}

		/**
		 * 振动
		 * 返回值 。1/2/3  成功/失败/结束
		 */
		public vibrateShort(): Promise< 1 | 2 | 3 >{
			return new Promise( (resolve, reject) => {
				wx.vibrateShort({
					success: ()=>{
						resolve(1)
					},
					fail: ()=>{
						resolve(2)
					},
					complete: ()=>{
						resolve(3)
					}
				})
			})
			
		}

		/**
		 * 小程序跳转
		 * @param appid 
		 * @param path
		 * @param imgUrl
		*/
		// public static readonly navigateAppId = 'wx53b1b4ab6fa0d044'
		// public static readonly navigatePath = 'pages/index/index?from=koudaizhu'
		
		public navigate2Program(appid: string, path: string, imgUrl: string): Promise<any> {
			return new Promise((resolve, reject) => {
				let navigate = wx.navigateToMiniProgram
				if (navigate) {
					console.log('appId', appid)
					console.log('path', path)
					navigate({
						appId: appid,
						path: path,
						success(res) {
							console.log('跳转成功', res)
						},
						fail(res) {
							console.log('跳转失败', res)
						}
					})
				} else {
					wx.previewImage({
						current: imgUrl,
						urls: [imgUrl]
					})
				}
			})
		}
	}

	// 视频ID，分别是：激励广告
	var adUnitIds = [
		'adunit-1a513703413fe167',
		'adunit-7d2a4278d6d2c537'
	];
}