/**
 * 寻光游戏统计
 * XG_STATISTICS.init(appid);  //初始化
 * 
 * XG_STATISTICS.send('123',{a:1})  //埋点
 * 以下三个接口都是第三方自发提供的，若不提供则不统计
 * XG_STATISTICS.sendUserInfo    //发送用户信息
 * XG_STATISTICS.sendKey         //发送用户唯一ID
 * XG_STATISTICS.sendLocation     //发送用户地理信息
 * 付费信息
 * XG_STATISTICS.sendPay          //发送付费信息
 */
var XG_STATISTICS = (function () {

    var appKey = '';             //小游戏appid
    var xg_obj = <any>{};       // SDK返回值
    var hasInit = false;        // 是否已经初始化，保证init只执行一次
    var LaunchInfo = <any>{};
    var sysInfoData = <any>{};
    var toolVision = "1.0.3";
    var hasOnhide = false;    //是否已经隐藏过，如果隐藏过才会发onshow没有过的话则不发。

    // 初始化
    xg_obj.init = function (appKey = '') {
        console.log('寻光统计')
        wx.setStorageSync("xg_appKey", appKey);
        // 首次接入游戏，获取渠道，tag，scene信息。
        LaunchInfo = getLaunchInfo();
        // 将这些信息全部存在storage里面，由于本步骤是同步的，因此不会与request请求发生先后顺序的问题
        wx.setStorageSync("LaunchInfo", LaunchInfo);
        XGRequest({
            action: 'dotOfStatistics',
            type: 'login',
        });

        /**
         * 用户首次登陆
         * 记录进入时间&&上传用户信息
         */
        if (!hasInit) {
            hasInit = true;

            // 1.0.3开始不单独发设备信息，但是每次都会带上设备信息
            var sysInfoData = getSysInfo();
            sysInfoData = JSON.stringify(sysInfoData);
            wx.setStorageSync("sysInfoData", sysInfoData);
            // 发送网络信息，
            wx.getNetworkType({
                complete: function(data){
                    XGRequest({
                        action: 'setNetWorkInfo',
                        type: 'networkInfo',
                        networkType: data.networkType,
                        appKey: appKey,
                    })
                }
            })
        }

        /**
         * 事件监听
         * onhide和onshow事件传launch信息，这样的话留存统计会更加准确
         */
        wx.onHide(function () {
            hasOnhide = true;
            LaunchInfo = getLaunchInfo();
            wx.setStorageSync("LaunchInfo", LaunchInfo);
            XGRequest({
                action: 'dotOfStatistics',
                type:"hide",
            });
        })
        wx.onShow(function () {
            if (!hasOnhide) { return false; }
            LaunchInfo = getLaunchInfo();
            wx.setStorageSync("LaunchInfo", LaunchInfo);
            XGRequest({
                action: 'dotOfStatistics',
                type:"show",
            });
        })

    }

    // 事件统计
    xg_obj.send = function (key = '', obj = {}) {
        var objr = {};
        if(obj && typeof(obj) != "object"){
            objr = {
                "null": obj + '',
            };
        }else{
            for(let i in obj){
                objr[i + ''] = obj[i] + '';
            }
        }
        XGRequest({
            type: 'event',
            action: 'eventDotData',
            eventKey: key,
            eventObj: objr,
        });
    }

    // 获取用户信息
    xg_obj.sendUserInfo = function (obj = {}) {
        var userInfo = obj['userInfo'];
        var userData = {
            nickName: userInfo.nickName.toString(),
            gender: userInfo.gender.toString(),
            language: userInfo.language.toString(),
            city: userInfo.city.toString(),
            province: userInfo.province.toString(),
            country: userInfo.country.toString(),
            avatarUrl: userInfo.avatarUrl.toString()
        };
        var userInfoData = JSON.stringify(userData);
        XGRequest({
            action: 'setWxBaseInfo',
            userBaseInfo: userInfoData,
        });
    }

    // 获取用户唯一标识KEY,如果有openID则用openID，没有则用开发者定义的唯一ID。
    xg_obj.sendKey = function (key = '') {
        XGRequest({
            action: 'bindKeyIdAndUuid',
            keyId: key,
        });
    }
    // 获取收入
    xg_obj.sendPay = function (key = "", incomNum = "", status = "") {
        XGRequest({
            action: 'incomeDataDot',
            type: 'income',
            incomeKey: key,
            incomeNum: incomNum,
            status: status,
        });
    }

    // 获取位置
    xg_obj.sendLocation = function (key = '', obj = {}) {
        var objData = {};
        objData = {
            latitude: obj['latitude'],
            longitude: obj['longitude'],
            speed: obj['speed']
        }
        XGRequest({
            action: 'sendLocation',
            key: key,
            obj: objData
        });
    }

    // 封装Request请求
    function XGRequest(data = {}) {
        appKey = wx.getStorageSync("xg_appKey") || '',
        LaunchInfo = wx.getStorageSync("LaunchInfo") || '',
        sysInfoData = wx.getStorageSync("sysInfoData") || '';
        // 给后端的数据，每次发的数据有UUID，APPKey，版本号，和渠道信息，设备信息
        (<any>Object).assign(data, { 
            uuid: Uuid(), 
            appKey: appKey, 
            version: toolVision,
            channelId: LaunchInfo['channelId'],
            channelTag: LaunchInfo['tag'],
            scene: LaunchInfo['scene'],
            deviceInfo: sysInfoData, 
        });
        console.log("给后端的data", data);
        wx.request({
            // url: "https://pkwegame.xunguanggame.com/data-acquisition/?action=",
            url: "https://log.xunguanggame.com/data-acquisition/?action=",
            // url: "http://123.206.88.21:8993/?action=",
            data: data,
            header: {
},
            method: "GET",   //后续会改
            success: function (t) {
            }
        })
    }

    // 生成用户唯一值uuid
    function Uuid() {
        var uuid = "";
        try {
            uuid = wx.getStorageSync("xgtj_uuid")
        } catch (t) {
            uuid = "uuid-getstoragesync"
        }
        if (!uuid) {
            uuid = "" + Date.now() + Math.floor(Math.random() * 1e7);
            try {
                wx.setStorageSync("xgtj_uuid", uuid)
            } catch (t) {
                wx.setStorageSync("xgtj_uuid", "uuid-getstoragesync")
            }
        }
        return uuid
    }

    // 获取时间戳，单位：秒
    function getTime() {
        var time = new Date().getTime();
        time = parseInt(String(time / 1000));
        return time;
    }

    // 获取设备信息
    function getSysInfo() {
        // https://developers.weixin.qq.com/minigame/dev/document/system/system-info/wx.getSystemInfoSync.html
        var gameUserData = {};
        var gameUserInfo = wx.getSystemInfoSync();
        gameUserData = {
            model: changeToString(gameUserInfo['model']),
            pixelRatio: changeToString(gameUserInfo['pixelRatio']),
            windowWidth: changeToString(gameUserInfo['windowWidth']),
            windowHeight: changeToString(gameUserInfo['windowHeight']),
            system: changeToString(gameUserInfo['system']),
            language: changeToString(gameUserInfo['language']),
            version: changeToString(gameUserInfo['version']),
            batteryLevel: changeToString(gameUserInfo['batteryLevel']),
            screenWidth: changeToString(gameUserInfo['screenWidth']),
            screenHeight: changeToString(gameUserInfo['screenHeight']),
            SDKVersion: changeToString(gameUserInfo['SDKVersion']),
            brand: changeToString(gameUserInfo['brand']),
            fontSizeSetting: changeToString(gameUserInfo['fontSizeSetting']),
            statusBarHeight: changeToString(gameUserInfo['statusBarHeight']),
            platform: changeToString(gameUserInfo['platform']),
            devicePixelRatio: changeToString(gameUserInfo['devicePixelRatio']),
        }
        return gameUserData;
    }

    // 将数值转为字符串
    function changeToString(value) {
        if (value) {
            if (typeof (value) == "number") {
                return value.toString();
            } else {
                return value;
            }
        } else {
            return "";
        }
    }

    // 获取渠道参数
    function getLaunchInfo() {
        // https://developers.weixin.qq.com/minigame/dev/document/system/life-cycle/wx.getLaunchOptionsSync.html
        var LaunchData = {};
        var LaunchInfo = wx.getLaunchOptionsSync();
        console.log("launch1", LaunchInfo);

        // 1047	扫描小程序码 1048	长按图片识别小程序码 1049	手机相册选取小程序码
        var sceneArray = [1011,1012,1013,1047, 1048,1049];  
        //  二维码进入小游戏
        if(sceneArray.indexOf(LaunchInfo.scene) >= 0){
            if (LaunchInfo.query.scene){
                var tmpArr = urlToObject(decodeURIComponent(LaunchInfo.query.scene));
                LaunchData = {
                    channelId: tmpArr['f'] || tmpArr['from'] ||'',   //from
                    tag: tmpArr['t'] || tmpArr['tag'] || '',         //tag
                    scene: LaunchInfo.scene || '',
                }
            }else{
                LaunchData = {
                    channelId: LaunchInfo.query.from || '',
                    tag: LaunchInfo.query.tag || '',
                    scene: LaunchInfo.scene || '',
                }
            }

        } else {
            LaunchData = {
                channelId: LaunchInfo.query.from || '',
                tag: LaunchInfo.query.tag || '',
                scene: LaunchInfo.scene || '',
            }
        }
        console.log(LaunchData)

        return LaunchData;
    }

    // URL转化为OBJ
    function urlToObject(url) {
        var string = url.split('&');
        var res = {};
        for (var i = 0; i < string.length; i++) {
            var str = string[i].split('=');
            res[str[0]] = str[1];
        }
        return res;
    }

    // 统计在线时长，待定，暂时由后端处理
    function getIntervalTime() {
        // https://www.zhihu.com/question/67543820
    }

    return xg_obj;
})();