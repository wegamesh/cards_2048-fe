class HitCountComponent extends eui.Component{
    private l_label: eui.Label
    public constructor(data){
        super()
        this.skinName = 'resource/scene/HitCountSkin.exml'
        this.l_label.text = 'x' + data
    }
}