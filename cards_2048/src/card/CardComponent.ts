const number2Color = {
    0: 0xffffff,
    2: 0xd1cfcf,
    4: 0xabe187,
    8: 0x5cac86,
    16: 0xfdb231,
    32: 0xc96d98,
    64: 0x34aaae,
    128: 0x2f5a7c,
    256: 0x547d76,
    512: 0x7d6a95,
    1024: 0x886669,
    2048: 0x585956
}

class CardComponent extends eui.Component{
    public r_bg: eui.Rect          //背景颜色
    public value: number | string       //数字
    public lastX: number
    public lastY: number
    public property: number        //属性

    public constructor(data){
        super()
        this.skinName = 'resource/scene/Card.exml'

        this.anchorOffsetX = this.width / 2
        this.anchorOffsetY = this.height / 2
        if(data > 0){
            this.value = data
        }else{
            this.r_bg.fillAlpha = 0
        }
        
        
        this.setBgColor(data)
    }

    public setBgColor(data: number | string){
        this.r_bg.fillColor = number2Color[data]
    }

    /**
     * 设置边框的颜色
     * @param data 颜色色值
     */
    public setRectStroke(data: number){
        this.r_bg.strokeColor = data
        // this.r_bg.strokeWeight = 4
    }

    public clearRectStroke(){
        this.r_bg.strokeColor = 0xffffff
        // this.r_bg.strokeWeight = 0
    }
}