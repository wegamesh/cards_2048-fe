/**
 * 排行榜界面
 */
class RankDialog extends Dialog {

	private m_imgTitle: eui.Image;
	private m_imgClose: eui.Image;
	private m_imgLast: eui.Image;
	private m_imgNext: eui.Image;
	private m_btnShare: eui.Image;
	private m_Bg: eui.Image;
	private m_gpShare: eui.Group;
	private m_pShare: egret.Bitmap;
	private bitmap: any;
	private global: any;
	private m_pRefresh: any;
	private m_pTimer: egret.Timer;

	public constructor() {
		super();
        this.skinName = "RankDialogSkin";
		// this.init();
		this.global = <any>window;
		this.height = egret.MainContext.instance.stage.stageHeight;
        this.width = egret.MainContext.instance.stage.stageWidth;
		// this.addChild(this.m_Bg);		
		// this.isShare = true;
		// this.setModel(0);
		// var Utils = new Utils();
		// 结束页面 排行-竖排
		this.initShareCanvas();

		// const bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        // bitmapdata.$deleteSource = false;
        // const texture = new egret.Texture();
        // texture._setBitmapData(bitmapdata);
        // var bitmap = new egret.Bitmap(texture);
        // bitmap.width = 750;
        // bitmap.height = 1334;
        // bitmap.x = 0;
        // // console.log(this.worldHeight)
        // bitmap.y = 10;

        // egret.startTick((timeStarmp: number) => {
        //     egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
        //     bitmapdata.webGLTexture = null;
        //     return false;
        // }, this);
		// this.addChild(bitmap);

		var self = this;
		var addl = self.addTouchTapListener;
		addl(self.m_imgClose, ()=>{
			// self.removeChild(bitmap);
			self.m_pShare.texture.dispose();
			self.m_pShare = null;
			Utils.removeTimer(self.m_pTimer, self.m_pRefresh, null);
			this.removeFromParent()
			pfUtils.postMessage('clear')
		}, self);
	}

	protected init(): void {
		// super.onCreate();

		// addl(self.m_imgLast, self.onLast, self);
		// addl(self.m_imgNext, self.onNext, self);
		// addl(self.m_btnShare, self.onShare, self);
	}

	protected onDestroy(): void {
		pfUtils.postMessage('exit', 2);
	}

	// protected initShareCanvas(): void {
	// 	// super.initShareCanvas(0.5);
	// }

	/**
	 * 点击上一排
	 */
	protected onLast(): void {
		pfUtils.postMessage('last');
	}

	/**
	 * 点击下一排
	 */
	protected onNext(): void {
		pfUtils.postMessage('next');
	}

	/**
	 * 点击底部按钮
	 */
	protected onShare(): void {
		var self = this;
		pfUtils.showShare().then(function (ticket) {
			if (ticket) {
				// self.setModel(1);
				pfUtils.postMessage('ticket', ticket);
			}
		});
	}

	/**
	 * 设置模式
	 * @param 是否显示群排行
	 */
	// protected setModel(model: number): void {
	// 	var self = this;
	// 	// self.m_imgTitle.source = 'title' + model + '_ranking_png';
	// 	// self.m_btnShare.source = 'rank_share' + model + '_png';
	// }

	private removeself(){
		this.removeChild(this.m_pShare);
		// this.bitmap.parent.removechild(this.bitmap);
        this.removeChildren();
    }

	/**
	 * 初始化离屏canvas
	 */
	private initShareCanvas(): void {
		var self = this;
		var stage = self.stage;
		pfUtils.postMessage('rank');	// 显示排行榜不清理底层
		pfUtils.postMessage('resume');
		var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
		var texture = new egret.Texture();
		bitmapdata.$deleteSource = false;
		texture._setBitmapData(bitmapdata);
		// 界面显示
		var bitmap = self.m_pShare = new egret.Bitmap(texture);
		// bitmap.width = 532;
		// bitmap.height = 800;
		// self.m_gpShare.addChild(bitmap);
		bitmap.width = this.width;
        bitmap.height = this.height;
        bitmap.x = 0;
        // console.log(this.worldHeight)
        bitmap.y = 10;
		self.addChild(bitmap);
		// 刷新
		var func = self.m_pRefresh = (timeStarmp: number) => {
			egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
			console.log("是否在循环")
			bitmapdata.webGLTexture = null;
			return false;
		};
		self.m_pTimer = Utils.createTimer(func, null, 0.05, 0);

		// egret.startTick(self.funcTimeStarmp, self);

		//         egret.startTick((timeStarmp: number) => {           
        //     egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
        //     bitmapdata.webGLTexture = null;
        //     return false;
        // }, this.global.self);
		// egret.startTick(func, null);

	}
	// public funcTimeStarmp(timeStarmp: number): boolean{    
    //         egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
    //         bitmapdata.webGLTexture = null;
    //         return false;
	// }

	/**
     * 添加TouchTap监听
     */
    public addTouchTapListener(target: egret.EventDispatcher, call: Function, thisObj?: any, useCapture?: boolean): void {
        target.addEventListener(egret.TouchEvent.TOUCH_BEGIN, call, thisObj, useCapture);
    }
}