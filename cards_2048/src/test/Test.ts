class Test extends Dialog{
    private l_start: eui.Label
    private g_awards: eui.Group
    private index: number  
    private r_frame: eui.Rect
    private timer: egret.Timer
    private targetIndex: number   //想要抽到的物品的index
    private currentIndex: number
    
    public constructor(){
        super('test/TestSkin')
        this.init()
    }

    private init(){
        this.bindClick()
    }
    
    private bindClick(){
        this.l_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStart, this)
    }

    private onStart(){
        this.targetIndex = 100
        this.index = 0
        this.currentIndex = -1
        this.timer = new egret.Timer(100)

        this.timer.addEventListener(egret.TimerEvent.TIMER, this.onStartTimer, this)
        this.timer.start()

    }

    private onStartTimer(){
        this.currentIndex++
        this.timer.delay = Math.floor(1000 / (this.targetIndex - this.currentIndex))
        
        var item = this.g_awards.$children[this.index]
        // console.log(item)
        this.r_frame.x = item.x
        this.r_frame.y = item.y
        if(this.currentIndex === this.targetIndex){
            this.timer.stop()
        }
        this.index++
        
        if(this.index >= this.g_awards.numChildren){
            this.index = 0
        }
    
    }
}