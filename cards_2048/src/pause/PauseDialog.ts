class PauseDialog extends Dialog{
    private g_home: eui.Group
    private g_continue: eui.Group
    private g_restart: eui.Group
    private g_closeCount: eui.Group  //结算游戏
    private r_bg: eui.Rect
    public constructor(){
        super('PauseSkin')
        this.init()
    }

    private init(){
        this.bindClick()
    }

    private bindClick(){
        this.g_home.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            this.removeFromParent()
            AppViews.removePanel(GamePanel)
        }, this)
        this.g_restart.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
            this.removeFromParent()
            AppViews.removePanel(GamePanel)
            AppViews.pushPanel(GamePanel)
        }, this)
        this.g_continue.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseTap, this)
        this.r_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
            this.removeFromParent()
        }, this)
        this.g_closeCount.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseCount, this)
        EventUtils.addScaleListener(this.g_home)
        EventUtils.addScaleListener(this.g_restart)
        EventUtils.addScaleListener(this.g_continue)
    }

    private onCloseTap(){
        this.removeFromParent()
    }

    /**
     * 游戏结束
     */
    private onCloseCount(){
        this.onCloseTap()
        var gamePage: any = AppViews.getTopCommont()
        gamePage.gameOver()
    }
}
