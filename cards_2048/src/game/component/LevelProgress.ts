const LevelInfo = [
    { id: 1, score: 0, color: 0xd1cfcf },
    { id: 2, score: 200, color: 0xabe187 },
    { id: 3, score: 1000, color: 0x5cac86 },
    { id: 4, score: 2000, color: 0xfdb231 },
    { id: 5, score: 4000, color: 0xabe187 },
    { id: 6, score: 10000, color: 0xc96d98 },
    { id: 7, score: 20000, color: 0x34aaae },
    { id: 8, score: 40000, color: 0x2f5a7c },
    { id: 9, score: 80000, color: 0x547d76 },
    { id: 10, score: 100000, color: 0x7d6a95 },
    { id: 11, score: 200000, color: 0x886669 }
]

interface ITFLevelInfo{
    id: number,
    score: number,
    color: number
}

class LevelProgress extends AppComponent {
    private r_bg: eui.Rect
    private r_progress: eui.Rect
    private r_currentLevelRect: eui.Rect
    private r_nextLevelRect: eui.Rect
    private currentLevel: number
    private nextLevel: number
    private currentInfo: ITFLevelInfo
    private nextInfo: ITFLevelInfo

    public constructor() {
        super('game/component/LevelProgressSkin')
    }

    public init(score: number = 0) {
        //找到比这个分大的段
        this.nextInfo = LevelInfo.find( item => {
            return item.score > score
        })
        this.nextLevel = this.nextInfo.id
        
        this.currentInfo = LevelInfo.find( item => {
            return item.id === this.nextLevel - 1
        })
        this.currentLevel = this.currentInfo.id
        
        this.setColor()
        
    }

    public setLevelProgress(score: number) {
        var progressTween = egret.Tween.get(this.r_progress)
        if(score > this.nextInfo.score){
            //这时候要升级
            let width = this.r_bg.width
            //算出多余的分数
            let otherScore = score - this.nextInfo.score
            
            progressTween.to({
                width: width
            },300).to({
                width: 0
            }).call(()=>{
                this.levelUp()
                //算出升级之后还要增加的长度
                let otherLength = otherScore / this.countScore() * this.r_bg.width
                progressTween.to({
                    width: otherLength
                })

            })            
        } else {
            //计算当前分数的长度
            let addScore = score - this.currentInfo.score
            console.log('addScore', addScore)
            let currentLength = addScore / this.countScore() * this.r_bg.width
            console.log(currentLength)
            progressTween.to({
                width: currentLength
            }, 300)
        }
        
        

    }

    /**
     * 计算出当前等级升级需要的分数差
     */
    private countScore(){
        return this.nextInfo.score - this.currentInfo.score 
    }

    /**
     * 升级
     */
    private levelUp(){
        this.currentLevel++
        this.nextLevel++
        this.currentInfo = LevelInfo.find(( item => {
            return item.id === this.currentLevel
        }))
        this.nextInfo = LevelInfo.find(( item => {
            return item.id === this.nextLevel
        }))
        this.setColor()
    }

    private setColor(){
        this.r_currentLevelRect.fillColor = this.r_bg.fillColor = this.currentInfo.color
        this.r_nextLevelRect.fillColor = this.nextInfo.color
    }
}