/**
 * 卡牌换一换
 * state0/state1/state2   不能点击/可以点击换卡牌/点击看视频加次数
 */
class CardChange extends AppComponent{
    private adTimes: number = 0  //已经换的次数
    public constructor(){
        super('game/component/CardChangeSkin')
        this.init()
    }

    private init(){
        this.currentState = 'state1'
        this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onClickTap, this)
    }

    private onClickTap(){
        switch(this.currentState){
            case 'state0':
                break
            case 'state1':
                this.onChangeTap()
                break
            case 'state2':
                this.onAdMovie()
                break
            default:
                break
        }
    }

    /**
     * 换卡牌
     */
    private onChangeTap(){
        var gamePage: any = AppViews.getTopCommont()
        gamePage.onChangeTap()
        //如果没有看视频的次数了就不能点击
        if(this.adTimes > SETTING.CHANGETIMEMAX){
            this.currentState = 'state0'
        }else{
            this.currentState = 'state2'
        }
    }

    /**
     * 看视频
     */
    private onAdMovie(){
        pfUtils.showLoading('加载广告')
        pfUtils.showAds(0).then(res => {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                this.adTimes++
                this.currentState = 'state1'
            }
        })
    }
}