class ResurgenceDialog extends Dialog {
    private g_resurgence: eui.Group   //看视频复活
    private l_skip: eui.Label         //跳过
    public constructor() {
        super('game/ResurgenceSkin')
        this.init()
        this.resurgenceBtnTween()
    }

    private init() {
        this.g_resurgence.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onResurgenceTap, this)
        this.l_skip.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSkip, this)
    }

    /**
     * 复活按钮的动画
     */
    private resurgenceBtnTween() {
        var tween = egret.Tween.get(this.g_resurgence, { loop: true })
        tween
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 1, scaleY: 1 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 1, scaleY: 1 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 1000)
    }

    private onResurgenceTap() {
        pfUtils.showLoading('加载视频')
        pfUtils.showAds(0).then(res => {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                this.removeFromParent()
                var gamePage: any = AppViews.getTopCommont()
                //看视频复活
                gamePage.movieResurgence()
            }
        })
        // this.removeFromParent()
        // var gamePage: any = AppViews.getTopCommont()
        // //看视频复活
        // gamePage.movieResurgence()
    }

    /**
     * 跳过
     */
    private onSkip() {
        this.removeFromParent()
        var gamePage: any = AppViews.getTopCommont()
        gamePage.gameOver()
    }
}