/**
 * 连击的提示
 */
class HitTextComponent extends AppComponent{
    private m_text: eui.Image 
    public constructor(type: number){
        super('game/HitTextSkin')
        this.init()
        this.setImage(type)
        
    }

    private init(){
        this.anchorOffsetX = this.width / 2
        this.anchorOffsetY = this.height / 2
        this.scaleX = this.scaleY = 0
    }

    /**
     * 设置图片
     * @param 连击数
     */
    private setImage(type: number){
        console.log(type)
        if(type > 6){
            type = 6
        }
        var url = `hitx${type}_png`
        //音效
        Sound.initPlay(`hitx${type}.mp3`)
        this.m_text.source = url
        
        egret.Tween.get(this).to({
            scaleX: 1,
            scaleY: 1
        },300,egret.Ease.bounceOut)
        .to({}, 800)
        .to({
            scaleX: 0,
            scaleY: 0
        },200).call(()=>{
            this.removeFromParent()
        })
    }
}