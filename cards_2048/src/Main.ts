window['global'] = window


const Utils = lie.Utils;
const TimeUtils = lie.TimeUtils;
const EventUtils = lie.EventUtils;
const AppViews = lie.AppViews
const Dispatch = lie.Dispatch
const Dialog = lie.Dialog
const UIComponent = lie.UIComponent
const AppComponent = lie.AppComponent;
const AppRenderer = lie.AppRenderer;
const LocalData = lie.LocalData;
const MovieClip = lie.MovieClip
const pfUtils = new lie.WXUtils
const CommonUtils = Common.CommonUtils
const PlatformUtils = lie.PlatformUtils


class Main extends eui.Component {
    
    public constructor() {
        super();
        // var pfUtils = new lie.WXUtils
        XG_STATISTICS.init('wxd809893d56d16080')
        pfUtils.init()
        // this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    
    protected createChildren(): void {
        super.createChildren();
        lie.LifeCycle.init();

        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin
        })

        egret.lifecycle.onPause = () => {
            egret.ticker.pause();
        }

        egret.lifecycle.onResume = () => {
            egret.ticker.resume();
        }


        //注入自定义的素材解析器
        egret.registerImplementation("eui.IAssetAdapter", new AssetAdapter())
        egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter())
        this.loadResource().catch(e => {
            console.log(e);
        });
    }


    // }
    private loadTheme() {
        return new Promise((resolve, reject) => {
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            let theme = new eui.Theme("resource/default.thm.json", this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, () => {
                resolve();
            }, this);
        })
    }
    

    // private async loadResource() {
    //     await this.loadResource()
    //     this.createGameScene();
    //     // const result = await RES.getResAsync("description_json")
    //     await platform.login();
    //     const userInfo = await platform.getUserInfo();
    //     console.log(userInfo);

    // }

    private async loadResource() {
        try {
            const loadingView = new LoadingUI();
            this.stage.addChild(loadingView);
            await RES.loadConfig("resource/default.res.json", "resource/");
            await this.loadTheme();
            await RES.loadGroup("preload", 0, loadingView);
            this.stage.removeChild(loadingView);
            // await platform.login();
            // const userInfo = await platform.getUserInfo();

            this.createGameScene();
            // const result = await RES.getResAsync("description_json")
            
            }
        catch (e) {
            console.error(e);
        }
    }

    private textfield: egret.TextField;

    /**
     * 创建游戏场景
     * Create a game scene
     */
    private createGameScene() {
        CommonUtils.setGlobalShare()
        AppViews.pushPanel(HomePanel)
        // pfUtils.postMessage('update', { "score": '10001', "time": Date.now() })
        // AppViews.pushDialog(ResurgenceDialog)
    }

    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    private createBitmapByName(name: string) {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }

}