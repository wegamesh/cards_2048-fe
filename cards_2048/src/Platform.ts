module lie {

	// 记录平台通用接口以及方法

	/**
	 * 微信通讯格式
	 */
	export interface ITFWXMessage {
		action: string;
		data: any;
	}

	/**
	 * 音乐接口
	 */
	export interface ITFMusic {
		play(): void;
		pause(): void;
		src: string;
		loop: boolean;
		volume: number;
	}

	export interface ITFShareData {
		score?: number,
		isRank?: boolean,
		type?: number
	}

	/**
	 * 保存的数据
	 */
	export interface ITFSaveData {
		score: number;
		time: number;
	}

	/**
	 * 平台工具方法接口
	 */
	export interface ITFPFUtils {
		/**
		 * 服务器监听位置
		 */
		type: string;
		init(): void;
		getGlobalShareInfo(): any;
		showShare(shareData?: ITFShareData): Promise<any>;
		showToast(msg: string): void;
		getSystemInfo(): string;
		setUserInfo(map: { [key: string]: any }): Promise<boolean>;
		postMessage(action: string, data?: any): void;
		getShareCanvas(): egret.Bitmap;
		/**
		 * 获取用户信息
		 */
		getUserInfo(): Promise<any>;
	}

	// export class PlatformUtils {

	// 	/**
	// 	 * 播放音效——可能平台播放音效有差异，暂放这里
	// 	 */
	// 	public static playEffect(res: string, loop: number = 1): egret.SoundChannel {
	// 		return AppConfig.isOpenMusic() && RES.getRes(res).play(0, loop);
	// 	}
	// }
}
// 该变量请看scripts目录下的bricks.ts和wxgame.ts，会初始化为
// WXUtils和BKUtils
// declare const pfUtils: lie.ITFPFUtils;	// 平台工具