class HomePanel extends lie.AppComponent{
    private g_start: eui.Group
    private g_rank: eui.Group
    private g_gameCircle: eui.Image
    public constructor(){
        super('HomeSkin')
        this.init()
    }

    private init(){
        pfUtils.showBannerAds()
        this.bindClick()
    }

    public onShow(){
        pfUtils.hideBannerAds()
        pfUtils.showBannerAds()
    }

    // public onHide(){
    //     pfUtils.hideBannerAds()
    // }

    private bindClick(){
        this.g_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameStart, this)
        this.g_rank.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
            AppViews.pushDialog(RankDialog)
        }, this)
        this.g_gameCircle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameCircleTap, this)
        EventUtils.addScaleListener(this.g_start)
        EventUtils.addScaleListener(this.g_rank)
    }

    private onGameStart(){
        AppViews.pushPanel(GamePanel)
    }

    private onGameCircleTap(){
        var appId = 'wx53b1b4ab6fa0d044'
        var path = 'pages/quan/quan?communityId=3&from=2048kapai_3'
        var imageUrl = "https://static.xunguanggame.com/pocket/assets/sharePic/youxidating4.png"
        pfUtils.navigate2Program(appId, path, imageUrl)
       
    }
}