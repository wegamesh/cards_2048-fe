const SETTING = {
    RECYCLETOTALNUM: 2,   //可以回收的总数量
    ALMIGHTYRANDOM: 20,   //万能卡出现的几率，1/N
    BOMBRANDOM: 5,        //炸弹出现的几率，1/N
    ALMIGHTYID: 1,        //万能卡的ID
    NORMALCARDID: 2,      //普通卡的ID
    BOMBCARDID: 3,        //炸弹牌的ID
    DUSTBINADMAXCOUNT: 5, //可以观看垃圾桶广告的上限次数 
    RECALLMAXCOUNT: 5,    //可以观看回退广告的上限次数 
    RESURGENCEMAX: 1,     //可以观看视频复活的总次数
    CHANGETIMEMAX: 2,     //可以看视频交换卡牌的次数上限
}

class GamePanel extends AppComponent {
    private g_cardsPool: eui.Group      //卡牌池
    private g_cardsGroup1: eui.Group
    private g_cardsGroup2: eui.Group
    private g_cardsGroup3: eui.Group
    private g_cardsGroup4: eui.Group
    private g_dustBin: eui.Group

    private g_explain: eui.Group        //游戏说明
    private m_pause: eui.Group          //暂停
    private currentArea: null | number  //移动到的区域
    private currentCard: CardComponent  //当前选中卡片
    private l_score: eui.Label          //分数
    private totalScore: number = 0      //当前分数
    private l_highScore: eui.Label      //最高分
    private l_hit: eui.Label            //连击
    private hitNum: number = 0          //连击数量
    private currentScore: number = 0

    private l_recallTime: eui.Label
    private l_recallX: eui.Label
    private m_recallAd: eui.Image       //回退广告图片
    private lastScore: number
    private g_recall: eui.Group         //回退按钮
    private recycleNum: number = 0      //回收掉的卡牌数量
    private lastRecycleNum: number      //回退前回收的卡牌数
    private saveInfo: Array<any>        //保存的信息为撤回做准备
    private scoreInterval: egret.Timer  //计分计时器
    private saveScore: number = 0       //记录分数回退用
    private saveCardsPool: Array<any>
    private $groups: Array<eui.Group> = [this.g_cardsGroup1, this.g_cardsGroup2, this.g_cardsGroup3, this.g_cardsGroup4, this.g_dustBin]

    private canRecall: boolean = false  //是否可以回撤(初始为不可以)
    private recallTime: number = 1      //回撤的次数默认送一个

    private anchorX: number  //点击位置离卡片的x轴偏移量
    private anchorY: number  //点击位置离卡片的Y轴偏移量

    private hitTimeout: any  //连击的定时器

    private beyondBitmap: egret.Bitmap   //超越好友
    private g_level: eui.Group
    private levelComponent: LevelProgress //等级

    //广告部分
    private l_x1: eui.Label          //垃圾桶的x的下方
    private l_x2: eui.Label          //垃圾桶的x的上方
    private m_dustBinAd1: eui.Image  //垃圾桶广告1
    private m_dustBinAd2: eui.Image  //垃圾桶广告2
    private dustBinAdCount: number = 0 //已经清空垃圾桶的次数
    private recallAdCount: number = 0  //已经观看的回退广告的次数
    private resurgenceTime: number = 0 //复活次数
    private g_change: eui.Group        //换卡牌

    public constructor() {
        super('game/GameSkin')
        this.init()
    }

    private init() {
        pfUtils.hideBannerAds()
        pfUtils.showBannerAds()
        this.initCardPond()
        this.initGroups()
        this.getHighScore()
        this.bindClick()
        this.initSounds()
        this.initGuide()

        this.levelComponent = new LevelProgress()
        this.levelComponent.init()
        this.g_level.addChild(this.levelComponent)
    }

    private initSounds() {
        Sound.initPlay('chopsticks.mp3', false, 0, true)
        Sound.initPlay('click.mp3', false, 0, true)
        Sound.initPlay('bomb.mp3', false, 0, true)
        Sound.initPlay('icon_close.mp3', false, 0, true)
        Sound.initPlay('unlock.mp3', false, 0, true)
    }

    private initGroups() {
        this.$groups.forEach((item, index) => {
            if (index !== this.$groups.length - 1) {
                var card = new CardComponent(0)

                item.addChild(card)
                card.x = card.width / 2
                card.y = card.height / 2
            }

        })
        // this.g_recall.getChildByName('count')['text'] = this.recallTime
        this.g_change.addChild(new CardChange())
    }

    /**
     * 新手引导
     */
    private initGuide() {
        //获取是不是新手的缓存
        var isNew = LocalData.getItem('CrardsIsNewHand')
        if (!isNew) {
            AppViews.pushDialog(GameExplain)
            LocalData.setItem('CrardsIsNewHand', new Date().toString())
        }

    }

    private bindClick() {
        // this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this)
        this.m_pause.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            AppViews.pushDialog(PauseDialog)
        }, this)
        this.g_explain.addEventListener(egret.TouchEvent.TOUCH_TAP, () => {
            AppViews.pushDialog(GameExplain)
        }, this)
        //垃圾桶视频
        this.m_dustBinAd1.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDustBinAdTap, this)
        this.m_dustBinAd2.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDustBinAdTap, this)
    }

    private onCardTouchBegin(e: egret.TouchEvent) {

        this.recordCardInfo()
        var card: any = e.currentTarget
        this.currentCard = card

        this.g_cardsPool.removeChild(card)
        card.x = e.stageX
        card.y = e.stageY - card.anchorOffsetY
        //记录手在卡牌上的偏移量
        this.addChild(card)
        this.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onCardTouchMove, this)
        this.addEventListener(egret.TouchEvent.TOUCH_END, this.onCardTouchEnd, this)


    }

    private onCardTouchMove(e: egret.TouchEvent) {
        var card: any = this.currentCard

        card.x = e.stageX
        card.y = e.stageY - card.anchorOffsetY
        // var y = card.y - card.anchorOffsetY
        //移动的时候判断在移动到哪个组内
        this.$groups.forEach((item, index) => {
            if (item.hitTestPoint(card.x, card.y)) {
                //振动
                //最后一个是垃圾桶
                if (index === this.$groups.length - 1) {
                    return
                } else {
                    let card2: any = item.$children[item.numChildren - 1]
                    if (card2.value === card.value || card.property === SETTING.ALMIGHTYID) {
                        card2.setRectStroke(0xacffae)
                    } else {
                        card2.setRectStroke(0x544F4F)
                    }
                }

            } else {
                if (index === this.$groups.length - 1) {
                    // this.dustBinReset()
                } else {
                    let card2: any = item.$children[item.numChildren - 1]
                    card2.clearRectStroke()
                }

            }
        })

        //是否碰到了垃圾桶  
        //给卡牌两个碰撞点
        let x2 = card.x + card.width / 2,
            y2 = card.y + card.height / 2

        var dustBin = this.$groups[4]
        if (dustBin.hitTestPoint(x2, y2) || dustBin.hitTestPoint(card.x, card.y)) {
            if (this.recycleNum <= 1) {
                dustBin.getChildByName((this.recycleNum + 1).toString()).alpha = 0.5
            }
        } else {
            this.dustBinReset()
        }
    }

    private onCardTouchEnd(e: egret.TouchEvent) {
        var _self = this
        var card: any = this.currentCard

        var index = this.judgeIndex(e, card)
        if (index >= 0) {

            //垃圾桶
            if (index === this.$groups.length - 1) {
                if (this.recycleNum >= SETTING.RECYCLETOTALNUM) {
                    this.cardReset(card)
                    return
                }
                this.throughToDustBin(card)
                nextStep()
                return
            }


            this.playCardSound(card.property)
            var group = this.$groups[index]
            //正在合并的时候不让放防止bug
            if (group['isCounting']) {
                this.cardReset(card)
                return
            }
            var card2: any = group.$children[group.numChildren - 1]
            card2 && card2.clearRectStroke()

            //万能卡不能移到空的位置
            if (card.property === SETTING.ALMIGHTYID && group.$children.length <= 1) {
                this.cardReset(card)
                return
            }

            if (card.property !== SETTING.BOMBCARDID && card.property !== SETTING.ALMIGHTYID) {
                //当卡牌池数量满了并且不能合并
                if (group.numChildren >= 9 && card.value !== card2.value) {
                    this.cardReset(card)
                    return
                }

                //如果不能合并
                if (card2.value !== card.value && card.property) {
                    //卡牌移入的动画
                    this.cardEndTween(card)
                }
            }





            pfUtils.vibrateShort()
            group.addChild(card)
            this.removeEvent(card)
            card.x = card.anchorOffsetX
            card.y = (group.numChildren - 2) * 60 + card.anchorOffsetY

            nextStep()
            this.countGroupCard(group)


        } else {
            //如果没有放对位置就回到卡牌池中
            this.cardReset(card)
        }

        function nextStep() {
            _self.produceCard()
            _self.setCardPond()

            if (!_self.canRecall) {
                _self.canRecall = true
                _self.recallBtnReset()
            }

        }

    }

    /**
     * 放到垃圾桶
     * @param card
     */
    private throughToDustBin(card) {
        Sound.play('chopsticks.mp3', 0.6)
        this.removeEvent(card)
        this.removeChild(card)
        this.recycleNum++
        if (this.dustBinAdCount < SETTING.DUSTBINADMAXCOUNT) {
            this.addDustBinAd()
        }
        this.dustBinReset()

    }
    /**
     * 记录上一步信息
     */
    private recordCardInfo() {
        //记录当前的所有卡牌数据
        var cardsPool = []
        this.g_cardsPool.$children.forEach((item: CardComponent) => {
            cardsPool.push({ value: item.value, property: item.property })
        })
        this.saveCardsPool = cardsPool

        this.saveInfo = []
        this.$groups.forEach((item, index) => {
            if (index !== this.$groups.length - 1) {
                let group = []
                item.$children.forEach((card: CardComponent) => {
                    group.push({ value: card.value, x: card.x, y: card.y })
                })
                this.saveInfo.push(group)
            }

        })

        this.lastRecycleNum = this.recycleNum
        this.saveScore = this.lastScore
    }

    /**
     * 播放放入卡的音效
     */
    private playCardSound(property: number) {
        if (property === SETTING.BOMBCARDID) {
            Sound.play('bomb.mp3')
        } else {
            Sound.play('click.mp3')
        }
    }

    /**
     * 获取最高分
     */
    private getHighScore() {
        var score = LocalData.getItem('highScore')
        if (score) {
            this.l_highScore.text = this.setScoreDigit(+score)
        } else {
            this.l_highScore.text = '0'
        }
    }

    /**
     * 卡牌回到原位
     * @param card
     */
    private cardReset(card: CardComponent) {
        this.removeEvent(card)
        egret.Tween.get(card).to({
            x: this.g_cardsPool.x + card.lastX,
            y: this.g_cardsPool.y + card.lastY
        }, 300).call(() => {
            this.removeChild(card)
            card.x = card.lastX
            card.y = card.lastY
            this.g_cardsPool.addChild(card)
            card.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onCardTouchBegin, this)
        })
    }



    /**
     * 将卡牌池的第二张移到可操作
     */
    private setCardPond() {
        var card: any = this.g_cardsPool.$children[1]
        var x = card.x + 110
        egret.Tween.get(card).to({
            x: x,
            scaleX: 1,
            scaleY: 1
        }, 500).call(() => {
            card.lastX = card.x
            card.lastY = card.y
            //绑定事件
            card.addEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onCardTouchBegin, this)
            // this.addEventListener(egret.TouchEvent.TOUCH_CANCEL, this.onCardTouchEnd, this)
        })
    }
    /**
     * 初始化一些属性
     */
    private propertyReset(group) {

    }

    /**
     * 判断在哪个位置
     * @param 
     */
    private judgeIndex(e: egret.TouchEvent, card: egret.DisplayObject): number | undefined {
        //移动的时候判断在移动到哪个组内
        var y = card.y
        let x2 = card.x + card.width / 2,
            y2 = card.y + card.height / 2

        for (let i = 0; i < this.$groups.length; i++) {

            var item = this.$groups[i]
            if (i === 4) {
                if (item.hitTestPoint(x2, y2) || item.hitTestPoint(card.x, card.y)) {
                    return 4
                }
            } else {
                if (item.hitTestPoint(card.x, y)) {
                    return i
                }
            }

        }

        return void 0
    }

    /**
     * 重置垃圾桶
     */
    private dustBinReset() {
        if (this.recycleNum === 1) {
            this.g_dustBin.getChildByName('1').alpha = 0.5
            this.g_dustBin.getChildByName('2').alpha = 0
            return
        }

        if (this.recycleNum === 2) {
            this.g_dustBin.getChildByName('1').alpha = 0.5
            this.g_dustBin.getChildByName('2').alpha = 0.5
            return
        }

        this.g_dustBin.getChildByName('1').alpha = 0
        this.g_dustBin.getChildByName('2').alpha = 0
    }

    /**
     * 加一个垃圾桶视频
     */
    private addDustBinAd() {
        if (!this.m_dustBinAd1.visible) {
            this.m_dustBinAd1.visible = true
            this.l_x1.visible = false
        } else {
            this.m_dustBinAd2.visible = true
            this.l_x2.visible = false
        }
    }

    /**
     * 减去一个垃圾桶视频
     */
    private removeDustBinAd() {
        if (this.m_dustBinAd2.visible) {
            this.m_dustBinAd2.visible = false
            this.l_x2.visible = true
        } else {
            this.m_dustBinAd1.visible = false
            this.l_x1.visible = true
        }
    }


    /**
     * 清除卡牌上的所有绑定事件
     */
    private removeEvent(card: eui.Component) {
        card.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onCardTouchBegin, this)
        this.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.onCardTouchMove, this)
        this.removeEventListener(egret.TouchEvent.TOUCH_END, this.onCardTouchEnd, this)
    }

    /**
     * 初始化生成的卡牌池
     */
    private initCardPond() {
        for (let i = 0; i < 2; i++) {
            this.produceCard(2)
        }
        this.setCardPond()
    }

    /**
     * 生成卡片
     * @param num   1固定万能卡/2固定普通卡/3炸弹牌默认
     */
    private produceCard(num: number = 0) {
        var _self = this
        // if (num === SETTING.ALMIGHTYID) {
        //     this.createCard(SETTING.ALMIGHTYID)
        // }
        // else if (num === SETTING.NORMALCARDID) {
        //     getNormalCard()
        // }
        // else {
        //     if (Math.floor(Math.random() * SETTING.ALMIGHTYRANDOM) === 0) {
        //         this.createCard(SETTING.ALMIGHTYID)
        //     } else {
        //         getNormalCard()
        //     }
        // }
        switch (num) {
            //万能牌
            case SETTING.ALMIGHTYID:
                this.createCard(num)
                break
            //普通牌
            case SETTING.NORMALCARDID:
                getNormalCard()
                break
            //炸弹牌
            case SETTING.BOMBCARDID:
                this.createCard(num)
                break
            default:
                if (Math.floor(Math.random() * SETTING.ALMIGHTYRANDOM) === 0) {
                    this.createCard(SETTING.ALMIGHTYID)
                } else if ((Math.floor(Math.random() * SETTING.BOMBRANDOM) === 0)) {
                    this.createCard(SETTING.BOMBCARDID)
                }
                else {
                    getNormalCard()
                }
                break
        }

        function getNormalCard() {
            var array = [1, 2, 3, 4, 5, 6]
            var randomIndex = Math.floor(Math.random() * array.length)
            var value = Math.pow(2, array[randomIndex])
            _self.createCard(SETTING.NORMALCARDID, value)
        }

    }

    /**
     * 生成一张卡牌
     */
    private createCard(type: number, value?: number) {
        var card = null

        switch (type) {
            //万能
            case SETTING.ALMIGHTYID:
                card = new AlmightyCard()
                break
            //炸弹
            case SETTING.BOMBCARDID:
                card = new CardBomb()
                break
            default:
                card = new CardComponent(value)
        }
        card.property = type

        this.g_cardsPool.addChildAt(card, 0)
    }

    /**
     * 判断group里面有没有相同的两张牌
     * 如果相同就合并
     */
    private countGroupCard(group: eui.Group) {
        var index = null   //有相同的话需要删掉的东西
        // this.setCalculate()
        //正在计算的时候不让放卡牌防止bug
        group['isCounting'] = true

        var lastIndex = group.numChildren - 1
        var lastCard: any = group.$children[lastIndex]
        var sideCard: any = group.$children[lastIndex - 1]
        //炸弹
        if (lastCard.property === SETTING.BOMBCARDID) {
            group['isCounting'] = false
            this.bombGroup(group)
            return
        }
        //如果最后一张卡是万能卡
        if (lastCard.property === SETTING.ALMIGHTYID) {
            lastCard.value = sideCard.value
        }

        if (lastCard.value === sideCard.value) {
            if (!group['hitCount']) {
                group['hitCount'] = 1
            } else {
                group['hitCount']++

            }

            var tween2 = egret.Tween.get(lastCard),
                tween = egret.Tween.get(sideCard)

            tween2
                .to({ anchorOffsetY: 170 }, 200)
                .to({ scaleX: 0.8, scaleY: 0.8 }, 200)

            tween
                .to({}, 200)
                .to({ scaleX: 0.8, scaleY: 0.8 }, 200)
                .call(() => {
                    Sound.play('unlock.mp3', 0.6)
                    //有翻倍
                    this.addScore(lastCard.value * group['hitCount'])
                    sideCard.value *= 2
                    sideCard.setBgColor(sideCard.value)
                    group.removeChild(lastCard)
                    group['hitCount'] > 1 && this.createLabelTween(group, group['hitCount'])
                })
                .to({ scaleX: 1, scaleY: 1 }, 200, egret.Ease.bounceInOut)
                .call(() => {
                    this.countGroupCard(group)
                })

        } else {
            //不能合卡牌的时候判断是否结束
            group['hitCount'] > 2 && this.hitRemind(group['hitCount'])
            group['hitCount'] = 0
            group['isCounting'] = false
            this.judgeGameOver()
        }

    }

    /**
     * 清除group的连击数
     */
    private clearHitCount(group: eui.Group) {
        group['hitCount'] = 0
    }

    /**
     * 加分数
     */
    private addScore(num: number) {
        this.totalScore += num

        this.levelComponent.setLevelProgress(this.totalScore)
        this.setBeyond(this.totalScore)
        //数字表示每秒加的次数
        var speed = Math.ceil(num / 20)

        if (this.scoreInterval) {
            this.scoreInterval.stop()
            this.scoreInterval = null
        }
        this.scoreInterval = new egret.Timer(30)
        this.scoreInterval.addEventListener(egret.TimerEvent.TIMER, () => {

            this.currentScore += speed
            this.l_score.text = this.setScoreDigit(this.currentScore)
            if (this.currentScore >= this.totalScore) {
                this.l_score.text = this.setScoreDigit(this.totalScore)
                this.scoreInterval.stop()
            }
        }, this)
        this.scoreInterval.start()

    }

    /**
     * 判断游戏结束
     */
    private judgeGameOver() {
        var totalFull = 0
        var groups = this.$groups.slice(0, 4)
        for (var i = 0; i < groups.length; i++) {
            var item = groups[i]
            if (item.numChildren >= 9) {
                totalFull++
            }
        }
        if (totalFull >= 4) {
            var boolMerge: boolean = true

            //卡牌中没有合成的了（包括没有万能卡）
            var poolCard: any = this.g_cardsPool.$children[this.g_cardsPool.numChildren - 1]
            var poolCard2: any = this.g_cardsPool.$children[0]

            groups.forEach(item => {
                var last: any = item.$children[item.numChildren - 1]
                //如果卡牌池有可以合成的卡牌或者卡牌池第一张是万能牌
                if (last.value === poolCard.value || poolCard.property === SETTING.ALMIGHTYID) {
                    boolMerge = false
                }
                //如果卡牌池第二张可以合成并且垃圾桶有位置
                if ((last.value === poolCard2.value || poolCard2.property === SETTING.ALMIGHTYID) && this.recycleNum < 2) {
                    boolMerge = false
                }
            })

            if (boolMerge) {
                //游戏结束
                if (this.resurgenceTime >= SETTING.RESURGENCEMAX) {
                    this.gameOver()
                } else {
                    AppViews.pushDialog(ResurgenceDialog)
                }

            }

        }
    }

    /**
     * =主动结束游戏
     */
    public gameOver() {
        AppViews.pushDialog(GameOverDialog, this.totalScore)
        this.setHighScore()
    }

    /**
     * 设置最高分
     */
    private setHighScore() {
        var score = this.totalScore

        var highScore = +LocalData.getItem('highScore')
        if (score >= highScore) {
            LocalData.setItem('highScore', score.toString())
        }
    }

    /**
     * 连击动画
     */
    private createLabelTween(obj: eui.Group, num: number) {
        var label = new HitCountComponent(num)

        var card = obj.$children[obj.numChildren - 1]
        label.x = obj.x + obj.width / 2 - label.width / 2
        label.y = card.y + card.height - 20
        this.addChild(label)
        egret.Tween.get(label).to({
            anchorOffsetY: 50,
            alpha: 0
        }, 600)
            .call(() => {
                this.removeChild(label)
            })

    }

    /**
     * 重置回撤按钮
     */
    private recallBtnReset() {
        if (this.recallTime >= 1) {
            this.g_recall.getChildByName('rect')['fillColor'] = 0xf97f70
            this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this)
            this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this)
            this.l_recallTime.visible = this.l_recallX.visible = true
            this.m_recallAd.visible = false

        } else {
            // this.g_recall.getChildByName('rect')['fillColor'] = 0x686767
            this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this)
            if (this.recallAdCount < SETTING.RECALLMAXCOUNT) {
                this.l_recallTime.visible = this.l_recallX.visible = false
                this.m_recallAd.visible = true
                this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this)
            } else {
                this.g_recall.getChildByName('rect')['fillColor'] = 0x686767
                this.l_recallTime.visible = this.l_recallX.visible = true
                this.m_recallAd.visible = false
                this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this)
            }

        }

    }

    /**
     * 回撤
     */
    private onRecallTap() {
        if (!this.canRecall || this.recallTime <= 0) {
            return
        }
        this.recallTime--
        this.canRecall = false
        this.recallBtnReset()
        this.saveInfo.forEach((item, index) => {
            var group = this.$groups[index]
            group.removeChildren()
            item.forEach(item2 => {
                var card = new CardComponent(item2.value)
                card.x = item2.x
                card.y = item2.y
                group.addChild(card)
            })
        })

        this.g_cardsPool.removeChildren()
        this.saveCardsPool = this.saveCardsPool.reverse()
        this.saveCardsPool.forEach(item => {
            this.createCard(item.property, item.value)
        })
        this.setCardPond()

        this.recycleNum = this.lastRecycleNum
        if (this.scoreInterval && this.scoreInterval.running) {
            this.scoreInterval.stop()
            this.l_score.text = this.setScoreDigit(this.saveScore) || '0'
        }

        this.dustBinReset()
    }

    /**
     * 看垃圾桶广告视频
     */
    private onDustBinAdTap() {
        pfUtils.showLoading('加载广告')
        pfUtils.showAds(0).then(res => {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                this.recycleNum--
                this.dustBinAdCount++
                this.dustBinReset()
                this.removeDustBinAd()
            }
        })
    }

    /**
     * 看视频复活
     */
    public movieResurgence() {
        //取 $group 的前四个
        var groups = this.$groups.slice(0, 4)
        groups.forEach((item, index) => {
            //删掉一半的牌
            for (let i = item.numChildren - 1; i > 4; i--) {
                item.removeChildAt(i)
            }
        })
        this.resurgenceTime++
    }

    /**
     * 回退广告
     */
    private recallAdShow() {
        pfUtils.showLoading('加载广告')
        pfUtils.showAds(0).then(res => {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                this.recallTime += 1
                this.recallAdCount++
                this.recallBtnReset()
            }
        })
    }


    /**
     * 弹出连击提示
     */
    private hitRemind(type: number) {
        var hitCom = new HitTextComponent(type)
        hitCom.x = this.width / 2
        hitCom.y = this.height / 2 - 200
        this.addChild(hitCom)
    }

    /**
     * 卡牌移入的动画
     */
    private cardEndTween(card: CardComponent) {

        egret.Tween.get(card)
            .to({
                scaleY: 0.7
            }, 200)
            .to({
                scaleY: 1
            }, 200, egret.Ease.bounceOut)
    }

    /**
     * 分数保留5位数
     */
    private setScoreDigit(score: number): string {
        var string = score.toString()

        if (score > 10000) {
            let num = (score / 1000).toFixed(2)
            string = num + 'k'
        }
        if (score > 1000000) {
            let num = (score / 1000000).toFixed(2)
            string = num + 'm'
        }
        return string
    }

    /**
     * 卡牌换一换
     */
    public onChangeTap() {
        this.g_cardsPool.removeChildren()
        this.initCardPond()
    }

    /**
     * 超越好友
     * @param score 分数
     */
    private setBeyond(score: number) {
        if (this.beyondBitmap) {
            this.removeChild(this.beyondBitmap)
        }
        pfUtils.postMessage('beyond', score)
        var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        var texture = new egret.Texture();
        bitmapdata.$deleteSource = false;
        texture._setBitmapData(bitmapdata);
        // 界面显示
        var bitmap = this.beyondBitmap = new egret.Bitmap(texture);
        bitmap.width = this.width;
        bitmap.height = this.height;
        bitmap.x = 135;
        bitmap.y = 90;
        this.addChild(bitmap);

        egret.setTimeout(() => {
            egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
            bitmapdata.webGLTexture = null;
        }, this, 500)
    }

    /**
     * 炸弹牌使用
     */
    private bombGroup(group: eui.Group) {
        var cards;
        var score = 0
        //把第一张炸弹牌消掉
        var bombCard: any = group.$children[group.$children.length - 1]

        var targetX = this.l_score.x + this.l_score.width / 2,
            targetY = this.l_score.y + this.l_score.height / 2

        group.removeChild(bombCard)
        cards = group.$children.slice(1, group.numChildren)
        cards.forEach((item: any, index) => {

            let x = group.x + item.x,
                y = group.y + item.y
            group.removeChild(item)

            if (item.property === SETTING.NORMALCARDID) {
                score += item.value * 2
                // this.addScore(item.value * 2)
            }

            item.x = x; item.y = y
            this.addChild(item)
            egret.Tween.get(item)
                .to({ x: this.width / 2, y: this.height / 2 }, 1000, egret.Ease.quartInOut)
                .to({ x: targetX, y: targetY, scaleX: 0, scaleY: 0, alpha: 0.3, }, 500, egret.Ease.quadOut)
                .call(() => {
                    this.removeChild(item)

                    if (index === cards.length - 1) {
                        this.addScore(score)
                    }
                })
        })
    }
}