class GameOverDialog extends Dialog{
    private g_start: eui.Group
    private g_rank: eui.Group
    private g_home: eui.Group   
    private g_share: eui.Group
    private score: number      //分数
    public constructor(score){
        super('GameOverSkin')
        this.score = score
        this.init()
        
    }

    private init(){
        pfUtils.hideBannerAds()
        pfUtils.showBannerAds()
        this.g_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameStart, this)
        this.g_rank.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
            AppViews.pushDialog(RankDialog)
        }, this)
        this.g_home.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
            this.removeFromParent()
            AppViews.removePanel(GamePanel)
        }, this)
        this.g_share.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onShareTap, this)
        EventUtils.addScaleListener(this.g_start)
        EventUtils.addScaleListener(this.g_home)
        EventUtils.addScaleListener(this.g_rank)
        this.uploadScore()
    }

    private onGameStart(){
        this.removeFromParent()
        AppViews.removePanel(GamePanel)
        AppViews.pushPanel(GamePanel)
    }

    /**
     * 上传分数
     */
    private uploadScore(){
        var post = pfUtils.postMessage
        var data: ITFUpdataData = {
            "score": this.score,
            "time": Date.now()
        }
       
        post('update', data)
        post('settle')
        post('clear')
        
        // post('resume')
    }

    /**
     * 分享
     */
    private onShareTap(){
        const title = `我获得了${this.score}分,还有谁？`
        const { imageUrl } = CommonUtils.getShareInfo()
        CommonUtils.showShare(title, imageUrl)
    }
}