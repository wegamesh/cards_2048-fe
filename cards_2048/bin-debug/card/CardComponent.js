var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var number2Color = {
    0: 0xffffff,
    2: 0xd1cfcf,
    4: 0xabe187,
    8: 0x5cac86,
    16: 0xfdb231,
    32: 0xc96d98,
    64: 0x34aaae,
    128: 0x2f5a7c,
    256: 0x547d76,
    512: 0x7d6a95,
    1024: 0x886669,
    2048: 0x585956
};
var CardComponent = (function (_super) {
    __extends(CardComponent, _super);
    function CardComponent(data) {
        var _this = _super.call(this) || this;
        _this.skinName = 'resource/scene/Card.exml';
        _this.anchorOffsetX = _this.width / 2;
        _this.anchorOffsetY = _this.height / 2;
        if (data > 0) {
            _this.value = data;
        }
        else {
            _this.r_bg.fillAlpha = 0;
        }
        _this.setBgColor(data);
        return _this;
    }
    CardComponent.prototype.setBgColor = function (data) {
        this.r_bg.fillColor = number2Color[data];
    };
    /**
     * 设置边框的颜色
     * @param data 颜色色值
     */
    CardComponent.prototype.setRectStroke = function (data) {
        this.r_bg.strokeColor = data;
        // this.r_bg.strokeWeight = 4
    };
    CardComponent.prototype.clearRectStroke = function () {
        this.r_bg.strokeColor = 0xffffff;
        // this.r_bg.strokeWeight = 0
    };
    return CardComponent;
}(eui.Component));
__reflect(CardComponent.prototype, "CardComponent");
