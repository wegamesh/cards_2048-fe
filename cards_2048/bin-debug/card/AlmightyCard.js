var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var AlmightyCard = (function (_super) {
    __extends(AlmightyCard, _super);
    function AlmightyCard() {
        var _this = _super.call(this, null) || this;
        _this.skinName = 'resource/scene/AlmightyCardSkin.exml';
        return _this;
    }
    return AlmightyCard;
}(CardComponent));
__reflect(AlmightyCard.prototype, "AlmightyCard");
