var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 排行榜界面
 */
var RankDialog = (function (_super) {
    __extends(RankDialog, _super);
    function RankDialog() {
        var _this = _super.call(this) || this;
        _this.skinName = "RankDialogSkin";
        // this.init();
        _this.global = window;
        _this.height = egret.MainContext.instance.stage.stageHeight;
        _this.width = egret.MainContext.instance.stage.stageWidth;
        // this.addChild(this.m_Bg);		
        // this.isShare = true;
        // this.setModel(0);
        // var Utils = new Utils();
        // 结束页面 排行-竖排
        _this.initShareCanvas();
        // const bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        // bitmapdata.$deleteSource = false;
        // const texture = new egret.Texture();
        // texture._setBitmapData(bitmapdata);
        // var bitmap = new egret.Bitmap(texture);
        // bitmap.width = 750;
        // bitmap.height = 1334;
        // bitmap.x = 0;
        // // console.log(this.worldHeight)
        // bitmap.y = 10;
        // egret.startTick((timeStarmp: number) => {
        //     egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
        //     bitmapdata.webGLTexture = null;
        //     return false;
        // }, this);
        // this.addChild(bitmap);
        var self = _this;
        var addl = self.addTouchTapListener;
        addl(self.m_imgClose, function () {
            // self.removeChild(bitmap);
            self.m_pShare.texture.dispose();
            self.m_pShare = null;
            Utils.removeTimer(self.m_pTimer, self.m_pRefresh, null);
            _this.removeFromParent();
            pfUtils.postMessage('clear');
        }, self);
        return _this;
    }
    RankDialog.prototype.init = function () {
        // super.onCreate();
        // addl(self.m_imgLast, self.onLast, self);
        // addl(self.m_imgNext, self.onNext, self);
        // addl(self.m_btnShare, self.onShare, self);
    };
    RankDialog.prototype.onDestroy = function () {
        pfUtils.postMessage('exit', 2);
    };
    // protected initShareCanvas(): void {
    // 	// super.initShareCanvas(0.5);
    // }
    /**
     * 点击上一排
     */
    RankDialog.prototype.onLast = function () {
        pfUtils.postMessage('last');
    };
    /**
     * 点击下一排
     */
    RankDialog.prototype.onNext = function () {
        pfUtils.postMessage('next');
    };
    /**
     * 点击底部按钮
     */
    RankDialog.prototype.onShare = function () {
        var self = this;
        pfUtils.showShare().then(function (ticket) {
            if (ticket) {
                // self.setModel(1);
                pfUtils.postMessage('ticket', ticket);
            }
        });
    };
    /**
     * 设置模式
     * @param 是否显示群排行
     */
    // protected setModel(model: number): void {
    // 	var self = this;
    // 	// self.m_imgTitle.source = 'title' + model + '_ranking_png';
    // 	// self.m_btnShare.source = 'rank_share' + model + '_png';
    // }
    RankDialog.prototype.removeself = function () {
        this.removeChild(this.m_pShare);
        // this.bitmap.parent.removechild(this.bitmap);
        this.removeChildren();
    };
    /**
     * 初始化离屏canvas
     */
    RankDialog.prototype.initShareCanvas = function () {
        var self = this;
        var stage = self.stage;
        pfUtils.postMessage('rank'); // 显示排行榜不清理底层
        pfUtils.postMessage('resume');
        var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        var texture = new egret.Texture();
        bitmapdata.$deleteSource = false;
        texture._setBitmapData(bitmapdata);
        // 界面显示
        var bitmap = self.m_pShare = new egret.Bitmap(texture);
        // bitmap.width = 532;
        // bitmap.height = 800;
        // self.m_gpShare.addChild(bitmap);
        bitmap.width = this.width;
        bitmap.height = this.height;
        bitmap.x = 0;
        // console.log(this.worldHeight)
        bitmap.y = 10;
        self.addChild(bitmap);
        // 刷新
        var func = self.m_pRefresh = function (timeStarmp) {
            egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
            console.log("是否在循环");
            bitmapdata.webGLTexture = null;
            return false;
        };
        self.m_pTimer = Utils.createTimer(func, null, 0.05, 0);
        // egret.startTick(self.funcTimeStarmp, self);
        //         egret.startTick((timeStarmp: number) => {           
        //     egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
        //     bitmapdata.webGLTexture = null;
        //     return false;
        // }, this.global.self);
        // egret.startTick(func, null);
    };
    // public funcTimeStarmp(timeStarmp: number): boolean{    
    //         egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
    //         bitmapdata.webGLTexture = null;
    //         return false;
    // }
    /**
     * 添加TouchTap监听
     */
    RankDialog.prototype.addTouchTapListener = function (target, call, thisObj, useCapture) {
        target.addEventListener(egret.TouchEvent.TOUCH_BEGIN, call, thisObj, useCapture);
    };
    return RankDialog;
}(Dialog));
__reflect(RankDialog.prototype, "RankDialog");
