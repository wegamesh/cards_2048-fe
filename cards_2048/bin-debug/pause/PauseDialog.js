var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var PauseDialog = (function (_super) {
    __extends(PauseDialog, _super);
    function PauseDialog() {
        var _this = _super.call(this, 'PauseSkin') || this;
        _this.init();
        return _this;
    }
    PauseDialog.prototype.init = function () {
        this.bindClick();
    };
    PauseDialog.prototype.bindClick = function () {
        var _this = this;
        this.g_home.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
            AppViews.removePanel(GamePanel);
        }, this);
        this.g_restart.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
            AppViews.removePanel(GamePanel);
            AppViews.pushPanel(GamePanel);
        }, this);
        this.g_continue.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseTap, this);
        this.r_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
        }, this);
        this.g_closeCount.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseCount, this);
        EventUtils.addScaleListener(this.g_home);
        EventUtils.addScaleListener(this.g_restart);
        EventUtils.addScaleListener(this.g_continue);
    };
    PauseDialog.prototype.onCloseTap = function () {
        this.removeFromParent();
    };
    /**
     * 游戏结束
     */
    PauseDialog.prototype.onCloseCount = function () {
        this.onCloseTap();
        var gamePage = AppViews.getTopCommont();
        gamePage.gameOver();
    };
    return PauseDialog;
}(Dialog));
__reflect(PauseDialog.prototype, "PauseDialog");
