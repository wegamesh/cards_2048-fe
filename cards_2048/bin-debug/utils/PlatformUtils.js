var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var lie;
(function (lie) {
    /**
     * 网络状态工具——基于微信
     */
    var NetworkUtils = (function () {
        function NetworkUtils() {
        }
        /**
         * 获取当前网络状态
         */
        NetworkUtils.getNetworkType = function () {
            return new Promise(function (resolve) {
                wx.getNetworkType({
                    success: function (res) {
                        resolve(res.networkTyp);
                    }
                });
            });
        };
        /**
         * 判断类型是否是最佳网络状态
         */
        NetworkUtils.isBestNetworkType = function (type) {
            return type == '4g' || type == 'wifi';
        };
        /**
         * 判断网络状态是否提升
         * @param oldType 旧类型
         * @param newType 新类型
         */
        NetworkUtils.networkUpgrade = function (oldType, newType) {
            var types = ['none', 'unknown', '2g', '3g', '4g', 'wifi'];
            var index0 = types.indexOf(oldType);
            var index1 = types.indexOf(newType);
            return index1 > index0;
        };
        /**
         * 添加网络变化回调
         * @param call 回调，参数是当前的网络状态
         * @param thisObj 回调对象
         * @param isOnce 是否是一次性的回调，即回调结束下次网络变化不再接受
         */
        NetworkUtils.addNetworkStatusChange = function (call, thisObj, isOnce) {
            NetworkUtils.$netCall.push([call, thisObj, isOnce]);
        };
        /**
         * 移除网络变化回调
         * @param call 回调，参数是当前的网络状态
         * @param thisObj 回调对象
         * @returns 返回移除结果
         */
        NetworkUtils.removeNetworkStatusChange = function (call, thisObj) {
            var netCall = NetworkUtils.$netCall;
            for (var i = 0, len = netCall.length; i < len; i++) {
                var item = netCall[i];
                if (item[0] == call && item[1] == thisObj) {
                    netCall.splice(i, 1);
                    return true;
                }
            }
            return false;
        };
        /**
         * 执行网络变化回调——请勿手动调用
         */
        NetworkUtils.excuteStatusChange = function (type) {
            var netCall = NetworkUtils.$netCall;
            for (var i = 0, len = netCall.length; i < len; i++) {
                var item = netCall[i];
                item[0].call(item[1], type);
                // 删除一次性回调
                if (item[2]) {
                    netCall.splice(i, 1);
                    i--;
                }
            }
        };
        NetworkUtils.$netCall = [];
        return NetworkUtils;
    }());
    lie.NetworkUtils = NetworkUtils;
    __reflect(NetworkUtils.prototype, "lie.NetworkUtils");
    var PlatformUtils = (function () {
        function PlatformUtils() {
        }
        //登录
        PlatformUtils.login = function () {
            return new Promise(function (resolve, reject) {
                wx.login({
                    success: function (res) {
                        console.log("weixin返回的登录数据", res);
                        var data = { "code": res.code };
                        Api.post(Api.login, data).then(function (loginRes) {
                            console.log("后端返回的登录数据", loginRes);
                            AppConfig.gameInfo = {
                                score: loginRes.UserInfo.Scroe,
                                userId: loginRes.UserInfo.UserId
                            };
                        });
                    }
                });
            });
        };
        return PlatformUtils;
    }());
    lie.PlatformUtils = PlatformUtils;
    __reflect(PlatformUtils.prototype, "lie.PlatformUtils");
})(lie || (lie = {}));
