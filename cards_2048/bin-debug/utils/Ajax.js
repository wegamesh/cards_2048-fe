var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var Ajax = (function () {
    // private api: Api = Api.getInstance()
    function Ajax() {
    }
    Ajax.getInstance = function () {
        if (!Ajax.ajax) {
            Ajax.ajax = new Ajax();
        }
        return Ajax.ajax;
    };
    Ajax.prototype.get = function (url, data) {
        var _this = this;
        var _self = this;
        return new Promise(function (resolve, reject) {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            var param = '';
            if (data) {
                if (data.__proto__ === Object.prototype) {
                    for (var key in data) {
                        param += '/' + data[key];
                    }
                }
                else {
                    param = '/' + data;
                }
            }
            request.open(url + param, egret.HttpMethod.GET);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send();
            request.addEventListener(egret.Event.COMPLETE, onGetComplete, _this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onGetIOError, _this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onGetProgress, _this);
            if (_self.timeout) {
                clearTimeout(_self.timeout);
            }
            if (_self.timeout2) {
                clearTimeout(_self.timeout2);
            }
            _self.timeout = setTimeout(function () {
                console.error(url);
                wx.showToast({
                    title: '连接超时',
                    icon: 'none'
                });
                clear();
                reject();
            }, 5000);
            _self.timeout2 = setTimeout(function () {
                Action.showLoading();
            }, 800);
            function clear() {
                clearTimeout(_self.timeout);
                clearTimeout(_self.timeout2);
                _self.timeout = null;
                _self.timeout2 = null;
                Action.hideLoading();
            }
            function onGetComplete(e) {
                // _self.sendDot('requestSendComplete')
                resolve(request.response);
                clear();
            }
            function onGetIOError(e) {
                wx.showToast({
                    title: '请求失败',
                    icon: 'none'
                });
                // _self.sendDot('requestSendError', e)
                clear();
                reject();
            }
            function onGetProgress(e) {
                // _self.sendDot('requestSendSuccess')
                resolve(JSON.parse(request.response));
                clear();
            }
        });
    };
    Ajax.prototype.post = function (url, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var _self = this;
        return new Promise(function (resolve, reject) {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            //设置为 POST 请求
            request.open(url + '?', egret.HttpMethod.POST);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(json2form(data));
            request.addEventListener(egret.Event.COMPLETE, onPostComplete, _this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, _this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onPostProgress, _this);
            if (_self.timeout) {
                clearTimeout(_self.timeout);
            }
            if (_self.timeout2) {
                clearTimeout(_self.timeout2);
            }
            _self.timeout = setTimeout(function () {
                console.error(url);
                wx.showToast({
                    title: '连接超时',
                    icon: 'none'
                });
                clear();
                reject();
            }, 5000);
            _self.timeout2 = setTimeout(function () {
                Action.showLoading();
            }, 800);
            function clear() {
                clearTimeout(_self.timeout);
                clearTimeout(_self.timeout2);
                _self.timeout = null;
                _self.timeout2 = null;
                Action.hideLoading();
            }
            function onPostComplete(e) {
                clear();
                // _self.sendDot('requestSendComplete')
                resolve(request.response);
            }
            function onPostIOError(e) {
                wx.showToast({
                    title: '请求失败',
                    icon: 'none'
                });
                // _self.sendDot('requestSendError', e)
                clear();
                reject();
            }
            function onPostProgress(e) {
                clear();
                // _self.sendDot('requestSendSuccess')
                resolve(JSON.parse(request.response));
            }
        });
    };
    /**
     * 发送打点信息
     * @param Action 打点信息
     */
    Ajax.prototype.sendDot = function (Action, error) {
        //打点
        XG_STATISTICS.send('pocket_' + Action || "wx");
        var myInfo = AppConfig.myInfo;
        var userId = 0;
        try {
            if (myInfo.sid) {
                userId = myInfo.sid;
            }
            var data = {
                userId: userId,
                appId: Api.appId,
                Action: 'pocket_' + Action || "wx",
                info: error
            };
            wx.request({
                url: Api.hostDot + '/Action',
                data: data,
                header: {},
                method: "POST",
                success: function (t) {
                }
            });
        }
        catch (error) {
            console.error(error);
        }
    };
    Ajax.prototype.dotPost = function (url, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return new Promise(function (resolve, reject) {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            //设置为 POST 请求
            request.open(url + '?', egret.HttpMethod.POST);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(json2form(data));
            request.addEventListener(egret.Event.COMPLETE, onPostComplete, _this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, _this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onPostProgress, _this);
            function onPostComplete(e) {
                resolve(request.response);
            }
            function onPostIOError(e) {
                reject();
            }
            function onPostProgress(e) {
                resolve(JSON.parse(request.response));
            }
        });
    };
    return Ajax;
}());
__reflect(Ajax.prototype, "Ajax");
function addItemsToForm(form, names, obj) {
    // eslint-disable-next-line no-use-before-define
    if (obj === undefined || obj === "" || obj === null)
        return addItemToForm(form, names, "");
    if (typeof obj === "string" ||
        typeof obj === "number" ||
        obj === true ||
        obj === false) {
        // eslint-disable-next-line no-use-before-define
        return addItemToForm(form, names, obj);
    }
    // eslint-disable-next-line no-use-before-define
    if (obj instanceof Date)
        return addItemToForm(form, names, obj.toJSON());
    // array or otherwise array-like
    if (obj instanceof Array) {
        return obj.forEach(function (v, i) {
            names.push("[" + i + "]");
            addItemsToForm(form, names, v);
            names.pop();
        });
    }
    if (typeof obj === "object") {
        return Object.keys(obj).forEach(function (k) {
            names.push(k);
            addItemsToForm(form, names, obj[k]);
            names.pop();
        });
    }
    return null;
}
function addItemToForm(form, names, value) {
    var name = encodeURIComponent(names.join(".").replace(/\.\[/g, "["));
    value = encodeURIComponent(value.toString());
    form.push(name + "=" + value);
}
function json2form(data) {
    var form = [];
    addItemsToForm(form, [], data);
    var body = form.join("&");
    return body;
}
