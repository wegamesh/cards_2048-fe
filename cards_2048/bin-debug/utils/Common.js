var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var Common;
(function (Common) {
    var CommonUtils = (function () {
        function CommonUtils() {
        }
        /**
             * 设置点击转发按钮
             */
        CommonUtils.setGlobalShare = function (userId) {
            wx.onShareAppMessage(function () {
                // 用户点击了“转发”按钮
                var shareInfo = CommonUtils.getShareInfo();
                return CommonUtils.getGlobalShareInfo(shareInfo.title, shareInfo.imageUrl);
            });
            wx.showShareMenu(); //自动显示
        };
        /**
         * 获取全局分享数据
         */
        CommonUtils.getGlobalShareInfo = function (title, imageUrl, inviteType, category) {
            // var query = `share=true&userId=${AppConfig.myInfo.sid}&userName=${AppConfig.myInfo.nickname}`;
            // inviteType && (query += `&inviteType=${inviteType}`)
            // category && (query += `&category=${category}`)
            return {
                title: title ? title : '卡牌2048了解一下!!!',
                imageUrl: imageUrl ? imageUrl : 'https://static.xunguanggame.com/card2048/sharePics/cards_logo.png'
            };
        };
        CommonUtils.getShareInfo = function () {
            var shareTexts = this.shareTexts, shareImgs = this.shareImgs;
            var title = shareTexts[Action.shuffle(shareTexts.length)];
            var imageUrl = shareImgs[Action.shuffle(shareImgs.length)];
            return {
                title: title,
                imageUrl: imageUrl
            };
        };
        /**
         * 主动拉起转发
         */
        CommonUtils.showShare = function (title, imageUrl, inviteType, category) {
            return new Promise(function (resolve, reject) {
                var obj = CommonUtils.getGlobalShareInfo(title, imageUrl, inviteType, category);
                obj.success = function (res) {
                    console.log(res);
                    resolve(res);
                };
                obj.fail = function (res) {
                    console.log(res);
                    reject(res);
                };
                wx.shareAppMessage(obj);
            });
        };
        /**
         * 分享的文案
         */
        CommonUtils.shareTexts = [
            '超魔性的卡牌2048了解一下～'
        ];
        /**
         * 分享的图片
         */
        CommonUtils.shareImgs = [
            'https://static.xunguanggame.com/card2048/sharePics/cards_logo.png'
        ];
        return CommonUtils;
    }());
    Common.CommonUtils = CommonUtils;
    __reflect(CommonUtils.prototype, "Common.CommonUtils");
})(Common || (Common = {}));
