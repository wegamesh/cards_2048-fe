var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var Api = (function () {
    function Api() {
    }
    Api.getInstance = function () {
        if (!Api.api) {
            Api.api = new Api();
        }
        return Api.api;
    };
    Api.get = function (url, para) {
        return new Promise(function (resolve, reject) {
            Api.ajax.get("" + Api.httpUrl + url, para).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error(res['Msg']);
                        if (json['Code'] === 1) {
                            reject('tokenError');
                        }
                        else {
                            reject(json['Msg']);
                        }
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error(res['Msg']);
                        if (res['Code'] === 1) {
                            reject('tokenError');
                        }
                        else {
                            reject(res['Msg']);
                        }
                    }
                }
            });
        });
    };
    /**
     * 发起post请求，请求异常reject空
     */
    Api.post = function (url, data) {
        console.log("" + Api.httpUrl + url, data);
        return new Promise(function (resolve, reject) {
            Api.ajax.post("" + Api.httpUrl + url, data).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error('Msg', json['Msg']);
                        reject(json['Msg']);
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error('Msg', res['Msg']);
                        reject(res['Msg']);
                    }
                }
            }).catch(reject);
        });
    };
    /**
     * 客服消息发送
     */
    Api.prototype.kfPost = function (url, data) {
        return new Promise(function (resolve, reject) {
            Api.ajax.post("https://pkwegame.xunguanggame.com" + url, data).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error(json['Msg']);
                        reject(json['Msg']);
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error(res['Msg']);
                        reject(res['Msg']);
                    }
                }
            });
        });
    };
    /**
     * 发送渠道打点
     */
    Api.prototype.sendChannelDot = function (channel) {
        var data = {
            userId: AppConfig.myInfo.sid,
            appId: Api.appId,
            channel: channel
        };
        Api.ajax.dotPost("" + Api.hostDot + '/channel', data).then(function (res) {
            console.log("\u6E20\u9053\u6570\u636E" + data.channel + "\u53D1\u9001\u6210\u529F ," + res);
        }).catch(function (err) {
            console.error("\u6E20\u9053\u6570\u636E" + data.channel + "\u53D1\u9001\u5931\u8D25 ," + err);
        });
    };
    /**
     * 发起post请求，请求异常reject空
     */
    Api.prototype.dotPost = function (url, data) {
        return new Promise(function (resolve, reject) {
            Api.ajax.dotPost("" + Api.httpUrl + url, data).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error('Msg', json['Msg']);
                        reject(json['Msg']);
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error('Msg', res['Msg']);
                        reject(res['Msg']);
                    }
                }
            }).catch(reject);
        });
    };
    /**
     *登陆游戏上报
     **/
    Api.prototype.sendloginChannelDot = function () {
        var option = wx.getLaunchOptionsSync() || {};
        console.log("启动参数 option", option);
        //有scene再上报
        var query = option.query;
        var scene = query.scene;
        scene && this.sendChannelDot(scene);
        // 广告主
        var adInfo = query.weixinadinfo;
        if (adInfo) {
            var attr = adInfo.split('.');
            var adtId = attr[0];
            var gdtVid = query.gdt_vid;
            console.log('weixinadinfo:', adtId, gdtVid);
            this.sendChannelDot(['wxad', adtId, gdtVid].join('_'));
        }
    };
    /**
     *游戏中上报游戏操作
     **/
    Api.prototype.sendGameActionDot = function (action) {
        var myInfo = AppConfig.myInfo;
        var data = {
            userId: myInfo.sid,
            appId: Api.appId,
            action: 'pocket_' + action || "wx"
        };
        //打点
        XG_STATISTICS.send('pocket_' + action || "wx");
        console.log('游戏中上报游戏操作!', data);
        Api.ajax.post("" + Api.hostDot + '/action', data).then(function (res) {
            console.log("\u4E0A\u62A5\u6E38\u620F\u64CD\u4F5C" + data.action + "\u53D1\u9001\u6210\u529F ," + res);
        }).catch(function (err) {
            console.error("\u4E0A\u62A5\u6E38\u620F\u64CD\u4F5C" + data.action + "\u53D1\u9001\u5931\u8D25 ," + err);
        });
    };
    Api.httpUrl = 'https://wxgame.xunguanggame.com/cardgame-go-qa';
    // public static httpUrl: string = 'https://wxgame.xunguanggame.com/cardgame-go'
    Api.hostDot = 'https://pkwegame.xunguanggame.com/platform-go';
    // 发送打点数据的host
    Api.appId = 'wxd809893d56d16080';
    Api.staticHost = 'https://static.xunguanggame.com/pocket';
    Api.ajax = Ajax.getInstance();
    /**
     * 登录
     * [post] code userId
     */
    Api.login = '/login';
    return Api;
}());
__reflect(Api.prototype, "Api");
