var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var lie;
(function (lie) {
    var app; // 存放单例
    /**
     * 层级容器
     */
    var Container = (function (_super) {
        __extends(Container, _super);
        function Container() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * 移除子控件触发
         */
        Container.prototype.$doRemoveChild = function (index, notifyListeners) {
            var target = _super.prototype.$doRemoveChild.call(this, index, notifyListeners);
            if (target) {
                // 当前层级最顶层
                var last = AppViews.getTopComponent(this);
                last && last.onShow();
            }
            return target;
        };
        return Container;
    }(egret.DisplayObjectContainer));
    __reflect(Container.prototype, "Container");
    /**
     * 应用的视图管理类
     */
    var AppViews = (function () {
        function AppViews() {
        }
        /**
         * 初始化
         */
        AppViews.prototype.init = function () {
            var self = this;
            AppViews.stage.removeChildren();
            self.$panelLevel = self.addLevel('panel');
            self.$dialogLevel = self.addLevel('dialog');
            self.$topLevel = self.addLevel('top', true);
            //如果从其他途径登陆,根据业务显示页面(暂时设置延迟0.5秒,后续根据情况再调整)
            setTimeout(function () {
                var datas = AppViews.loginData;
                if (datas) {
                    // if (global.guideInfo.isEnd) {
                    // } else {
                    // 	wx.showToast({
                    // 		title: '请先完成新手引导再赠送食物哦',
                    // 		icon: 'none'
                    // 	})
                    // }
                    self.excuteCall(datas[0], datas[1]);
                    AppViews.loginData = null;
                }
            }, 500);
        };
        Object.defineProperty(AppViews.prototype, "panelLevel", {
            /**
             * 获取面板层
             */
            get: function () {
                return this.$panelLevel;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "dialogLevel", {
            /**
             * 获取对话框层
             */
            get: function () {
                return this.$dialogLevel;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "topLevel", {
            /**
             * 获取顶层
             */
            get: function () {
                return this.$topLevel;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 添加层
         * @param name 名称
         */
        AppViews.prototype.addLevel = function (name, isOld) {
            var container = isOld ? new Container() : new Container();
            container.name = name;
            AppViews.stage.addChild(container);
            return container;
        };
        /**
         * 获取层
         */
        AppViews.prototype.getLevel = function (name) {
            return AppViews.stage.getChildByName(name);
        };
        Object.defineProperty(AppViews.prototype, "curPanel", {
            /**
             * 获取最顶层
             */
            get: function () {
                var panel = this.panelLevel;
                return panel.$children[panel.numChildren - 1];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "curDialog", {
            /**
             * 获取最顶对话框
             */
            get: function () {
                var panel = this.dialogLevel;
                return panel.$children[panel.numChildren - 1];
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 获取视图管理类单例
         */
        AppViews.app = function () {
            if (!app) {
                app = new AppViews;
                app.init();
            }
            return app;
        };
        Object.defineProperty(AppViews, "stage", {
            /**
             * 获取舞台
             */
            get: function () {
                return egret.sys.$TempStage; // egret.MainContext.instance.stage
            },
            enumerable: true,
            configurable: true
        });
        // 重点，层及控制管理体现
        /**
         * 获取当前面板的最顶元素（该面板没有则取下一面板）
         */
        AppViews.getTopComponent = function (cont) {
            if (cont.numChildren == 0) {
                var parent_1 = cont.parent;
                var index = parent_1.getChildIndex(cont);
                cont = null;
                for (var i = index - 1; i >= 0; i--) {
                    cont = parent_1.getChildAt(i);
                    if (cont.numChildren)
                        break;
                }
                if (!cont)
                    return null;
            }
            return cont.$children[cont.numChildren - 1];
        };
        /**
         * 获取最顶的控件——对话框之下
         */
        AppViews.getTopCommont = function () {
            return AppViews.getTopComponent(AppViews.app().dialogLevel);
        };
        Object.defineProperty(AppViews, "curPanel", {
            /**
             * 获取当前面板
             */
            get: function () {
                return app && app.curPanel;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews, "curDialog", {
            /**
             * 获取当前对话框
             */
            get: function () {
                return app && app.curDialog;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 插入一个控件，并居中
         * @param attr AppViews对象的属性名
         * @param clzz 需要添加的对象类
         */
        AppViews.pushChild = function (attr, clzz, para1, para2, para3) {
            var self = AppViews;
            var child = new clzz(para1, para2, para3);
            var container = self.app()[attr];
            var last = self.getTopComponent(container); // 当前层级最顶层
            // 隐藏
            last && last.onHide();
            // 控件动画
            // child.scaleX = 0.6
            // child.scaleY = 0.6
            // child.alpha = 0
            // child.an
            child.anchorOffsetX = child.width / 2;
            child.anchorOffsetY = child.height / 2;
            // var tw = egret.Tween.get(child)
            // tw.to({
            // 	scaleX: 1,
            // 	scaleY: 1,
            // 	alpha: 1
            // }, 300, egret.Ease.backOut).call(() => {
            // 	// if(child.setObscureAlpha){
            // 	// 	child.setObscureAlpha(1)
            // 	// }
            // })
            container.addChild(child);
            self.setInCenter(child);
            return child;
        };
        /**
         * 移除层里的控件
         * @param attr AppViews对象的属性名
         * @param clzz 需要移除的对象类
         */
        AppViews.removeChild = function (attr, clzz) {
            var panel = AppViews.app()[attr];
            // for (let i = 0, num = panel.numChildren; i < num; i++) {
            // 	let child = panel.getChildAt(i);
            // 	if (child instanceof clzz){
            // 		// let tw = egret.Tween.get(child)
            // 		// tw.to({
            // 		// 	scaleX: 0.5,
            // 		// 	scaleY: 0.5,
            // 		// 	alpha: 0
            // 		// },100).call(()=>{
            // 		panel.removeChildAt(i);
            // 		// })
            // 	}
            // }
            var child = panel.$children.find(function (item) {
                return item instanceof clzz;
            });
            if (child) {
                panel.removeChild(child);
            }
            // panel.$children.forEach( item => {
            // 	if(item instanceof clzz){
            // 		panel.removeChild(item)
            // 	}
            // })
        };
        /**
         * 将子控件设置在父控件中心点
         */
        AppViews.setInCenter = function (child) {
            var stage = AppViews.stage;
            child.x = stage.stageWidth / 2;
            child.y = stage.stageHeight / 2;
        };
        /**
         * 新建一个面板并放入
         */
        AppViews.pushPanel = function (clzz, para1, para2) {
            return AppViews.pushChild('panelLevel', clzz, para1, para2);
        };
        /**
         * 新建一个对话框并放入
         */
        AppViews.pushDialog = function (clzz, para1, para2, para3) {
            return AppViews.pushChild('dialogLevel', clzz, para1, para2, para3);
        };
        /**
         * 新建一个对话框并放入
         */
        AppViews.pushTop = function (clzz, para1, para2) {
            return AppViews.pushChild('topLevel', clzz, para1, para2);
        };
        /**
         * 移除面板
         */
        AppViews.removePanel = function (clzz) {
            AppViews.removeChild('panelLevel', clzz);
        };
        /**
         * 移除对话框
         */
        AppViews.removeDialog = function (clzz) {
            AppViews.removeChild('dialogLevel', clzz);
        };
        AppViews.prototype.excuteCall = function (call, thisObj) {
            call && call.call(thisObj);
        };
        /**
         * 添加初始化后回调，如果已经初始化，则立即执行
         */
        AppViews.addInitCall = function (call, thisObj) {
            if (app) {
                AppViews.removePanelAndDialog().then(function (res) {
                    app.excuteCall(call, thisObj);
                });
            }
            else {
                AppViews.loginData = [call, thisObj];
            }
        };
        /**
         * 进入游戏移除除主界面外的其他页面
         */
        AppViews.removePanelAndDialog = function () {
            return new Promise(function (resolve, reject) {
                var panelNum = AppViews.app().panelLevel.numChildren;
                var dialogNum = AppViews.app().dialogLevel.numChildren;
                //移除所有dialog
                for (var i = 0; i < dialogNum; i++) {
                    var dialog = AppViews.app().dialogLevel.getChildAt(i);
                    AppViews.app().dialogLevel.removeChild(dialog);
                }
                //移除除主界面以外的panel,从1开始
                if (panelNum > 1) {
                    for (var i = 1; i < panelNum; i++) {
                        var panel = AppViews.app().panelLevel.getChildAt(i);
                        AppViews.app().panelLevel.removeChild(panel);
                    }
                }
                console.log('remove all child');
                resolve();
            });
        };
        /**
         * 界面上是否拥有该类
         */
        AppViews.hasView = function (clzz, cont) {
            for (var i = 0, num = cont.numChildren; i < num; i++) {
                var child = cont.getChildAt(i);
                if (child instanceof clzz)
                    return true;
            }
            return false;
        };
        return AppViews;
    }());
    lie.AppViews = AppViews;
    __reflect(AppViews.prototype, "lie.AppViews");
})(lie || (lie = {}));
