var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var lie;
(function (lie) {
    /**
     * 游戏生命周期管理类
     * 注：调试模式不要开启
     */
    var LifeCycle = (function () {
        function LifeCycle() {
        }
        /**
         * 游戏生命周期初始化
         */
        LifeCycle.init = function () {
            var self = LifeCycle;
            var lifecycle = egret.lifecycle;
            lifecycle.addLifecycleListener(function (context) {
                context.onUpdate = self.update;
            });
            lifecycle.onPause = self.pause;
            lifecycle.onResume = self.resume;
        };
        /**
         * 全局游戏更新函数
         */
        LifeCycle.update = function () {
        };
        /**
         * 全局游戏暂停函数
         */
        LifeCycle.pause = function () {
            egret.ticker.pause();
        };
        /**
         * 全局游戏恢复函数
         */
        LifeCycle.resume = function () {
            egret.ticker.resume();
        };
        return LifeCycle;
    }());
    lie.LifeCycle = LifeCycle;
    __reflect(LifeCycle.prototype, "lie.LifeCycle");
})(lie || (lie = {}));
