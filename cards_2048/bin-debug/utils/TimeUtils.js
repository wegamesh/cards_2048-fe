var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var lie;
(function (lie) {
    /**
     * 时间工具类
     */
    var TimeUtils = (function () {
        function TimeUtils() {
        }
        /**
         * 获取天数
         * @param second
         */
        TimeUtils.getDay = function (second) {
            return (second + 8 * 3600) / 86400 | 0;
        };
        /**
         * 检测两个秒数是否同一天
         */
        TimeUtils.isSameDay = function (second0, second1) {
            var get = TimeUtils.getDay;
            return get(second0) == get(second1);
        };
        /**
         * 检测日期是否是同一天
         */
        TimeUtils.isSameDayDate = function (date0, date1) {
            var g = function (date) {
                return Math.floor(date.getTime() / 1000);
            };
            return TimeUtils.isSameDay(g(date0), g(date1));
        };
        return TimeUtils;
    }());
    lie.TimeUtils = TimeUtils;
    __reflect(TimeUtils.prototype, "lie.TimeUtils");
})(lie || (lie = {}));
