var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var lie;
(function (lie) {
    /**
     * 工具类，存放一些常用的且不好归类的方法
     */
    var Utils = (function () {
        function Utils() {
        }
        // 工具私有变量，请勿乱动
        /**
         * 初始化一个长度为length，值全为value的数组
         * 注意，由于数组公用一个value，因此，value适用于基础类型，对于对象类的，请用memset2方法
         */
        Utils.memset = function (length, value) {
            return Array.apply(null, Array(length)).map(function () { return value; });
        };
        /**
         * 初始化一个长度为length，值通过getValue函数获取，注意getValue第一个参数是没用，第二个参数是当前数组的下标，即你返回的数据将存放在数组的该下标
         */
        Utils.memset2 = function (length, getValue) {
            return Array.apply(null, Array(length)).map(getValue);
        };
        /**
         * 创建一个定时器
         * @param listener 定时回调
         * @param thisObject 回调所属对象
         * @param second 回调间隔，单位秒，默认一秒
         * @param repeat 循环次数，默认一直重复
         */
        Utils.createTimer = function (listener, thisObject, second, repeat) {
            if (second === void 0) { second = 1; }
            var timer = new egret.Timer(second * 1000, repeat);
            timer.addEventListener(egret.TimerEvent.TIMER, listener, thisObject);
            timer.start();
            return timer;
        };
        /**
         * 移除一个定时器
         * @param timer 被移除的定时器
         * @param listener 监听函数
         * @param thisObject 函数所属对象
         */
        Utils.removeTimer = function (timer, listener, thisObject) {
            timer.stop();
            timer.removeEventListener(egret.TimerEvent.TIMER, listener, thisObject);
        };
        /**
         * 获取网页参数
         */
        Utils.getQueryString = function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var ret = window.location.search.substr(1).match(reg);
            return ret ? decodeURIComponent(ret[2]) : '';
        };
        /**
         * 获取cr1位于cr0的方向
         * 注：0为上，顺时针开始数，共八个方向
         */
        Utils.getDistance = function (col0, row0, col1, row1) {
            var row, col, c = 2, xs = 1;
            if (row0 < row1)
                row = -1;
            else if (row0 > row1)
                row = 1;
            else
                row = 0;
            if (col0 > col1) {
                c = 6;
                xs = -1;
            }
            else if (col0 == col1)
                xs = 2;
            return c - row * xs;
        };
        /**
         * 自定义格式化字符串
         * @param reg 正则
         * @param str 字符串
         * @param args 填充参数
         */
        Utils.formatStringReg = function (reg, str, args) {
            for (var i in args) {
                var arg = args[i];
                if (reg.test(str))
                    str = str.replace(reg, args[i]);
                else
                    break;
            }
            return str;
        };
        /**
         * 格式化字符串
         */
        Utils.formatString = function (str) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            str = str.replace(/%%/g, '%'); // 有些识别问题出现两个%号
            return Utils.formatStringReg(/%d|%s/i, str, args); // 忽略大小写
        };
        /**
         * 偏移整型数组
         * @param array 需要偏移的数组
         * @param offset 偏移量，对应array长度，没有值时默认0
         */
        Utils.offsetArray = function (array) {
            var offset = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                offset[_i - 1] = arguments[_i];
            }
            for (var i in array)
                array[i] += (offset[i] || 0);
            return array;
        };
        /**
         *为组件添加朦层
         *
        */
        Utils.setObscure = function (width, height) {
            var rect = new eui.Rect;
            rect.name = 'obscure';
            rect.x = 0;
            rect.y = 0;
            rect.width = width;
            rect.height = height - 3;
            rect.fillColor = 0x000000;
            rect.alpha = 0.3;
            rect.strokeAlpha = 0;
            rect.ellipseHeight = 44;
            rect.ellipseWidth = 44;
            return rect;
        };
        /**
         * 添加矩形遮罩
         */
        Utils.addRectMask = function (bitmap) {
            var shape = new egret.Shape;
            var graphics = shape.graphics;
            shape.x = bitmap.x + 2;
            shape.y = bitmap.y + 2;
            bitmap.parent.addChild(shape);
            graphics.beginFill(0);
            graphics.drawRoundRect(0, 0, bitmap.width - 4, bitmap.height - 4, 10);
            graphics.endFill();
            bitmap.mask = shape;
        };
        /**
         * 设置纹理的九宫格
         */
        Utils.setScale9 = function (b, x, y, w, h) {
            var s = egret.Rectangle.create();
            s.x = x;
            s.y = y;
            s.width = w;
            s.height = h;
            b.scale9Grid = s;
        };
        /**
         * 二次延迟
         */
        Utils.callLater = function (call, thisObj) {
            var params = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                params[_i - 2] = arguments[_i];
            }
            var later = egret.callLater;
            later(function () {
                later(function () {
                    call.apply(thisObj, params);
                }, null);
            }, null);
        };
        /**
         * 检测点是否在矩形上
         */
        Utils.pointInRect = function (x, y, rect) {
            x -= rect.x;
            y -= rect.y;
            return x >= 0 && x <= rect.width && y >= 0 && y <= rect.height;
        };
        /**
         * 控件是否显示
         */
        Utils.isVisible = function (display) {
            var stage = display.stage;
            var visible;
            do {
                visible = display.visible;
                display = display.parent;
            } while (visible && (visible = !!display) && display != stage);
            return visible;
        };
        /**
         * 移除List的数据
         * @param list
         * @param isCache 是否是缓存数据，是的话不清除
         */
        Utils.removeListData = function (list, isCache) {
            var datas = list.dataProvider;
            datas && !isCache && datas.removeAll();
            list.dataProvider = null;
        };
        /**
         * 更新列表数据
         * @param list
         * @param datas 新数据源
         */
        Utils.updateListData = function (list, datas) {
            var array = list.dataProvider;
            if (array)
                array.replaceAll(datas);
            else
                list.dataProvider = new eui.ArrayCollection(datas);
        };
        /**
         * 添加滤镜
         */
        Utils.setColorMatrixFilter = function (image, colorMatrix) {
            var colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
            if (image.filters && image.filters.length) {
                image.filters[0] = colorFlilter;
            }
            else {
                image.filters = [colorFlilter];
            }
        };
        /**
         * 复制一个对象
         * @param obj 需要复制的对象
         * @param copy 被复制对象，不存在则创建
         * @returns 返回copy
         */
        Utils.copyObj = function (obj, copy) {
            if (copy === void 0) { copy = Object.create(null); }
            for (var i in obj) {
                copy[i] = obj[i];
            }
            return copy;
        };
        return Utils;
    }());
    lie.Utils = Utils;
    __reflect(Utils.prototype, "lie.Utils");
})(lie || (lie = {}));
