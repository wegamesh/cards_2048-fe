var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var lie;
(function (lie) {
    var inInit = false;
    var flag = RES.FEATURE_FLAG;
    /**
     * 重写
     */
    var init = function () {
        if (!inInit) {
            inInit = true;
            flag.FIX_DUPLICATE_LOAD = 0; // 允许重复加载，针对合图、位图等
            // 重写，注意不需要该文件请不要加入工程
            var prototype = RES.ResourceLoader.prototype;
            var next = prototype.next;
            // 返回进度
            var curProgress_1 = 0;
            var onProgress_1 = function (reporter, current, total) {
                var onProgress = reporter && reporter.onProgress;
                if (onProgress) {
                    onProgress(current + curProgress_1, total + curProgress_1);
                }
            };
            prototype.next = function () {
                var self = this;
                // 获取错误次数
                var getECount = function (groupName) {
                    var array = self.loadItemErrorDic[groupName];
                    return array ? array.length : 0;
                };
                var _loop_1 = function () {
                    var r = self.getOneResourceInfo();
                    if (!r)
                        return "break";
                    self.loadingCount++;
                    self.loadResource(r)
                        .then(function (response) {
                        self.loadingCount--;
                        RES.host.save(r, response);
                        var groupName = r.groupNames.shift();
                        if (r.groupNames.length == 0) {
                            r.groupNames = undefined;
                        }
                        var reporter = self.reporterDic[groupName];
                        self.numLoadedDic[groupName]++;
                        var current = self.numLoadedDic[groupName];
                        var total = self.groupTotalDic[groupName];
                        var success = current - getECount(groupName);
                        onProgress_1(reporter, success, total);
                        if (current == total) {
                            var groupError = self.groupErrorDic[groupName];
                            self.removeGroupName(groupName);
                            delete self.groupTotalDic[groupName];
                            delete self.numLoadedDic[groupName];
                            delete self.itemListDic[groupName];
                            delete self.groupErrorDic[groupName];
                            var dispatcher = self.dispatcherDic[groupName];
                            if (groupError) {
                                var itemList = self.loadItemErrorDic[groupName];
                                delete self.loadItemErrorDic[groupName];
                                var error = self.errorDic[groupName];
                                delete self.errorDic[groupName];
                                dispatcher.dispatchEventWith("error", false, { itemList: itemList, error: error });
                            }
                            else {
                                dispatcher.dispatchEventWith("complete");
                            }
                            curProgress_1 += success;
                        }
                        self.next();
                    }).catch(function (error) {
                        if (!error.__resource_manager_error__) {
                            // throw error;
                            console.error('资源重大bug，请维护：', error);
                        }
                        self.loadingCount--;
                        delete RES.host.state[r.root + r.name];
                        var times = self.retryTimesDic[r.name] || 1;
                        if (times > self.maxRetryTimes) {
                            delete self.retryTimesDic[r.name];
                            var groupName = r.groupNames.shift();
                            if (r.groupNames.length == 0) {
                                delete r.groupNames;
                            }
                            if (!self.loadItemErrorDic[groupName]) {
                                self.loadItemErrorDic[groupName] = [];
                            }
                            if (self.loadItemErrorDic[groupName].indexOf(r) == -1) {
                                self.loadItemErrorDic[groupName].push(r);
                            }
                            self.groupErrorDic[groupName] = true;
                            var reporter = self.reporterDic[groupName];
                            self.numLoadedDic[groupName]++;
                            var current = self.numLoadedDic[groupName];
                            var total = self.groupTotalDic[groupName];
                            var success = current - getECount(groupName);
                            onProgress_1(reporter, success, total);
                            if (current == total) {
                                var groupError = self.groupErrorDic[groupName];
                                self.removeGroupName(groupName);
                                delete self.groupTotalDic[groupName];
                                delete self.numLoadedDic[groupName];
                                delete self.itemListDic[groupName];
                                delete self.groupErrorDic[groupName];
                                var itemList = self.loadItemErrorDic[groupName];
                                delete self.loadItemErrorDic[groupName];
                                var dispatcher = self.dispatcherDic[groupName];
                                dispatcher.dispatchEventWith("error", false, { itemList: itemList, error: error });
                                curProgress_1 += success;
                            }
                            else {
                                self.errorDic[groupName] = error;
                            }
                            self.next();
                        }
                        else {
                            self.retryTimesDic[r.name] = times + 1;
                            self.failedList.push(r);
                            self.next();
                            return;
                        }
                    });
                };
                while (self.loadingCount < self.thread) {
                    var state_1 = _loop_1();
                    if (state_1 === "break")
                        break;
                }
            };
        }
    };
    /**
     * 游戏开始页面
     */
    var LoadingView = (function (_super) {
        __extends(LoadingView, _super);
        /**
         * @param groupName 预加载组名
         * @param bgUrl 背景图，若存在则会先加入loading
         */
        function LoadingView(groupName) {
            var _this = _super.call(this) || this;
            _this.eGroupName = '$errorGroup'; // 重加载的错误组名，可重写
            _this.eTimeOut = 5000; // 超时检测时间
            _this.$groupName = groupName;
            init();
            return _this;
        }
        /**
         * 开始加载
         */
        LoadingView.prototype.onLoadStart = function () {
            var self = this;
            if (!self.isLoading) {
                var event_1 = RES.ResourceEvent.ITEM_LOAD_ERROR;
                var gName_1 = self.$groupName;
                var remove_1 = function () {
                    self.isLoading = false;
                    gName_1 == self.eGroupName &&
                        delete RES.config.config.groups[gName_1]; // 删除组名
                    RES.removeEventListener(event_1, self.onItemError, self);
                };
                self.isLoading = true;
                self.$errorKeys = [];
                RES.loadGroup(gName_1, 0, self).then(function () {
                    remove_1();
                    flag.FIX_DUPLICATE_LOAD = 1;
                    self.onSuccess();
                }).catch(function () {
                    remove_1();
                    self.onReload();
                });
                RES.addEventListener(event_1, self.onItemError, self);
            }
        };
        /**
         * 加载错误的key值
         */
        LoadingView.prototype.onItemError = function (event) {
            this.$errorKeys.push(event.resItem.name);
        };
        /**
         * 移除监听
         */
        LoadingView.prototype.$removeListener = function () {
            var self = this;
            egret.clearTimeout(self.$timeout);
            lie.NetworkUtils.removeNetworkStatusChange(self.onStatusChange, self);
        };
        /**
         * 重新加载判断
         */
        LoadingView.prototype.onReload = function () {
            var self = this;
            var utils = lie.NetworkUtils;
            utils.getNetworkType().then(function (type) {
                var eGroupName = self.eGroupName;
                var createGroup = eGroupName && RES.createGroup(eGroupName, self.$errorKeys);
                // 创建新组成功
                if (createGroup) {
                    // 修改错误组
                    self.$groupName = eGroupName;
                    // 当前网络状态已是最佳状态
                    if (utils.isBestNetworkType(type))
                        self.onError(1);
                    else {
                        self.$errorNType = type;
                        // 超时检测
                        self.$timeout = egret.setTimeout(self.onError, self, self.eTimeOut, 0);
                        // 状态变化
                        utils.addNetworkStatusChange(self.onStatusChange, self);
                    }
                }
                else {
                    self.onError(-1);
                }
            });
        };
        /**
         * 监听网络状态变化回调
         */
        LoadingView.prototype.onStatusChange = function (type) {
            var self = this;
            // 网络状态升高，自动重新加载
            if (lie.NetworkUtils.networkUpgrade(self.$errorNType, type)) {
                self.$removeListener();
                self.onLoadStart();
            }
        };
        /////////////以下方法供子类修改，记得加上super，上面的方法看不懂则请勿乱改
        /**
         * 进度更新
         */
        LoadingView.prototype.onProgress = function (current, total) {
            // 加载结束
            if (current == total) {
                var self_1 = this;
                // 进度条卡100%情况处理
                egret.setTimeout(function () {
                    !self_1.$isFinish && self_1.onSuccess();
                }, null, 1000);
            }
        };
        /**
         * 加载结束
         */
        LoadingView.prototype.onSuccess = function () {
            this.$isFinish = true;
            this.$removeListener();
        };
        /**
         * 加载错误
         * @param errorCode 错误编码：-1组名错误，请前端同志修改eGroupName，0正常错误，1资源路径错误
         */
        LoadingView.prototype.onError = function (errorCode) {
            this.$removeListener();
        };
        return LoadingView;
    }(egret.DisplayObjectContainer));
    lie.LoadingView = LoadingView;
    __reflect(LoadingView.prototype, "lie.LoadingView");
})(lie || (lie = {}));
