var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var lie;
(function (lie) {
    /**
     * 影片剪辑，实现可于皮肤创建
     */
    var MovieClip = (function (_super) {
        __extends(MovieClip, _super);
        function MovieClip() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MovieClip.prototype.childrenCreated = function () {
            var self = this;
            var getr = RES.getResAsync;
            // 这里就不检测资源的正确性了，调用的时候注意即可
            getr(self.image, function (texture) {
                if (!self.isDestroy) {
                    getr(self.frame, function () {
                        if (!self.isDestroy)
                            self.initMovieClip();
                    }, null);
                }
            }, null);
        };
        MovieClip.prototype.onDestroy = function () {
            var self = this;
            var clip = self.$movieClip;
            self.clearMovieCall();
            self.$factory = null;
            // 移除动画
            if (clip) {
                clip.stop();
                self.$movieClip = null;
            }
        };
        /**
         * 初始化数据
         */
        MovieClip.prototype.initMovieClip = function () {
            var self = this;
            var getr = RES.getRes;
            self.$factory = new egret.MovieClipDataFactory(getr(self.frame), getr(self.image));
            self.addChild(self.$movieClip = new egret.MovieClip);
            self.carryMovieCall();
        };
        /**
         * 执行回调
         */
        MovieClip.prototype.carryMovieCall = function () {
            var self = this;
            var call = self.m_pClipCall;
            if (call) {
                var clip = self.$movieClip;
                // 初始化数据
                clip.movieClipData = self.$factory.generateMovieClipData(self.action);
                call.call(self.m_pThisObj, clip);
                self.clearMovieCall(); // 执行完销毁
            }
        };
        /**
         * 清空回调
         */
        MovieClip.prototype.clearMovieCall = function () {
            var self = this;
            self.m_pClipCall = self.m_pThisObj = null;
        };
        Object.defineProperty(MovieClip.prototype, "res", {
            /**
             * 若命名一致，可取其前缀
             */
            set: function (value) {
                this.image = value + '_png';
                this.frame = value + '_json';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "image", {
            /**
             * 获取帧动画合图资源名
             */
            get: function () {
                return this.$image;
            },
            /**
             * 设置帧动画合图资源名
             */
            set: function (value) {
                this.$image = String(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "frame", {
            /**
             * 获取帧动画数据资源名
             */
            get: function () {
                return this.$frame;
            },
            /**
             * 设置帧动画数据资源名
             */
            set: function (value) {
                this.$frame = String(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "action", {
            /**
             * 获取当前动画名称
             */
            get: function () {
                return this.$action;
            },
            /**
             * 设置当前动画名称
             */
            set: function (value) {
                this.$action = String(value);
            },
            enumerable: true,
            configurable: true
        });
        // 供外部使用的方法
        /**
         * 只能通过该方法来获取MovieClip对象
         */
        MovieClip.prototype.addMovieCall = function (call, thisObj) {
            var self = this;
            self.m_pClipCall = call;
            self.m_pThisObj = thisObj;
            self.$factory && self.carryMovieCall(); // 存在则立马执行
        };
        /**
         * 播放
         */
        MovieClip.prototype.play = function (playTimes, cb, thisObj) {
            this.addMovieCall(function (clip) {
                clip.gotoAndPlay(0, playTimes);
                cb && clip.once(egret.Event.COMPLETE, cb, thisObj);
            }, null);
        };
        /**
         * 暂停
         */
        MovieClip.prototype.stop = function (cb, thisObj) {
            this.addMovieCall(function (clip) {
                clip.stop();
            }, null);
        };
        return MovieClip;
    }(lie.UIComponent));
    lie.MovieClip = MovieClip;
    __reflect(MovieClip.prototype, "lie.MovieClip");
})(lie || (lie = {}));
