var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var lie;
(function (lie) {
    var single;
    /**
     * 对话框，注意onDestroy加了点东西
     */
    var Dialog = (function (_super) {
        __extends(Dialog, _super);
        function Dialog() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Dialog.prototype.onCreate = function () {
            this.initObscure();
        };
        Dialog.prototype.$onRemoveFromStage = function () {
            _super.prototype.$onRemoveFromStage.call(this);
            var self = this;
            var call = self.m_pCall;
            if (call) {
                call(self.m_pThis);
                self.m_pCall = self.m_pThis = null;
            }
        };
        /**
         * 初始化朦层
         */
        Dialog.prototype.initObscure = function () {
            var _this = this;
            var self = this;
            var stage = self.stage;
            var rect = self.obscure = new eui.Rect;
            rect.width = stage.stageWidth;
            rect.height = stage.stageHeight;
            rect.horizontalCenter = rect.verticalCenter = 0;
            // rect.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
            // 	this.removeFromParent()
            // }, this)
            self.addChildAt(rect, 0);
            self.setObscureAlpha(0.5);
            rect.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
                _this.removeFromParent();
            }, this);
        };
        /**
         * 设置朦层的透明度
         */
        Dialog.prototype.setObscureAlpha = function (alpha) {
            this.obscure.alpha = alpha;
        };
        /**
         * 添加朦层监听，点击关闭
         */
        Dialog.prototype.addOCloseEvent = function () {
            var self = this;
            lie.EventUtils.addTouchTapListener(self.obscure, self.onClose, self);
        };
        /**
         * 添加关闭回调
         */
        Dialog.prototype.addCloseCall = function (call, thisObj) {
            var self = this;
            self.m_pCall = call;
            self.m_pThis = thisObj;
        };
        /**
         * 关闭窗口
         */
        Dialog.prototype.onClose = function (event) {
            event.stopImmediatePropagation();
            this.removeFromParent();
        };
        /**
         * 显示单一的对话框，对话框样式固定
         * @param 内容
         * @param 点击确定的回调
         * @param 回调所属对象
         */
        Dialog.showSingleDialog = function (content, call, thisObj) {
            if (!single) {
                single = lie.AppViews.pushDialog(SignleDialog);
                single.skinName = AppConfig.getSkin('SingleDialogSkin');
            }
            single.setText(content);
            single.addCall(call, thisObj);
            return single;
        };
        /**
         * 隐藏单一对话框
         */
        Dialog.hideSingleDialog = function () {
            if (single) {
                single.visible = false;
                single.clearCall();
            }
        };
        /**
         * 移除单一对话框
         */
        Dialog.removeSingDialog = function () {
            var r = single;
            if (r) {
                single = null;
                r.removeFromParent();
            }
        };
        /**
         * 运行循环
         */
        Dialog.runTurn = function () {
            var self = Dialog;
            var clzz = self.$turnDialogs.shift();
            if (clzz) {
                var param = self.$turnParams.shift();
                lie.AppViews.pushDialog(clzz, param).
                    addCloseCall(self.runTurn);
            }
            else
                self.$isShowTurn = false;
        };
        /**
         * 轮流显示对话框
         * @param dialogs 需要现实的对话框类
         * @param params 对话框类构造函数参数，注意长度一致，注意每隔类的参数只支持一个，多个参数的请修改该类
         */
        Dialog.showDialogsInTurn = function (dialogs, params) {
            var length = dialogs.length;
            if (length) {
                var self_1 = Dialog;
                var turns = self_1.$turnDialogs;
                if (params == void 0)
                    params = lie.Utils.memset(length, void 0);
                else
                    params.length = length;
                self_1.$turnDialogs = turns.concat(dialogs);
                self_1.$turnParams = self_1.$turnParams.concat(params);
                if (!self_1.$isShowTurn) {
                    self_1.$isShowTurn = true;
                    self_1.runTurn();
                }
            }
        };
        /**
         * 放对话框进入轮流
         * @param dialog
         * @param param 构造参数，仅支持一个
         * @param endDialog 将dialog放在endDialog的后面，不存在则默认最后
         */
        Dialog.pushDialogInTurn = function (dialog, param, endDialog) {
            var self = Dialog;
            var turns = self.$turnDialogs;
            var index = turns.length;
            if (endDialog) {
                for (var i = 0, len = turns.length; i < len; i++) {
                    if (endDialog == turns[i]) {
                        index = i;
                        break;
                    }
                }
            }
            turns.splice(index, 0, dialog);
            self.$turnParams.splice(index, 0, param);
        };
        /**
         * 在固定位置插入对话框
         * @param dialog
         * @param param
         * @param index 下标
         */
        Dialog.insertDialogInTuen = function (dialog, param, index) {
            var self = Dialog;
            var turns = self.$turnDialogs;
            if (isNaN(index))
                index = turns.length;
            self.$turnDialogs.splice(index, 0, dialog);
            self.$turnParams.splice(index, 0, param);
            !self.$isShowTurn && self.runTurn();
        };
        //// 轮流对话框
        Dialog.$turnDialogs = [];
        Dialog.$turnParams = [];
        return Dialog;
    }(lie.AppComponent));
    lie.Dialog = Dialog;
    __reflect(Dialog.prototype, "lie.Dialog");
    /**
     * 只允许弹出一个的对话框，也可以称为通用对话框
     */
    var SignleDialog = (function (_super) {
        __extends(SignleDialog, _super);
        function SignleDialog() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        SignleDialog.prototype.onCreate = function () {
            _super.prototype.onCreate.call(this);
            var self = this;
            lie.EventUtils.addTouchTapScaleListener(self.m_btnOk, self.removeFromParent, self);
        };
        SignleDialog.prototype.onDestroy = function () {
            _super.prototype.onDestroy.call(this);
            var self = this;
            lie.EventUtils.removeEventListener(self.m_btnOk);
            // 执行回调
            var call = self.$call;
            if (call) {
                call.call(self.$thisObje);
                self.clearCall();
            }
        };
        /**
         * 设置文本
         */
        SignleDialog.prototype.setText = function (text) {
            this.m_lblText.text = text;
        };
        /**
         * 添加监听
         */
        SignleDialog.prototype.addCall = function (call, thisObje) {
            var self = this;
            self.$call = call;
            self.$thisObje = thisObje;
        };
        /**
         * 清除回调
         */
        SignleDialog.prototype.clearCall = function () {
            var self = this;
            self.$call = self.$thisObje = null;
        };
        return SignleDialog;
    }(Dialog));
    __reflect(SignleDialog.prototype, "SignleDialog");
})(lie || (lie = {}));
