var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var lie;
(function (lie) {
    var $canvas; // 不存在的变量别用var
    /**
     * 微信平台工具类——微信工具除登录和获取用户信息
     */
    var WXUtils = (function () {
        function WXUtils() {
            this.type = 'wxgame';
        }
        /**
         * 游戏进入初始化——调用一次即可
         */
        WXUtils.prototype.init = function () {
            console.log('初始化');
            var self = this;
            // 分享携带ShareTicket
            wx.updateShareMenu({
                withShareTicket: true
            });
            wx.onShow(self.onShow);
            wx.onHide(self.onHide);
            // 分享
            wx.onShareAppMessage(function () {
                // 用户点击了“转发”按钮
                return self.getGlobalShareInfo();
            });
            wx.showShareMenu(); // 自动显示
            ; // 不需要任何操作
            lie.PlatformUtils.login();
        };
        /**
         * 微信onShow
         */
        WXUtils.prototype.onShow = function (res) {
            // 分享打开的链接
            var isShare = res.query.share == 'true';
            var type = res.query.type || 0;
            var ticket = isShare ? (res.shareTicket || '1') : void 0;
            //AppMsg.notice(MsgId.onShow, ticket);
        };
        /**
         * 显示广告
         */
        WXUtils.prototype.showAds = function (type) {
            if (type === void 0) { type = 0; }
            var self = this;
            // let clzz = UserData.gameInfo;
            return new Promise(function (resolve, reject) {
                // reject()
                if (!self.isShowAds) {
                    self.isShowAds = true;
                    // let turn = !clzz.Location.IsCore;
                    var sdk_1 = self.getSystemInfoSync().SDKVersion;
                    // 转换回调
                    var turnCall_1 = function (str) {
                        self.isShowAds = false;
                        // if (turn)
                        // 	reject(str);
                        // else {
                        self.showToast(str);
                        resolve(false);
                        // }
                    };
                    // 字符串
                    if (sdk_1 >= '2.0.4') {
                        var videoAd_1 = wx.createRewardedVideoAd({
                            adUnitId: adUnitIds[type]
                        });
                        videoAd_1.load().then(function () {
                            wx.hideLoading();
                            // 监听回调
                            var call = videoAd_1.onClose(function (res) {
                                self.isShowAds = false;
                                videoAd_1.offClose(call);
                                console.log('sdk: ', sdk_1);
                                // if (clzz.Location && clzz.Location.IsCore && sdk >= '2.1.0' && res) {
                                // 	console.log('isEnded', res.isEnded)
                                // 	if (res.isEnded) {
                                // 		resolve(true);
                                // 	} else {
                                // 		self.showToast('广告未看完无法获得奖励');
                                // 		resolve(false);
                                // 	}
                                // } else {
                                if (res.isEnded) {
                                    resolve(true);
                                }
                                // }
                            });
                            videoAd_1.show();
                        }).catch(function (err) {
                            turnCall_1('广告播放异常');
                        });
                    }
                    else {
                        turnCall_1('该微信版本暂不支持广告播放，请升级！');
                    }
                }
            });
        };
        /**
         * 显示banner广告
         */
        WXUtils.prototype.showBannerAds = function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                var info = self.getSystemInfoSync();
                var sdk = info.SDKVersion;
                // 字符串
                if (sdk >= '2.0.4') {
                    var stage = lie.AppViews.stage;
                    var sWidth = stage.stageWidth;
                    var floor = Math.floor;
                    var width = 750, height = 250;
                    var x = (sWidth - width) / 2;
                    var y = stage.stageHeight - height;
                    var scaleWidth = info.screenWidth / sWidth;
                    // let style = {
                    // 	left: floor(x * scaleWidth),
                    // 	top: floor(y * scaleWidth)
                    // 	// height: floor(height * scaleWidth)
                    // };
                    var style = {
                        left: 0,
                        top: floor(y * scaleWidth),
                        width: floor(width * scaleWidth),
                        height: floor(height * scaleWidth)
                    };
                    var bannerAd_1 = wx.createBannerAd({
                        adUnitId: adUnitIds[1],
                        style: style
                    });
                    bannerAd_1.onResize && bannerAd_1.onResize(function (e) {
                        bannerAd_1.style && (bannerAd_1.style.top = wx.getSystemInfoSync().windowHeight - e.height);
                    });
                    bannerAd_1.show();
                    self.bannerAd = bannerAd_1;
                }
            });
        };
        /**
         * 销毁banner广告
         */
        WXUtils.prototype.hideBannerAds = function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                var sdk = self.getSystemInfoSync().SDKVersion;
                var show = lie.Dialog.showSingleDialog;
                // 字符串
                if (sdk >= '2.0.4') {
                    self.bannerAd.destroy();
                }
            });
        };
        /**
         * 微信onHide
         */
        WXUtils.prototype.onHide = function (res) {
            //AppMsg.notice(MsgId.onHide);
        };
        /**
         * 获取开放域
         */
        WXUtils.prototype.getShareCanvas = function () {
            var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
            var texture = new egret.Texture();
            bitmapdata.$deleteSource = false;
            texture.bitmapData = bitmapdata;
            return new egret.Bitmap(texture);
        };
        /**
         * 获取全局分享数据
         */
        WXUtils.prototype.getGlobalShareInfo = function () {
            return {
                title: '测试',
                imageUrl: 'resource/assets/share/wx_share_join.png',
                query: 'share=true'
            };
        };
        /**
         * 系统信息
         */
        WXUtils.prototype.getSystemInfoSync = function () {
            return wx.getSystemInfoSync();
        };
        /**
         * 直接弹出分享
         */
        WXUtils.prototype.showShare = function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                var obj = self.getGlobalShareInfo();
                obj.success = function (res) {
                    resolve(true);
                };
                obj.fail = function (res) {
                    resolve(false);
                };
                wx.shareAppMessage(obj);
            });
        };
        /**
         * app文字提示
         */
        WXUtils.prototype.showToast = function (msg) {
            wx.showToast({
                title: msg,
                icon: 'none'
            });
        };
        /**
         * 获取系统描述
         */
        WXUtils.prototype.getSystemInfo = function () {
            return wx.getSystemInfoSync().system;
        };
        /**
         * 设置用户数据
         */
        WXUtils.prototype.setUserInfo = function (map) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, new Promise(function (resolve, reject) {
                            //let clzz = AppConfig;
                            var clzz;
                            var datas = [];
                            var setCS = wx.setUserCloudStorage;
                            var isObj = function (v) {
                                return typeof v === 'object';
                            };
                            for (var i in map) {
                                var v = map[i];
                                if (isObj(v))
                                    v = JSON.parse(v);
                                datas.push({ key: i, value: v + '' });
                            }
                            // 低版本不支持
                            setCS ? setCS({
                                KVDataList: datas,
                                success: function () {
                                    resolve(true);
                                },
                                fail: function () {
                                    resolve(false);
                                }
                            }) : reject();
                        })];
                });
            });
        };
        /**
         * 由主域向开放域推送消息
         * @param action 消息标志
         * @param data 消息参数
         */
        WXUtils.prototype.postMessage = function (action, data) {
            var msg = {};
            msg.action = action;
            msg.data = data;
            // console.log("抛消息");
            console.log("发送消息");
            console.log(msg);
            wx.postMessage(msg);
        };
        /**
         * 获取玩家信息
         */
        WXUtils.prototype.getUserInfo = function () {
            return new Promise(function (resolve) {
                // let clzz = AppConfig;
                // let info = clzz.userInfo;
                // info ? resolve(info) :
                // 	platform.getUserInfo().then(function (res) {
                // 		clzz.userInfo = res;
                // 		resolve(res);
                // 	});
            });
        };
        /**
         * loading
         */
        WXUtils.prototype.showLoading = function (title, mask) {
            if (mask === void 0) { mask = false; }
            wx.showLoading({
                title: title || '',
                mask: mask
            });
        };
        WXUtils.prototype.hideLoading = function () {
            wx.hideLoading();
        };
        /**
         * 振动
         * 返回值 。1/2/3  成功/失败/结束
         */
        WXUtils.prototype.vibrateShort = function () {
            return new Promise(function (resolve, reject) {
                wx.vibrateShort({
                    success: function () {
                        resolve(1);
                    },
                    fail: function () {
                        resolve(2);
                    },
                    complete: function () {
                        resolve(3);
                    }
                });
            });
        };
        /**
         * 小程序跳转
         * @param appid
         * @param path
         * @param imgUrl
        */
        // public static readonly navigateAppId = 'wx53b1b4ab6fa0d044'
        // public static readonly navigatePath = 'pages/index/index?from=koudaizhu'
        WXUtils.prototype.navigate2Program = function (appid, path, imgUrl) {
            return new Promise(function (resolve, reject) {
                var navigate = wx.navigateToMiniProgram;
                if (navigate) {
                    console.log('appId', appid);
                    console.log('path', path);
                    navigate({
                        appId: appid,
                        path: path,
                        success: function (res) {
                            console.log('跳转成功', res);
                        },
                        fail: function (res) {
                            console.log('跳转失败', res);
                        }
                    });
                }
                else {
                    wx.previewImage({
                        current: imgUrl,
                        urls: [imgUrl]
                    });
                }
            });
        };
        return WXUtils;
    }());
    lie.WXUtils = WXUtils;
    __reflect(WXUtils.prototype, "lie.WXUtils", ["lie.ITFPFUtils"]);
    // 视频ID，分别是：激励广告
    var adUnitIds = [
        'adunit-1a513703413fe167',
        'adunit-7d2a4278d6d2c537'
    ];
})(lie || (lie = {}));
