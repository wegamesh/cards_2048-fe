/*
这个类里面的东西基本都需要玩家根据当前游戏
来修改，下面标※的都是要改的，不标的话可不改
*/
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
/**
 * 游戏配置，由每个游戏自己定义
 */
var AppConfig = (function () {
    function AppConfig() {
    }
    /**
     * 获取skins下的皮肤
     */
    AppConfig.getSkin = function (name) {
        return 'resource/scene/' + name + '.exml';
    };
    AppConfig.isCheckIosPay = false; //标记是否检测过ios充值了
    return AppConfig;
}());
__reflect(AppConfig.prototype, "AppConfig");
