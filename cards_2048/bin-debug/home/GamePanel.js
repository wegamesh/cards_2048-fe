var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var SETTING = {
    RECYCLETOTALNUM: 2,
    ALMIGHTYRANDOM: 20,
    BOMBRANDOM: 5,
    ALMIGHTYID: 1,
    NORMALCARDID: 2,
    BOMBCARDID: 3,
    DUSTBINADMAXCOUNT: 5,
    RECALLMAXCOUNT: 5,
    RESURGENCEMAX: 1,
    CHANGETIMEMAX: 2,
};
var GamePanel = (function (_super) {
    __extends(GamePanel, _super);
    function GamePanel() {
        var _this = _super.call(this, 'game/GameSkin') || this;
        _this.totalScore = 0; //当前分数
        _this.hitNum = 0; //连击数量
        _this.currentScore = 0;
        _this.recycleNum = 0; //回收掉的卡牌数量
        _this.saveScore = 0; //记录分数回退用
        _this.$groups = [_this.g_cardsGroup1, _this.g_cardsGroup2, _this.g_cardsGroup3, _this.g_cardsGroup4, _this.g_dustBin];
        _this.canRecall = false; //是否可以回撤(初始为不可以)
        _this.recallTime = 1; //回撤的次数默认送一个
        _this.dustBinAdCount = 0; //已经清空垃圾桶的次数
        _this.recallAdCount = 0; //已经观看的回退广告的次数
        _this.resurgenceTime = 0; //复活次数
        _this.init();
        return _this;
    }
    GamePanel.prototype.init = function () {
        pfUtils.hideBannerAds();
        pfUtils.showBannerAds();
        this.initCardPond();
        this.initGroups();
        this.getHighScore();
        this.bindClick();
        this.initSounds();
        this.initGuide();
    };
    GamePanel.prototype.initSounds = function () {
        Sound.initPlay('chopsticks.mp3', false, 0, true);
        Sound.initPlay('click.mp3', false, 0, true);
        Sound.initPlay('icon_close.mp3', false, 0, true);
        Sound.initPlay('unlock.mp3', false, 0, true);
    };
    GamePanel.prototype.initGroups = function () {
        var _this = this;
        this.$groups.forEach(function (item, index) {
            if (index !== _this.$groups.length - 1) {
                var card = new CardComponent(0);
                item.addChild(card);
                card.x = card.width / 2;
                card.y = card.height / 2;
            }
        });
        // this.g_recall.getChildByName('count')['text'] = this.recallTime
        this.g_change.addChild(new CardChange());
    };
    /**
     * 新手引导
     */
    GamePanel.prototype.initGuide = function () {
        //获取是不是新手的缓存
        var isNew = LocalData.getItem('CrardsIsNewHand');
        if (!isNew) {
            AppViews.pushDialog(GameExplain);
            LocalData.setItem('CrardsIsNewHand', new Date().toString());
        }
    };
    GamePanel.prototype.bindClick = function () {
        // this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this)
        this.m_pause.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(PauseDialog);
        }, this);
        this.g_explain.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(GameExplain);
        }, this);
        //垃圾桶视频
        this.m_dustBinAd1.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDustBinAdTap, this);
        this.m_dustBinAd2.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDustBinAdTap, this);
    };
    GamePanel.prototype.onCardTouchBegin = function (e) {
        this.recordCardInfo();
        var card = e.currentTarget;
        this.currentCard = card;
        this.g_cardsPool.removeChild(card);
        card.x = e.stageX;
        card.y = e.stageY - card.anchorOffsetY;
        //记录手在卡牌上的偏移量
        this.addChild(card);
        this.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onCardTouchMove, this);
        this.addEventListener(egret.TouchEvent.TOUCH_END, this.onCardTouchEnd, this);
    };
    GamePanel.prototype.onCardTouchMove = function (e) {
        var _this = this;
        var card = this.currentCard;
        card.x = e.stageX;
        card.y = e.stageY - card.anchorOffsetY;
        // var y = card.y - card.anchorOffsetY
        //移动的时候判断在移动到哪个组内
        this.$groups.forEach(function (item, index) {
            if (item.hitTestPoint(card.x, card.y)) {
                //振动
                //最后一个是垃圾桶
                if (index === _this.$groups.length - 1) {
                    return;
                }
                else {
                    var card2 = item.$children[item.numChildren - 1];
                    if (card2.value === card.value || card.property === SETTING.ALMIGHTYID) {
                        card2.setRectStroke(0xacffae);
                    }
                    else {
                        card2.setRectStroke(0x544F4F);
                    }
                }
            }
            else {
                if (index === _this.$groups.length - 1) {
                    // this.dustBinReset()
                }
                else {
                    var card2 = item.$children[item.numChildren - 1];
                    card2.clearRectStroke();
                }
            }
        });
        //是否碰到了垃圾桶  
        //给卡牌两个碰撞点
        var x2 = card.x + card.width / 2, y2 = card.y + card.height / 2;
        var dustBin = this.$groups[4];
        if (dustBin.hitTestPoint(x2, y2) || dustBin.hitTestPoint(card.x, card.y)) {
            if (this.recycleNum <= 1) {
                dustBin.getChildByName((this.recycleNum + 1).toString()).alpha = 0.5;
            }
        }
        else {
            this.dustBinReset();
        }
    };
    GamePanel.prototype.onCardTouchEnd = function (e) {
        var _self = this;
        var card = this.currentCard;
        var index = this.judgeIndex(e, card);
        if (index >= 0) {
            //垃圾桶
            if (index === this.$groups.length - 1) {
                if (this.recycleNum >= SETTING.RECYCLETOTALNUM) {
                    this.cardReset(card);
                    return;
                }
                this.throughToDustBin(card);
                nextStep();
                return;
            }
            Sound.play('click.mp3', 0.6);
            var group = this.$groups[index];
            //正在合并的时候不让放防止bug
            if (group['isCounting']) {
                this.cardReset(card);
                return;
            }
            var card2 = group.$children[group.numChildren - 1];
            card2 && card2.clearRectStroke();
            //当卡牌池数量满了并且不能合并
            if (group.numChildren >= 9 && card.value !== card2.value && card.property !== SETTING.ALMIGHTYID) {
                this.cardReset(card);
                return;
            }
            //万能卡不能移到空的位置
            if (card.property === SETTING.ALMIGHTYID && group.$children.length <= 1) {
                this.cardReset(card);
                return;
            }
            if (card2.value !== card.value && card.property !== SETTING.ALMIGHTYID) {
                //卡牌移入的动画
                this.cardEndTween(card);
            }
            pfUtils.vibrateShort();
            group.addChild(card);
            this.removeEvent(card);
            card.x = card.anchorOffsetX;
            card.y = (group.numChildren - 2) * 60 + card.anchorOffsetY;
            nextStep();
            this.countGroupCard(group);
        }
        else {
            //如果没有放对位置就回到卡牌池中
            this.cardReset(card);
        }
        function nextStep() {
            _self.produceCard();
            _self.setCardPond();
            if (!_self.canRecall) {
                _self.canRecall = true;
                _self.recallBtnReset();
            }
        }
    };
    /**
     * 放到垃圾桶
     * @param card
     */
    GamePanel.prototype.throughToDustBin = function (card) {
        Sound.play('chopsticks.mp3', 0.6);
        this.removeEvent(card);
        this.removeChild(card);
        this.recycleNum++;
        if (this.dustBinAdCount < SETTING.DUSTBINADMAXCOUNT) {
            this.addDustBinAd();
        }
        this.dustBinReset();
    };
    /**
     * 记录上一步信息
     */
    GamePanel.prototype.recordCardInfo = function () {
        var _this = this;
        //记录当前的所有卡牌数据
        var cardsPool = [];
        this.g_cardsPool.$children.forEach(function (item) {
            cardsPool.push({ value: item.value, property: item.property });
        });
        this.saveCardsPool = cardsPool;
        this.saveInfo = [];
        this.$groups.forEach(function (item, index) {
            if (index !== _this.$groups.length - 1) {
                var group_1 = [];
                item.$children.forEach(function (card) {
                    group_1.push({ value: card.value, x: card.x, y: card.y });
                });
                _this.saveInfo.push(group_1);
            }
        });
        this.lastRecycleNum = this.recycleNum;
        this.saveScore = this.lastScore;
    };
    /**
     * 获取最高分
     */
    GamePanel.prototype.getHighScore = function () {
        var score = LocalData.getItem('highScore');
        if (score) {
            this.l_highScore.text = this.setScoreDigit(+score);
        }
        else {
            this.l_highScore.text = '0';
        }
    };
    /**
     * 卡牌回到原位
     * @param card
     */
    GamePanel.prototype.cardReset = function (card) {
        var _this = this;
        this.removeEvent(card);
        egret.Tween.get(card).to({
            x: this.g_cardsPool.x + card.lastX,
            y: this.g_cardsPool.y + card.lastY
        }, 300).call(function () {
            _this.removeChild(card);
            card.x = card.lastX;
            card.y = card.lastY;
            _this.g_cardsPool.addChild(card);
            card.addEventListener(egret.TouchEvent.TOUCH_BEGIN, _this.onCardTouchBegin, _this);
        });
    };
    /**
     * 将卡牌池的第二张移到可操作
     */
    GamePanel.prototype.setCardPond = function () {
        var _this = this;
        var card = this.g_cardsPool.$children[1];
        var x = card.x + 110;
        egret.Tween.get(card).to({
            x: x,
            scaleX: 1,
            scaleY: 1
        }, 500).call(function () {
            card.lastX = card.x;
            card.lastY = card.y;
            //绑定事件
            card.addEventListener(egret.TouchEvent.TOUCH_BEGIN, _this.onCardTouchBegin, _this);
            // this.addEventListener(egret.TouchEvent.TOUCH_CANCEL, this.onCardTouchEnd, this)
        });
    };
    /**
     * 初始化一些属性
     */
    GamePanel.prototype.propertyReset = function (group) {
    };
    /**
     * 判断在哪个位置
     * @param
     */
    GamePanel.prototype.judgeIndex = function (e, card) {
        //移动的时候判断在移动到哪个组内
        var y = card.y;
        var x2 = card.x + card.width / 2, y2 = card.y + card.height / 2;
        for (var i = 0; i < this.$groups.length; i++) {
            var item = this.$groups[i];
            if (i === 4) {
                if (item.hitTestPoint(x2, y2) || item.hitTestPoint(card.x, card.y)) {
                    return 4;
                }
            }
            else {
                if (item.hitTestPoint(card.x, y)) {
                    return i;
                }
            }
        }
        return void 0;
    };
    /**
     * 重置垃圾桶
     */
    GamePanel.prototype.dustBinReset = function () {
        if (this.recycleNum === 1) {
            this.g_dustBin.getChildByName('1').alpha = 0.5;
            this.g_dustBin.getChildByName('2').alpha = 0;
            return;
        }
        if (this.recycleNum === 2) {
            this.g_dustBin.getChildByName('1').alpha = 0.5;
            this.g_dustBin.getChildByName('2').alpha = 0.5;
            return;
        }
        this.g_dustBin.getChildByName('1').alpha = 0;
        this.g_dustBin.getChildByName('2').alpha = 0;
    };
    /**
     * 加一个垃圾桶视频
     */
    GamePanel.prototype.addDustBinAd = function () {
        if (!this.m_dustBinAd1.visible) {
            this.m_dustBinAd1.visible = true;
            this.l_x1.visible = false;
        }
        else {
            this.m_dustBinAd2.visible = true;
            this.l_x2.visible = false;
        }
    };
    /**
     * 减去一个垃圾桶视频
     */
    GamePanel.prototype.removeDustBinAd = function () {
        if (this.m_dustBinAd2.visible) {
            this.m_dustBinAd2.visible = false;
            this.l_x2.visible = true;
        }
        else {
            this.m_dustBinAd1.visible = false;
            this.l_x1.visible = true;
        }
    };
    /**
     * 清除卡牌上的所有绑定事件
     */
    GamePanel.prototype.removeEvent = function (card) {
        card.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onCardTouchBegin, this);
        this.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.onCardTouchMove, this);
        this.removeEventListener(egret.TouchEvent.TOUCH_END, this.onCardTouchEnd, this);
    };
    /**
     * 初始化生成的卡牌池
     */
    GamePanel.prototype.initCardPond = function () {
        for (var i = 0; i < 2; i++) {
            this.produceCard(2);
        }
        this.setCardPond();
    };
    /**
     * 生成卡片
     * @param num   1固定万能卡/2固定普通卡/3炸弹牌默认
     */
    GamePanel.prototype.produceCard = function (num) {
        if (num === void 0) { num = 0; }
        var _self = this;
        // if (num === SETTING.ALMIGHTYID) {
        //     this.createCard(SETTING.ALMIGHTYID)
        // }
        // else if (num === SETTING.NORMALCARDID) {
        //     getNormalCard()
        // }
        // else {
        //     if (Math.floor(Math.random() * SETTING.ALMIGHTYRANDOM) === 0) {
        //         this.createCard(SETTING.ALMIGHTYID)
        //     } else {
        //         getNormalCard()
        //     }
        // }
        switch (num) {
            //万能牌
            case SETTING.ALMIGHTYID:
                this.createCard(num);
                break;
            //普通牌
            case SETTING.NORMALCARDID:
                getNormalCard();
                break;
            //炸弹牌
            case SETTING.BOMBCARDID:
                this.createCard(num);
                break;
            default:
                if (Math.floor(Math.random() * SETTING.ALMIGHTYRANDOM) === 0) {
                    this.createCard(SETTING.ALMIGHTYID);
                }
                else if ((Math.floor(Math.random() * SETTING.BOMBRANDOM) === 0)) {
                    this.createCard(SETTING.BOMBCARDID);
                }
                else {
                    getNormalCard();
                }
                break;
        }
        function getNormalCard() {
            var array = [1, 2, 3, 4, 5, 6];
            var randomIndex = Math.floor(Math.random() * array.length);
            var value = Math.pow(2, array[randomIndex]);
            _self.createCard(SETTING.NORMALCARDID, value);
        }
    };
    /**
     * 生成一张卡牌
     */
    GamePanel.prototype.createCard = function (type, value) {
        var card = null;
        switch (type) {
            //万能
            case SETTING.ALMIGHTYID:
                card = new AlmightyCard();
                break;
            //炸弹
            case SETTING.BOMBCARDID:
                card = new CardBomb();
                break;
            default:
                card = new CardComponent(value);
        }
        card.property = type;
        this.g_cardsPool.addChildAt(card, 0);
    };
    /**
     * 判断group里面有没有相同的两张牌
     * 如果相同就合并
     */
    GamePanel.prototype.countGroupCard = function (group) {
        var _this = this;
        var index = null; //有相同的话需要删掉的东西
        // this.setCalculate()
        //正在计算的时候不让放卡牌防止bug
        group['isCounting'] = true;
        var lastIndex = group.numChildren - 1;
        var lastCard = group.$children[lastIndex];
        var sideCard = group.$children[lastIndex - 1];
        //炸弹
        if (lastCard.property === SETTING.BOMBCARDID) {
            group['isCounting'] = false;
            this.bombGroup(group);
            return;
        }
        //如果最后一张卡是万能卡
        if (lastCard.property === SETTING.ALMIGHTYID) {
            lastCard.value = sideCard.value;
        }
        if (lastCard.value === sideCard.value) {
            if (!group['hitCount']) {
                group['hitCount'] = 1;
            }
            else {
                group['hitCount']++;
            }
            var tween2 = egret.Tween.get(lastCard), tween = egret.Tween.get(sideCard);
            tween2
                .to({ anchorOffsetY: 170 }, 200)
                .to({ scaleX: 0.8, scaleY: 0.8 }, 200);
            tween
                .to({}, 200)
                .to({ scaleX: 0.8, scaleY: 0.8 }, 200)
                .call(function () {
                Sound.play('unlock.mp3', 0.6);
                //有翻倍
                _this.addScore(lastCard.value * group['hitCount']);
                sideCard.value *= 2;
                sideCard.setBgColor(sideCard.value);
                group.removeChild(lastCard);
                group['hitCount'] > 1 && _this.createLabelTween(group, group['hitCount']);
            })
                .to({ scaleX: 1, scaleY: 1 }, 200, egret.Ease.bounceInOut)
                .call(function () {
                _this.countGroupCard(group);
            });
        }
        else {
            //不能合卡牌的时候判断是否结束
            group['hitCount'] > 2 && this.hitRemind(group['hitCount']);
            group['hitCount'] = 0;
            group['isCounting'] = false;
            this.judgeGameOver();
        }
    };
    /**
     * 清除group的连击数
     */
    GamePanel.prototype.clearHitCount = function (group) {
        group['hitCount'] = 0;
    };
    /**
     * 加分数
     */
    GamePanel.prototype.addScore = function (num) {
        var _this = this;
        this.totalScore += num;
        this.setBeyond(this.totalScore);
        //数字表示每秒加的次数
        var speed = Math.ceil(num / 20);
        if (this.scoreInterval) {
            this.scoreInterval.stop();
            this.scoreInterval = null;
        }
        this.scoreInterval = new egret.Timer(30);
        this.scoreInterval.addEventListener(egret.TimerEvent.TIMER, function () {
            _this.currentScore += speed;
            _this.l_score.text = _this.setScoreDigit(_this.currentScore);
            if (_this.currentScore >= _this.totalScore) {
                _this.l_score.text = _this.setScoreDigit(_this.totalScore);
                _this.scoreInterval.stop();
            }
        }, this);
        this.scoreInterval.start();
    };
    /**
     * 判断游戏结束
     */
    GamePanel.prototype.judgeGameOver = function () {
        var _this = this;
        var totalFull = 0;
        var groups = this.$groups.slice(0, 4);
        for (var i = 0; i < groups.length; i++) {
            var item = groups[i];
            if (item.numChildren >= 9) {
                totalFull++;
            }
        }
        if (totalFull >= 4) {
            var boolMerge = true;
            //卡牌中没有合成的了（包括没有万能卡）
            var poolCard = this.g_cardsPool.$children[this.g_cardsPool.numChildren - 1];
            var poolCard2 = this.g_cardsPool.$children[0];
            groups.forEach(function (item) {
                var last = item.$children[item.numChildren - 1];
                //如果卡牌池有可以合成的卡牌或者卡牌池第一张是万能牌
                if (last.value === poolCard.value || poolCard.property === SETTING.ALMIGHTYID) {
                    boolMerge = false;
                }
                //如果卡牌池第二张可以合成并且垃圾桶有位置
                if ((last.value === poolCard2.value || poolCard2.property === SETTING.ALMIGHTYID) && _this.recycleNum < 2) {
                    boolMerge = false;
                }
            });
            if (boolMerge) {
                //游戏结束
                if (this.resurgenceTime >= SETTING.RESURGENCEMAX) {
                    this.gameOver();
                }
                else {
                    AppViews.pushDialog(ResurgenceDialog);
                }
            }
        }
    };
    /**
     * =主动结束游戏
     */
    GamePanel.prototype.gameOver = function () {
        AppViews.pushDialog(GameOverDialog, this.totalScore);
        this.setHighScore();
    };
    /**
     * 设置最高分
     */
    GamePanel.prototype.setHighScore = function () {
        var score = this.totalScore;
        var highScore = +LocalData.getItem('highScore');
        if (score >= highScore) {
            LocalData.setItem('highScore', score.toString());
        }
    };
    /**
     * 连击动画
     */
    GamePanel.prototype.createLabelTween = function (obj, num) {
        var _this = this;
        var label = new HitCountComponent(num);
        var card = obj.$children[obj.numChildren - 1];
        label.x = obj.x + obj.width / 2 - label.width / 2;
        label.y = card.y + card.height - 20;
        this.addChild(label);
        egret.Tween.get(label).to({
            anchorOffsetY: 50,
            alpha: 0
        }, 600)
            .call(function () {
            _this.removeChild(label);
        });
    };
    /**
     * 重置回撤按钮
     */
    GamePanel.prototype.recallBtnReset = function () {
        if (this.recallTime >= 1) {
            this.g_recall.getChildByName('rect')['fillColor'] = 0xf97f70;
            this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this);
            this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this);
            this.l_recallTime.visible = this.l_recallX.visible = true;
            this.m_recallAd.visible = false;
        }
        else {
            // this.g_recall.getChildByName('rect')['fillColor'] = 0x686767
            this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this);
            if (this.recallAdCount < SETTING.RECALLMAXCOUNT) {
                this.l_recallTime.visible = this.l_recallX.visible = false;
                this.m_recallAd.visible = true;
                this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this);
            }
            else {
                this.g_recall.getChildByName('rect')['fillColor'] = 0x686767;
                this.l_recallTime.visible = this.l_recallX.visible = true;
                this.m_recallAd.visible = false;
                this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this);
            }
        }
    };
    /**
     * 回撤
     */
    GamePanel.prototype.onRecallTap = function () {
        var _this = this;
        if (!this.canRecall || this.recallTime <= 0) {
            return;
        }
        this.recallTime--;
        this.canRecall = false;
        this.recallBtnReset();
        this.saveInfo.forEach(function (item, index) {
            var group = _this.$groups[index];
            group.removeChildren();
            item.forEach(function (item2) {
                var card = new CardComponent(item2.value);
                card.x = item2.x;
                card.y = item2.y;
                group.addChild(card);
            });
        });
        this.g_cardsPool.removeChildren();
        this.saveCardsPool = this.saveCardsPool.reverse();
        this.saveCardsPool.forEach(function (item) {
            _this.createCard(item.property, item.value);
        });
        this.setCardPond();
        this.recycleNum = this.lastRecycleNum;
        if (this.scoreInterval && this.scoreInterval.running) {
            this.scoreInterval.stop();
            this.l_score.text = this.setScoreDigit(this.saveScore) || '0';
        }
        this.dustBinReset();
    };
    /**
     * 看垃圾桶广告视频
     */
    GamePanel.prototype.onDustBinAdTap = function () {
        var _this = this;
        pfUtils.showLoading('加载广告');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.recycleNum--;
                _this.dustBinAdCount++;
                _this.dustBinReset();
                _this.removeDustBinAd();
            }
        });
    };
    /**
     * 看视频复活
     */
    GamePanel.prototype.movieResurgence = function () {
        //取 $group 的前四个
        var groups = this.$groups.slice(0, 4);
        groups.forEach(function (item, index) {
            //删掉一半的牌
            for (var i = item.numChildren - 1; i > 4; i--) {
                item.removeChildAt(i);
            }
        });
        this.resurgenceTime++;
    };
    /**
     * 回退广告
     */
    GamePanel.prototype.recallAdShow = function () {
        var _this = this;
        pfUtils.showLoading('加载广告');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.recallTime += 1;
                _this.recallAdCount++;
                _this.recallBtnReset();
            }
        });
    };
    /**
     * 弹出连击提示
     */
    GamePanel.prototype.hitRemind = function (type) {
        var hitCom = new HitTextComponent(type);
        hitCom.x = this.width / 2;
        hitCom.y = this.height / 2 - 200;
        this.addChild(hitCom);
    };
    /**
     * 卡牌移入的动画
     */
    GamePanel.prototype.cardEndTween = function (card) {
        egret.Tween.get(card)
            .to({
            scaleY: 0.7
        }, 200)
            .to({
            scaleY: 1
        }, 200, egret.Ease.bounceOut);
    };
    /**
     * 分数保留5位数
     */
    GamePanel.prototype.setScoreDigit = function (score) {
        var string = score.toString();
        if (score > 10000) {
            var num = (score / 1000).toFixed(2);
            string = num + 'k';
        }
        if (score > 1000000) {
            var num = (score / 1000000).toFixed(2);
            string = num + 'm';
        }
        return string;
    };
    /**
     * 卡牌换一换
     */
    GamePanel.prototype.onChangeTap = function () {
        this.g_cardsPool.removeChildren();
        this.initCardPond();
    };
    /**
     * 超越好友
     * @param score 分数
     */
    GamePanel.prototype.setBeyond = function (score) {
        if (this.beyondBitmap) {
            this.removeChild(this.beyondBitmap);
        }
        pfUtils.postMessage('beyond', score);
        var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        var texture = new egret.Texture();
        bitmapdata.$deleteSource = false;
        texture._setBitmapData(bitmapdata);
        // 界面显示
        var bitmap = this.beyondBitmap = new egret.Bitmap(texture);
        bitmap.width = this.width;
        bitmap.height = this.height;
        bitmap.x = 135;
        bitmap.y = 90;
        this.addChild(bitmap);
        egret.setTimeout(function () {
            egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
            bitmapdata.webGLTexture = null;
        }, this, 500);
    };
    /**
     * 炸弹牌使用
     */
    GamePanel.prototype.bombGroup = function (group) {
        var _this = this;
        var cards = group.$children.slice(1, group.numChildren);
        var targetX = this.l_score.x + this.l_score.width / 2, targetY = this.l_score.y + this.l_score.height / 2;
        cards.forEach(function (item) {
            var x = group.x + item.x, y = group.y + item.y;
            group.removeChild(item);
            item.x = x;
            item.y = y;
            _this.addChild(item);
            egret.Tween.get(item)
                .to({ x: _this.width / 2, y: _this.height / 2 }, 1000, egret.Ease.circInOut)
                .to({ x: targetX, y: targetY, scaleX: 0, scaleY: 0, alpha: 0.3, }, 500, egret.Ease.quadOut)
                .call(function () {
                _this.removeChild(item);
                if (item.property === SETTING.NORMALCARDID) {
                    _this.addScore(item.value);
                }
            });
        });
    };
    return GamePanel;
}(AppComponent));
__reflect(GamePanel.prototype, "GamePanel");
