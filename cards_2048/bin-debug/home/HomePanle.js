var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var HomePanel = (function (_super) {
    __extends(HomePanel, _super);
    function HomePanel() {
        var _this = _super.call(this, 'HomeSkin') || this;
        _this.init();
        return _this;
    }
    HomePanel.prototype.init = function () {
        pfUtils.showBannerAds();
        this.bindClick();
    };
    HomePanel.prototype.onShow = function () {
        pfUtils.hideBannerAds();
        pfUtils.showBannerAds();
    };
    // public onHide(){
    //     pfUtils.hideBannerAds()
    // }
    HomePanel.prototype.bindClick = function () {
        this.g_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameStart, this);
        this.g_rank.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(RankDialog);
        }, this);
        this.g_gameCircle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameCircleTap, this);
        EventUtils.addScaleListener(this.g_start);
        EventUtils.addScaleListener(this.g_rank);
    };
    HomePanel.prototype.onGameStart = function () {
        AppViews.pushPanel(GamePanel);
    };
    HomePanel.prototype.onGameCircleTap = function () {
        var appId = 'wx53b1b4ab6fa0d044';
        var path = 'pages/quan/quan?communityId=3&from=2048kapai_3';
        var imageUrl = "https://static.xunguanggame.com/pocket/assets/sharePic/youxidating4.png";
        pfUtils.navigate2Program(appId, path, imageUrl);
    };
    return HomePanel;
}(lie.AppComponent));
__reflect(HomePanel.prototype, "HomePanel");
