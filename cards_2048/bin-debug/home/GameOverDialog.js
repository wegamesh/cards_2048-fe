var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var GameOverDialog = (function (_super) {
    __extends(GameOverDialog, _super);
    function GameOverDialog(score) {
        var _this = _super.call(this, 'GameOverSkin') || this;
        _this.score = score;
        _this.init();
        return _this;
    }
    GameOverDialog.prototype.init = function () {
        var _this = this;
        pfUtils.hideBannerAds();
        pfUtils.showBannerAds();
        this.g_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameStart, this);
        this.g_rank.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(RankDialog);
        }, this);
        this.g_home.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
            AppViews.removePanel(GamePanel);
        }, this);
        this.g_share.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onShareTap, this);
        EventUtils.addScaleListener(this.g_start);
        EventUtils.addScaleListener(this.g_home);
        EventUtils.addScaleListener(this.g_rank);
        this.uploadScore();
    };
    GameOverDialog.prototype.onGameStart = function () {
        this.removeFromParent();
        AppViews.removePanel(GamePanel);
        AppViews.pushPanel(GamePanel);
    };
    /**
     * 上传分数
     */
    GameOverDialog.prototype.uploadScore = function () {
        var post = pfUtils.postMessage;
        var data = {
            "score": this.score,
            "time": Date.now()
        };
        post('update', data);
        post('settle');
        post('clear');
        // post('resume')
    };
    /**
     * 分享
     */
    GameOverDialog.prototype.onShareTap = function () {
        var title = "\u6211\u83B7\u5F97\u4E86" + this.score + "\u5206,\u8FD8\u6709\u8C01\uFF1F";
        var imageUrl = CommonUtils.getShareInfo().imageUrl;
        CommonUtils.showShare(title, imageUrl);
    };
    return GameOverDialog;
}(Dialog));
__reflect(GameOverDialog.prototype, "GameOverDialog");
