var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var Test = (function (_super) {
    __extends(Test, _super);
    function Test() {
        var _this = _super.call(this, 'test/TestSkin') || this;
        _this.init();
        return _this;
    }
    Test.prototype.init = function () {
        this.bindClick();
    };
    Test.prototype.bindClick = function () {
        this.l_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStart, this);
    };
    Test.prototype.onStart = function () {
        this.targetIndex = 100;
        this.index = 0;
        this.currentIndex = -1;
        this.timer = new egret.Timer(100);
        this.timer.addEventListener(egret.TimerEvent.TIMER, this.onStartTimer, this);
        this.timer.start();
    };
    Test.prototype.onStartTimer = function () {
        this.currentIndex++;
        this.timer.delay = Math.floor(1000 / (this.targetIndex - this.currentIndex));
        var item = this.g_awards.$children[this.index];
        // console.log(item)
        this.r_frame.x = item.x;
        this.r_frame.y = item.y;
        if (this.currentIndex === this.targetIndex) {
            this.timer.stop();
        }
        this.index++;
        if (this.index >= this.g_awards.numChildren) {
            this.index = 0;
        }
    };
    return Test;
}(Dialog));
__reflect(Test.prototype, "Test");
