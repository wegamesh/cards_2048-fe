var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 连击的提示
 */
var HitTextComponent = (function (_super) {
    __extends(HitTextComponent, _super);
    function HitTextComponent(type) {
        var _this = _super.call(this, 'game/HitTextSkin') || this;
        _this.init();
        _this.setImage(type);
        return _this;
    }
    HitTextComponent.prototype.init = function () {
        this.anchorOffsetX = this.width / 2;
        this.anchorOffsetY = this.height / 2;
        this.scaleX = this.scaleY = 0;
    };
    /**
     * 设置图片
     * @param 连击数
     */
    HitTextComponent.prototype.setImage = function (type) {
        var _this = this;
        console.log(type);
        if (type > 6) {
            type = 6;
        }
        var url = "hitx" + type + "_png";
        //音效
        Sound.initPlay("hitx" + type + ".mp3");
        this.m_text.source = url;
        egret.Tween.get(this).to({
            scaleX: 1,
            scaleY: 1
        }, 300, egret.Ease.bounceOut)
            .to({}, 800)
            .to({
            scaleX: 0,
            scaleY: 0
        }, 200).call(function () {
            _this.removeFromParent();
        });
    };
    return HitTextComponent;
}(AppComponent));
__reflect(HitTextComponent.prototype, "HitTextComponent");
