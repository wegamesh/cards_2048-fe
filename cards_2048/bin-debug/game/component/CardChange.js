var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 卡牌换一换
 * state0/state1/state2   不能点击/可以点击换卡牌/点击看视频加次数
 */
var CardChange = (function (_super) {
    __extends(CardChange, _super);
    function CardChange() {
        var _this = _super.call(this, 'game/component/CardChangeSkin') || this;
        _this.adTimes = 0; //已经换的次数
        _this.init();
        return _this;
    }
    CardChange.prototype.init = function () {
        this.currentState = 'state1';
        this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onClickTap, this);
    };
    CardChange.prototype.onClickTap = function () {
        switch (this.currentState) {
            case 'state0':
                break;
            case 'state1':
                this.onChangeTap();
                break;
            case 'state2':
                this.onAdMovie();
                break;
            default:
                break;
        }
    };
    /**
     * 换卡牌
     */
    CardChange.prototype.onChangeTap = function () {
        var gamePage = AppViews.getTopCommont();
        gamePage.onChangeTap();
        //如果没有看视频的次数了就不能点击
        if (this.adTimes > SETTING.CHANGETIMEMAX) {
            this.currentState = 'state0';
        }
        else {
            this.currentState = 'state2';
        }
    };
    /**
     * 看视频
     */
    CardChange.prototype.onAdMovie = function () {
        var _this = this;
        pfUtils.showLoading('加载广告');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.adTimes++;
                _this.currentState = 'state1';
            }
        });
    };
    return CardChange;
}(AppComponent));
__reflect(CardChange.prototype, "CardChange");
