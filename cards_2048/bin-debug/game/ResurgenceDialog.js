var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var ResurgenceDialog = (function (_super) {
    __extends(ResurgenceDialog, _super);
    function ResurgenceDialog() {
        var _this = _super.call(this, 'game/ResurgenceSkin') || this;
        _this.init();
        _this.resurgenceBtnTween();
        return _this;
    }
    ResurgenceDialog.prototype.init = function () {
        this.g_resurgence.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onResurgenceTap, this);
        this.l_skip.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSkip, this);
    };
    /**
     * 复活按钮的动画
     */
    ResurgenceDialog.prototype.resurgenceBtnTween = function () {
        var tween = egret.Tween.get(this.g_resurgence, { loop: true });
        tween
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 1, scaleY: 1 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 1, scaleY: 1 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 1000);
    };
    ResurgenceDialog.prototype.onResurgenceTap = function () {
        var _this = this;
        pfUtils.showLoading('加载视频');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.removeFromParent();
                var gamePage = AppViews.getTopCommont();
                //看视频复活
                gamePage.movieResurgence();
            }
        });
        // this.removeFromParent()
        // var gamePage: any = AppViews.getTopCommont()
        // //看视频复活
        // gamePage.movieResurgence()
    };
    /**
     * 跳过
     */
    ResurgenceDialog.prototype.onSkip = function () {
        this.removeFromParent();
        var gamePage = AppViews.getTopCommont();
        gamePage.gameOver();
    };
    return ResurgenceDialog;
}(Dialog));
__reflect(ResurgenceDialog.prototype, "ResurgenceDialog");
