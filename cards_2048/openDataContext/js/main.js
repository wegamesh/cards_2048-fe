var egret = window.egret;var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var lie;
(function (lie) {
    /**
     * 事件分发工具类，异步操作用
     */
    var Dispatch = (function () {
        function Dispatch() {
        }
        /**
         * 注册对象
         * @param key 事件的唯一标志
         * @param obj 注册事件
         * @param replace 是否替换掉之前的回调
         */
        Dispatch.$register = function (key, obj, replace) {
            var self = Dispatch;
            var target = self.$targets[key];
            var isReg = !target || replace;
            isReg && (self.$targets[key] = obj);
            return isReg;
        };
        /**
         * 注册事件
         * @param key 事件的唯一标志
         * @param call 回调函数
         * @param thisObj 回调所属对象
         * @param replace 是否替换掉之前的回调
         */
        Dispatch.register = function (key, call, thisObj, replace) {
            return Dispatch.$register(key, [call, thisObj], replace);
        };
        /**
         * 通知，触发回调
         * @param key 事件标志
         * @param param 回调参数
         */
        Dispatch.notice = function (key) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            var target = Dispatch.$targets[key];
            target && target[0].apply(target[1], params);
        };
        /**
         * 移除事件
         */
        Dispatch.remove = function (key) {
            delete Dispatch.$targets[key];
        };
        /**
         * 是否注册了某个事件
         */
        Dispatch.hasRegister = function (key) {
            return !!Dispatch.$targets[key];
        };
        /**
         * 注册多个事件，统一回调，参数功能看register
         */
        Dispatch.registers = function (keys, call, thisObj, replace) {
            var obj = [call, thisObj];
            for (var i in keys) {
                var key = keys[i];
                Dispatch.$register(key, obj, replace);
            }
        };
        /**
         * 清除所有事件
         */
        Dispatch.clear = function () {
            Dispatch.$targets = {};
        };
        Dispatch.$targets = {}; // 存放所有回调函数
        return Dispatch;
    }());
    lie.Dispatch = Dispatch;
    __reflect(Dispatch.prototype, "lie.Dispatch");
})(lie || (lie = {}));
var Dispatch = lie.Dispatch;
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = this;
        console.log("发放域");
        _this = _super.call(this) || this;
        // 注册
        Dispatch.register('updateSocre', _this.updateSocre, _this);
        // 初始化
        _this.width = 750;
        _this.height = 1334;
        WXUtils.getUserInfo(); // 自己的信息先拿
        // this.onExit();  // 刚进来没东西给你看
        wx.onMessage(_this.onMessage.bind(_this));
        // 主菜单分数文本
        var label = _this.m_lblScore = new egret.TextField;
        label.size = 80;
        label.y = 280;
        label.textColor = 0xE87A20;
        _this.addChild(label);
        _this.updateSocre();
        _this.updateUser();
        return _this;
    }
    /**
     * 清除多余的控件
     */
    Main.prototype.clearChild = function (clear) {
        var self = this;
        // 除了主界面的分数，其他的都删了
        for (var i = 1, num = self.numChildren; i < num; i++) {
            clear ? self.removeChildAt(1) : (self.getChildAt(i).visible = false);
        }
    };
    /**
     * 接收消息
     */
    Main.prototype.onMessage = function (msg) {
        var self = this;
        var action = msg.action;
        var data = msg.data;
        console.log('onMessage', msg);
        switch (action) {
            case 'score':
                console.log("score");
                self.m_lblScore.visible = true;
                break;
            case 'update':
                self.updateUser(data);
                break;
            // 结算界面
            case 'settle':
                self.initView(0);
                break;
            // 复活界面
            case 'revive':
                self.initView(1, data);
                break;
            // 排行榜界面
            case 'rank':
                self.initView(2, data);
                break;
            // case 'last':
            //     self.setRankPage(-1);
            //     break;
            // case 'next':
            //     self.setRankPage(1);
            //     break;
            case 'exit':
                self.onExit(data);
            case 'pause':
                egret.ticker.pause();
                break;
            case 'resume':
                egret.ticker.resume();
                break;
        }
    };
    /**
     * 更新玩家分数
     */
    Main.prototype.updateSocre = function (data) {
        var self = this;
        var label = self.m_lblScore;
        label.text = '' + (data ? (data.score || 0) : 0);
        label.x = self.width - label.width >> 1;
        // 更新列表
        var settle = self.$children[1];
        if (settle instanceof SettleList)
            settle.initUI();
    };
    /**
     * 更新用户信息
     */
    Main.prototype.updateUser = function (newInfo) {
        var self = this;
        var clzz = WXUtils;
        clzz.updateUserKVData(newInfo);
    };
    /**
     * 初始化界面
     */
    Main.prototype.initView = function (type, data) {
        var self = this, target;
        var clear = true;
        self.m_lblScore.visible = false;
        switch (type) {
            case 0:
                target = new SettleList;
                target.x = 375;
                target.y = 424;
                break;
            case 1:
                target = new ReviveView(data);
                break;
            case 2:
                target = new RankList;
                clear = data;
                break;
        }
        self.clearChild(clear);
        self.addChild(target);
    };
    /**
     * 设置排行榜页数
     */
    // public setRankPage(change: number): void {
    //     var children = this.$children;
    //     var rank = children[children.length - 1];
    //     if (rank instanceof RankList) {
    //         rank.page += change;
    //     }
    // }
    /**
     * 移除结算页
     * @param type 对应initView
     */
    Main.prototype.onExit = function (type) {
        var self = this;
        var clzz;
        switch (type) {
            case 0:
                clzz = SettleList;
                break;
            case 1:
                clzz = ReviveView;
                break;
            case 2:
                clzz = RankList;
                break;
        }
        if (clzz)
            for (var i = self.numChildren - 1; i > 0; i--) {
                var child = self.getChildAt(i);
                if (child instanceof clzz) {
                    self.removeChildAt(i);
                    self.getChildAt(i - 1).visible = true;
                    return;
                }
            }
        self.clearChild(true);
    };
    /**
     * 加载资源
     * @param url 资源地址
     * @param call 回调
     * @param thisObj 回调所属对象
     * @param param 回调参数，注意第一个默认是纹理，因此param会在第二个参数
     */
    Main.loadUrl = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        var self = Main;
                        var texture = self.m_pCache[url];
                        var endCall = function () {
                            resolve(texture);
                        };
                        if (texture)
                            endCall();
                        else {
                            var loader = new egret.ImageLoader();
                            loader.addEventListener(egret.Event.COMPLETE, function (event) {
                                var imageLoader = event.currentTarget;
                                texture = new egret.Texture();
                                texture._setBitmapData(imageLoader.data);
                                self.m_pCache[url] = texture;
                                endCall();
                            }, null);
                            loader.load(url);
                        }
                    })];
            });
        });
    };
    /**
     * 设置纹理
     */
    Main.setTexture = function (bitmap, url) {
        return __awaiter(this, void 0, void 0, function () {
            var t;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Main.loadUrl(url)];
                    case 1:
                        t = _a.sent();
                        bitmap.texture = t;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 移除缓存
     */
    Main.clearCache = function () {
        Main.m_pCache = null;
    };
    /**
     * 添加圆形遮罩
     */
    Main.getCircleShape = function (size) {
        var shape = new eui.Rect;
        shape.width = shape.height = size;
        // shape.ellipseWidth = size;
        // shape.ellipseHeight = size;
        return shape;
    };
    /**
     * 获取编码所占字节数
     */
    Main.getCodeLen = function (c) {
        var len = 0;
        if (c < 0x007f) {
            len = 1;
        }
        else if ((0x0080 <= c) && (c <= 0x07ff)) {
            len = 2;
        }
        else if ((0x0800 <= c) && (c <= 0xffff)) {
            len = 3;
        }
        return len;
    };
    /**
     * 字符串接省略号
     * @param name 字符串
     * @param max 限制字节数
     */
    Main.getLimitStr = function (name, max) {
        var str = '';
        var total = 0;
        for (var i = 0, len = name.length; i < len; i++) {
            var c = name.charCodeAt(i);
            total += Main.getCodeLen(c);
            if (total > max && i < len - 1) {
                str += '..';
                break;
            }
            str += name[i];
        }
        return str;
    };
    // 控件工具方法
    Main.m_pCache = {}; // 存放url资源
    return Main;
}(egret.DisplayObjectContainer));
__reflect(Main.prototype, "Main");
/**
 * 排行榜子项
 */
var RankItem = (function (_super) {
    __extends(RankItem, _super);
    function RankItem() {
        var _this = _super.call(this) || this;
        _this.initView();
        return _this;
    }
    RankItem.prototype.onDestroy = function () {
        this.data = null;
    };
    /**
     * 初始化界面
     */
    RankItem.prototype.initView = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self, clzz, bg, width, height, rank, head, hSize, shape, rank1, name, score;
            return __generator(this, function (_a) {
                self = this, clzz = Main;
                bg = self.m_imgBg = new eui.Image;
                width = self.width = 508;
                height = self.height = 105;
                self.addChild(bg);
                rank = self.m_lblRank = new eui.Label;
                rank.size = 38;
                rank.verticalCenter = 0;
                rank.horizontalCenter = -242;
                self.addChild(rank);
                head = self.m_imgHead = new eui.Image;
                hSize = head.height = head.width = 60;
                shape = clzz.getCircleShape(hSize);
                head.verticalCenter = shape.verticalCenter = 0;
                head.horizontalCenter = shape.horizontalCenter = -182;
                head.mask = shape;
                self.addChild(head);
                self.addChild(shape);
                rank1 = self.m_imgRank = new eui.Image;
                rank1.y = 3;
                rank1.horizontalCenter = -152;
                self.addChild(rank1);
                name = self.m_lblName = new eui.Label;
                // name.x = 210;
                // name.y = 22;
                name.verticalCenter = 0;
                name.horizontalCenter = 16;
                name.size = 30;
                // name.textAlign = name.left;
                self.addChild(name);
                score = self.m_lblScore = new eui.Label;
                // score.x = 500;
                // score.y = 22;
                score.verticalCenter = 0;
                score.horizontalCenter = 200;
                score.size = 38;
                self.addChild(score);
                return [2 /*return*/];
            });
        });
    };
    /**
     * 数据变化
     */
    RankItem.prototype.dataChanged = function () {
        var self = this;
        var clzz = Main;
        var setTexture = clzz.setTexture;
        var data = self.data;
        var rank = data.rank;
        var rankImg = self.m_imgRank;
        var headImg = self.m_imgHead;
        var rankLbl = self.m_lblRank;
        // 排名
        rankLbl.text = '' + rank;
        rankLbl.textColor = self.getRankColor(rank);
        if (rankImg.visible = rank < 4) {
            setTexture(rankImg, 'resource/asserts/settle/icon_no' + rank + '_report.png');
        }
        // 头像
        setTexture(headImg, data.avatarUrl);
        setTexture(self.m_imgBg, self.getItemBg(rank));
        self.m_lblName.text = Main.getLimitStr(data.nickname, 27);
        self.m_lblScore.text = '' + data.score;
    };
    /**
     * 获取背景
     */
    RankItem.prototype.getItemBg = function (rank) {
        // var idx = rank < 4 ? rank : '';
        // return 'resource/rank/kuabg' + idx + '.png'
        var idx = rank % 2;
        if (idx == 1) {
            return 'resource/asserts/rankPict/othersRank.png';
        }
        // else{
        // 	return 'resource/asserts/rankPict/myRank.png'
        // }	
    };
    /**
     * 获取排名颜色
     */
    RankItem.prototype.getRankColor = function (rank) {
        var colors = [0xffdb00, 0xfd973d, 0xd59f7b];
        return colors[rank - 1] || 0xa5a5a5;
    };
    return RankItem;
}(eui.ItemRenderer));
__reflect(RankItem.prototype, "RankItem");
/**
 * 我的排名
 */
var MyRankItem = (function (_super) {
    __extends(MyRankItem, _super);
    function MyRankItem() {
        var _this = _super.call(this) || this;
        _this.initView();
        return _this;
    }
    /**
     * 初始化界面
     */
    MyRankItem.prototype.initView = function () {
        var self = this;
        // 排名
        var rank = self.m_lblRank = new eui.Label;
        rank.size = 40;
        // rank.verticalCenter = 0;
        // rank.horizontalCenter = -206;
        // rank.verticalCenter = 0;
        // rank.horizontalCenter = -230;
        rank.x = 28;
        rank.y = 12;
        self.addChild(rank);
        // 头像
        var head = self.m_imgHead = new eui.Image;
        var hSize = head.height = head.width = 60;
        var shape = Main.getCircleShape(hSize);
        // head.verticalCenter = shape.verticalCenter = 0;
        // head.horizontalCenter = shape.horizontalCenter = -82;
        head.x = shape.x = 75;
        head.y = shape.y = -3;
        head.mask = shape;
        self.addChild(head);
        self.addChild(shape);
        // 排名皇冠
        var rank1 = self.m_imgRank = new eui.Image;
        rank1.x = 115;
        rank1.y = -16;
        // rank1.horizontalCenter = -82;
        self.addChild(rank1);
        // 微信名字
        var name = self.m_lblName = new eui.Label;
        name.x = 240;
        name.y = 12;
        name.size = 36;
        // name.textAlign = name.left;
        self.addChild(name);
        // 分数
        var score = self.m_lblScore = new eui.Label;
        score.x = 450;
        score.y = 12;
        score.size = 40;
        self.addChild(score);
    };
    /**
     * 设置界面
     */
    MyRankItem.prototype.setData = function (data) {
        var self = this;
        self.m_lblRank.text = data.rank;
        // 头像
        Main.setTexture(self.m_imgHead, data.avatarUrl);
        // setTexture(data.avatarUrl, self.onLoadHead, self);
        self.m_lblName.text = data.nickname;
        self.m_lblScore.text = '';
        self.m_lblScore.text = data.score;
        // console.log("走到这里了吗");
        console.log(data.nickname, "昵称");
        console.log(data.score, "昵称fenshu");
        console.log(data.rank, "rank");
        console.log(data.avatarUrl, "touxiang");
        var rankImg = self.m_imgRank;
        if (rankImg.visible = data.rank < 4) {
            Main.setTexture(rankImg, 'resource/asserts/settle/icon_no' + data.rank + '_report.png');
        }
    };
    /**
     * 加载头像回调
     */
    MyRankItem.prototype.onLoadHead = function (texture) {
        this.m_imgHead.texture = texture;
    };
    return MyRankItem;
}(egret.DisplayObjectContainer));
__reflect(MyRankItem.prototype, "MyRankItem");
/**
 * 结算页子项
 */
var SettleItem = (function (_super) {
    __extends(SettleItem, _super);
    function SettleItem(data) {
        var _this = _super.call(this) || this;
        var width = _this.width = 110;
        var height = _this.height = 240;
        // 排名
        var rank = new egret.TextField;
        rank.textColor = data.isMe ? 0x58D334 : 0xffffff;
        rank.text = data.rank + '';
        rank.x = width - rank.width >> 1;
        // rank.size = 28;
        rank.size = 32;
        // 头像
        var head = new egret.Bitmap;
        head.width = head.height = 75;
        head.x = width - head.width >> 1;
        // head.y = 36;
        head.y = 50;
        Main.setTexture(head, data.avatarUrl);
        // 名称
        var name = new egret.TextField;
        name.text = Main.getLimitStr(data.nickname, 9);
        name.size = 30;
        name.x = width - name.width >> 1;
        name.y = 150;
        // 分数
        var score = new egret.TextField;
        score.text = data.score + '';
        score.size = 34;
        score.x = width - score.width >> 1;
        score.y = 200;
        _this.addChild(rank);
        _this.addChild(head);
        _this.addChild(name);
        _this.addChild(score);
        return _this;
    }
    return SettleItem;
}(egret.DisplayObjectContainer));
__reflect(SettleItem.prototype, "SettleItem");
/**
 * 排行榜界面
 */
var RankList = (function (_super) {
    __extends(RankList, _super);
    function RankList() {
        var _this = _super.call(this) || this;
        // var infos = this.$infos;
        _this.initView();
        // 列表
        // var list = this.m_pList = new eui.List;
        // var layout = list.layout = new eui.VerticalLayout;
        // var width = list.width = 508;
        // // list.height = 760;
        // list.height = 1000;
        // layout.gap = 14;
        // list.itemRenderer = RankItem;
        // list.x = 750 - width >> 1;
        // list.y = 240;
        // this.addChild(list);
        //在这里定义自己的单独列表
        WXUtils.getFriendInfos().then(function (infos) {
            // 排名初始化
            infos.sort(function (a, b) { return b.score - a.score; });
            infos.map(function (v, i) { v.rank = i + 1; });
            // 更新页数
            _this.$infos = infos;
            _this.setListData(_this.$infos);
        });
        return _this;
    }
    RankList.prototype.initView = function () {
        var self = this;
        var scroller = new eui.Scroller;
        var content = self.m_pList = new eui.List;
        var width = scroller.width = 574;
        scroller.height = 560;
        scroller.x = 750 - width >> 1;
        scroller.y = 240;
        self.addChild(scroller);
        scroller.viewport = content;
        content.itemRenderer = RankItem;
        scroller.bounces = false;
        scroller.scrollPolicyV = eui.ScrollPolicy.ON;
        // 个人信息
        var myItem = this.m_pMyItem = new MyRankItem;
        myItem.height = 208;
        myItem.width = width;
        myItem.x = 750 - width >> 1;
        myItem.y = 962;
        this.addChild(myItem);
    };
    /**
     * 设置列表数据类型
     */
    // public setType(type: number = 0): void {
    // 	var self = this;
    // 	var utils = WXUtils;
    // 	var infos = self.m_pCurInfos;
    // 	// var filterInfo=utils.sortInfo(infos, type);
    // 	self.m_pList.dataProvider = new eui.ArrayCollection(infos);	// 用的静态数据，只能new
    // 	//找出自己的信息
    // 	var myInfo;
    // 	for (let i = 0, len = infos.length; i < len; i++) {
    // 		let info = infos[i];
    // 		if (WXUtils.isMyInfo(info)) {
    // 			myInfo = info;
    // 			break;
    // 		}
    // 	}
    // 	// 初始化我的信息
    // 	// console.log(WXUtils.$userInfo);
    // 	this.m_pMyItem.setData(myInfo);
    // }
    /**
     * 设置列表数据
     */
    RankList.prototype.setListData = function (list) {
        var self = this;
        self.m_pCurInfos = list;
        var utils = WXUtils;
        var infos = self.m_pCurInfos;
        // var filterInfo=utils.sortInfo(infos, type);
        self.m_pList.dataProvider = new eui.ArrayCollection(infos); // 用的静态数据，只能new
        //找出自己的信息
        var myInfo;
        for (var i = 0, len = infos.length; i < len; i++) {
            var info = infos[i];
            if (WXUtils.isMyInfo(info)) {
                myInfo = info;
                break;
            }
        }
        // 初始化我的信息
        // console.log(WXUtils.$userInfo);
        this.m_pMyItem.setData(myInfo);
    };
    return RankList;
}(egret.DisplayObjectContainer));
__reflect(RankList.prototype, "RankList");
/**
 * 复活界面
 */
var ReviveView = (function (_super) {
    __extends(ReviveView, _super);
    /**
     * @param 传入当前的分数
     */
    function ReviveView(score) {
        var _this = _super.call(this) || this;
        _this.initHisScore();
        _this.initExceed(score);
        return _this;
    }
    /**
     * 初始化历史最高分
     */
    ReviveView.prototype.initHisScore = function () {
        return __awaiter(this, void 0, void 0, function () {
            var kvData, info, text;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, WXUtils.getUserKVData()];
                    case 1:
                        kvData = _a.sent();
                        info = WXUtils.getInfoByKVData(kvData);
                        text = new egret.TextField;
                        text.size = 38;
                        text.y = 324;
                        text.textColor = 0xfbf0b0;
                        text.text = '历史最高分：' + info.score;
                        text.x = 750 - text.width >> 1;
                        this.addChild(text);
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 初始化即将超过谁
     */
    ReviveView.prototype.initExceed = function (score) {
        return __awaiter(this, void 0, void 0, function () {
            var clzz, infos, i, len, info;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        clzz = WXUtils;
                        return [4 /*yield*/, clzz.getFriendInfos()];
                    case 1:
                        infos = _a.sent();
                        infos.sort(function (a, b) {
                            return a.score - b.score;
                        });
                        for (i = 0, len = infos.length; i < len; i++) {
                            info = infos[i];
                            if (!clzz.isMyInfo(info) && info.score > score) {
                                // 创建即将超过谁的文本
                                this.initBotView(info);
                                break;
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 创建底部界面
     */
    ReviveView.prototype.initBotView = function (info) {
        var view = new egret.DisplayObjectContainer;
        var height = view.height = 96;
        // 左边文本
        var text0 = new egret.TextField;
        text0.size = 38;
        text0.text = '下个即将超过的好友：';
        // 中间头像
        var head = new egret.Bitmap;
        head.height = head.width = height;
        Main.setTexture(head, info.avatarUrl);
        // 右边分数
        var text1 = new egret.TextField;
        text1.size = 38;
        text1.text = info.score + '';
        // 加入view
        view.addChild(text0);
        view.addChild(head);
        view.addChild(text1);
        // 设置坐标
        text1.y = text0.y = height - text0.height >> 1; // 文字尺寸一样可以这样处理
        head.x = text0.width - 4;
        text1.x = head.x + height + 14;
        view.x = 750 - view.width >> 1;
        view.y = 1180;
        this.addChild(view);
    };
    return ReviveView;
}(egret.DisplayObjectContainer));
__reflect(ReviveView.prototype, "ReviveView");
/**
 * 结算页——锚点在中心，请居中显示该控件
 */
var SettleList = (function (_super) {
    __extends(SettleList, _super);
    function SettleList() {
        return _super.call(this) || this;
    }
    /**
     * 初始化UI界面
     */
    SettleList.prototype.initUI = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self, utils, infos, user, data, uInfo, text, myInfo, i, len, info, shows, length, startX, i, item;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        utils = WXUtils;
                        return [4 /*yield*/, utils.getFriendInfos()];
                    case 1:
                        infos = _a.sent();
                        return [4 /*yield*/, utils.getUserInfo()];
                    case 2:
                        user = _a.sent();
                        return [4 /*yield*/, utils.getUserKVData()];
                    case 3:
                        data = _a.sent();
                        uInfo = utils.getInfoByKVData(data);
                        text = new egret.TextField;
                        text.size = 34;
                        text.textColor = 0xfbf0b0;
                        text.text = '历史最高分：' + uInfo.score;
                        text.x = -text.width / 2;
                        self.addChild(text);
                        for (i = 0, len = infos.length; i < len; i++) {
                            info = infos[i];
                            if (utils.isMyInfo(info)) {
                                info.score = Number(uInfo.score);
                                info.isMe = true;
                                myInfo = info;
                                break;
                            }
                        }
                        // 新用户数据来不及存进去的时候
                        if (!myInfo) {
                            user.score = uInfo.score;
                            user.isMe = true;
                            infos.push(user);
                        }
                        // 按分数从大到小排序
                        infos.sort(function (a, b) {
                            return b.score - a.score;
                        });
                        shows = utils.getNearInfos(infos, myInfo);
                        length = shows.length - 1;
                        startX = [-10, -130, -220][length];
                        for (i in shows) {
                            item = new SettleItem(shows[i]);
                            item.x = startX + Number(i) * 170;
                            item.y = 66;
                            self.addChild(item);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return SettleList;
}(egret.DisplayObjectContainer));
__reflect(SettleList.prototype, "SettleList");
var WXUtils = (function () {
    function WXUtils() {
    }
    // public static userInfo: UserGameData;					// 玩家信息数据
    /**
     * 获取玩家的信息
     */
    WXUtils.getUserInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self;
            return __generator(this, function (_a) {
                self = WXUtils;
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var info = self.$userInfo;
                        info ? resolve(info) :
                            wx.getUserInfo({
                                openIdList: ['selfOpenId'],
                                lang: 'zh_CN',
                                success: function (res) {
                                    // 注意这里的openid假的，不知为何
                                    info = self.$userInfo = res.data[0];
                                    info.nickname = info.nickName; // 注意个人信息这个坑
                                    resolve(info);
                                },
                                fail: function (res) {
                                    reject();
                                }
                            });
                    })];
            });
        });
    };
    /**
     * 获取玩家游戏数据
     */
    WXUtils.getUserKVData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self;
            return __generator(this, function (_a) {
                self = WXUtils;
                return [2 /*return*/, new Promise(function (resolve) {
                        var data = self.$userKVData;
                        data ? resolve(data) :
                            wx.getUserCloudStorage({
                                keyList: self.keyList,
                                success: function (res) {
                                    resolve(res.KVDataList);
                                },
                                fail: function (res) {
                                    resolve();
                                }
                            });
                    })];
            });
        });
    };
    /**
     * 更新玩家游戏数据
     */
    WXUtils.updateUserKVData = function (nInfo) {
        var self = WXUtils;
        var oData = self.$userKVData;
        var notice = Dispatch.notice;
        var message = 'updateSocre';
        if (nInfo && oData) {
            var oInfo = self.getInfoByKVData(oData);
            // 存入新数据
            if (oInfo.score < nInfo.score) {
                var kvData = self.getKVDataByInfo(nInfo);
                ;
                self.setUserKVData(kvData);
                oInfo = nInfo;
            }
            notice(message, oInfo);
        }
        else {
            self.getUserKVData().then(function (data) {
                var info = data && self.getInfoByKVData(data);
                if (nInfo) {
                    if (!info || info.score < nInfo.score) {
                        self.setUserKVData(self.getKVDataByInfo(nInfo));
                        info = nInfo;
                    }
                }
                notice(message, info);
            });
        }
    };
    /**
     * 获取玩家游戏数据
     */
    WXUtils.getGameInfos = function (attr, keyList, resolve, shareTicket) {
        var self = this;
        var call = wx[attr];
        if (call) {
            // 获取缓存内的数据
            var infos_1 = WXUtils.m_pInfos;
            var cache = infos_1[attr];
            if (cache)
                resolve(cache);
            else {
                var obj = {
                    keyList: keyList,
                    success: function (res) {
                        console.log(res);
                        var nData = infos_1[attr] = self.initGameDatas(res.data);
                        resolve(nData);
                    },
                    fail: function (res) {
                        self.$getUserInfo(resolve);
                    }
                };
                shareTicket && (obj.shareTicket = shareTicket);
                call(obj);
            }
        }
        else
            call([]);
    };
    /**
     * 获取好友数据
     */
    WXUtils.getFriendInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self;
            return __generator(this, function (_a) {
                self = WXUtils;
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        self.getGameInfos('getFriendCloudStorage', self.keyList, resolve);
                    })];
            });
        });
    };
    /**
     * 获取自己的数据
     */
    WXUtils.$getUserInfo = function (resolve) {
        var self = WXUtils;
        Promise.all([self.getUserInfo(), self.getUserKVData()]).then(function (array) {
            var infos = [];
            if (array) {
                var info = array[0];
                info.KVDataList = array[1];
                infos[0] = info;
            }
            resolve(infos);
        });
    };
    /**
     * 保存玩家游戏数据
     */
    WXUtils.setUserKVData = function (data) {
        return new Promise(function (resolve) {
            WXUtils.$userKVData = data;
            wx.setUserCloudStorage({
                KVDataList: data,
                success: function () {
                    resolve(true);
                },
                fail: function () {
                    resolve(false);
                }
            });
        });
    };
    /**
     * 获取KV数据
     */
    WXUtils.getInfoByKVData = function (datas) {
        var info = { score: 0 };
        for (var i in datas) {
            var data = datas[i];
            info[data.key] = data.value;
        }
        return info;
    };
    /**
     * 获取游戏数据
     */
    WXUtils.getKVDataByInfo = function (info) {
        var datas = [];
        var getStr = function (v) {
            if (typeof v === 'object')
                v = JSON.parse(v);
            return v + '';
        };
        for (var i in info) {
            var data = {};
            data.key = i;
            data.value = getStr(info[i]);
            datas.push(data);
        }
        return datas;
    };
    /**
     * 检测info是否是我的数据
     */
    WXUtils.isMyInfo = function (info) {
        var myInfo = WXUtils.$userInfo;
        return myInfo.avatarUrl == info.avatarUrl
            && myInfo.nickName == info.nickname; // 小心大小写
    };
    /**
     * 清除数据
     */
    WXUtils.clearData = function () {
        WXUtils.m_pInfos = {};
    };
    /**
     * 获取相近的数据（返回的列表会加上rank字段）
     * @param infos 列表
     * @param myInfo 我的数据，保证myInfo在列表中
     * @param maxNum 最多找几个人，奇数
     */
    WXUtils.getNearInfos = function (infos, myInfo, maxNum) {
        if (maxNum === void 0) { maxNum = 3; }
        // 根据自己的位置找出附近三人
        var myIndex = infos.indexOf(myInfo), len = infos.length;
        var startI;
        // 自己排名最前
        if (myIndex == 0)
            startI = myIndex;
        else if (myIndex == len - 1)
            startI = myIndex - maxNum + 1;
        else
            startI = myIndex - (maxNum / 2 | 0);
        if (startI < 0)
            startI = 0;
        // 限制最多拿三个
        var shows = [];
        for (var i = startI, len_1 = infos.length, j = 0; i < len_1 && j < maxNum; i++, j++) {
            var info = infos[i];
            info.rank = i + 1;
            shows.push(info);
        }
        return shows;
    };
    // 根据游戏自己的字段进行修改
    /**
     * 初始化需要的数据
     */
    WXUtils.initGameDatas = function (datas) {
        var self = WXUtils;
        var nData = [];
        for (var i in datas) {
            var item = datas[i];
            if (item.openid) {
                var info = self.getInfoByKVData(item.KVDataList);
                // 初始化分数字段
                item.score = Number(info.score);
                item.time = Number(info.time);
                nData.push(item);
            }
        }
        return nData;
    };
    WXUtils.keyList = ['score', 'time']; // 游戏存放数据Key值
    WXUtils.m_pInfos = {}; // 存放玩家好友、群数据
    return WXUtils;
}());
__reflect(WXUtils.prototype, "WXUtils");
;window.Main = Main;