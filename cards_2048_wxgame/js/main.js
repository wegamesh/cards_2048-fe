var egret = window.egret;var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var lie;
(function (lie) {
    /**
     * 自带清理方法的控件
     */
    var UIComponent = (function (_super) {
        __extends(UIComponent, _super);
        function UIComponent() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        UIComponent.prototype.childrenCreated = function () {
            this.onCreate();
        };
        UIComponent.prototype.$onRemoveFromStage = function () {
            var self = this;
            self.isDestroy = true;
            lie.EventUtils.removeEventListeners(self);
            self.onDestroy();
            _super.prototype.$onRemoveFromStage.call(this);
        };
        /**
         * 控件进入场景时回调
         */
        UIComponent.prototype.onCreate = function () {
        };
        /**
         * 控件离开场景时回调——onRemoveFromStage实际意义上应该是私有函数，如果没有
         * 写上super.XXX，它没办法有效移除，为避免出错，才有该函数存在
         */
        UIComponent.prototype.onDestroy = function () {
        };
        /**
         * 从父控件移除
         */
        UIComponent.prototype.removeFromParent = function () {
            var self = this;
            var parent = self.parent;
            parent && parent.removeChild(self);
        };
        /**
         * 层级变化——被覆盖，AppViews
         */
        UIComponent.prototype.onHide = function () {
        };
        /**
         * 层级变化——显示，AppViews
         */
        UIComponent.prototype.onShow = function () {
        };
        return UIComponent;
    }(eui.Component));
    lie.UIComponent = UIComponent;
    __reflect(UIComponent.prototype, "lie.UIComponent");
    /**
     * 锚点在中心的图片
     */
    var CenImage = (function (_super) {
        __extends(CenImage, _super);
        function CenImage() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CenImage.prototype.$setTexture = function (texture) {
            var width = 0, height = 0;
            if (texture) {
                width = texture.textureWidth;
                height = texture.textureHeight;
            }
            this.anchorOffsetX = width / 2;
            this.anchorOffsetY = height / 2;
            return _super.prototype.$setTexture.call(this, texture);
        };
        return CenImage;
    }(eui.Image));
    lie.CenImage = CenImage;
    __reflect(CenImage.prototype, "lie.CenImage");
    /**
     * App的视图
     */
    var AppComponent = (function (_super) {
        __extends(AppComponent, _super);
        function AppComponent(skinName) {
            var _this = _super.call(this) || this;
            skinName && (_this.skinName = AppConfig.getSkin(skinName));
            return _this;
        }
        return AppComponent;
    }(UIComponent));
    lie.AppComponent = AppComponent;
    __reflect(AppComponent.prototype, "lie.AppComponent");
    /**
     * 模仿AppComponent的构造，其余模仿UIComponent
     */
    var AppRenderer = (function (_super) {
        __extends(AppRenderer, _super);
        function AppRenderer(skinName) {
            var _this = _super.call(this) || this;
            skinName && (_this.skinName = AppConfig.getSkin(skinName));
            return _this;
        }
        AppRenderer.prototype.childrenCreated = function () {
            this.onCreate();
        };
        AppRenderer.prototype.$onRemoveFromStage = function () {
            var self = this;
            self.isDestroy = true;
            lie.EventUtils.removeEventListeners(self);
            self.onDestroy();
            _super.prototype.$onRemoveFromStage.call(this);
        };
        /**
         * 控件进入场景时回调
         */
        AppRenderer.prototype.onCreate = function () {
        };
        /**
         * 控件离开场景时回调——onRemoveFromStage实际意义上应该是私有函数，如果没有
         * 写上super.XXX，它没办法有效移除，为避免出错，才有该函数存在
         */
        AppRenderer.prototype.onDestroy = function () {
        };
        return AppRenderer;
    }(eui.ItemRenderer));
    lie.AppRenderer = AppRenderer;
    __reflect(AppRenderer.prototype, "lie.AppRenderer");
})(lie || (lie = {}));
var lie;
(function (lie) {
    var app; // 存放单例
    /**
     * 层级容器
     */
    var Container = (function (_super) {
        __extends(Container, _super);
        function Container() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         * 移除子控件触发
         */
        Container.prototype.$doRemoveChild = function (index, notifyListeners) {
            var target = _super.prototype.$doRemoveChild.call(this, index, notifyListeners);
            if (target) {
                // 当前层级最顶层
                var last = AppViews.getTopComponent(this);
                last && last.onShow();
            }
            return target;
        };
        return Container;
    }(egret.DisplayObjectContainer));
    __reflect(Container.prototype, "Container");
    /**
     * 应用的视图管理类
     */
    var AppViews = (function () {
        function AppViews() {
        }
        /**
         * 初始化
         */
        AppViews.prototype.init = function () {
            var self = this;
            AppViews.stage.removeChildren();
            self.$panelLevel = self.addLevel('panel');
            self.$dialogLevel = self.addLevel('dialog');
            self.$topLevel = self.addLevel('top', true);
            //如果从其他途径登陆,根据业务显示页面(暂时设置延迟0.5秒,后续根据情况再调整)
            setTimeout(function () {
                var datas = AppViews.loginData;
                if (datas) {
                    // if (global.guideInfo.isEnd) {
                    // } else {
                    // 	wx.showToast({
                    // 		title: '请先完成新手引导再赠送食物哦',
                    // 		icon: 'none'
                    // 	})
                    // }
                    self.excuteCall(datas[0], datas[1]);
                    AppViews.loginData = null;
                }
            }, 500);
        };
        Object.defineProperty(AppViews.prototype, "panelLevel", {
            /**
             * 获取面板层
             */
            get: function () {
                return this.$panelLevel;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "dialogLevel", {
            /**
             * 获取对话框层
             */
            get: function () {
                return this.$dialogLevel;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "topLevel", {
            /**
             * 获取顶层
             */
            get: function () {
                return this.$topLevel;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 添加层
         * @param name 名称
         */
        AppViews.prototype.addLevel = function (name, isOld) {
            var container = isOld ? new Container() : new Container();
            container.name = name;
            AppViews.stage.addChild(container);
            return container;
        };
        /**
         * 获取层
         */
        AppViews.prototype.getLevel = function (name) {
            return AppViews.stage.getChildByName(name);
        };
        Object.defineProperty(AppViews.prototype, "curPanel", {
            /**
             * 获取最顶层
             */
            get: function () {
                var panel = this.panelLevel;
                return panel.$children[panel.numChildren - 1];
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews.prototype, "curDialog", {
            /**
             * 获取最顶对话框
             */
            get: function () {
                var panel = this.dialogLevel;
                return panel.$children[panel.numChildren - 1];
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 获取视图管理类单例
         */
        AppViews.app = function () {
            if (!app) {
                app = new AppViews;
                app.init();
            }
            return app;
        };
        Object.defineProperty(AppViews, "stage", {
            /**
             * 获取舞台
             */
            get: function () {
                return egret.sys.$TempStage; // egret.MainContext.instance.stage
            },
            enumerable: true,
            configurable: true
        });
        // 重点，层及控制管理体现
        /**
         * 获取当前面板的最顶元素（该面板没有则取下一面板）
         */
        AppViews.getTopComponent = function (cont) {
            if (cont.numChildren == 0) {
                var parent_1 = cont.parent;
                var index = parent_1.getChildIndex(cont);
                cont = null;
                for (var i = index - 1; i >= 0; i--) {
                    cont = parent_1.getChildAt(i);
                    if (cont.numChildren)
                        break;
                }
                if (!cont)
                    return null;
            }
            return cont.$children[cont.numChildren - 1];
        };
        /**
         * 获取最顶的控件——对话框之下
         */
        AppViews.getTopCommont = function () {
            return AppViews.getTopComponent(AppViews.app().dialogLevel);
        };
        Object.defineProperty(AppViews, "curPanel", {
            /**
             * 获取当前面板
             */
            get: function () {
                return app && app.curPanel;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AppViews, "curDialog", {
            /**
             * 获取当前对话框
             */
            get: function () {
                return app && app.curDialog;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * 插入一个控件，并居中
         * @param attr AppViews对象的属性名
         * @param clzz 需要添加的对象类
         */
        AppViews.pushChild = function (attr, clzz, para1, para2, para3) {
            var self = AppViews;
            var child = new clzz(para1, para2, para3);
            var container = self.app()[attr];
            var last = self.getTopComponent(container); // 当前层级最顶层
            // 隐藏
            last && last.onHide();
            // 控件动画
            // child.scaleX = 0.6
            // child.scaleY = 0.6
            // child.alpha = 0
            // child.an
            child.anchorOffsetX = child.width / 2;
            child.anchorOffsetY = child.height / 2;
            // var tw = egret.Tween.get(child)
            // tw.to({
            // 	scaleX: 1,
            // 	scaleY: 1,
            // 	alpha: 1
            // }, 300, egret.Ease.backOut).call(() => {
            // 	// if(child.setObscureAlpha){
            // 	// 	child.setObscureAlpha(1)
            // 	// }
            // })
            container.addChild(child);
            self.setInCenter(child);
            return child;
        };
        /**
         * 移除层里的控件
         * @param attr AppViews对象的属性名
         * @param clzz 需要移除的对象类
         */
        AppViews.removeChild = function (attr, clzz) {
            var panel = AppViews.app()[attr];
            // for (let i = 0, num = panel.numChildren; i < num; i++) {
            // 	let child = panel.getChildAt(i);
            // 	if (child instanceof clzz){
            // 		// let tw = egret.Tween.get(child)
            // 		// tw.to({
            // 		// 	scaleX: 0.5,
            // 		// 	scaleY: 0.5,
            // 		// 	alpha: 0
            // 		// },100).call(()=>{
            // 		panel.removeChildAt(i);
            // 		// })
            // 	}
            // }
            var child = panel.$children.find(function (item) {
                return item instanceof clzz;
            });
            if (child) {
                panel.removeChild(child);
            }
            // panel.$children.forEach( item => {
            // 	if(item instanceof clzz){
            // 		panel.removeChild(item)
            // 	}
            // })
        };
        /**
         * 将子控件设置在父控件中心点
         */
        AppViews.setInCenter = function (child) {
            var stage = AppViews.stage;
            child.x = stage.stageWidth / 2;
            child.y = stage.stageHeight / 2;
        };
        /**
         * 新建一个面板并放入
         */
        AppViews.pushPanel = function (clzz, para1, para2) {
            return AppViews.pushChild('panelLevel', clzz, para1, para2);
        };
        /**
         * 新建一个对话框并放入
         */
        AppViews.pushDialog = function (clzz, para1, para2, para3) {
            return AppViews.pushChild('dialogLevel', clzz, para1, para2, para3);
        };
        /**
         * 新建一个对话框并放入
         */
        AppViews.pushTop = function (clzz, para1, para2) {
            return AppViews.pushChild('topLevel', clzz, para1, para2);
        };
        /**
         * 移除面板
         */
        AppViews.removePanel = function (clzz) {
            AppViews.removeChild('panelLevel', clzz);
        };
        /**
         * 移除对话框
         */
        AppViews.removeDialog = function (clzz) {
            AppViews.removeChild('dialogLevel', clzz);
        };
        AppViews.prototype.excuteCall = function (call, thisObj) {
            call && call.call(thisObj);
        };
        /**
         * 添加初始化后回调，如果已经初始化，则立即执行
         */
        AppViews.addInitCall = function (call, thisObj) {
            if (app) {
                AppViews.removePanelAndDialog().then(function (res) {
                    app.excuteCall(call, thisObj);
                });
            }
            else {
                AppViews.loginData = [call, thisObj];
            }
        };
        /**
         * 进入游戏移除除主界面外的其他页面
         */
        AppViews.removePanelAndDialog = function () {
            return new Promise(function (resolve, reject) {
                var panelNum = AppViews.app().panelLevel.numChildren;
                var dialogNum = AppViews.app().dialogLevel.numChildren;
                //移除所有dialog
                for (var i = 0; i < dialogNum; i++) {
                    var dialog = AppViews.app().dialogLevel.getChildAt(i);
                    AppViews.app().dialogLevel.removeChild(dialog);
                }
                //移除除主界面以外的panel,从1开始
                if (panelNum > 1) {
                    for (var i = 1; i < panelNum; i++) {
                        var panel = AppViews.app().panelLevel.getChildAt(i);
                        AppViews.app().panelLevel.removeChild(panel);
                    }
                }
                console.log('remove all child');
                resolve();
            });
        };
        /**
         * 界面上是否拥有该类
         */
        AppViews.hasView = function (clzz, cont) {
            for (var i = 0, num = cont.numChildren; i < num; i++) {
                var child = cont.getChildAt(i);
                if (child instanceof clzz)
                    return true;
            }
            return false;
        };
        return AppViews;
    }());
    lie.AppViews = AppViews;
    __reflect(AppViews.prototype, "lie.AppViews");
})(lie || (lie = {}));
var lie;
(function (lie) {
    var single;
    /**
     * 对话框，注意onDestroy加了点东西
     */
    var Dialog = (function (_super) {
        __extends(Dialog, _super);
        function Dialog() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Dialog.prototype.onCreate = function () {
            this.initObscure();
        };
        Dialog.prototype.$onRemoveFromStage = function () {
            _super.prototype.$onRemoveFromStage.call(this);
            var self = this;
            var call = self.m_pCall;
            if (call) {
                call(self.m_pThis);
                self.m_pCall = self.m_pThis = null;
            }
        };
        /**
         * 初始化朦层
         */
        Dialog.prototype.initObscure = function () {
            var _this = this;
            var self = this;
            var stage = self.stage;
            var rect = self.obscure = new eui.Rect;
            rect.width = stage.stageWidth;
            rect.height = stage.stageHeight;
            rect.horizontalCenter = rect.verticalCenter = 0;
            // rect.addEventListener(egret.TouchEvent.TOUCH_TAP, ()=>{
            // 	this.removeFromParent()
            // }, this)
            self.addChildAt(rect, 0);
            self.setObscureAlpha(0.5);
            rect.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
                _this.removeFromParent();
            }, this);
        };
        /**
         * 设置朦层的透明度
         */
        Dialog.prototype.setObscureAlpha = function (alpha) {
            this.obscure.alpha = alpha;
        };
        /**
         * 添加朦层监听，点击关闭
         */
        Dialog.prototype.addOCloseEvent = function () {
            var self = this;
            lie.EventUtils.addTouchTapListener(self.obscure, self.onClose, self);
        };
        /**
         * 添加关闭回调
         */
        Dialog.prototype.addCloseCall = function (call, thisObj) {
            var self = this;
            self.m_pCall = call;
            self.m_pThis = thisObj;
        };
        /**
         * 关闭窗口
         */
        Dialog.prototype.onClose = function (event) {
            event.stopImmediatePropagation();
            this.removeFromParent();
        };
        /**
         * 显示单一的对话框，对话框样式固定
         * @param 内容
         * @param 点击确定的回调
         * @param 回调所属对象
         */
        Dialog.showSingleDialog = function (content, call, thisObj) {
            if (!single) {
                single = lie.AppViews.pushDialog(SignleDialog);
                single.skinName = AppConfig.getSkin('SingleDialogSkin');
            }
            single.setText(content);
            single.addCall(call, thisObj);
            return single;
        };
        /**
         * 隐藏单一对话框
         */
        Dialog.hideSingleDialog = function () {
            if (single) {
                single.visible = false;
                single.clearCall();
            }
        };
        /**
         * 移除单一对话框
         */
        Dialog.removeSingDialog = function () {
            var r = single;
            if (r) {
                single = null;
                r.removeFromParent();
            }
        };
        /**
         * 运行循环
         */
        Dialog.runTurn = function () {
            var self = Dialog;
            var clzz = self.$turnDialogs.shift();
            if (clzz) {
                var param = self.$turnParams.shift();
                lie.AppViews.pushDialog(clzz, param).
                    addCloseCall(self.runTurn);
            }
            else
                self.$isShowTurn = false;
        };
        /**
         * 轮流显示对话框
         * @param dialogs 需要现实的对话框类
         * @param params 对话框类构造函数参数，注意长度一致，注意每隔类的参数只支持一个，多个参数的请修改该类
         */
        Dialog.showDialogsInTurn = function (dialogs, params) {
            var length = dialogs.length;
            if (length) {
                var self_1 = Dialog;
                var turns = self_1.$turnDialogs;
                if (params == void 0)
                    params = lie.Utils.memset(length, void 0);
                else
                    params.length = length;
                self_1.$turnDialogs = turns.concat(dialogs);
                self_1.$turnParams = self_1.$turnParams.concat(params);
                if (!self_1.$isShowTurn) {
                    self_1.$isShowTurn = true;
                    self_1.runTurn();
                }
            }
        };
        /**
         * 放对话框进入轮流
         * @param dialog
         * @param param 构造参数，仅支持一个
         * @param endDialog 将dialog放在endDialog的后面，不存在则默认最后
         */
        Dialog.pushDialogInTurn = function (dialog, param, endDialog) {
            var self = Dialog;
            var turns = self.$turnDialogs;
            var index = turns.length;
            if (endDialog) {
                for (var i = 0, len = turns.length; i < len; i++) {
                    if (endDialog == turns[i]) {
                        index = i;
                        break;
                    }
                }
            }
            turns.splice(index, 0, dialog);
            self.$turnParams.splice(index, 0, param);
        };
        /**
         * 在固定位置插入对话框
         * @param dialog
         * @param param
         * @param index 下标
         */
        Dialog.insertDialogInTuen = function (dialog, param, index) {
            var self = Dialog;
            var turns = self.$turnDialogs;
            if (isNaN(index))
                index = turns.length;
            self.$turnDialogs.splice(index, 0, dialog);
            self.$turnParams.splice(index, 0, param);
            !self.$isShowTurn && self.runTurn();
        };
        //// 轮流对话框
        Dialog.$turnDialogs = [];
        Dialog.$turnParams = [];
        return Dialog;
    }(lie.AppComponent));
    lie.Dialog = Dialog;
    __reflect(Dialog.prototype, "lie.Dialog");
    /**
     * 只允许弹出一个的对话框，也可以称为通用对话框
     */
    var SignleDialog = (function (_super) {
        __extends(SignleDialog, _super);
        function SignleDialog() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        SignleDialog.prototype.onCreate = function () {
            _super.prototype.onCreate.call(this);
            var self = this;
            lie.EventUtils.addTouchTapScaleListener(self.m_btnOk, self.removeFromParent, self);
        };
        SignleDialog.prototype.onDestroy = function () {
            _super.prototype.onDestroy.call(this);
            var self = this;
            lie.EventUtils.removeEventListener(self.m_btnOk);
            // 执行回调
            var call = self.$call;
            if (call) {
                call.call(self.$thisObje);
                self.clearCall();
            }
        };
        /**
         * 设置文本
         */
        SignleDialog.prototype.setText = function (text) {
            this.m_lblText.text = text;
        };
        /**
         * 添加监听
         */
        SignleDialog.prototype.addCall = function (call, thisObje) {
            var self = this;
            self.$call = call;
            self.$thisObje = thisObje;
        };
        /**
         * 清除回调
         */
        SignleDialog.prototype.clearCall = function () {
            var self = this;
            self.$call = self.$thisObje = null;
        };
        return SignleDialog;
    }(Dialog));
    __reflect(SignleDialog.prototype, "SignleDialog");
})(lie || (lie = {}));
var lie;
(function (lie) {
    var $canvas; // 不存在的变量别用var
    /**
     * 微信平台工具类——微信工具除登录和获取用户信息
     */
    var WXUtils = (function () {
        function WXUtils() {
            this.type = 'wxgame';
        }
        /**
         * 游戏进入初始化——调用一次即可
         */
        WXUtils.prototype.init = function () {
            console.log('初始化');
            var self = this;
            // 分享携带ShareTicket
            wx.updateShareMenu({
                withShareTicket: true
            });
            wx.onShow(self.onShow);
            wx.onHide(self.onHide);
            // 分享
            wx.onShareAppMessage(function () {
                // 用户点击了“转发”按钮
                return self.getGlobalShareInfo();
            });
            wx.showShareMenu(); // 自动显示
            ; // 不需要任何操作
            lie.PlatformUtils.login();
        };
        /**
         * 微信onShow
         */
        WXUtils.prototype.onShow = function (res) {
            // 分享打开的链接
            var isShare = res.query.share == 'true';
            var type = res.query.type || 0;
            var ticket = isShare ? (res.shareTicket || '1') : void 0;
            //AppMsg.notice(MsgId.onShow, ticket);
        };
        /**
         * 显示广告
         */
        WXUtils.prototype.showAds = function (type) {
            if (type === void 0) { type = 0; }
            var self = this;
            // let clzz = UserData.gameInfo;
            return new Promise(function (resolve, reject) {
                // reject()
                if (!self.isShowAds) {
                    self.isShowAds = true;
                    // let turn = !clzz.Location.IsCore;
                    var sdk_1 = self.getSystemInfoSync().SDKVersion;
                    // 转换回调
                    var turnCall_1 = function (str) {
                        self.isShowAds = false;
                        // if (turn)
                        // 	reject(str);
                        // else {
                        self.showToast(str);
                        resolve(false);
                        // }
                    };
                    // 字符串
                    if (sdk_1 >= '2.0.4') {
                        var videoAd_1 = wx.createRewardedVideoAd({
                            adUnitId: adUnitIds[type]
                        });
                        videoAd_1.load().then(function () {
                            wx.hideLoading();
                            // 监听回调
                            var call = videoAd_1.onClose(function (res) {
                                self.isShowAds = false;
                                videoAd_1.offClose(call);
                                console.log('sdk: ', sdk_1);
                                // if (clzz.Location && clzz.Location.IsCore && sdk >= '2.1.0' && res) {
                                // 	console.log('isEnded', res.isEnded)
                                // 	if (res.isEnded) {
                                // 		resolve(true);
                                // 	} else {
                                // 		self.showToast('广告未看完无法获得奖励');
                                // 		resolve(false);
                                // 	}
                                // } else {
                                if (res.isEnded) {
                                    resolve(true);
                                }
                                // }
                            });
                            videoAd_1.show();
                        }).catch(function (err) {
                            turnCall_1('广告播放异常');
                        });
                    }
                    else {
                        turnCall_1('该微信版本暂不支持广告播放，请升级！');
                    }
                }
            });
        };
        /**
         * 显示banner广告
         */
        WXUtils.prototype.showBannerAds = function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                var info = self.getSystemInfoSync();
                var sdk = info.SDKVersion;
                // 字符串
                if (sdk >= '2.0.4') {
                    var stage = lie.AppViews.stage;
                    var sWidth = stage.stageWidth;
                    var floor = Math.floor;
                    var width = 750, height = 250;
                    var x = (sWidth - width) / 2;
                    var y = stage.stageHeight - height;
                    var scaleWidth = info.screenWidth / sWidth;
                    // let style = {
                    // 	left: floor(x * scaleWidth),
                    // 	top: floor(y * scaleWidth)
                    // 	// height: floor(height * scaleWidth)
                    // };
                    var style = {
                        left: 0,
                        top: floor(y * scaleWidth),
                        width: floor(width * scaleWidth),
                        height: floor(height * scaleWidth)
                    };
                    var bannerAd_1 = wx.createBannerAd({
                        adUnitId: adUnitIds[1],
                        style: style
                    });
                    bannerAd_1.onResize && bannerAd_1.onResize(function (e) {
                        bannerAd_1.style && (bannerAd_1.style.top = wx.getSystemInfoSync().windowHeight - e.height);
                    });
                    bannerAd_1.show();
                    self.bannerAd = bannerAd_1;
                }
            });
        };
        /**
         * 销毁banner广告
         */
        WXUtils.prototype.hideBannerAds = function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                var sdk = self.getSystemInfoSync().SDKVersion;
                var show = lie.Dialog.showSingleDialog;
                // 字符串
                if (sdk >= '2.0.4') {
                    self.bannerAd.destroy();
                }
            });
        };
        /**
         * 微信onHide
         */
        WXUtils.prototype.onHide = function (res) {
            //AppMsg.notice(MsgId.onHide);
        };
        /**
         * 获取开放域
         */
        WXUtils.prototype.getShareCanvas = function () {
            var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
            var texture = new egret.Texture();
            bitmapdata.$deleteSource = false;
            texture.bitmapData = bitmapdata;
            return new egret.Bitmap(texture);
        };
        /**
         * 获取全局分享数据
         */
        WXUtils.prototype.getGlobalShareInfo = function () {
            return {
                title: '测试',
                imageUrl: 'resource/assets/share/wx_share_join.png',
                query: 'share=true'
            };
        };
        /**
         * 系统信息
         */
        WXUtils.prototype.getSystemInfoSync = function () {
            return wx.getSystemInfoSync();
        };
        /**
         * 直接弹出分享
         */
        WXUtils.prototype.showShare = function () {
            var self = this;
            return new Promise(function (resolve, reject) {
                var obj = self.getGlobalShareInfo();
                obj.success = function (res) {
                    resolve(true);
                };
                obj.fail = function (res) {
                    resolve(false);
                };
                wx.shareAppMessage(obj);
            });
        };
        /**
         * app文字提示
         */
        WXUtils.prototype.showToast = function (msg) {
            wx.showToast({
                title: msg,
                icon: 'none'
            });
        };
        /**
         * 获取系统描述
         */
        WXUtils.prototype.getSystemInfo = function () {
            return wx.getSystemInfoSync().system;
        };
        /**
         * 设置用户数据
         */
        WXUtils.prototype.setUserInfo = function (map) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    return [2 /*return*/, new Promise(function (resolve, reject) {
                            //let clzz = AppConfig;
                            var clzz;
                            var datas = [];
                            var setCS = wx.setUserCloudStorage;
                            var isObj = function (v) {
                                return typeof v === 'object';
                            };
                            for (var i in map) {
                                var v = map[i];
                                if (isObj(v))
                                    v = JSON.parse(v);
                                datas.push({ key: i, value: v + '' });
                            }
                            // 低版本不支持
                            setCS ? setCS({
                                KVDataList: datas,
                                success: function () {
                                    resolve(true);
                                },
                                fail: function () {
                                    resolve(false);
                                }
                            }) : reject();
                        })];
                });
            });
        };
        /**
         * 由主域向开放域推送消息
         * @param action 消息标志
         * @param data 消息参数
         */
        WXUtils.prototype.postMessage = function (action, data) {
            var msg = {};
            msg.action = action;
            msg.data = data;
            // console.log("抛消息");
            console.log("发送消息");
            console.log(msg);
            wx.postMessage(msg);
        };
        /**
         * 获取玩家信息
         */
        WXUtils.prototype.getUserInfo = function () {
            return new Promise(function (resolve) {
                // let clzz = AppConfig;
                // let info = clzz.userInfo;
                // info ? resolve(info) :
                // 	platform.getUserInfo().then(function (res) {
                // 		clzz.userInfo = res;
                // 		resolve(res);
                // 	});
            });
        };
        /**
         * loading
         */
        WXUtils.prototype.showLoading = function (title, mask) {
            if (mask === void 0) { mask = false; }
            wx.showLoading({
                title: title || '',
                mask: mask
            });
        };
        WXUtils.prototype.hideLoading = function () {
            wx.hideLoading();
        };
        /**
         * 振动
         * 返回值 。1/2/3  成功/失败/结束
         */
        WXUtils.prototype.vibrateShort = function () {
            return new Promise(function (resolve, reject) {
                wx.vibrateShort({
                    success: function () {
                        resolve(1);
                    },
                    fail: function () {
                        resolve(2);
                    },
                    complete: function () {
                        resolve(3);
                    }
                });
            });
        };
        /**
         * 小程序跳转
         * @param appid
         * @param path
         * @param imgUrl
        */
        // public static readonly navigateAppId = 'wx53b1b4ab6fa0d044'
        // public static readonly navigatePath = 'pages/index/index?from=koudaizhu'
        WXUtils.prototype.navigate2Program = function (appid, path, imgUrl) {
            return new Promise(function (resolve, reject) {
                var navigate = wx.navigateToMiniProgram;
                if (navigate) {
                    console.log('appId', appid);
                    console.log('path', path);
                    navigate({
                        appId: appid,
                        path: path,
                        success: function (res) {
                            console.log('跳转成功', res);
                        },
                        fail: function (res) {
                            console.log('跳转失败', res);
                        }
                    });
                }
                else {
                    wx.previewImage({
                        current: imgUrl,
                        urls: [imgUrl]
                    });
                }
            });
        };
        return WXUtils;
    }());
    lie.WXUtils = WXUtils;
    __reflect(WXUtils.prototype, "lie.WXUtils", ["lie.ITFPFUtils"]);
    // 视频ID，分别是：激励广告
    var adUnitIds = [
        'adunit-1a513703413fe167',
        'adunit-7d2a4278d6d2c537'
    ];
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 工具类，存放一些常用的且不好归类的方法
     */
    var Utils = (function () {
        function Utils() {
        }
        // 工具私有变量，请勿乱动
        /**
         * 初始化一个长度为length，值全为value的数组
         * 注意，由于数组公用一个value，因此，value适用于基础类型，对于对象类的，请用memset2方法
         */
        Utils.memset = function (length, value) {
            return Array.apply(null, Array(length)).map(function () { return value; });
        };
        /**
         * 初始化一个长度为length，值通过getValue函数获取，注意getValue第一个参数是没用，第二个参数是当前数组的下标，即你返回的数据将存放在数组的该下标
         */
        Utils.memset2 = function (length, getValue) {
            return Array.apply(null, Array(length)).map(getValue);
        };
        /**
         * 创建一个定时器
         * @param listener 定时回调
         * @param thisObject 回调所属对象
         * @param second 回调间隔，单位秒，默认一秒
         * @param repeat 循环次数，默认一直重复
         */
        Utils.createTimer = function (listener, thisObject, second, repeat) {
            if (second === void 0) { second = 1; }
            var timer = new egret.Timer(second * 1000, repeat);
            timer.addEventListener(egret.TimerEvent.TIMER, listener, thisObject);
            timer.start();
            return timer;
        };
        /**
         * 移除一个定时器
         * @param timer 被移除的定时器
         * @param listener 监听函数
         * @param thisObject 函数所属对象
         */
        Utils.removeTimer = function (timer, listener, thisObject) {
            timer.stop();
            timer.removeEventListener(egret.TimerEvent.TIMER, listener, thisObject);
        };
        /**
         * 获取网页参数
         */
        Utils.getQueryString = function (name) {
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var ret = window.location.search.substr(1).match(reg);
            return ret ? decodeURIComponent(ret[2]) : '';
        };
        /**
         * 获取cr1位于cr0的方向
         * 注：0为上，顺时针开始数，共八个方向
         */
        Utils.getDistance = function (col0, row0, col1, row1) {
            var row, col, c = 2, xs = 1;
            if (row0 < row1)
                row = -1;
            else if (row0 > row1)
                row = 1;
            else
                row = 0;
            if (col0 > col1) {
                c = 6;
                xs = -1;
            }
            else if (col0 == col1)
                xs = 2;
            return c - row * xs;
        };
        /**
         * 自定义格式化字符串
         * @param reg 正则
         * @param str 字符串
         * @param args 填充参数
         */
        Utils.formatStringReg = function (reg, str, args) {
            for (var i in args) {
                var arg = args[i];
                if (reg.test(str))
                    str = str.replace(reg, args[i]);
                else
                    break;
            }
            return str;
        };
        /**
         * 格式化字符串
         */
        Utils.formatString = function (str) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            str = str.replace(/%%/g, '%'); // 有些识别问题出现两个%号
            return Utils.formatStringReg(/%d|%s/i, str, args); // 忽略大小写
        };
        /**
         * 偏移整型数组
         * @param array 需要偏移的数组
         * @param offset 偏移量，对应array长度，没有值时默认0
         */
        Utils.offsetArray = function (array) {
            var offset = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                offset[_i - 1] = arguments[_i];
            }
            for (var i in array)
                array[i] += (offset[i] || 0);
            return array;
        };
        /**
         *为组件添加朦层
         *
        */
        Utils.setObscure = function (width, height) {
            var rect = new eui.Rect;
            rect.name = 'obscure';
            rect.x = 0;
            rect.y = 0;
            rect.width = width;
            rect.height = height - 3;
            rect.fillColor = 0x000000;
            rect.alpha = 0.3;
            rect.strokeAlpha = 0;
            rect.ellipseHeight = 44;
            rect.ellipseWidth = 44;
            return rect;
        };
        /**
         * 添加矩形遮罩
         */
        Utils.addRectMask = function (bitmap) {
            var shape = new egret.Shape;
            var graphics = shape.graphics;
            shape.x = bitmap.x + 2;
            shape.y = bitmap.y + 2;
            bitmap.parent.addChild(shape);
            graphics.beginFill(0);
            graphics.drawRoundRect(0, 0, bitmap.width - 4, bitmap.height - 4, 10);
            graphics.endFill();
            bitmap.mask = shape;
        };
        /**
         * 设置纹理的九宫格
         */
        Utils.setScale9 = function (b, x, y, w, h) {
            var s = egret.Rectangle.create();
            s.x = x;
            s.y = y;
            s.width = w;
            s.height = h;
            b.scale9Grid = s;
        };
        /**
         * 二次延迟
         */
        Utils.callLater = function (call, thisObj) {
            var params = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                params[_i - 2] = arguments[_i];
            }
            var later = egret.callLater;
            later(function () {
                later(function () {
                    call.apply(thisObj, params);
                }, null);
            }, null);
        };
        /**
         * 检测点是否在矩形上
         */
        Utils.pointInRect = function (x, y, rect) {
            x -= rect.x;
            y -= rect.y;
            return x >= 0 && x <= rect.width && y >= 0 && y <= rect.height;
        };
        /**
         * 控件是否显示
         */
        Utils.isVisible = function (display) {
            var stage = display.stage;
            var visible;
            do {
                visible = display.visible;
                display = display.parent;
            } while (visible && (visible = !!display) && display != stage);
            return visible;
        };
        /**
         * 移除List的数据
         * @param list
         * @param isCache 是否是缓存数据，是的话不清除
         */
        Utils.removeListData = function (list, isCache) {
            var datas = list.dataProvider;
            datas && !isCache && datas.removeAll();
            list.dataProvider = null;
        };
        /**
         * 更新列表数据
         * @param list
         * @param datas 新数据源
         */
        Utils.updateListData = function (list, datas) {
            var array = list.dataProvider;
            if (array)
                array.replaceAll(datas);
            else
                list.dataProvider = new eui.ArrayCollection(datas);
        };
        /**
         * 添加滤镜
         */
        Utils.setColorMatrixFilter = function (image, colorMatrix) {
            var colorFlilter = new egret.ColorMatrixFilter(colorMatrix);
            if (image.filters && image.filters.length) {
                image.filters[0] = colorFlilter;
            }
            else {
                image.filters = [colorFlilter];
            }
        };
        /**
         * 复制一个对象
         * @param obj 需要复制的对象
         * @param copy 被复制对象，不存在则创建
         * @returns 返回copy
         */
        Utils.copyObj = function (obj, copy) {
            if (copy === void 0) { copy = Object.create(null); }
            for (var i in obj) {
                copy[i] = obj[i];
            }
            return copy;
        };
        return Utils;
    }());
    lie.Utils = Utils;
    __reflect(Utils.prototype, "lie.Utils");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 时间工具类
     */
    var TimeUtils = (function () {
        function TimeUtils() {
        }
        /**
         * 获取天数
         * @param second
         */
        TimeUtils.getDay = function (second) {
            return (second + 8 * 3600) / 86400 | 0;
        };
        /**
         * 检测两个秒数是否同一天
         */
        TimeUtils.isSameDay = function (second0, second1) {
            var get = TimeUtils.getDay;
            return get(second0) == get(second1);
        };
        /**
         * 检测日期是否是同一天
         */
        TimeUtils.isSameDayDate = function (date0, date1) {
            var g = function (date) {
                return Math.floor(date.getTime() / 1000);
            };
            return TimeUtils.isSameDay(g(date0), g(date1));
        };
        return TimeUtils;
    }());
    lie.TimeUtils = TimeUtils;
    __reflect(TimeUtils.prototype, "lie.TimeUtils");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 影片剪辑，实现可于皮肤创建
     */
    var MovieClip = (function (_super) {
        __extends(MovieClip, _super);
        function MovieClip() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MovieClip.prototype.childrenCreated = function () {
            var self = this;
            var getr = RES.getResAsync;
            // 这里就不检测资源的正确性了，调用的时候注意即可
            getr(self.image, function (texture) {
                if (!self.isDestroy) {
                    getr(self.frame, function () {
                        if (!self.isDestroy)
                            self.initMovieClip();
                    }, null);
                }
            }, null);
        };
        MovieClip.prototype.onDestroy = function () {
            var self = this;
            var clip = self.$movieClip;
            self.clearMovieCall();
            self.$factory = null;
            // 移除动画
            if (clip) {
                clip.stop();
                self.$movieClip = null;
            }
        };
        /**
         * 初始化数据
         */
        MovieClip.prototype.initMovieClip = function () {
            var self = this;
            var getr = RES.getRes;
            self.$factory = new egret.MovieClipDataFactory(getr(self.frame), getr(self.image));
            self.addChild(self.$movieClip = new egret.MovieClip);
            self.carryMovieCall();
        };
        /**
         * 执行回调
         */
        MovieClip.prototype.carryMovieCall = function () {
            var self = this;
            var call = self.m_pClipCall;
            if (call) {
                var clip = self.$movieClip;
                // 初始化数据
                clip.movieClipData = self.$factory.generateMovieClipData(self.action);
                call.call(self.m_pThisObj, clip);
                self.clearMovieCall(); // 执行完销毁
            }
        };
        /**
         * 清空回调
         */
        MovieClip.prototype.clearMovieCall = function () {
            var self = this;
            self.m_pClipCall = self.m_pThisObj = null;
        };
        Object.defineProperty(MovieClip.prototype, "res", {
            /**
             * 若命名一致，可取其前缀
             */
            set: function (value) {
                this.image = value + '_png';
                this.frame = value + '_json';
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "image", {
            /**
             * 获取帧动画合图资源名
             */
            get: function () {
                return this.$image;
            },
            /**
             * 设置帧动画合图资源名
             */
            set: function (value) {
                this.$image = String(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "frame", {
            /**
             * 获取帧动画数据资源名
             */
            get: function () {
                return this.$frame;
            },
            /**
             * 设置帧动画数据资源名
             */
            set: function (value) {
                this.$frame = String(value);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(MovieClip.prototype, "action", {
            /**
             * 获取当前动画名称
             */
            get: function () {
                return this.$action;
            },
            /**
             * 设置当前动画名称
             */
            set: function (value) {
                this.$action = String(value);
            },
            enumerable: true,
            configurable: true
        });
        // 供外部使用的方法
        /**
         * 只能通过该方法来获取MovieClip对象
         */
        MovieClip.prototype.addMovieCall = function (call, thisObj) {
            var self = this;
            self.m_pClipCall = call;
            self.m_pThisObj = thisObj;
            self.$factory && self.carryMovieCall(); // 存在则立马执行
        };
        /**
         * 播放
         */
        MovieClip.prototype.play = function (playTimes, cb, thisObj) {
            this.addMovieCall(function (clip) {
                clip.gotoAndPlay(0, playTimes);
                cb && clip.once(egret.Event.COMPLETE, cb, thisObj);
            }, null);
        };
        /**
         * 暂停
         */
        MovieClip.prototype.stop = function (cb, thisObj) {
            this.addMovieCall(function (clip) {
                clip.stop();
            }, null);
        };
        return MovieClip;
    }(lie.UIComponent));
    lie.MovieClip = MovieClip;
    __reflect(MovieClip.prototype, "lie.MovieClip");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 存放和获取一些本地变量名，属于应用级别上的存储
     */
    var LocalData = (function () {
        function LocalData() {
        }
        /**
         * 设置Item
         */
        LocalData.setItem = function (key, value) {
            return egret.localStorage.setItem(key, value);
        };
        /**
         * 获取Item的值
         */
        LocalData.getItem = function (key) {
            return egret.localStorage.getItem(key);
        };
        /**
         * 注意value必须是对象，否则会出现奇怪的现象
         */
        LocalData.setObject = function (key, value) {
            return LocalData.setItem(key, JSON.stringify(value));
        };
        /**
         * 获取一个对象
         */
        LocalData.getObject = function (key) {
            try {
                return JSON.parse(LocalData.getItem(key));
            }
            catch (e) { }
        };
        return LocalData;
    }());
    lie.LocalData = LocalData;
    __reflect(LocalData.prototype, "lie.LocalData");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 控件监听工具类
     */
    var EventUtils = (function () {
        function EventUtils() {
        }
        /**
         * 移除控件上的所有监听，该方法也适用于没有通过addEventListener来添加的控件
         */
        EventUtils.removeEventListener = function (target) {
            var value = target.$EventDispatcher;
            var list = [].concat(value[1] || [], value[2] || []);
            var events = [];
            for (var i in list) {
                var item = list[i];
                for (var j in item) {
                    var datas = item[j];
                    for (var k in datas) {
                        var event_1 = datas[k];
                        target.removeEventListener(event_1.type, event_1.listener, event_1.thisObject, event_1.useCapture);
                    }
                }
            }
        };
        /**
         * 移除root往下所有的点击事件
         */
        EventUtils.removeEventListeners = function (root) {
            var self = EventUtils;
            if (root instanceof egret.DisplayObjectContainer)
                for (var i = 0, num = root.numChildren; i < num; i++)
                    self.removeEventListeners(root.getChildAt(i));
            else
                self.removeEventListener(root);
        };
        /**
         * 添加缩放监听，记得用removeEventListener来移除这个监听
         */
        EventUtils.addScaleListener = function (target, scale) {
            if (scale === void 0) { scale = 0.95; }
            var addE = function (type, call, thisObj) {
                target.addEventListener(type, call, thisObj);
            };
            var self = EventUtils;
            var clzz = egret.TouchEvent;
            target.$evtScale = scale;
            target.$bgScaleX = target.scaleX;
            target.$bgScaleY = target.scaleY;
            addE(clzz.TOUCH_BEGIN, self.onScleBegin, self);
            addE(clzz.TOUCH_END, self.onScaleEnd, self);
            addE(clzz.TOUCH_CANCEL, self.onScaleEnd, self);
            addE(clzz.TOUCH_RELEASE_OUTSIDE, self.onScaleEnd, self);
        };
        /**
         * 缩放开始
         */
        EventUtils.onScleBegin = function (event) {
            var target = event.currentTarget;
            var tween = egret.Tween;
            var scale = target.$evtScale;
            var scaleX = target.scaleX * scale;
            var scaleY = target.scaleY * scale;
            tween.removeTweens(target);
            tween.get(target).to({ scaleX: scaleX, scaleY: scaleY }, EventUtils.$scaleTime);
        };
        /**
         * 缩放结束
         */
        EventUtils.onScaleEnd = function (event) {
            var target = event.currentTarget;
            var time = EventUtils.$scaleTime;
            var tween = egret.Tween;
            var scaleX = target.$bgScaleX;
            var scaleY = target.$bgScaleY;
            var bScaleX = scaleX * 1.1;
            var bScaleY = scaleY * 1.1;
            tween.removeTweens(target);
            tween.get(target).to({ scaleX: bScaleX, scaleY: bScaleY }, time).to({ scaleX: scaleX, scaleY: scaleY }, time);
        };
        // 常用的监听类型归类
        /**
         * 添加TouchTap监听
         */
        EventUtils.addTouchTapListener = function (target, call, thisObj, useCapture) {
            target.addEventListener(egret.TouchEvent.TOUCH_TAP, call, thisObj, useCapture);
        };
        /**
         * 在TouchTap的基础上进行缩放
         */
        EventUtils.addTouchTapScaleListener = function (target, call, thisObj, scale, useCapture) {
            var self = EventUtils;
            self.addScaleListener(target, scale);
            self.addTouchTapListener(target, call, thisObj, useCapture);
        };
        /**
         * 添加按住监听
         * @param target
         * @param begin 按住时的回调
         * @param end 松手时的回调，会调用多次，请自己在end里判断
         */
        EventUtils.addTouchingListener = function (target, begin, end, thisObj) {
            var event = egret.TouchEvent;
            target.addEventListener(event.TOUCH_BEGIN, begin, thisObj);
            target.addEventListener(event.TOUCH_END, end, thisObj);
            target.addEventListener(event.TOUCH_CANCEL, end, thisObj);
            target.addEventListener(event.TOUCH_RELEASE_OUTSIDE, end, thisObj);
        };
        /**
         * 添加移动监听
         */
        EventUtils.addTouchMoveListener = function (target) {
            var event = egret.TouchEvent;
            var touchX, touchY;
            target.addEventListener(event.TOUCH_BEGIN, function (e) {
                e.stopImmediatePropagation();
                touchX = e.stageX;
                touchY = e.stageY;
            }, null);
            target.addEventListener(event.TOUCH_MOVE, function (e) {
                e.stopImmediatePropagation();
                var newX = e.stageX, newY = e.stageY;
                target.x += newX - touchX;
                target.y += newY - touchY;
                touchX = newX;
                touchY = newY;
            }, null);
        };
        /**
         * 添加list的选项监听
         */
        EventUtils.addItemTapListener = function (list, call, thisObj) {
            list.addEventListener(eui.ItemTapEvent.ITEM_TAP, call, thisObj);
        };
        EventUtils.$scaleTime = 100; // 缩放动画时间
        return EventUtils;
    }());
    lie.EventUtils = EventUtils;
    __reflect(EventUtils.prototype, "lie.EventUtils");
})(lie || (lie = {}));
var lie;
(function (lie) {
    /**
     * 事件分发工具类，异步操作用
     */
    var Dispatch = (function () {
        function Dispatch() {
        }
        /**
         * 注册对象
         * @param key 事件的唯一标志
         * @param obj 注册事件
         * @param replace 是否替换掉之前的回调
         */
        Dispatch.$register = function (key, obj, replace) {
            var self = Dispatch;
            var target = self.$targets[key];
            var isReg = !target || replace;
            isReg && (self.$targets[key] = obj);
            self.$removeLater(key);
            return isReg;
        };
        /**
         * 移除延迟
         */
        Dispatch.$removeLater = function (key) {
            var self = Dispatch;
            var params = self.$laters[key];
            if (params !== void 0) {
                delete self.$laters[key];
                self.$notice(key, params);
            }
        };
        /**
         * 被动触发回调
         */
        Dispatch.$notice = function (key, params) {
            var self = Dispatch;
            params.unshift(key);
            self.notice.apply(self, params);
        };
        /**
         * 注册事件
         * @param key 事件的唯一标志
         * @param call 回调函数
         * @param thisObj 回调所属对象
         * @param replace 是否替换掉之前的回调
         */
        Dispatch.register = function (key, call, thisObj, replace) {
            return Dispatch.$register(key, [call, thisObj], replace);
        };
        /**
         * 通知，触发回调
         * @param key 事件标志
         * @param param 回调参数
         */
        Dispatch.notice = function (key) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            var target = Dispatch.$targets[key];
            target && target[0].apply(target[1], params);
        };
        /**
         * 移除事件
         */
        Dispatch.remove = function (key) {
            delete Dispatch.$targets[key];
        };
        /**
         * 是否注册了某个事件
         */
        Dispatch.hasRegister = function (key) {
            return !!Dispatch.$targets[key];
        };
        /**
         * 注册多个事件，统一回调，参数功能看register
         */
        Dispatch.registers = function (keys, call, thisObj, replace) {
            var obj = [call, thisObj];
            for (var i in keys) {
                var key = keys[i];
                Dispatch.$register(key, obj, replace);
            }
        };
        /**
         * 清除所有事件
         */
        Dispatch.clear = function () {
            var self = Dispatch;
            self.$targets = {};
            self.$laters = {};
        };
        /**
         * 延迟通知，如果已经注册的事件功能同notice，如果未注册，则会等到
         * 事件注册时立马触发回调
         * @param key 事件标志
         * @param param 回调参数
         */
        Dispatch.noticeLater = function (key) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            var self = Dispatch;
            if (self.hasRegister(key))
                self.$notice(key, params);
            else
                self.$laters[key] = params;
        };
        Dispatch.$targets = {}; // 存放所有回调函数
        Dispatch.$laters = {}; // 存放延迟数据
        return Dispatch;
    }());
    lie.Dispatch = Dispatch;
    __reflect(Dispatch.prototype, "lie.Dispatch");
})(lie || (lie = {}));
var Common;
(function (Common) {
    var CommonUtils = (function () {
        function CommonUtils() {
        }
        /**
             * 设置点击转发按钮
             */
        CommonUtils.setGlobalShare = function (userId) {
            wx.onShareAppMessage(function () {
                // 用户点击了“转发”按钮
                var shareInfo = CommonUtils.getShareInfo();
                return CommonUtils.getGlobalShareInfo(shareInfo.title, shareInfo.imageUrl);
            });
            wx.showShareMenu(); //自动显示
        };
        /**
         * 获取全局分享数据
         */
        CommonUtils.getGlobalShareInfo = function (title, imageUrl, inviteType, category) {
            // var query = `share=true&userId=${AppConfig.myInfo.sid}&userName=${AppConfig.myInfo.nickname}`;
            // inviteType && (query += `&inviteType=${inviteType}`)
            // category && (query += `&category=${category}`)
            return {
                title: title ? title : '卡牌2048了解一下!!!',
                imageUrl: imageUrl ? imageUrl : 'https://static.xunguanggame.com/card2048/sharePics/cards_logo.png'
            };
        };
        CommonUtils.getShareInfo = function () {
            var shareTexts = this.shareTexts, shareImgs = this.shareImgs;
            var title = shareTexts[Action.shuffle(shareTexts.length)];
            var imageUrl = shareImgs[Action.shuffle(shareImgs.length)];
            return {
                title: title,
                imageUrl: imageUrl
            };
        };
        /**
         * 主动拉起转发
         */
        CommonUtils.showShare = function (title, imageUrl, inviteType, category) {
            return new Promise(function (resolve, reject) {
                var obj = CommonUtils.getGlobalShareInfo(title, imageUrl, inviteType, category);
                obj.success = function (res) {
                    console.log(res);
                    resolve(res);
                };
                obj.fail = function (res) {
                    console.log(res);
                    reject(res);
                };
                wx.shareAppMessage(obj);
            });
        };
        /**
         * 分享的文案
         */
        CommonUtils.shareTexts = [
            '超魔性的卡牌2048了解一下～'
        ];
        /**
         * 分享的图片
         */
        CommonUtils.shareImgs = [
            'https://static.xunguanggame.com/card2048/sharePics/cards_logo.png'
        ];
        return CommonUtils;
    }());
    Common.CommonUtils = CommonUtils;
    __reflect(CommonUtils.prototype, "Common.CommonUtils");
})(Common || (Common = {}));
var lie;
(function (lie) {
    /**
     * 网络状态工具——基于微信
     */
    var NetworkUtils = (function () {
        function NetworkUtils() {
        }
        /**
         * 获取当前网络状态
         */
        NetworkUtils.getNetworkType = function () {
            return new Promise(function (resolve) {
                wx.getNetworkType({
                    success: function (res) {
                        resolve(res.networkTyp);
                    }
                });
            });
        };
        /**
         * 判断类型是否是最佳网络状态
         */
        NetworkUtils.isBestNetworkType = function (type) {
            return type == '4g' || type == 'wifi';
        };
        /**
         * 判断网络状态是否提升
         * @param oldType 旧类型
         * @param newType 新类型
         */
        NetworkUtils.networkUpgrade = function (oldType, newType) {
            var types = ['none', 'unknown', '2g', '3g', '4g', 'wifi'];
            var index0 = types.indexOf(oldType);
            var index1 = types.indexOf(newType);
            return index1 > index0;
        };
        /**
         * 添加网络变化回调
         * @param call 回调，参数是当前的网络状态
         * @param thisObj 回调对象
         * @param isOnce 是否是一次性的回调，即回调结束下次网络变化不再接受
         */
        NetworkUtils.addNetworkStatusChange = function (call, thisObj, isOnce) {
            NetworkUtils.$netCall.push([call, thisObj, isOnce]);
        };
        /**
         * 移除网络变化回调
         * @param call 回调，参数是当前的网络状态
         * @param thisObj 回调对象
         * @returns 返回移除结果
         */
        NetworkUtils.removeNetworkStatusChange = function (call, thisObj) {
            var netCall = NetworkUtils.$netCall;
            for (var i = 0, len = netCall.length; i < len; i++) {
                var item = netCall[i];
                if (item[0] == call && item[1] == thisObj) {
                    netCall.splice(i, 1);
                    return true;
                }
            }
            return false;
        };
        /**
         * 执行网络变化回调——请勿手动调用
         */
        NetworkUtils.excuteStatusChange = function (type) {
            var netCall = NetworkUtils.$netCall;
            for (var i = 0, len = netCall.length; i < len; i++) {
                var item = netCall[i];
                item[0].call(item[1], type);
                // 删除一次性回调
                if (item[2]) {
                    netCall.splice(i, 1);
                    i--;
                }
            }
        };
        NetworkUtils.$netCall = [];
        return NetworkUtils;
    }());
    lie.NetworkUtils = NetworkUtils;
    __reflect(NetworkUtils.prototype, "lie.NetworkUtils");
    var PlatformUtils = (function () {
        function PlatformUtils() {
        }
        //登录
        PlatformUtils.login = function () {
            return new Promise(function (resolve, reject) {
                wx.login({
                    success: function (res) {
                        console.log("weixin返回的登录数据", res);
                        var data = { "code": res.code };
                        Api.post(Api.login, data).then(function (loginRes) {
                            console.log("后端返回的登录数据", loginRes);
                            AppConfig.gameInfo = {
                                score: loginRes.UserInfo.Scroe,
                                userId: loginRes.UserInfo.UserId
                            };
                        });
                    }
                });
            });
        };
        return PlatformUtils;
    }());
    lie.PlatformUtils = PlatformUtils;
    __reflect(PlatformUtils.prototype, "lie.PlatformUtils");
})(lie || (lie = {}));
window['global'] = window;
var Utils = lie.Utils;
var TimeUtils = lie.TimeUtils;
var EventUtils = lie.EventUtils;
var AppViews = lie.AppViews;
var Dispatch = lie.Dispatch;
var Dialog = lie.Dialog;
var UIComponent = lie.UIComponent;
var AppComponent = lie.AppComponent;
var AppRenderer = lie.AppRenderer;
var LocalData = lie.LocalData;
var MovieClip = lie.MovieClip;
var pfUtils = new lie.WXUtils;
var CommonUtils = Common.CommonUtils;
var PlatformUtils = lie.PlatformUtils;
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = _super.call(this) || this;
        // var pfUtils = new lie.WXUtils
        XG_STATISTICS.init('wxd809893d56d16080');
        pfUtils.init();
        return _this;
        // this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    Main.prototype.createChildren = function () {
        _super.prototype.createChildren.call(this);
        lie.LifeCycle.init();
        egret.lifecycle.addLifecycleListener(function (context) {
            // custom lifecycle plugin
        });
        egret.lifecycle.onPause = function () {
            egret.ticker.pause();
        };
        egret.lifecycle.onResume = function () {
            egret.ticker.resume();
        };
        //注入自定义的素材解析器
        egret.registerImplementation("eui.IAssetAdapter", new AssetAdapter());
        egret.registerImplementation("eui.IThemeAdapter", new ThemeAdapter());
        this.loadResource().catch(function (e) {
            console.log(e);
        });
    };
    // }
    Main.prototype.loadTheme = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            //加载皮肤主题配置文件,可以手动修改这个文件。替换默认皮肤。
            var theme = new eui.Theme("resource/default.thm.json", _this.stage);
            theme.addEventListener(eui.UIEvent.COMPLETE, function () {
                resolve();
            }, _this);
        });
    };
    // private async loadResource() {
    //     await this.loadResource()
    //     this.createGameScene();
    //     // const result = await RES.getResAsync("description_json")
    //     await platform.login();
    //     const userInfo = await platform.getUserInfo();
    //     console.log(userInfo);
    // }
    Main.prototype.loadResource = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loadingView, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        loadingView = new LoadingUI();
                        this.stage.addChild(loadingView);
                        return [4 /*yield*/, RES.loadConfig("resource/default.res.json", "resource/")];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.loadTheme()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, RES.loadGroup("preload", 0, loadingView)];
                    case 3:
                        _a.sent();
                        this.stage.removeChild(loadingView);
                        // await platform.login();
                        // const userInfo = await platform.getUserInfo();
                        this.createGameScene();
                        return [3 /*break*/, 5];
                    case 4:
                        e_1 = _a.sent();
                        console.error(e_1);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 创建游戏场景
     * Create a game scene
     */
    Main.prototype.createGameScene = function () {
        CommonUtils.setGlobalShare();
        AppViews.pushPanel(HomePanel);
        // pfUtils.postMessage('update', { "score": '10001', "time": Date.now() })
        // AppViews.pushDialog(ResurgenceDialog)
    };
    /**
     * 根据name关键字创建一个Bitmap对象。name属性请参考resources/resource.json配置文件的内容。
     * Create a Bitmap object according to name keyword.As for the property of name please refer to the configuration file of resources/resource.json.
     */
    Main.prototype.createBitmapByName = function (name) {
        var result = new egret.Bitmap();
        var texture = RES.getRes(name);
        result.texture = texture;
        return result;
    };
    return Main;
}(eui.Component));
__reflect(Main.prototype, "Main");
var number2Color = {
    0: 0xffffff,
    2: 0xd1cfcf,
    4: 0xabe187,
    8: 0x5cac86,
    16: 0xfdb231,
    32: 0xc96d98,
    64: 0x34aaae,
    128: 0x2f5a7c,
    256: 0x547d76,
    512: 0x7d6a95,
    1024: 0x886669,
    2048: 0x585956
};
var CardComponent = (function (_super) {
    __extends(CardComponent, _super);
    function CardComponent(data) {
        var _this = _super.call(this) || this;
        _this.skinName = 'resource/scene/Card.exml';
        _this.anchorOffsetX = _this.width / 2;
        _this.anchorOffsetY = _this.height / 2;
        if (data > 0) {
            _this.value = data;
        }
        else {
            _this.r_bg.fillAlpha = 0;
        }
        _this.setBgColor(data);
        return _this;
    }
    CardComponent.prototype.setBgColor = function (data) {
        this.r_bg.fillColor = number2Color[data];
    };
    /**
     * 设置边框的颜色
     * @param data 颜色色值
     */
    CardComponent.prototype.setRectStroke = function (data) {
        this.r_bg.strokeColor = data;
        // this.r_bg.strokeWeight = 4
    };
    CardComponent.prototype.clearRectStroke = function () {
        this.r_bg.strokeColor = 0xffffff;
        // this.r_bg.strokeWeight = 0
    };
    return CardComponent;
}(eui.Component));
__reflect(CardComponent.prototype, "CardComponent");
var Ajax = (function () {
    // private api: Api = Api.getInstance()
    function Ajax() {
    }
    Ajax.getInstance = function () {
        if (!Ajax.ajax) {
            Ajax.ajax = new Ajax();
        }
        return Ajax.ajax;
    };
    Ajax.prototype.get = function (url, data) {
        var _this = this;
        var _self = this;
        return new Promise(function (resolve, reject) {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            var param = '';
            if (data) {
                if (data.__proto__ === Object.prototype) {
                    for (var key in data) {
                        param += '/' + data[key];
                    }
                }
                else {
                    param = '/' + data;
                }
            }
            request.open(url + param, egret.HttpMethod.GET);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send();
            request.addEventListener(egret.Event.COMPLETE, onGetComplete, _this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onGetIOError, _this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onGetProgress, _this);
            if (_self.timeout) {
                clearTimeout(_self.timeout);
            }
            if (_self.timeout2) {
                clearTimeout(_self.timeout2);
            }
            _self.timeout = setTimeout(function () {
                console.error(url);
                wx.showToast({
                    title: '连接超时',
                    icon: 'none'
                });
                clear();
                reject();
            }, 5000);
            _self.timeout2 = setTimeout(function () {
                Action.showLoading();
            }, 800);
            function clear() {
                clearTimeout(_self.timeout);
                clearTimeout(_self.timeout2);
                _self.timeout = null;
                _self.timeout2 = null;
                Action.hideLoading();
            }
            function onGetComplete(e) {
                // _self.sendDot('requestSendComplete')
                resolve(request.response);
                clear();
            }
            function onGetIOError(e) {
                wx.showToast({
                    title: '请求失败',
                    icon: 'none'
                });
                // _self.sendDot('requestSendError', e)
                clear();
                reject();
            }
            function onGetProgress(e) {
                // _self.sendDot('requestSendSuccess')
                resolve(JSON.parse(request.response));
                clear();
            }
        });
    };
    Ajax.prototype.post = function (url, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var _self = this;
        return new Promise(function (resolve, reject) {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            //设置为 POST 请求
            request.open(url + '?', egret.HttpMethod.POST);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(json2form(data));
            request.addEventListener(egret.Event.COMPLETE, onPostComplete, _this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, _this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onPostProgress, _this);
            if (_self.timeout) {
                clearTimeout(_self.timeout);
            }
            if (_self.timeout2) {
                clearTimeout(_self.timeout2);
            }
            _self.timeout = setTimeout(function () {
                console.error(url);
                wx.showToast({
                    title: '连接超时',
                    icon: 'none'
                });
                clear();
                reject();
            }, 5000);
            _self.timeout2 = setTimeout(function () {
                Action.showLoading();
            }, 800);
            function clear() {
                clearTimeout(_self.timeout);
                clearTimeout(_self.timeout2);
                _self.timeout = null;
                _self.timeout2 = null;
                Action.hideLoading();
            }
            function onPostComplete(e) {
                clear();
                // _self.sendDot('requestSendComplete')
                resolve(request.response);
            }
            function onPostIOError(e) {
                wx.showToast({
                    title: '请求失败',
                    icon: 'none'
                });
                // _self.sendDot('requestSendError', e)
                clear();
                reject();
            }
            function onPostProgress(e) {
                clear();
                // _self.sendDot('requestSendSuccess')
                resolve(JSON.parse(request.response));
            }
        });
    };
    /**
     * 发送打点信息
     * @param Action 打点信息
     */
    Ajax.prototype.sendDot = function (Action, error) {
        //打点
        XG_STATISTICS.send('pocket_' + Action || "wx");
        var myInfo = AppConfig.myInfo;
        var userId = 0;
        try {
            if (myInfo.sid) {
                userId = myInfo.sid;
            }
            var data = {
                userId: userId,
                appId: Api.appId,
                Action: 'pocket_' + Action || "wx",
                info: error
            };
            wx.request({
                url: Api.hostDot + '/Action',
                data: data,
                header: {},
                method: "POST",
                success: function (t) {
                }
            });
        }
        catch (error) {
            console.error(error);
        }
    };
    Ajax.prototype.dotPost = function (url, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        return new Promise(function (resolve, reject) {
            var request = new egret.HttpRequest();
            request.responseType = egret.HttpResponseType.TEXT;
            //设置为 POST 请求
            request.open(url + '?', egret.HttpMethod.POST);
            request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            request.send(json2form(data));
            request.addEventListener(egret.Event.COMPLETE, onPostComplete, _this);
            request.addEventListener(egret.IOErrorEvent.IO_ERROR, onPostIOError, _this);
            request.addEventListener(egret.ProgressEvent.PROGRESS, onPostProgress, _this);
            function onPostComplete(e) {
                resolve(request.response);
            }
            function onPostIOError(e) {
                reject();
            }
            function onPostProgress(e) {
                resolve(JSON.parse(request.response));
            }
        });
    };
    return Ajax;
}());
__reflect(Ajax.prototype, "Ajax");
function addItemsToForm(form, names, obj) {
    // eslint-disable-next-line no-use-before-define
    if (obj === undefined || obj === "" || obj === null)
        return addItemToForm(form, names, "");
    if (typeof obj === "string" ||
        typeof obj === "number" ||
        obj === true ||
        obj === false) {
        // eslint-disable-next-line no-use-before-define
        return addItemToForm(form, names, obj);
    }
    // eslint-disable-next-line no-use-before-define
    if (obj instanceof Date)
        return addItemToForm(form, names, obj.toJSON());
    // array or otherwise array-like
    if (obj instanceof Array) {
        return obj.forEach(function (v, i) {
            names.push("[" + i + "]");
            addItemsToForm(form, names, v);
            names.pop();
        });
    }
    if (typeof obj === "object") {
        return Object.keys(obj).forEach(function (k) {
            names.push(k);
            addItemsToForm(form, names, obj[k]);
            names.pop();
        });
    }
    return null;
}
function addItemToForm(form, names, value) {
    var name = encodeURIComponent(names.join(".").replace(/\.\[/g, "["));
    value = encodeURIComponent(value.toString());
    form.push(name + "=" + value);
}
function json2form(data) {
    var form = [];
    addItemsToForm(form, [], data);
    var body = form.join("&");
    return body;
}
var GameOverDialog = (function (_super) {
    __extends(GameOverDialog, _super);
    function GameOverDialog(score) {
        var _this = _super.call(this, 'GameOverSkin') || this;
        _this.score = score;
        _this.init();
        return _this;
    }
    GameOverDialog.prototype.init = function () {
        var _this = this;
        pfUtils.hideBannerAds();
        pfUtils.showBannerAds();
        this.g_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameStart, this);
        this.g_rank.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(RankDialog);
        }, this);
        this.g_home.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
            AppViews.removePanel(GamePanel);
        }, this);
        this.g_share.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onShareTap, this);
        EventUtils.addScaleListener(this.g_start);
        EventUtils.addScaleListener(this.g_home);
        EventUtils.addScaleListener(this.g_rank);
        this.uploadScore();
    };
    GameOverDialog.prototype.onGameStart = function () {
        this.removeFromParent();
        AppViews.removePanel(GamePanel);
        AppViews.pushPanel(GamePanel);
    };
    /**
     * 上传分数
     */
    GameOverDialog.prototype.uploadScore = function () {
        var post = pfUtils.postMessage;
        var data = {
            "score": this.score,
            "time": Date.now()
        };
        post('update', data);
        post('settle');
        post('clear');
        // post('resume')
    };
    /**
     * 分享
     */
    GameOverDialog.prototype.onShareTap = function () {
        var title = "\u6211\u83B7\u5F97\u4E86" + this.score + "\u5206,\u8FD8\u6709\u8C01\uFF1F";
        var imageUrl = CommonUtils.getShareInfo().imageUrl;
        CommonUtils.showShare(title, imageUrl);
    };
    return GameOverDialog;
}(Dialog));
__reflect(GameOverDialog.prototype, "GameOverDialog");
var HomePanel = (function (_super) {
    __extends(HomePanel, _super);
    function HomePanel() {
        var _this = _super.call(this, 'HomeSkin') || this;
        _this.init();
        return _this;
    }
    HomePanel.prototype.init = function () {
        pfUtils.showBannerAds();
        this.bindClick();
    };
    HomePanel.prototype.onShow = function () {
        pfUtils.hideBannerAds();
        pfUtils.showBannerAds();
    };
    // public onHide(){
    //     pfUtils.hideBannerAds()
    // }
    HomePanel.prototype.bindClick = function () {
        this.g_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameStart, this);
        this.g_rank.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(RankDialog);
        }, this);
        this.g_gameCircle.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onGameCircleTap, this);
        EventUtils.addScaleListener(this.g_start);
        EventUtils.addScaleListener(this.g_rank);
    };
    HomePanel.prototype.onGameStart = function () {
        AppViews.pushPanel(GamePanel);
    };
    HomePanel.prototype.onGameCircleTap = function () {
        var appId = 'wx53b1b4ab6fa0d044';
        var path = 'pages/quan/quan?communityId=3&from=2048kapai_3';
        var imageUrl = "https://static.xunguanggame.com/pocket/assets/sharePic/youxidating4.png";
        pfUtils.navigate2Program(appId, path, imageUrl);
    };
    return HomePanel;
}(lie.AppComponent));
__reflect(HomePanel.prototype, "HomePanel");
var PauseDialog = (function (_super) {
    __extends(PauseDialog, _super);
    function PauseDialog() {
        var _this = _super.call(this, 'PauseSkin') || this;
        _this.init();
        return _this;
    }
    PauseDialog.prototype.init = function () {
        this.bindClick();
    };
    PauseDialog.prototype.bindClick = function () {
        var _this = this;
        this.g_home.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
            AppViews.removePanel(GamePanel);
        }, this);
        this.g_restart.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
            AppViews.removePanel(GamePanel);
            AppViews.pushPanel(GamePanel);
        }, this);
        this.g_continue.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseTap, this);
        this.r_bg.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            _this.removeFromParent();
        }, this);
        this.g_closeCount.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onCloseCount, this);
        EventUtils.addScaleListener(this.g_home);
        EventUtils.addScaleListener(this.g_restart);
        EventUtils.addScaleListener(this.g_continue);
    };
    PauseDialog.prototype.onCloseTap = function () {
        this.removeFromParent();
    };
    /**
     * 游戏结束
     */
    PauseDialog.prototype.onCloseCount = function () {
        this.onCloseTap();
        var gamePage = AppViews.getTopCommont();
        gamePage.gameOver();
    };
    return PauseDialog;
}(Dialog));
__reflect(PauseDialog.prototype, "PauseDialog");
/**
 * 排行榜界面
 */
var RankDialog = (function (_super) {
    __extends(RankDialog, _super);
    function RankDialog() {
        var _this = _super.call(this) || this;
        _this.skinName = "RankDialogSkin";
        // this.init();
        _this.global = window;
        _this.height = egret.MainContext.instance.stage.stageHeight;
        _this.width = egret.MainContext.instance.stage.stageWidth;
        // this.addChild(this.m_Bg);		
        // this.isShare = true;
        // this.setModel(0);
        // var Utils = new Utils();
        // 结束页面 排行-竖排
        _this.initShareCanvas();
        // const bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        // bitmapdata.$deleteSource = false;
        // const texture = new egret.Texture();
        // texture._setBitmapData(bitmapdata);
        // var bitmap = new egret.Bitmap(texture);
        // bitmap.width = 750;
        // bitmap.height = 1334;
        // bitmap.x = 0;
        // // console.log(this.worldHeight)
        // bitmap.y = 10;
        // egret.startTick((timeStarmp: number) => {
        //     egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
        //     bitmapdata.webGLTexture = null;
        //     return false;
        // }, this);
        // this.addChild(bitmap);
        var self = _this;
        var addl = self.addTouchTapListener;
        addl(self.m_imgClose, function () {
            // self.removeChild(bitmap);
            self.m_pShare.texture.dispose();
            self.m_pShare = null;
            Utils.removeTimer(self.m_pTimer, self.m_pRefresh, null);
            _this.removeFromParent();
            pfUtils.postMessage('clear');
        }, self);
        return _this;
    }
    RankDialog.prototype.init = function () {
        // super.onCreate();
        // addl(self.m_imgLast, self.onLast, self);
        // addl(self.m_imgNext, self.onNext, self);
        // addl(self.m_btnShare, self.onShare, self);
    };
    RankDialog.prototype.onDestroy = function () {
        pfUtils.postMessage('exit', 2);
    };
    // protected initShareCanvas(): void {
    // 	// super.initShareCanvas(0.5);
    // }
    /**
     * 点击上一排
     */
    RankDialog.prototype.onLast = function () {
        pfUtils.postMessage('last');
    };
    /**
     * 点击下一排
     */
    RankDialog.prototype.onNext = function () {
        pfUtils.postMessage('next');
    };
    /**
     * 点击底部按钮
     */
    RankDialog.prototype.onShare = function () {
        var self = this;
        pfUtils.showShare().then(function (ticket) {
            if (ticket) {
                // self.setModel(1);
                pfUtils.postMessage('ticket', ticket);
            }
        });
    };
    /**
     * 设置模式
     * @param 是否显示群排行
     */
    // protected setModel(model: number): void {
    // 	var self = this;
    // 	// self.m_imgTitle.source = 'title' + model + '_ranking_png';
    // 	// self.m_btnShare.source = 'rank_share' + model + '_png';
    // }
    RankDialog.prototype.removeself = function () {
        this.removeChild(this.m_pShare);
        // this.bitmap.parent.removechild(this.bitmap);
        this.removeChildren();
    };
    /**
     * 初始化离屏canvas
     */
    RankDialog.prototype.initShareCanvas = function () {
        var self = this;
        var stage = self.stage;
        pfUtils.postMessage('rank'); // 显示排行榜不清理底层
        pfUtils.postMessage('resume');
        var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        var texture = new egret.Texture();
        bitmapdata.$deleteSource = false;
        texture._setBitmapData(bitmapdata);
        // 界面显示
        var bitmap = self.m_pShare = new egret.Bitmap(texture);
        // bitmap.width = 532;
        // bitmap.height = 800;
        // self.m_gpShare.addChild(bitmap);
        bitmap.width = this.width;
        bitmap.height = this.height;
        bitmap.x = 0;
        // console.log(this.worldHeight)
        bitmap.y = 10;
        self.addChild(bitmap);
        // 刷新
        var func = self.m_pRefresh = function (timeStarmp) {
            egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
            console.log("是否在循环");
            bitmapdata.webGLTexture = null;
            return false;
        };
        self.m_pTimer = Utils.createTimer(func, null, 0.05, 0);
        // egret.startTick(self.funcTimeStarmp, self);
        //         egret.startTick((timeStarmp: number) => {           
        //     egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
        //     bitmapdata.webGLTexture = null;
        //     return false;
        // }, this.global.self);
        // egret.startTick(func, null);
    };
    // public funcTimeStarmp(timeStarmp: number): boolean{    
    //         egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
    //         bitmapdata.webGLTexture = null;
    //         return false;
    // }
    /**
     * 添加TouchTap监听
     */
    RankDialog.prototype.addTouchTapListener = function (target, call, thisObj, useCapture) {
        target.addEventListener(egret.TouchEvent.TOUCH_BEGIN, call, thisObj, useCapture);
    };
    return RankDialog;
}(Dialog));
__reflect(RankDialog.prototype, "RankDialog");
var Test = (function (_super) {
    __extends(Test, _super);
    function Test() {
        var _this = _super.call(this, 'test/TestSkin') || this;
        _this.init();
        return _this;
    }
    Test.prototype.init = function () {
        this.bindClick();
    };
    Test.prototype.bindClick = function () {
        this.l_start.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onStart, this);
    };
    Test.prototype.onStart = function () {
        this.targetIndex = 100;
        this.index = 0;
        this.currentIndex = -1;
        this.timer = new egret.Timer(100);
        this.timer.addEventListener(egret.TimerEvent.TIMER, this.onStartTimer, this);
        this.timer.start();
    };
    Test.prototype.onStartTimer = function () {
        this.currentIndex++;
        this.timer.delay = Math.floor(1000 / (this.targetIndex - this.currentIndex));
        var item = this.g_awards.$children[this.index];
        // console.log(item)
        this.r_frame.x = item.x;
        this.r_frame.y = item.y;
        if (this.currentIndex === this.targetIndex) {
            this.timer.stop();
        }
        this.index++;
        if (this.index >= this.g_awards.numChildren) {
            this.index = 0;
        }
    };
    return Test;
}(Dialog));
__reflect(Test.prototype, "Test");
var Action;
(function (Action) {
    /**
     * 计算两个时间戳的时间间隔
     * 一天以内返回小时差，一天以外显示天数差
     * @param faultDate 传过来的时间
     * @param completeTime 当前时间
     */
    function countTime(faultDate, completeTime) {
        var usedTime = completeTime - faultDate; //两个时间戳相差的毫秒数  
        var days = Math.floor(usedTime / (24 * 3600 * 1000));
        //计算出小时数  
        var leave1 = usedTime % (24 * 3600 * 1000); //计算天数后剩余的毫秒数  
        var hours = Math.floor(leave1 / (3600 * 1000));
        //计算相差分钟数  
        var leave2 = leave1 % (3600 * 1000); //计算小时数后剩余的毫秒数  
        var minutes = Math.floor(leave2 / (60 * 1000));
        var time = days + "天" + hours + "时" + minutes + "分";
        if (hours < 1) {
            return '刚刚';
        }
        if (days > 0) {
            return days + '天前';
        }
        else {
            return hours + '小时前';
        }
    }
    Action.countTime = countTime;
    /**
     * 根据传来的秒数 转化成hh:mm:ss的格式
     */
    function turnSecond(second) {
        if (second / (3600 * 24) > 1) {
            var d = Math.floor(second / (3600 * 24));
            var h = Math.floor(second % (3600 * 24) / 3600);
            h = h < 10 ? '0' + h : h;
            var m = Math.floor(second % (3600 * 24) % 3600 / 60);
            m = m < 10 ? '0' + m : m;
            var s = Math.floor((second % (3600 * 24) % 3600 % 60));
            s = s < 10 ? '0' + s : s;
            return d + "天" + h + ":" + m + ":" + s;
        }
        if (second / 3600 > 1) {
            var h = Math.floor(second / 3600);
            h = h < 10 ? '0' + h : h;
            var m = Math.floor((second % 3600) / 60);
            m = m < 10 ? '0' + m : m;
            var s = Math.floor((second % 3600) % 60);
            s = s < 10 ? '0' + s : s;
            return h + ":" + m + ":" + s;
        }
        else if (second / 60 > 1) {
            var m = Math.floor(second / 60);
            m = m < 10 ? '0' + m : m;
            var s = Math.floor((second % 60));
            s = s < 10 ? '0' + s : s;
            return '00:' + m + ":" + s;
        }
        else {
            var s = second;
            s = s < 10 ? '0' + s : s;
            return "00:00:" + s;
        }
    }
    Action.turnSecond = turnSecond;
    /**
     * 对象克隆
     */
    function clone(obj) {
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj)
            return obj;
        // Handle Date
        if (obj instanceof Date) {
            var copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }
        // Handle Array
        if (obj instanceof Array) {
            var copy = [];
            for (var i = 0; i < obj.length; ++i) {
                copy[i] = Action.clone(obj[i]);
            }
            return copy;
        }
        // Handle Object
        if (obj instanceof Object) {
            var copy = {};
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr))
                    copy[attr] = Action.clone(obj[attr]);
            }
            return copy;
        }
        throw new Error("Unable to copy obj! Its type isn't supported.");
    }
    Action.clone = clone;
    // **
    // 	 * 加载资源
    // 	 * @param url 资源地址
    // 	 * @param call 回调
    // 	 * @param thisObj 回调所属对象
    // 	 * @param param 回调参数，注意第一个默认是纹理，因此param会在第二个参数
    // 	 */
    function loadUrl(url, call, thisObj, param) {
        var self = this;
        //暂时不用缓存
        // let texture = self.picCache[url]
        var texture = null;
        var endCall = function () {
            return call.call(thisObj, texture, param);
        };
        var loader = new egret.ImageLoader;
        loader.addEventListener(egret.Event.COMPLETE, function (evt) {
            var imageLoader = evt.currentTarget;
            texture = new egret.Texture;
            texture._setBitmapData(imageLoader.data);
            endCall();
        }, self);
        loader.load(url);
    }
    Action.loadUrl = loadUrl;
    /**
     * 取随机数整数(获取数组随机下标用)
     * @prama num 随机数的最大值+1 (可以传数组的length)
     */
    function shuffle(num) {
        return Math.floor((Math.random() * num));
    }
    Action.shuffle = shuffle;
    /**
     * 加载提示
     */
    function showLoading(words) {
        if (words === void 0) { words = '正在加载'; }
        wx.showLoading({
            title: words
        });
    }
    Action.showLoading = showLoading;
    /**
     * 加载资源
     */
    function loadResByUrl(url, type) {
        if (type === void 0) { type = 'image'; }
        return new Promise(function (resolve, reject) {
            var loader = new egret.ImageLoader;
            loader.addEventListener(egret.Event.COMPLETE, function (event) {
                var imageLoader = event.currentTarget;
                var texture = new egret.Texture;
                texture._setBitmapData(imageLoader.data);
                resolve(texture);
            }, null);
            loader.load(url);
        });
    }
    Action.loadResByUrl = loadResByUrl;
    /**
     * 移除加载提示
     */
    function hideLoading() {
        wx.hideLoading();
    }
    Action.hideLoading = hideLoading;
})(Action || (Action = {}));
//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var LoadingUI = (function (_super) {
    __extends(LoadingUI, _super);
    function LoadingUI() {
        var _this = _super.call(this) || this;
        _this.createView();
        return _this;
    }
    LoadingUI.prototype.createView = function () {
        this.textField = new egret.TextField();
        this.addChild(this.textField);
        this.textField.y = 300;
        this.textField.width = 480;
        this.textField.height = 100;
        this.textField.textAlign = "center";
    };
    LoadingUI.prototype.onProgress = function (current, total) {
        this.textField.text = "Loading..." + current + "/" + total;
    };
    return LoadingUI;
}(egret.Sprite));
__reflect(LoadingUI.prototype, "LoadingUI", ["RES.PromiseTaskReporter"]);
var Api = (function () {
    function Api() {
    }
    Api.getInstance = function () {
        if (!Api.api) {
            Api.api = new Api();
        }
        return Api.api;
    };
    Api.get = function (url, para) {
        return new Promise(function (resolve, reject) {
            Api.ajax.get("" + Api.httpUrl + url, para).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error(res['Msg']);
                        if (json['Code'] === 1) {
                            reject('tokenError');
                        }
                        else {
                            reject(json['Msg']);
                        }
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error(res['Msg']);
                        if (res['Code'] === 1) {
                            reject('tokenError');
                        }
                        else {
                            reject(res['Msg']);
                        }
                    }
                }
            });
        });
    };
    /**
     * 发起post请求，请求异常reject空
     */
    Api.post = function (url, data) {
        console.log("" + Api.httpUrl + url, data);
        return new Promise(function (resolve, reject) {
            Api.ajax.post("" + Api.httpUrl + url, data).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error('Msg', json['Msg']);
                        reject(json['Msg']);
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error('Msg', res['Msg']);
                        reject(res['Msg']);
                    }
                }
            }).catch(reject);
        });
    };
    /**
     * 客服消息发送
     */
    Api.prototype.kfPost = function (url, data) {
        return new Promise(function (resolve, reject) {
            Api.ajax.post("https://pkwegame.xunguanggame.com" + url, data).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error(json['Msg']);
                        reject(json['Msg']);
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error(res['Msg']);
                        reject(res['Msg']);
                    }
                }
            });
        });
    };
    /**
     * 发送渠道打点
     */
    Api.prototype.sendChannelDot = function (channel) {
        var data = {
            userId: AppConfig.myInfo.sid,
            appId: Api.appId,
            channel: channel
        };
        Api.ajax.dotPost("" + Api.hostDot + '/channel', data).then(function (res) {
            console.log("\u6E20\u9053\u6570\u636E" + data.channel + "\u53D1\u9001\u6210\u529F ," + res);
        }).catch(function (err) {
            console.error("\u6E20\u9053\u6570\u636E" + data.channel + "\u53D1\u9001\u5931\u8D25 ," + err);
        });
    };
    /**
     * 发起post请求，请求异常reject空
     */
    Api.prototype.dotPost = function (url, data) {
        return new Promise(function (resolve, reject) {
            Api.ajax.dotPost("" + Api.httpUrl + url, data).then(function (res) {
                if (typeof res === 'string') {
                    var json = JSON.parse(res);
                    if (json['Code'] === 0) {
                        resolve(json['Data']);
                    }
                    else {
                        console.error('Msg', json['Msg']);
                        reject(json['Msg']);
                    }
                }
                else {
                    if (res['Code'] === 0) {
                        resolve(res['Data']);
                    }
                    else {
                        console.error('Msg', res['Msg']);
                        reject(res['Msg']);
                    }
                }
            }).catch(reject);
        });
    };
    /**
     *登陆游戏上报
     **/
    Api.prototype.sendloginChannelDot = function () {
        var option = wx.getLaunchOptionsSync() || {};
        console.log("启动参数 option", option);
        //有scene再上报
        var query = option.query;
        var scene = query.scene;
        scene && this.sendChannelDot(scene);
        // 广告主
        var adInfo = query.weixinadinfo;
        if (adInfo) {
            var attr = adInfo.split('.');
            var adtId = attr[0];
            var gdtVid = query.gdt_vid;
            console.log('weixinadinfo:', adtId, gdtVid);
            this.sendChannelDot(['wxad', adtId, gdtVid].join('_'));
        }
    };
    /**
     *游戏中上报游戏操作
     **/
    Api.prototype.sendGameActionDot = function (action) {
        var myInfo = AppConfig.myInfo;
        var data = {
            userId: myInfo.sid,
            appId: Api.appId,
            action: 'pocket_' + action || "wx"
        };
        //打点
        XG_STATISTICS.send('pocket_' + action || "wx");
        console.log('游戏中上报游戏操作!', data);
        Api.ajax.post("" + Api.hostDot + '/action', data).then(function (res) {
            console.log("\u4E0A\u62A5\u6E38\u620F\u64CD\u4F5C" + data.action + "\u53D1\u9001\u6210\u529F ," + res);
        }).catch(function (err) {
            console.error("\u4E0A\u62A5\u6E38\u620F\u64CD\u4F5C" + data.action + "\u53D1\u9001\u5931\u8D25 ," + err);
        });
    };
    Api.httpUrl = 'https://wxgame.xunguanggame.com/cardgame-go-qa';
    // public static httpUrl: string = 'https://wxgame.xunguanggame.com/cardgame-go'
    Api.hostDot = 'https://pkwegame.xunguanggame.com/platform-go';
    // 发送打点数据的host
    Api.appId = 'wxd809893d56d16080';
    Api.staticHost = 'https://static.xunguanggame.com/pocket';
    Api.ajax = Ajax.getInstance();
    /**
     * 登录
     * [post] code userId
     */
    Api.login = '/login';
    return Api;
}());
__reflect(Api.prototype, "Api");
/*
这个类里面的东西基本都需要玩家根据当前游戏
来修改，下面标※的都是要改的，不标的话可不改
*/
/**
 * 游戏配置，由每个游戏自己定义
 */
var AppConfig = (function () {
    function AppConfig() {
    }
    /**
     * 获取skins下的皮肤
     */
    AppConfig.getSkin = function (name) {
        return 'resource/scene/' + name + '.exml';
    };
    AppConfig.isCheckIosPay = false; //标记是否检测过ios充值了
    return AppConfig;
}());
__reflect(AppConfig.prototype, "AppConfig");
var LevelInfo = [
    { id: 1, score: 0, color: 0xd1cfcf },
    { id: 2, score: 200, color: 0xabe187 },
    { id: 3, score: 1000, color: 0x5cac86 },
    { id: 4, score: 2000, color: 0xfdb231 },
    { id: 5, score: 4000, color: 0xabe187 },
    { id: 6, score: 10000, color: 0xc96d98 },
    { id: 7, score: 20000, color: 0x34aaae },
    { id: 8, score: 40000, color: 0x2f5a7c },
    { id: 9, score: 80000, color: 0x547d76 },
    { id: 10, score: 100000, color: 0x7d6a95 },
    { id: 11, score: 200000, color: 0x886669 }
];
var LevelProgress = (function (_super) {
    __extends(LevelProgress, _super);
    function LevelProgress() {
        return _super.call(this, 'game/component/LevelProgressSkin') || this;
    }
    LevelProgress.prototype.init = function (score) {
        var _this = this;
        if (score === void 0) { score = 0; }
        //找到比这个分大的段
        this.nextInfo = LevelInfo.find(function (item) {
            return item.score > score;
        });
        this.nextLevel = this.nextInfo.id;
        this.currentInfo = LevelInfo.find(function (item) {
            return item.id === _this.nextLevel - 1;
        });
        this.currentLevel = this.currentInfo.id;
        this.setColor();
    };
    LevelProgress.prototype.setLevelProgress = function (score) {
        var _this = this;
        var progressTween = egret.Tween.get(this.r_progress);
        if (score > this.nextInfo.score) {
            //这时候要升级
            var width = this.r_bg.width;
            //算出多余的分数
            var otherScore_1 = score - this.nextInfo.score;
            progressTween.to({
                width: width
            }, 300).to({
                width: 0
            }).call(function () {
                _this.levelUp();
                //算出升级之后还要增加的长度
                var otherLength = otherScore_1 / _this.countScore() * _this.r_bg.width;
                progressTween.to({
                    width: otherLength
                });
            });
        }
        else {
            //计算当前分数的长度
            var addScore = score - this.currentInfo.score;
            console.log('addScore', addScore);
            var currentLength = addScore / this.countScore() * this.r_bg.width;
            console.log(currentLength);
            progressTween.to({
                width: currentLength
            }, 300);
        }
    };
    /**
     * 计算出当前等级升级需要的分数差
     */
    LevelProgress.prototype.countScore = function () {
        return this.nextInfo.score - this.currentInfo.score;
    };
    /**
     * 升级
     */
    LevelProgress.prototype.levelUp = function () {
        var _this = this;
        this.currentLevel++;
        this.nextLevel++;
        this.currentInfo = LevelInfo.find((function (item) {
            return item.id === _this.currentLevel;
        }));
        this.nextInfo = LevelInfo.find((function (item) {
            return item.id === _this.nextLevel;
        }));
        this.setColor();
    };
    LevelProgress.prototype.setColor = function () {
        this.r_currentLevelRect.fillColor = this.r_bg.fillColor = this.currentInfo.color;
        this.r_nextLevelRect.fillColor = this.nextInfo.color;
    };
    return LevelProgress;
}(AppComponent));
__reflect(LevelProgress.prototype, "LevelProgress");
/**
 * 卡牌换一换
 * state0/state1/state2   不能点击/可以点击换卡牌/点击看视频加次数
 */
var CardChange = (function (_super) {
    __extends(CardChange, _super);
    function CardChange() {
        var _this = _super.call(this, 'game/component/CardChangeSkin') || this;
        _this.adTimes = 0; //已经换的次数
        _this.init();
        return _this;
    }
    CardChange.prototype.init = function () {
        this.currentState = 'state1';
        this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onClickTap, this);
    };
    CardChange.prototype.onClickTap = function () {
        switch (this.currentState) {
            case 'state0':
                break;
            case 'state1':
                this.onChangeTap();
                break;
            case 'state2':
                this.onAdMovie();
                break;
            default:
                break;
        }
    };
    /**
     * 换卡牌
     */
    CardChange.prototype.onChangeTap = function () {
        var gamePage = AppViews.getTopCommont();
        gamePage.onChangeTap();
        //如果没有看视频的次数了就不能点击
        if (this.adTimes > SETTING.CHANGETIMEMAX) {
            this.currentState = 'state0';
        }
        else {
            this.currentState = 'state2';
        }
    };
    /**
     * 看视频
     */
    CardChange.prototype.onAdMovie = function () {
        var _this = this;
        pfUtils.showLoading('加载广告');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.adTimes++;
                _this.currentState = 'state1';
            }
        });
    };
    return CardChange;
}(AppComponent));
__reflect(CardChange.prototype, "CardChange");
var ResurgenceDialog = (function (_super) {
    __extends(ResurgenceDialog, _super);
    function ResurgenceDialog() {
        var _this = _super.call(this, 'game/ResurgenceSkin') || this;
        _this.init();
        _this.resurgenceBtnTween();
        return _this;
    }
    ResurgenceDialog.prototype.init = function () {
        this.g_resurgence.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onResurgenceTap, this);
        this.l_skip.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSkip, this);
    };
    /**
     * 复活按钮的动画
     */
    ResurgenceDialog.prototype.resurgenceBtnTween = function () {
        var tween = egret.Tween.get(this.g_resurgence, { loop: true });
        tween
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 1, scaleY: 1 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 1, scaleY: 1 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 300)
            .to({ scaleX: 0.8, scaleY: 0.8 }, 1000);
    };
    ResurgenceDialog.prototype.onResurgenceTap = function () {
        var _this = this;
        pfUtils.showLoading('加载视频');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.removeFromParent();
                var gamePage = AppViews.getTopCommont();
                //看视频复活
                gamePage.movieResurgence();
            }
        });
        // this.removeFromParent()
        // var gamePage: any = AppViews.getTopCommont()
        // //看视频复活
        // gamePage.movieResurgence()
    };
    /**
     * 跳过
     */
    ResurgenceDialog.prototype.onSkip = function () {
        this.removeFromParent();
        var gamePage = AppViews.getTopCommont();
        gamePage.gameOver();
    };
    return ResurgenceDialog;
}(Dialog));
__reflect(ResurgenceDialog.prototype, "ResurgenceDialog");
/**
 * 连击的提示
 */
var HitTextComponent = (function (_super) {
    __extends(HitTextComponent, _super);
    function HitTextComponent(type) {
        var _this = _super.call(this, 'game/HitTextSkin') || this;
        _this.init();
        _this.setImage(type);
        return _this;
    }
    HitTextComponent.prototype.init = function () {
        this.anchorOffsetX = this.width / 2;
        this.anchorOffsetY = this.height / 2;
        this.scaleX = this.scaleY = 0;
    };
    /**
     * 设置图片
     * @param 连击数
     */
    HitTextComponent.prototype.setImage = function (type) {
        var _this = this;
        console.log(type);
        if (type > 6) {
            type = 6;
        }
        var url = "hitx" + type + "_png";
        //音效
        Sound.initPlay("hitx" + type + ".mp3");
        this.m_text.source = url;
        egret.Tween.get(this).to({
            scaleX: 1,
            scaleY: 1
        }, 300, egret.Ease.bounceOut)
            .to({}, 800)
            .to({
            scaleX: 0,
            scaleY: 0
        }, 200).call(function () {
            _this.removeFromParent();
        });
    };
    return HitTextComponent;
}(AppComponent));
__reflect(HitTextComponent.prototype, "HitTextComponent");
var SETTING = {
    RECYCLETOTALNUM: 2,
    ALMIGHTYRANDOM: 20,
    BOMBRANDOM: 5,
    ALMIGHTYID: 1,
    NORMALCARDID: 2,
    BOMBCARDID: 3,
    DUSTBINADMAXCOUNT: 5,
    RECALLMAXCOUNT: 5,
    RESURGENCEMAX: 1,
    CHANGETIMEMAX: 2,
};
var GamePanel = (function (_super) {
    __extends(GamePanel, _super);
    function GamePanel() {
        var _this = _super.call(this, 'game/GameSkin') || this;
        _this.totalScore = 0; //当前分数
        _this.hitNum = 0; //连击数量
        _this.currentScore = 0;
        _this.recycleNum = 0; //回收掉的卡牌数量
        _this.saveScore = 0; //记录分数回退用
        _this.$groups = [_this.g_cardsGroup1, _this.g_cardsGroup2, _this.g_cardsGroup3, _this.g_cardsGroup4, _this.g_dustBin];
        _this.canRecall = false; //是否可以回撤(初始为不可以)
        _this.recallTime = 1; //回撤的次数默认送一个
        _this.dustBinAdCount = 0; //已经清空垃圾桶的次数
        _this.recallAdCount = 0; //已经观看的回退广告的次数
        _this.resurgenceTime = 0; //复活次数
        _this.init();
        return _this;
    }
    GamePanel.prototype.init = function () {
        pfUtils.hideBannerAds();
        pfUtils.showBannerAds();
        this.initCardPond();
        this.initGroups();
        this.getHighScore();
        this.bindClick();
        this.initSounds();
        this.initGuide();
        this.levelComponent = new LevelProgress();
        this.levelComponent.init();
        this.g_level.addChild(this.levelComponent);
    };
    GamePanel.prototype.initSounds = function () {
        Sound.initPlay('chopsticks.mp3', false, 0, true);
        Sound.initPlay('click.mp3', false, 0, true);
        Sound.initPlay('bomb.mp3', false, 0, true);
        Sound.initPlay('icon_close.mp3', false, 0, true);
        Sound.initPlay('unlock.mp3', false, 0, true);
    };
    GamePanel.prototype.initGroups = function () {
        var _this = this;
        this.$groups.forEach(function (item, index) {
            if (index !== _this.$groups.length - 1) {
                var card = new CardComponent(0);
                item.addChild(card);
                card.x = card.width / 2;
                card.y = card.height / 2;
            }
        });
        // this.g_recall.getChildByName('count')['text'] = this.recallTime
        this.g_change.addChild(new CardChange());
    };
    /**
     * 新手引导
     */
    GamePanel.prototype.initGuide = function () {
        //获取是不是新手的缓存
        var isNew = LocalData.getItem('CrardsIsNewHand');
        if (!isNew) {
            AppViews.pushDialog(GameExplain);
            LocalData.setItem('CrardsIsNewHand', new Date().toString());
        }
    };
    GamePanel.prototype.bindClick = function () {
        // this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this)
        this.m_pause.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(PauseDialog);
        }, this);
        this.g_explain.addEventListener(egret.TouchEvent.TOUCH_TAP, function () {
            AppViews.pushDialog(GameExplain);
        }, this);
        //垃圾桶视频
        this.m_dustBinAd1.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDustBinAdTap, this);
        this.m_dustBinAd2.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onDustBinAdTap, this);
    };
    GamePanel.prototype.onCardTouchBegin = function (e) {
        this.recordCardInfo();
        var card = e.currentTarget;
        this.currentCard = card;
        this.g_cardsPool.removeChild(card);
        card.x = e.stageX;
        card.y = e.stageY - card.anchorOffsetY;
        //记录手在卡牌上的偏移量
        this.addChild(card);
        this.addEventListener(egret.TouchEvent.TOUCH_MOVE, this.onCardTouchMove, this);
        this.addEventListener(egret.TouchEvent.TOUCH_END, this.onCardTouchEnd, this);
    };
    GamePanel.prototype.onCardTouchMove = function (e) {
        var _this = this;
        var card = this.currentCard;
        card.x = e.stageX;
        card.y = e.stageY - card.anchorOffsetY;
        // var y = card.y - card.anchorOffsetY
        //移动的时候判断在移动到哪个组内
        this.$groups.forEach(function (item, index) {
            if (item.hitTestPoint(card.x, card.y)) {
                //振动
                //最后一个是垃圾桶
                if (index === _this.$groups.length - 1) {
                    return;
                }
                else {
                    var card2 = item.$children[item.numChildren - 1];
                    if (card2.value === card.value || card.property === SETTING.ALMIGHTYID) {
                        card2.setRectStroke(0xacffae);
                    }
                    else {
                        card2.setRectStroke(0x544F4F);
                    }
                }
            }
            else {
                if (index === _this.$groups.length - 1) {
                    // this.dustBinReset()
                }
                else {
                    var card2 = item.$children[item.numChildren - 1];
                    card2.clearRectStroke();
                }
            }
        });
        //是否碰到了垃圾桶  
        //给卡牌两个碰撞点
        var x2 = card.x + card.width / 2, y2 = card.y + card.height / 2;
        var dustBin = this.$groups[4];
        if (dustBin.hitTestPoint(x2, y2) || dustBin.hitTestPoint(card.x, card.y)) {
            if (this.recycleNum <= 1) {
                dustBin.getChildByName((this.recycleNum + 1).toString()).alpha = 0.5;
            }
        }
        else {
            this.dustBinReset();
        }
    };
    GamePanel.prototype.onCardTouchEnd = function (e) {
        var _self = this;
        var card = this.currentCard;
        var index = this.judgeIndex(e, card);
        if (index >= 0) {
            //垃圾桶
            if (index === this.$groups.length - 1) {
                if (this.recycleNum >= SETTING.RECYCLETOTALNUM) {
                    this.cardReset(card);
                    return;
                }
                this.throughToDustBin(card);
                nextStep();
                return;
            }
            this.playCardSound(card.property);
            var group = this.$groups[index];
            //正在合并的时候不让放防止bug
            if (group['isCounting']) {
                this.cardReset(card);
                return;
            }
            var card2 = group.$children[group.numChildren - 1];
            card2 && card2.clearRectStroke();
            //万能卡不能移到空的位置
            if (card.property === SETTING.ALMIGHTYID && group.$children.length <= 1) {
                this.cardReset(card);
                return;
            }
            if (card.property !== SETTING.BOMBCARDID && card.property !== SETTING.ALMIGHTYID) {
                //当卡牌池数量满了并且不能合并
                if (group.numChildren >= 9 && card.value !== card2.value) {
                    this.cardReset(card);
                    return;
                }
                //如果不能合并
                if (card2.value !== card.value && card.property) {
                    //卡牌移入的动画
                    this.cardEndTween(card);
                }
            }
            pfUtils.vibrateShort();
            group.addChild(card);
            this.removeEvent(card);
            card.x = card.anchorOffsetX;
            card.y = (group.numChildren - 2) * 60 + card.anchorOffsetY;
            nextStep();
            this.countGroupCard(group);
        }
        else {
            //如果没有放对位置就回到卡牌池中
            this.cardReset(card);
        }
        function nextStep() {
            _self.produceCard();
            _self.setCardPond();
            if (!_self.canRecall) {
                _self.canRecall = true;
                _self.recallBtnReset();
            }
        }
    };
    /**
     * 放到垃圾桶
     * @param card
     */
    GamePanel.prototype.throughToDustBin = function (card) {
        Sound.play('chopsticks.mp3', 0.6);
        this.removeEvent(card);
        this.removeChild(card);
        this.recycleNum++;
        if (this.dustBinAdCount < SETTING.DUSTBINADMAXCOUNT) {
            this.addDustBinAd();
        }
        this.dustBinReset();
    };
    /**
     * 记录上一步信息
     */
    GamePanel.prototype.recordCardInfo = function () {
        var _this = this;
        //记录当前的所有卡牌数据
        var cardsPool = [];
        this.g_cardsPool.$children.forEach(function (item) {
            cardsPool.push({ value: item.value, property: item.property });
        });
        this.saveCardsPool = cardsPool;
        this.saveInfo = [];
        this.$groups.forEach(function (item, index) {
            if (index !== _this.$groups.length - 1) {
                var group_1 = [];
                item.$children.forEach(function (card) {
                    group_1.push({ value: card.value, x: card.x, y: card.y });
                });
                _this.saveInfo.push(group_1);
            }
        });
        this.lastRecycleNum = this.recycleNum;
        this.saveScore = this.lastScore;
    };
    /**
     * 播放放入卡的音效
     */
    GamePanel.prototype.playCardSound = function (property) {
        if (property === SETTING.BOMBCARDID) {
            Sound.play('bomb.mp3');
        }
        else {
            Sound.play('click.mp3');
        }
    };
    /**
     * 获取最高分
     */
    GamePanel.prototype.getHighScore = function () {
        var score = LocalData.getItem('highScore');
        if (score) {
            this.l_highScore.text = this.setScoreDigit(+score);
        }
        else {
            this.l_highScore.text = '0';
        }
    };
    /**
     * 卡牌回到原位
     * @param card
     */
    GamePanel.prototype.cardReset = function (card) {
        var _this = this;
        this.removeEvent(card);
        egret.Tween.get(card).to({
            x: this.g_cardsPool.x + card.lastX,
            y: this.g_cardsPool.y + card.lastY
        }, 300).call(function () {
            _this.removeChild(card);
            card.x = card.lastX;
            card.y = card.lastY;
            _this.g_cardsPool.addChild(card);
            card.addEventListener(egret.TouchEvent.TOUCH_BEGIN, _this.onCardTouchBegin, _this);
        });
    };
    /**
     * 将卡牌池的第二张移到可操作
     */
    GamePanel.prototype.setCardPond = function () {
        var _this = this;
        var card = this.g_cardsPool.$children[1];
        var x = card.x + 110;
        egret.Tween.get(card).to({
            x: x,
            scaleX: 1,
            scaleY: 1
        }, 500).call(function () {
            card.lastX = card.x;
            card.lastY = card.y;
            //绑定事件
            card.addEventListener(egret.TouchEvent.TOUCH_BEGIN, _this.onCardTouchBegin, _this);
            // this.addEventListener(egret.TouchEvent.TOUCH_CANCEL, this.onCardTouchEnd, this)
        });
    };
    /**
     * 初始化一些属性
     */
    GamePanel.prototype.propertyReset = function (group) {
    };
    /**
     * 判断在哪个位置
     * @param
     */
    GamePanel.prototype.judgeIndex = function (e, card) {
        //移动的时候判断在移动到哪个组内
        var y = card.y;
        var x2 = card.x + card.width / 2, y2 = card.y + card.height / 2;
        for (var i = 0; i < this.$groups.length; i++) {
            var item = this.$groups[i];
            if (i === 4) {
                if (item.hitTestPoint(x2, y2) || item.hitTestPoint(card.x, card.y)) {
                    return 4;
                }
            }
            else {
                if (item.hitTestPoint(card.x, y)) {
                    return i;
                }
            }
        }
        return void 0;
    };
    /**
     * 重置垃圾桶
     */
    GamePanel.prototype.dustBinReset = function () {
        if (this.recycleNum === 1) {
            this.g_dustBin.getChildByName('1').alpha = 0.5;
            this.g_dustBin.getChildByName('2').alpha = 0;
            return;
        }
        if (this.recycleNum === 2) {
            this.g_dustBin.getChildByName('1').alpha = 0.5;
            this.g_dustBin.getChildByName('2').alpha = 0.5;
            return;
        }
        this.g_dustBin.getChildByName('1').alpha = 0;
        this.g_dustBin.getChildByName('2').alpha = 0;
    };
    /**
     * 加一个垃圾桶视频
     */
    GamePanel.prototype.addDustBinAd = function () {
        if (!this.m_dustBinAd1.visible) {
            this.m_dustBinAd1.visible = true;
            this.l_x1.visible = false;
        }
        else {
            this.m_dustBinAd2.visible = true;
            this.l_x2.visible = false;
        }
    };
    /**
     * 减去一个垃圾桶视频
     */
    GamePanel.prototype.removeDustBinAd = function () {
        if (this.m_dustBinAd2.visible) {
            this.m_dustBinAd2.visible = false;
            this.l_x2.visible = true;
        }
        else {
            this.m_dustBinAd1.visible = false;
            this.l_x1.visible = true;
        }
    };
    /**
     * 清除卡牌上的所有绑定事件
     */
    GamePanel.prototype.removeEvent = function (card) {
        card.removeEventListener(egret.TouchEvent.TOUCH_BEGIN, this.onCardTouchBegin, this);
        this.removeEventListener(egret.TouchEvent.TOUCH_MOVE, this.onCardTouchMove, this);
        this.removeEventListener(egret.TouchEvent.TOUCH_END, this.onCardTouchEnd, this);
    };
    /**
     * 初始化生成的卡牌池
     */
    GamePanel.prototype.initCardPond = function () {
        for (var i = 0; i < 2; i++) {
            this.produceCard(2);
        }
        this.setCardPond();
    };
    /**
     * 生成卡片
     * @param num   1固定万能卡/2固定普通卡/3炸弹牌默认
     */
    GamePanel.prototype.produceCard = function (num) {
        if (num === void 0) { num = 0; }
        var _self = this;
        // if (num === SETTING.ALMIGHTYID) {
        //     this.createCard(SETTING.ALMIGHTYID)
        // }
        // else if (num === SETTING.NORMALCARDID) {
        //     getNormalCard()
        // }
        // else {
        //     if (Math.floor(Math.random() * SETTING.ALMIGHTYRANDOM) === 0) {
        //         this.createCard(SETTING.ALMIGHTYID)
        //     } else {
        //         getNormalCard()
        //     }
        // }
        switch (num) {
            //万能牌
            case SETTING.ALMIGHTYID:
                this.createCard(num);
                break;
            //普通牌
            case SETTING.NORMALCARDID:
                getNormalCard();
                break;
            //炸弹牌
            case SETTING.BOMBCARDID:
                this.createCard(num);
                break;
            default:
                if (Math.floor(Math.random() * SETTING.ALMIGHTYRANDOM) === 0) {
                    this.createCard(SETTING.ALMIGHTYID);
                }
                else if ((Math.floor(Math.random() * SETTING.BOMBRANDOM) === 0)) {
                    this.createCard(SETTING.BOMBCARDID);
                }
                else {
                    getNormalCard();
                }
                break;
        }
        function getNormalCard() {
            var array = [1, 2, 3, 4, 5, 6];
            var randomIndex = Math.floor(Math.random() * array.length);
            var value = Math.pow(2, array[randomIndex]);
            _self.createCard(SETTING.NORMALCARDID, value);
        }
    };
    /**
     * 生成一张卡牌
     */
    GamePanel.prototype.createCard = function (type, value) {
        var card = null;
        switch (type) {
            //万能
            case SETTING.ALMIGHTYID:
                card = new AlmightyCard();
                break;
            //炸弹
            case SETTING.BOMBCARDID:
                card = new CardBomb();
                break;
            default:
                card = new CardComponent(value);
        }
        card.property = type;
        this.g_cardsPool.addChildAt(card, 0);
    };
    /**
     * 判断group里面有没有相同的两张牌
     * 如果相同就合并
     */
    GamePanel.prototype.countGroupCard = function (group) {
        var _this = this;
        var index = null; //有相同的话需要删掉的东西
        // this.setCalculate()
        //正在计算的时候不让放卡牌防止bug
        group['isCounting'] = true;
        var lastIndex = group.numChildren - 1;
        var lastCard = group.$children[lastIndex];
        var sideCard = group.$children[lastIndex - 1];
        //炸弹
        if (lastCard.property === SETTING.BOMBCARDID) {
            group['isCounting'] = false;
            this.bombGroup(group);
            return;
        }
        //如果最后一张卡是万能卡
        if (lastCard.property === SETTING.ALMIGHTYID) {
            lastCard.value = sideCard.value;
        }
        if (lastCard.value === sideCard.value) {
            if (!group['hitCount']) {
                group['hitCount'] = 1;
            }
            else {
                group['hitCount']++;
            }
            var tween2 = egret.Tween.get(lastCard), tween = egret.Tween.get(sideCard);
            tween2
                .to({ anchorOffsetY: 170 }, 200)
                .to({ scaleX: 0.8, scaleY: 0.8 }, 200);
            tween
                .to({}, 200)
                .to({ scaleX: 0.8, scaleY: 0.8 }, 200)
                .call(function () {
                Sound.play('unlock.mp3', 0.6);
                //有翻倍
                _this.addScore(lastCard.value * group['hitCount']);
                sideCard.value *= 2;
                sideCard.setBgColor(sideCard.value);
                group.removeChild(lastCard);
                group['hitCount'] > 1 && _this.createLabelTween(group, group['hitCount']);
            })
                .to({ scaleX: 1, scaleY: 1 }, 200, egret.Ease.bounceInOut)
                .call(function () {
                _this.countGroupCard(group);
            });
        }
        else {
            //不能合卡牌的时候判断是否结束
            group['hitCount'] > 2 && this.hitRemind(group['hitCount']);
            group['hitCount'] = 0;
            group['isCounting'] = false;
            this.judgeGameOver();
        }
    };
    /**
     * 清除group的连击数
     */
    GamePanel.prototype.clearHitCount = function (group) {
        group['hitCount'] = 0;
    };
    /**
     * 加分数
     */
    GamePanel.prototype.addScore = function (num) {
        var _this = this;
        this.totalScore += num;
        this.levelComponent.setLevelProgress(this.totalScore);
        this.setBeyond(this.totalScore);
        //数字表示每秒加的次数
        var speed = Math.ceil(num / 20);
        if (this.scoreInterval) {
            this.scoreInterval.stop();
            this.scoreInterval = null;
        }
        this.scoreInterval = new egret.Timer(30);
        this.scoreInterval.addEventListener(egret.TimerEvent.TIMER, function () {
            _this.currentScore += speed;
            _this.l_score.text = _this.setScoreDigit(_this.currentScore);
            if (_this.currentScore >= _this.totalScore) {
                _this.l_score.text = _this.setScoreDigit(_this.totalScore);
                _this.scoreInterval.stop();
            }
        }, this);
        this.scoreInterval.start();
    };
    /**
     * 判断游戏结束
     */
    GamePanel.prototype.judgeGameOver = function () {
        var _this = this;
        var totalFull = 0;
        var groups = this.$groups.slice(0, 4);
        for (var i = 0; i < groups.length; i++) {
            var item = groups[i];
            if (item.numChildren >= 9) {
                totalFull++;
            }
        }
        if (totalFull >= 4) {
            var boolMerge = true;
            //卡牌中没有合成的了（包括没有万能卡）
            var poolCard = this.g_cardsPool.$children[this.g_cardsPool.numChildren - 1];
            var poolCard2 = this.g_cardsPool.$children[0];
            groups.forEach(function (item) {
                var last = item.$children[item.numChildren - 1];
                //如果卡牌池有可以合成的卡牌或者卡牌池第一张是万能牌
                if (last.value === poolCard.value || poolCard.property === SETTING.ALMIGHTYID) {
                    boolMerge = false;
                }
                //如果卡牌池第二张可以合成并且垃圾桶有位置
                if ((last.value === poolCard2.value || poolCard2.property === SETTING.ALMIGHTYID) && _this.recycleNum < 2) {
                    boolMerge = false;
                }
            });
            if (boolMerge) {
                //游戏结束
                if (this.resurgenceTime >= SETTING.RESURGENCEMAX) {
                    this.gameOver();
                }
                else {
                    AppViews.pushDialog(ResurgenceDialog);
                }
            }
        }
    };
    /**
     * =主动结束游戏
     */
    GamePanel.prototype.gameOver = function () {
        AppViews.pushDialog(GameOverDialog, this.totalScore);
        this.setHighScore();
    };
    /**
     * 设置最高分
     */
    GamePanel.prototype.setHighScore = function () {
        var score = this.totalScore;
        var highScore = +LocalData.getItem('highScore');
        if (score >= highScore) {
            LocalData.setItem('highScore', score.toString());
        }
    };
    /**
     * 连击动画
     */
    GamePanel.prototype.createLabelTween = function (obj, num) {
        var _this = this;
        var label = new HitCountComponent(num);
        var card = obj.$children[obj.numChildren - 1];
        label.x = obj.x + obj.width / 2 - label.width / 2;
        label.y = card.y + card.height - 20;
        this.addChild(label);
        egret.Tween.get(label).to({
            anchorOffsetY: 50,
            alpha: 0
        }, 600)
            .call(function () {
            _this.removeChild(label);
        });
    };
    /**
     * 重置回撤按钮
     */
    GamePanel.prototype.recallBtnReset = function () {
        if (this.recallTime >= 1) {
            this.g_recall.getChildByName('rect')['fillColor'] = 0xf97f70;
            this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this);
            this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this);
            this.l_recallTime.visible = this.l_recallX.visible = true;
            this.m_recallAd.visible = false;
        }
        else {
            // this.g_recall.getChildByName('rect')['fillColor'] = 0x686767
            this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onRecallTap, this);
            if (this.recallAdCount < SETTING.RECALLMAXCOUNT) {
                this.l_recallTime.visible = this.l_recallX.visible = false;
                this.m_recallAd.visible = true;
                this.g_recall.addEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this);
            }
            else {
                this.g_recall.getChildByName('rect')['fillColor'] = 0x686767;
                this.l_recallTime.visible = this.l_recallX.visible = true;
                this.m_recallAd.visible = false;
                this.g_recall.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.recallAdShow, this);
            }
        }
    };
    /**
     * 回撤
     */
    GamePanel.prototype.onRecallTap = function () {
        var _this = this;
        if (!this.canRecall || this.recallTime <= 0) {
            return;
        }
        this.recallTime--;
        this.canRecall = false;
        this.recallBtnReset();
        this.saveInfo.forEach(function (item, index) {
            var group = _this.$groups[index];
            group.removeChildren();
            item.forEach(function (item2) {
                var card = new CardComponent(item2.value);
                card.x = item2.x;
                card.y = item2.y;
                group.addChild(card);
            });
        });
        this.g_cardsPool.removeChildren();
        this.saveCardsPool = this.saveCardsPool.reverse();
        this.saveCardsPool.forEach(function (item) {
            _this.createCard(item.property, item.value);
        });
        this.setCardPond();
        this.recycleNum = this.lastRecycleNum;
        if (this.scoreInterval && this.scoreInterval.running) {
            this.scoreInterval.stop();
            this.l_score.text = this.setScoreDigit(this.saveScore) || '0';
        }
        this.dustBinReset();
    };
    /**
     * 看垃圾桶广告视频
     */
    GamePanel.prototype.onDustBinAdTap = function () {
        var _this = this;
        pfUtils.showLoading('加载广告');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.recycleNum--;
                _this.dustBinAdCount++;
                _this.dustBinReset();
                _this.removeDustBinAd();
            }
        });
    };
    /**
     * 看视频复活
     */
    GamePanel.prototype.movieResurgence = function () {
        //取 $group 的前四个
        var groups = this.$groups.slice(0, 4);
        groups.forEach(function (item, index) {
            //删掉一半的牌
            for (var i = item.numChildren - 1; i > 4; i--) {
                item.removeChildAt(i);
            }
        });
        this.resurgenceTime++;
    };
    /**
     * 回退广告
     */
    GamePanel.prototype.recallAdShow = function () {
        var _this = this;
        pfUtils.showLoading('加载广告');
        pfUtils.showAds(0).then(function (res) {
            if (res) {
                //观看成功，可以获得奖励
                // pfUtils.showToast('可以获得奖励')
                _this.recallTime += 1;
                _this.recallAdCount++;
                _this.recallBtnReset();
            }
        });
    };
    /**
     * 弹出连击提示
     */
    GamePanel.prototype.hitRemind = function (type) {
        var hitCom = new HitTextComponent(type);
        hitCom.x = this.width / 2;
        hitCom.y = this.height / 2 - 200;
        this.addChild(hitCom);
    };
    /**
     * 卡牌移入的动画
     */
    GamePanel.prototype.cardEndTween = function (card) {
        egret.Tween.get(card)
            .to({
            scaleY: 0.7
        }, 200)
            .to({
            scaleY: 1
        }, 200, egret.Ease.bounceOut);
    };
    /**
     * 分数保留5位数
     */
    GamePanel.prototype.setScoreDigit = function (score) {
        var string = score.toString();
        if (score > 10000) {
            var num = (score / 1000).toFixed(2);
            string = num + 'k';
        }
        if (score > 1000000) {
            var num = (score / 1000000).toFixed(2);
            string = num + 'm';
        }
        return string;
    };
    /**
     * 卡牌换一换
     */
    GamePanel.prototype.onChangeTap = function () {
        this.g_cardsPool.removeChildren();
        this.initCardPond();
    };
    /**
     * 超越好友
     * @param score 分数
     */
    GamePanel.prototype.setBeyond = function (score) {
        if (this.beyondBitmap) {
            this.removeChild(this.beyondBitmap);
        }
        pfUtils.postMessage('beyond', score);
        var bitmapdata = new egret.BitmapData(window["sharedCanvas"]);
        var texture = new egret.Texture();
        bitmapdata.$deleteSource = false;
        texture._setBitmapData(bitmapdata);
        // 界面显示
        var bitmap = this.beyondBitmap = new egret.Bitmap(texture);
        bitmap.width = this.width;
        bitmap.height = this.height;
        bitmap.x = 135;
        bitmap.y = 90;
        this.addChild(bitmap);
        egret.setTimeout(function () {
            egret.WebGLUtils.deleteWebGLTexture(bitmapdata.webGLTexture);
            bitmapdata.webGLTexture = null;
        }, this, 500);
    };
    /**
     * 炸弹牌使用
     */
    GamePanel.prototype.bombGroup = function (group) {
        var _this = this;
        var cards;
        var score = 0;
        //把第一张炸弹牌消掉
        var bombCard = group.$children[group.$children.length - 1];
        var targetX = this.l_score.x + this.l_score.width / 2, targetY = this.l_score.y + this.l_score.height / 2;
        group.removeChild(bombCard);
        cards = group.$children.slice(1, group.numChildren);
        cards.forEach(function (item, index) {
            var x = group.x + item.x, y = group.y + item.y;
            group.removeChild(item);
            if (item.property === SETTING.NORMALCARDID) {
                score += item.value * 2;
                // this.addScore(item.value * 2)
            }
            item.x = x;
            item.y = y;
            _this.addChild(item);
            egret.Tween.get(item)
                .to({ x: _this.width / 2, y: _this.height / 2 }, 1000, egret.Ease.quartInOut)
                .to({ x: targetX, y: targetY, scaleX: 0, scaleY: 0, alpha: 0.3, }, 500, egret.Ease.quadOut)
                .call(function () {
                _this.removeChild(item);
                if (index === cards.length - 1) {
                    _this.addScore(score);
                }
            });
        });
    };
    return GamePanel;
}(AppComponent));
__reflect(GamePanel.prototype, "GamePanel");
// 存放不好归类的接口
/**
 * 红点
 */
var RedInfo;
(function (RedInfo) {
    RedInfo[RedInfo["T_PARK"] = 0] = "T_PARK";
    RedInfo[RedInfo["T_ATTENTION"] = 1] = "T_ATTENTION";
    RedInfo[RedInfo["T_GETSKIN"] = 2] = "T_GETSKIN";
    RedInfo[RedInfo["T_DAILYTASK"] = 3] = "T_DAILYTASK"; //日常任务红点
})(RedInfo || (RedInfo = {}));
var lie;
(function (lie) {
    /**
     * 游戏生命周期管理类
     * 注：调试模式不要开启
     */
    var LifeCycle = (function () {
        function LifeCycle() {
        }
        /**
         * 游戏生命周期初始化
         */
        LifeCycle.init = function () {
            var self = LifeCycle;
            var lifecycle = egret.lifecycle;
            lifecycle.addLifecycleListener(function (context) {
                context.onUpdate = self.update;
            });
            lifecycle.onPause = self.pause;
            lifecycle.onResume = self.resume;
        };
        /**
         * 全局游戏更新函数
         */
        LifeCycle.update = function () {
        };
        /**
         * 全局游戏暂停函数
         */
        LifeCycle.pause = function () {
            egret.ticker.pause();
        };
        /**
         * 全局游戏恢复函数
         */
        LifeCycle.resume = function () {
            egret.ticker.resume();
        };
        return LifeCycle;
    }());
    lie.LifeCycle = LifeCycle;
    __reflect(LifeCycle.prototype, "lie.LifeCycle");
})(lie || (lie = {}));
var GameExplain = (function (_super) {
    __extends(GameExplain, _super);
    function GameExplain() {
        return _super.call(this, 'game/GameExplainSkin') || this;
    }
    return GameExplain;
}(Dialog));
__reflect(GameExplain.prototype, "GameExplain");
var RectButton = (function (_super) {
    __extends(RectButton, _super);
    function RectButton() {
        var _this = _super.call(this) || this;
        _this.skinName = 'resource/scene/com/RectButtonSkin.exml';
        return _this;
    }
    return RectButton;
}(eui.Component));
__reflect(RectButton.prototype, "RectButton");
/**
 * 声音控制类
 */
var Sound = (function () {
    function Sound() {
    }
    /**
     * 初始化音效并播放
     * name: 名称
     * loop？: 是否循环播放
     * volume? : 音量
     * isPreLoad? : 是否是预加载
     */
    Sound.initPlay = function (name, loop, volume, isPreLoad) {
        var volume1 = (volume == undefined) ? 0.6 : volume;
        if (!Sound.music["" + name]) {
            var url = "https://static.xunguanggame.com/card2048/sounds/" + name;
            var innerAudioContext = wx.createInnerAudioContext();
            innerAudioContext.src = url;
            //系统静音开关
            innerAudioContext.obeyMuteSwitch = true;
            innerAudioContext.volume = volume1;
            //是否循环播放
            if (loop) {
                innerAudioContext.loop = true;
            }
            Sound.music["" + name] = innerAudioContext;
            //播放音乐
            Sound.play(name, volume1, isPreLoad);
        }
        else {
            Sound.play(name, volume1, isPreLoad);
        }
    };
    /**
     * 暂停
     * pramas:
     * name: 名称
     */
    Sound.pause = function (name) {
        try {
            var music = Sound.music["" + name];
            music && music.pause();
            // global.userGameInfo.soundOn && music && music.pause()
        }
        catch (error) {
            console.log(error);
        }
    };
    /**
     * 播放
     * pramas：
     * name: 名称
     */
    Sound.play = function (name, volume, isPreLoad) {
        if (volume === void 0) { volume = 0.6; }
        try {
            var music = Sound.music["" + name];
            music && (music.volume = volume);
            //预加载先静音播放一遍
            if (isPreLoad) {
                music.volume = 0;
                music.play();
                return;
            }
            music && music.play();
            //记录当前播放音乐名(过滤掉音效)
        }
        catch (error) {
            console.log(error);
        }
    };
    /**
     * 静音
     */
    Sound.soundOff = function () {
        for (var key in Sound.music) {
            Sound.music[key].stop();
        }
    };
    Sound.music = {};
    return Sound;
}());
__reflect(Sound.prototype, "Sound");
var HitCountComponent = (function (_super) {
    __extends(HitCountComponent, _super);
    function HitCountComponent(data) {
        var _this = _super.call(this) || this;
        _this.skinName = 'resource/scene/HitCountSkin.exml';
        _this.l_label.text = 'x' + data;
        return _this;
    }
    return HitCountComponent;
}(eui.Component));
__reflect(HitCountComponent.prototype, "HitCountComponent");
var lie;
(function (lie) {
    /**
     * 类型检验工具类
     */
    var TypeUtils = (function () {
        function TypeUtils() {
        }
        /**
         * 检测是否是字符串类型，注意new String检测不通过的
         */
        TypeUtils.isString = function (obj) {
            return typeof obj === 'string' && obj.constructor === String;
        };
        /**
         * 检测是不是数组
         */
        TypeUtils.isArray = function (obj) {
            return Object.prototype.toString.call(obj) === '[object Array]';
        };
        /**
         * 检测是不是数字，注意new Number不算进来
         */
        TypeUtils.isNumber = function (obj) {
            return typeof obj === 'number' && !isNaN(obj); // type NaN === 'number' 所以要去掉
        };
        return TypeUtils;
    }());
    lie.TypeUtils = TypeUtils;
    __reflect(TypeUtils.prototype, "lie.TypeUtils");
})(lie || (lie = {}));
var CardBomb = (function (_super) {
    __extends(CardBomb, _super);
    function CardBomb() {
        var _this = _super.call(this, null) || this;
        _this.skinName = 'resource/scene/card/CardBombSkin.exml';
        return _this;
    }
    return CardBomb;
}(CardComponent));
__reflect(CardBomb.prototype, "CardBomb");
var AlmightyCard = (function (_super) {
    __extends(AlmightyCard, _super);
    function AlmightyCard() {
        var _this = _super.call(this, null) || this;
        _this.skinName = 'resource/scene/AlmightyCardSkin.exml';
        return _this;
    }
    return AlmightyCard;
}(CardComponent));
__reflect(AlmightyCard.prototype, "AlmightyCard");
/**
 * 寻光游戏统计
 * XG_STATISTICS.init(appid);  //初始化
 *
 * XG_STATISTICS.send('123',{a:1})  //埋点
 * 以下三个接口都是第三方自发提供的，若不提供则不统计
 * XG_STATISTICS.sendUserInfo    //发送用户信息
 * XG_STATISTICS.sendKey         //发送用户唯一ID
 * XG_STATISTICS.sendLocation     //发送用户地理信息
 * 付费信息
 * XG_STATISTICS.sendPay          //发送付费信息
 */
var XG_STATISTICS = (function () {
    var appKey = ''; //小游戏appid
    var xg_obj = {}; // SDK返回值
    var hasInit = false; // 是否已经初始化，保证init只执行一次
    var LaunchInfo = {};
    var sysInfoData = {};
    var toolVision = "1.0.3";
    var hasOnhide = false; //是否已经隐藏过，如果隐藏过才会发onshow没有过的话则不发。
    // 初始化
    xg_obj.init = function (appKey) {
        if (appKey === void 0) { appKey = ''; }
        console.log('寻光统计');
        wx.setStorageSync("xg_appKey", appKey);
        // 首次接入游戏，获取渠道，tag，scene信息。
        LaunchInfo = getLaunchInfo();
        // 将这些信息全部存在storage里面，由于本步骤是同步的，因此不会与request请求发生先后顺序的问题
        wx.setStorageSync("LaunchInfo", LaunchInfo);
        XGRequest({
            action: 'dotOfStatistics',
            type: 'login',
        });
        /**
         * 用户首次登陆
         * 记录进入时间&&上传用户信息
         */
        if (!hasInit) {
            hasInit = true;
            // 1.0.3开始不单独发设备信息，但是每次都会带上设备信息
            var sysInfoData = getSysInfo();
            sysInfoData = JSON.stringify(sysInfoData);
            wx.setStorageSync("sysInfoData", sysInfoData);
            // 发送网络信息，
            wx.getNetworkType({
                complete: function (data) {
                    XGRequest({
                        action: 'setNetWorkInfo',
                        type: 'networkInfo',
                        networkType: data.networkType,
                        appKey: appKey,
                    });
                }
            });
        }
        /**
         * 事件监听
         * onhide和onshow事件传launch信息，这样的话留存统计会更加准确
         */
        wx.onHide(function () {
            hasOnhide = true;
            LaunchInfo = getLaunchInfo();
            wx.setStorageSync("LaunchInfo", LaunchInfo);
            XGRequest({
                action: 'dotOfStatistics',
                type: "hide",
            });
        });
        wx.onShow(function () {
            if (!hasOnhide) {
                return false;
            }
            LaunchInfo = getLaunchInfo();
            wx.setStorageSync("LaunchInfo", LaunchInfo);
            XGRequest({
                action: 'dotOfStatistics',
                type: "show",
            });
        });
    };
    // 事件统计
    xg_obj.send = function (key, obj) {
        if (key === void 0) { key = ''; }
        if (obj === void 0) { obj = {}; }
        var objr = {};
        if (obj && typeof (obj) != "object") {
            objr = {
                "null": obj + '',
            };
        }
        else {
            for (var i in obj) {
                objr[i + ''] = obj[i] + '';
            }
        }
        XGRequest({
            type: 'event',
            action: 'eventDotData',
            eventKey: key,
            eventObj: objr,
        });
    };
    // 获取用户信息
    xg_obj.sendUserInfo = function (obj) {
        if (obj === void 0) { obj = {}; }
        var userInfo = obj['userInfo'];
        var userData = {
            nickName: userInfo.nickName.toString(),
            gender: userInfo.gender.toString(),
            language: userInfo.language.toString(),
            city: userInfo.city.toString(),
            province: userInfo.province.toString(),
            country: userInfo.country.toString(),
            avatarUrl: userInfo.avatarUrl.toString()
        };
        var userInfoData = JSON.stringify(userData);
        XGRequest({
            action: 'setWxBaseInfo',
            userBaseInfo: userInfoData,
        });
    };
    // 获取用户唯一标识KEY,如果有openID则用openID，没有则用开发者定义的唯一ID。
    xg_obj.sendKey = function (key) {
        if (key === void 0) { key = ''; }
        XGRequest({
            action: 'bindKeyIdAndUuid',
            keyId: key,
        });
    };
    // 获取收入
    xg_obj.sendPay = function (key, incomNum, status) {
        if (key === void 0) { key = ""; }
        if (incomNum === void 0) { incomNum = ""; }
        if (status === void 0) { status = ""; }
        XGRequest({
            action: 'incomeDataDot',
            type: 'income',
            incomeKey: key,
            incomeNum: incomNum,
            status: status,
        });
    };
    // 获取位置
    xg_obj.sendLocation = function (key, obj) {
        if (key === void 0) { key = ''; }
        if (obj === void 0) { obj = {}; }
        var objData = {};
        objData = {
            latitude: obj['latitude'],
            longitude: obj['longitude'],
            speed: obj['speed']
        };
        XGRequest({
            action: 'sendLocation',
            key: key,
            obj: objData
        });
    };
    // 封装Request请求
    function XGRequest(data) {
        if (data === void 0) { data = {}; }
        appKey = wx.getStorageSync("xg_appKey") || '',
            LaunchInfo = wx.getStorageSync("LaunchInfo") || '',
            sysInfoData = wx.getStorageSync("sysInfoData") || '';
        // 给后端的数据，每次发的数据有UUID，APPKey，版本号，和渠道信息，设备信息
        Object.assign(data, {
            uuid: Uuid(),
            appKey: appKey,
            version: toolVision,
            channelId: LaunchInfo['channelId'],
            channelTag: LaunchInfo['tag'],
            scene: LaunchInfo['scene'],
            deviceInfo: sysInfoData,
        });
        console.log("给后端的data", data);
        wx.request({
            // url: "https://pkwegame.xunguanggame.com/data-acquisition/?action=",
            url: "https://log.xunguanggame.com/data-acquisition/?action=",
            // url: "http://123.206.88.21:8993/?action=",
            data: data,
            header: {},
            method: "GET",
            success: function (t) {
            }
        });
    }
    // 生成用户唯一值uuid
    function Uuid() {
        var uuid = "";
        try {
            uuid = wx.getStorageSync("xgtj_uuid");
        }
        catch (t) {
            uuid = "uuid-getstoragesync";
        }
        if (!uuid) {
            uuid = "" + Date.now() + Math.floor(Math.random() * 1e7);
            try {
                wx.setStorageSync("xgtj_uuid", uuid);
            }
            catch (t) {
                wx.setStorageSync("xgtj_uuid", "uuid-getstoragesync");
            }
        }
        return uuid;
    }
    // 获取时间戳，单位：秒
    function getTime() {
        var time = new Date().getTime();
        time = parseInt(String(time / 1000));
        return time;
    }
    // 获取设备信息
    function getSysInfo() {
        // https://developers.weixin.qq.com/minigame/dev/document/system/system-info/wx.getSystemInfoSync.html
        var gameUserData = {};
        var gameUserInfo = wx.getSystemInfoSync();
        gameUserData = {
            model: changeToString(gameUserInfo['model']),
            pixelRatio: changeToString(gameUserInfo['pixelRatio']),
            windowWidth: changeToString(gameUserInfo['windowWidth']),
            windowHeight: changeToString(gameUserInfo['windowHeight']),
            system: changeToString(gameUserInfo['system']),
            language: changeToString(gameUserInfo['language']),
            version: changeToString(gameUserInfo['version']),
            batteryLevel: changeToString(gameUserInfo['batteryLevel']),
            screenWidth: changeToString(gameUserInfo['screenWidth']),
            screenHeight: changeToString(gameUserInfo['screenHeight']),
            SDKVersion: changeToString(gameUserInfo['SDKVersion']),
            brand: changeToString(gameUserInfo['brand']),
            fontSizeSetting: changeToString(gameUserInfo['fontSizeSetting']),
            statusBarHeight: changeToString(gameUserInfo['statusBarHeight']),
            platform: changeToString(gameUserInfo['platform']),
            devicePixelRatio: changeToString(gameUserInfo['devicePixelRatio']),
        };
        return gameUserData;
    }
    // 将数值转为字符串
    function changeToString(value) {
        if (value) {
            if (typeof (value) == "number") {
                return value.toString();
            }
            else {
                return value;
            }
        }
        else {
            return "";
        }
    }
    // 获取渠道参数
    function getLaunchInfo() {
        // https://developers.weixin.qq.com/minigame/dev/document/system/life-cycle/wx.getLaunchOptionsSync.html
        var LaunchData = {};
        var LaunchInfo = wx.getLaunchOptionsSync();
        console.log("launch1", LaunchInfo);
        // 1047	扫描小程序码 1048	长按图片识别小程序码 1049	手机相册选取小程序码
        var sceneArray = [1011, 1012, 1013, 1047, 1048, 1049];
        //  二维码进入小游戏
        if (sceneArray.indexOf(LaunchInfo.scene) >= 0) {
            if (LaunchInfo.query.scene) {
                var tmpArr = urlToObject(decodeURIComponent(LaunchInfo.query.scene));
                LaunchData = {
                    channelId: tmpArr['f'] || tmpArr['from'] || '',
                    tag: tmpArr['t'] || tmpArr['tag'] || '',
                    scene: LaunchInfo.scene || '',
                };
            }
            else {
                LaunchData = {
                    channelId: LaunchInfo.query.from || '',
                    tag: LaunchInfo.query.tag || '',
                    scene: LaunchInfo.scene || '',
                };
            }
        }
        else {
            LaunchData = {
                channelId: LaunchInfo.query.from || '',
                tag: LaunchInfo.query.tag || '',
                scene: LaunchInfo.scene || '',
            };
        }
        console.log(LaunchData);
        return LaunchData;
    }
    // URL转化为OBJ
    function urlToObject(url) {
        var string = url.split('&');
        var res = {};
        for (var i = 0; i < string.length; i++) {
            var str = string[i].split('=');
            res[str[0]] = str[1];
        }
        return res;
    }
    // 统计在线时长，待定，暂时由后端处理
    function getIntervalTime() {
        // https://www.zhihu.com/question/67543820
    }
    return xg_obj;
})();
//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var ThemeAdapter = (function () {
    function ThemeAdapter() {
    }
    /**
     * 解析主题
     * @param url 待解析的主题url
     * @param onSuccess 解析完成回调函数，示例：compFunc(e:egret.Event):void;
     * @param onError 解析失败回调函数，示例：errorFunc():void;
     * @param thisObject 回调的this引用
     */
    ThemeAdapter.prototype.getTheme = function (url, onSuccess, onError, thisObject) {
        function onResGet(e) {
            onSuccess.call(thisObject, e);
        }
        function onResError(e) {
            if (e.resItem.url == url) {
                RES.removeEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, onResError, null);
                onError.call(thisObject);
            }
        }
        if (typeof generateEUI !== 'undefined') {
            egret.callLater(function () {
                onSuccess.call(thisObject, generateEUI);
            }, this);
        }
        else {
            RES.addEventListener(RES.ResourceEvent.ITEM_LOAD_ERROR, onResError, null);
            RES.getResByUrl(url, onResGet, this, RES.ResourceItem.TYPE_TEXT);
        }
    };
    return ThemeAdapter;
}());
__reflect(ThemeAdapter.prototype, "ThemeAdapter", ["eui.IThemeAdapter"]);
var lie;
(function (lie) {
    var inInit = false;
    var flag = RES.FEATURE_FLAG;
    /**
     * 重写
     */
    var init = function () {
        if (!inInit) {
            inInit = true;
            flag.FIX_DUPLICATE_LOAD = 0; // 允许重复加载，针对合图、位图等
            // 重写，注意不需要该文件请不要加入工程
            var prototype = RES.ResourceLoader.prototype;
            var next = prototype.next;
            // 返回进度
            var curProgress_1 = 0;
            var onProgress_1 = function (reporter, current, total) {
                var onProgress = reporter && reporter.onProgress;
                if (onProgress) {
                    onProgress(current + curProgress_1, total + curProgress_1);
                }
            };
            prototype.next = function () {
                var self = this;
                // 获取错误次数
                var getECount = function (groupName) {
                    var array = self.loadItemErrorDic[groupName];
                    return array ? array.length : 0;
                };
                var _loop_1 = function () {
                    var r = self.getOneResourceInfo();
                    if (!r)
                        return "break";
                    self.loadingCount++;
                    self.loadResource(r)
                        .then(function (response) {
                        self.loadingCount--;
                        RES.host.save(r, response);
                        var groupName = r.groupNames.shift();
                        if (r.groupNames.length == 0) {
                            r.groupNames = undefined;
                        }
                        var reporter = self.reporterDic[groupName];
                        self.numLoadedDic[groupName]++;
                        var current = self.numLoadedDic[groupName];
                        var total = self.groupTotalDic[groupName];
                        var success = current - getECount(groupName);
                        onProgress_1(reporter, success, total);
                        if (current == total) {
                            var groupError = self.groupErrorDic[groupName];
                            self.removeGroupName(groupName);
                            delete self.groupTotalDic[groupName];
                            delete self.numLoadedDic[groupName];
                            delete self.itemListDic[groupName];
                            delete self.groupErrorDic[groupName];
                            var dispatcher = self.dispatcherDic[groupName];
                            if (groupError) {
                                var itemList = self.loadItemErrorDic[groupName];
                                delete self.loadItemErrorDic[groupName];
                                var error = self.errorDic[groupName];
                                delete self.errorDic[groupName];
                                dispatcher.dispatchEventWith("error", false, { itemList: itemList, error: error });
                            }
                            else {
                                dispatcher.dispatchEventWith("complete");
                            }
                            curProgress_1 += success;
                        }
                        self.next();
                    }).catch(function (error) {
                        if (!error.__resource_manager_error__) {
                            // throw error;
                            console.error('资源重大bug，请维护：', error);
                        }
                        self.loadingCount--;
                        delete RES.host.state[r.root + r.name];
                        var times = self.retryTimesDic[r.name] || 1;
                        if (times > self.maxRetryTimes) {
                            delete self.retryTimesDic[r.name];
                            var groupName = r.groupNames.shift();
                            if (r.groupNames.length == 0) {
                                delete r.groupNames;
                            }
                            if (!self.loadItemErrorDic[groupName]) {
                                self.loadItemErrorDic[groupName] = [];
                            }
                            if (self.loadItemErrorDic[groupName].indexOf(r) == -1) {
                                self.loadItemErrorDic[groupName].push(r);
                            }
                            self.groupErrorDic[groupName] = true;
                            var reporter = self.reporterDic[groupName];
                            self.numLoadedDic[groupName]++;
                            var current = self.numLoadedDic[groupName];
                            var total = self.groupTotalDic[groupName];
                            var success = current - getECount(groupName);
                            onProgress_1(reporter, success, total);
                            if (current == total) {
                                var groupError = self.groupErrorDic[groupName];
                                self.removeGroupName(groupName);
                                delete self.groupTotalDic[groupName];
                                delete self.numLoadedDic[groupName];
                                delete self.itemListDic[groupName];
                                delete self.groupErrorDic[groupName];
                                var itemList = self.loadItemErrorDic[groupName];
                                delete self.loadItemErrorDic[groupName];
                                var dispatcher = self.dispatcherDic[groupName];
                                dispatcher.dispatchEventWith("error", false, { itemList: itemList, error: error });
                                curProgress_1 += success;
                            }
                            else {
                                self.errorDic[groupName] = error;
                            }
                            self.next();
                        }
                        else {
                            self.retryTimesDic[r.name] = times + 1;
                            self.failedList.push(r);
                            self.next();
                            return;
                        }
                    });
                };
                while (self.loadingCount < self.thread) {
                    var state_1 = _loop_1();
                    if (state_1 === "break")
                        break;
                }
            };
        }
    };
    /**
     * 游戏开始页面
     */
    var LoadingView = (function (_super) {
        __extends(LoadingView, _super);
        /**
         * @param groupName 预加载组名
         * @param bgUrl 背景图，若存在则会先加入loading
         */
        function LoadingView(groupName) {
            var _this = _super.call(this) || this;
            _this.eGroupName = '$errorGroup'; // 重加载的错误组名，可重写
            _this.eTimeOut = 5000; // 超时检测时间
            _this.$groupName = groupName;
            init();
            return _this;
        }
        /**
         * 开始加载
         */
        LoadingView.prototype.onLoadStart = function () {
            var self = this;
            if (!self.isLoading) {
                var event_2 = RES.ResourceEvent.ITEM_LOAD_ERROR;
                var gName_1 = self.$groupName;
                var remove_1 = function () {
                    self.isLoading = false;
                    gName_1 == self.eGroupName &&
                        delete RES.config.config.groups[gName_1]; // 删除组名
                    RES.removeEventListener(event_2, self.onItemError, self);
                };
                self.isLoading = true;
                self.$errorKeys = [];
                RES.loadGroup(gName_1, 0, self).then(function () {
                    remove_1();
                    flag.FIX_DUPLICATE_LOAD = 1;
                    self.onSuccess();
                }).catch(function () {
                    remove_1();
                    self.onReload();
                });
                RES.addEventListener(event_2, self.onItemError, self);
            }
        };
        /**
         * 加载错误的key值
         */
        LoadingView.prototype.onItemError = function (event) {
            this.$errorKeys.push(event.resItem.name);
        };
        /**
         * 移除监听
         */
        LoadingView.prototype.$removeListener = function () {
            var self = this;
            egret.clearTimeout(self.$timeout);
            lie.NetworkUtils.removeNetworkStatusChange(self.onStatusChange, self);
        };
        /**
         * 重新加载判断
         */
        LoadingView.prototype.onReload = function () {
            var self = this;
            var utils = lie.NetworkUtils;
            utils.getNetworkType().then(function (type) {
                var eGroupName = self.eGroupName;
                var createGroup = eGroupName && RES.createGroup(eGroupName, self.$errorKeys);
                // 创建新组成功
                if (createGroup) {
                    // 修改错误组
                    self.$groupName = eGroupName;
                    // 当前网络状态已是最佳状态
                    if (utils.isBestNetworkType(type))
                        self.onError(1);
                    else {
                        self.$errorNType = type;
                        // 超时检测
                        self.$timeout = egret.setTimeout(self.onError, self, self.eTimeOut, 0);
                        // 状态变化
                        utils.addNetworkStatusChange(self.onStatusChange, self);
                    }
                }
                else {
                    self.onError(-1);
                }
            });
        };
        /**
         * 监听网络状态变化回调
         */
        LoadingView.prototype.onStatusChange = function (type) {
            var self = this;
            // 网络状态升高，自动重新加载
            if (lie.NetworkUtils.networkUpgrade(self.$errorNType, type)) {
                self.$removeListener();
                self.onLoadStart();
            }
        };
        /////////////以下方法供子类修改，记得加上super，上面的方法看不懂则请勿乱改
        /**
         * 进度更新
         */
        LoadingView.prototype.onProgress = function (current, total) {
            // 加载结束
            if (current == total) {
                var self_2 = this;
                // 进度条卡100%情况处理
                egret.setTimeout(function () {
                    !self_2.$isFinish && self_2.onSuccess();
                }, null, 1000);
            }
        };
        /**
         * 加载结束
         */
        LoadingView.prototype.onSuccess = function () {
            this.$isFinish = true;
            this.$removeListener();
        };
        /**
         * 加载错误
         * @param errorCode 错误编码：-1组名错误，请前端同志修改eGroupName，0正常错误，1资源路径错误
         */
        LoadingView.prototype.onError = function (errorCode) {
            this.$removeListener();
        };
        return LoadingView;
    }(egret.DisplayObjectContainer));
    lie.LoadingView = LoadingView;
    __reflect(LoadingView.prototype, "lie.LoadingView");
})(lie || (lie = {}));
// 该变量请看scripts目录下的bricks.ts和wxgame.ts，会初始化为
// WXUtils和BKUtils
// declare const pfUtils: lie.ITFPFUtils;	// 平台工具 
//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-present, Egret Technology.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////
var AssetAdapter = (function () {
    function AssetAdapter() {
    }
    /**
     * @language zh_CN
     * 解析素材
     * @param source 待解析的新素材标识符
     * @param compFunc 解析完成回调函数，示例：callBack(content:any,source:string):void;
     * @param thisObject callBack的 this 引用
     */
    AssetAdapter.prototype.getAsset = function (source, compFunc, thisObject) {
        function onGetRes(data) {
            compFunc.call(thisObject, data, source);
        }
        if (RES.hasRes(source)) {
            var data = RES.getRes(source);
            if (data) {
                onGetRes(data);
            }
            else {
                RES.getResAsync(source, onGetRes, this);
            }
        }
        else {
            RES.getResByUrl(source, onGetRes, this, RES.ResourceItem.TYPE_IMAGE);
        }
    };
    return AssetAdapter;
}());
__reflect(AssetAdapter.prototype, "AssetAdapter", ["eui.IAssetAdapter"]);
;window.Main = Main;pfUtils=new lie.WXUtils;