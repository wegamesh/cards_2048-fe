var egret = window.egret;window.skins={};
                function __extends(d, b) {
                    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
                        function __() {
                            this.constructor = d;
                        }
                    __.prototype = b.prototype;
                    d.prototype = new __();
                };
                window.generateEUI = {};
                generateEUI.paths = {};
                generateEUI.styles = undefined;
                generateEUI.skins = {"eui.Button":"resource/eui_skins/ButtonSkin.exml","eui.CheckBox":"resource/eui_skins/CheckBoxSkin.exml","eui.HScrollBar":"resource/eui_skins/HScrollBarSkin.exml","eui.HSlider":"resource/eui_skins/HSliderSkin.exml","eui.Panel":"resource/eui_skins/PanelSkin.exml","eui.TextInput":"resource/eui_skins/TextInputSkin.exml","eui.ProgressBar":"resource/eui_skins/ProgressBarSkin.exml","eui.RadioButton":"resource/eui_skins/RadioButtonSkin.exml","eui.Scroller":"resource/eui_skins/ScrollerSkin.exml","eui.ToggleSwitch":"resource/eui_skins/ToggleSwitchSkin.exml","eui.VScrollBar":"resource/eui_skins/VScrollBarSkin.exml","eui.VSlider":"resource/eui_skins/VSliderSkin.exml","eui.ItemRenderer":"resource/eui_skins/ItemRendererSkin.exml"};generateEUI.paths['resource/scene/AlmightyCardSkin.exml'] = window.AlmightyCard = (function (_super) {
	__extends(AlmightyCard, _super);
	function AlmightyCard() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 240;
		this.width = 170;
		this.elementsContent = [this._Image1_i(),this._Label1_i()];
	}
	var _proto = AlmightyCard.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bottom = 0;
		t.left = 0;
		t.right = 0;
		t.source = "almightyCard_png";
		t.top = 0;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.text = "万能卡";
		t.y = 186;
		return t;
	};
	return AlmightyCard;
})(eui.Skin);generateEUI.paths['resource/scene/Card.exml'] = window.Card = (function (_super) {
	__extends(Card, _super);
	function Card() {
		_super.call(this);
		this.skinParts = ["r_bg"];
		
		this.height = 240;
		this.width = 170;
		this.elementsContent = [this.r_bg_i(),this._Label1_i()];
		
		eui.Binding.$bindProperties(this, ["hostComponent.value"],[0],this._Label1,"text");
	}
	var _proto = Card.prototype;

	_proto.r_bg_i = function () {
		var t = new eui.Rect();
		this.r_bg = t;
		t.anchorOffsetX = 75;
		t.anchorOffsetY = 105;
		t.bottom = 0;
		t.ellipseHeight = 50;
		t.ellipseWidth = 50;
		t.fillColor = 0x0f0f0f;
		t.left = 0;
		t.right = 0;
		t.scaleX = 1;
		t.strokeColor = 0xffffff;
		t.strokeWeight = 4;
		t.top = 0;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		this._Label1 = t;
		t.bold = true;
		t.fontFamily = "Arial";
		t.size = 30;
		t.strokeColor = 0xffffff;
		t.textColor = 0xffffff;
		t.x = 18;
		t.y = 18;
		return t;
	};
	return Card;
})(eui.Skin);generateEUI.paths['resource/scene/GameOverSkin.exml'] = window.GameOverSkin = (function (_super) {
	__extends(GameOverSkin, _super);
	function GameOverSkin() {
		_super.call(this);
		this.skinParts = ["g_share","g_rank","g_start","g_home"];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this._Rect1_i(),this._Group1_i()];
		
		eui.Binding.$bindProperties(this, ["hostComponent.score"],[0],this._Label2,"text");
	}
	var _proto = GameOverSkin.prototype;

	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.alpha = 0.6;
		t.bottom = 0;
		t.fillColor = 0xffffff;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		return t;
	};
	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.horizontalCenter = 0;
		t.verticalCenter = 0;
		t.elementsContent = [this._Label1_i(),this._Label2_i(),this._Label3_i(),this.g_share_i(),this.g_rank_i(),this.g_start_i(),this.g_home_i()];
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.bold = true;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 1;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 100;
		t.text = "游戏结束";
		t.textColor = 0x424242;
		t.x = 15;
		t.y = 0;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		this._Label2 = t;
		t.horizontalCenter = 33;
		t.textColor = 0x424242;
		t.y = 145;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = -49;
		t.text = "得分：";
		t.textColor = 0x424242;
		t.y = 145;
		return t;
	};
	_proto.g_share_i = function () {
		var t = new eui.Group();
		this.g_share = t;
		t.horizontalCenter = 0.5;
		t.scaleX = 1;
		t.scaleY = 1;
		t.x = 73;
		t.y = 239.15;
		t.elementsContent = [this._Image1_i(),this._Label4_i()];
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 136;
		t.anchorOffsetY = 54;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "btn_blue_png";
		t.x = 136;
		t.y = 54;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 0;
		t.size = 40;
		t.text = "分享给好友";
		t.textColor = 0x424242;
		t.verticalCenter = 0.5;
		return t;
	};
	_proto.g_rank_i = function () {
		var t = new eui.Group();
		this.g_rank = t;
		t.horizontalCenter = 0.5;
		t.scaleX = 1;
		t.scaleY = 1;
		t.x = 63;
		t.y = 659.15;
		t.elementsContent = [this._Image2_i(),this._Label5_i(),this._Image3_i()];
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 138;
		t.anchorOffsetY = 54;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "btn_yellow_png";
		t.x = 138;
		t.y = 54;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 32.5;
		t.size = 40;
		t.text = "排行榜";
		t.textColor = 0x424242;
		t.verticalCenter = 0.5;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.source = "rank_png";
		t.x = 39.59;
		t.y = 31.5;
		return t;
	};
	_proto.g_start_i = function () {
		var t = new eui.Group();
		this.g_start = t;
		t.horizontalCenter = 0.5;
		t.scaleX = 1;
		t.scaleY = 1;
		t.x = 63;
		t.y = 377.15;
		t.elementsContent = [this._Image4_i(),this._Label6_i()];
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 138;
		t.anchorOffsetY = 54;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "btn_green_png";
		t.x = 138;
		t.y = 54;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 0;
		t.size = 40;
		t.text = "再来一局";
		t.textColor = 0x424242;
		t.verticalCenter = 0.5;
		return t;
	};
	_proto.g_home_i = function () {
		var t = new eui.Group();
		this.g_home = t;
		t.horizontalCenter = 0.5;
		t.scaleX = 1;
		t.scaleY = 1;
		t.x = 63;
		t.y = 517.15;
		t.elementsContent = [this._Image5_i(),this._Label7_i()];
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 138;
		t.anchorOffsetY = 54;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "btn_red_png";
		t.x = 138;
		t.y = 54;
		return t;
	};
	_proto._Label7_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 0;
		t.size = 40;
		t.text = "回到首页";
		t.textColor = 0x424242;
		t.verticalCenter = 0;
		return t;
	};
	return GameOverSkin;
})(eui.Skin);generateEUI.paths['resource/scene/HitCountSkin.exml'] = window.HitCountSkin = (function (_super) {
	__extends(HitCountSkin, _super);
	function HitCountSkin() {
		_super.call(this);
		this.skinParts = ["l_label"];
		
		this.height = 20;
		this.width = 40;
		this.elementsContent = [this._Rect1_i(),this.l_label_i()];
	}
	var _proto = HitCountSkin.prototype;

	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.bottom = 0;
		t.ellipseHeight = 10;
		t.ellipseWidth = 10;
		t.fillColor = 0xfc8144;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		return t;
	};
	_proto.l_label_i = function () {
		var t = new eui.Label();
		this.l_label = t;
		t.horizontalCenter = 0;
		t.size = 18;
		t.text = "";
		t.verticalCenter = 0;
		return t;
	};
	return HitCountSkin;
})(eui.Skin);generateEUI.paths['resource/scene/HomeSkin.exml'] = window.HomeSkin = (function (_super) {
	__extends(HomeSkin, _super);
	function HomeSkin() {
		_super.call(this);
		this.skinParts = ["g_start","g_rank","g_gameCircle"];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this._Rect1_i(),this._Image1_i(),this.g_start_i(),this.g_rank_i(),this.g_gameCircle_i()];
	}
	var _proto = HomeSkin.prototype;

	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.bottom = 0;
		t.fillColor = 0xffffff;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.source = "logo_png";
		t.x = 0;
		t.y = 284;
		return t;
	};
	_proto.g_start_i = function () {
		var t = new eui.Group();
		this.g_start = t;
		t.horizontalCenter = 0.5;
		t.y = 796.79;
		t.elementsContent = [this._Image2_i(),this._Label1_i()];
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "btn_blue_png";
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 0;
		t.size = 40;
		t.text = "开始游戏";
		t.textColor = 0x424242;
		t.verticalCenter = 0;
		return t;
	};
	_proto.g_rank_i = function () {
		var t = new eui.Group();
		this.g_rank = t;
		t.horizontalCenter = 0.5;
		t.y = 943.15;
		t.elementsContent = [this._Image3_i(),this._Label2_i(),this._Image4_i()];
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "btn_yellow_png";
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 32.5;
		t.size = 40;
		t.text = "排行榜";
		t.textColor = 0x424242;
		t.verticalCenter = 0.5;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.source = "rank_png";
		t.x = 39.59;
		t.y = 31.5;
		return t;
	};
	_proto.g_gameCircle_i = function () {
		var t = new eui.Group();
		this.g_gameCircle = t;
		t.visible = false;
		t.x = 0;
		t.y = 930.33;
		t.elementsContent = [this._Image5_i(),this._Label3_i()];
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 82.56;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "dark_png";
		t.width = 82.56;
		t.x = 0;
		t.y = 2.5599999999999454;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 24;
		t.text = "游戏圈";
		t.textColor = 0x212121;
		t.x = 5.279999999999999;
		t.y = 91.15000000000009;
		return t;
	};
	return HomeSkin;
})(eui.Skin);generateEUI.paths['resource/scene/PauseSkin.exml'] = window.PauseSkin = (function (_super) {
	__extends(PauseSkin, _super);
	function PauseSkin() {
		_super.call(this);
		this.skinParts = ["r_bg","g_home","g_restart","g_continue","g_closeCount"];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this.r_bg_i(),this._Group1_i()];
	}
	var _proto = PauseSkin.prototype;

	_proto.r_bg_i = function () {
		var t = new eui.Rect();
		this.r_bg = t;
		t.alpha = 0.6;
		t.bottom = 0;
		t.fillColor = 0xFFFFFF;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		return t;
	};
	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.anchorOffsetX = 0;
		t.horizontalCenter = 1.5;
		t.verticalCenter = 0.5;
		t.width = 281.03;
		t.elementsContent = [this._Label1_i(),this.g_home_i(),this.g_restart_i(),this.g_continue_i(),this.g_closeCount_i()];
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.bold = true;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0.5;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 100;
		t.text = "暂停";
		t.textColor = 0x424242;
		t.x = 15;
		t.y = 0;
		return t;
	};
	_proto.g_home_i = function () {
		var t = new eui.Group();
		this.g_home = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.x = 1;
		t.y = 647.81;
		t.elementsContent = [this._Image1_i(),this._Label2_i()];
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 137.33;
		t.anchorOffsetY = 56;
		t.source = "btn_red_png";
		t.x = 137.33;
		t.y = 56;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 36;
		t.text = "回到首页";
		t.textAlign = "center";
		t.textColor = 0x424242;
		t.verticalAlign = "middle";
		t.verticalCenter = 0;
		return t;
	};
	_proto.g_restart_i = function () {
		var t = new eui.Group();
		this.g_restart = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.x = 1;
		t.y = 514.81;
		t.elementsContent = [this._Image2_i(),this._Label3_i()];
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 136;
		t.anchorOffsetY = 57.33;
		t.source = "btn_blue_png";
		t.x = 136;
		t.y = 57.33;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 36;
		t.text = "重新开始";
		t.textAlign = "center";
		t.textColor = 0x424242;
		t.verticalAlign = "middle";
		t.verticalCenter = 0;
		return t;
	};
	_proto.g_continue_i = function () {
		var t = new eui.Group();
		this.g_continue = t;
		t.anchorOffsetX = 137;
		t.anchorOffsetY = 54;
		t.x = 139.92;
		t.y = 430.98;
		t.elementsContent = [this._Image3_i(),this._Label4_i()];
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.source = "btn_green_png";
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 36;
		t.text = "继续游戏";
		t.textAlign = "center";
		t.textColor = 0x424242;
		t.verticalAlign = "middle";
		t.verticalCenter = 0;
		return t;
	};
	_proto.g_closeCount_i = function () {
		var t = new eui.Group();
		this.g_closeCount = t;
		t.anchorOffsetX = 137;
		t.anchorOffsetY = 54;
		t.x = 139.3;
		t.y = 292.49;
		t.elementsContent = [this._Image4_i(),this._Label5_i()];
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.source = "btn_yellow_png";
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 36;
		t.text = "结束游戏";
		t.textAlign = "center";
		t.textColor = 0x424242;
		t.verticalAlign = "middle";
		t.verticalCenter = 0;
		return t;
	};
	return PauseSkin;
})(eui.Skin);generateEUI.paths['resource/scene/RankDialogSkin.exml'] = window.RankDialogSkin = (function (_super) {
	__extends(RankDialogSkin, _super);
	function RankDialogSkin() {
		_super.call(this);
		this.skinParts = ["m_Bg","m_imgTitle","m_imgClose","m_btnShare"];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this.m_Bg_i(),this._Rect1_i(),this._Image1_i(),this.m_imgTitle_i(),this.m_imgClose_i(),this.m_btnShare_i(),this._Image2_i(),this._Label1_i()];
	}
	var _proto = RankDialogSkin.prototype;

	_proto.m_Bg_i = function () {
		var t = new eui.Image();
		this.m_Bg = t;
		t.alpha = 1;
		t.percentHeight = 100;
		t.source = "gameBg_png";
		t.percentWidth = 100;
		return t;
	};
	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.fillAlpha = 0.2;
		t.percentHeight = 100;
		t.percentWidth = 100;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 679;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(1,1,2,2);
		t.source = "rankBg_png";
		t.width = 550;
		t.y = 200;
		return t;
	};
	_proto.m_imgTitle_i = function () {
		var t = new eui.Image();
		this.m_imgTitle = t;
		t.horizontalCenter = 0;
		t.source = "friendRank_png";
		t.y = 125;
		return t;
	};
	_proto.m_imgClose_i = function () {
		var t = new eui.Image();
		this.m_imgClose = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bottom = 1208;
		t.height = 107;
		t.source = "rankReturn_png";
		t.width = 113;
		t.x = 19;
		return t;
	};
	_proto.m_btnShare_i = function () {
		var t = new eui.Image();
		this.m_btnShare = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bottom = 150;
		t.height = 107;
		t.source = "shareToGroup_png";
		t.visible = false;
		t.width = 330;
		t.x = 301;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 109;
		t.horizontalCenter = 0;
		t.source = "myRank_png";
		t.width = 550;
		t.y = 955;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.size = 25;
		t.text = "每周一更新排行榜";
		t.x = 132;
		t.y = 213;
		return t;
	};
	return RankDialogSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ButtonSkin.exml'] = window.skins.ButtonSkin = (function (_super) {
	__extends(ButtonSkin, _super);
	function ButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay","iconDisplay"];
		
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [this._Image1_i(),this.labelDisplay_i(),this.iconDisplay_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
	}
	var _proto = ButtonSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.bottom = 8;
		t.left = 8;
		t.right = 8;
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.top = 8;
		t.verticalAlign = "middle";
		return t;
	};
	_proto.iconDisplay_i = function () {
		var t = new eui.Image();
		this.iconDisplay = t;
		t.horizontalCenter = 0;
		t.verticalCenter = 0;
		return t;
	};
	return ButtonSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/CheckBoxSkin.exml'] = window.skins.CheckBoxSkin = (function (_super) {
	__extends(CheckBoxSkin, _super);
	function CheckBoxSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.elementsContent = [this._Group1_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","alpha",0.7)
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_up_png")
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_down_png")
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_disabled_png")
				])
		];
	}
	var _proto = CheckBoxSkin.prototype;

	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.percentHeight = 100;
		t.percentWidth = 100;
		t.layout = this._HorizontalLayout1_i();
		t.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		return t;
	};
	_proto._HorizontalLayout1_i = function () {
		var t = new eui.HorizontalLayout();
		t.verticalAlign = "middle";
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.alpha = 1;
		t.fillMode = "scale";
		t.source = "checkbox_unselect_png";
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		return t;
	};
	return CheckBoxSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/HScrollBarSkin.exml'] = window.skins.HScrollBarSkin = (function (_super) {
	__extends(HScrollBarSkin, _super);
	function HScrollBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb"];
		
		this.minHeight = 8;
		this.minWidth = 20;
		this.elementsContent = [this.thumb_i()];
	}
	var _proto = HScrollBarSkin.prototype;

	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.height = 8;
		t.scale9Grid = new egret.Rectangle(3,3,2,2);
		t.source = "roundthumb_png";
		t.verticalCenter = 0;
		t.width = 30;
		return t;
	};
	return HScrollBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/HSliderSkin.exml'] = window.skins.HSliderSkin = (function (_super) {
	__extends(HSliderSkin, _super);
	function HSliderSkin() {
		_super.call(this);
		this.skinParts = ["track","thumb"];
		
		this.minHeight = 8;
		this.minWidth = 20;
		this.elementsContent = [this.track_i(),this.thumb_i()];
	}
	var _proto = HSliderSkin.prototype;

	_proto.track_i = function () {
		var t = new eui.Image();
		this.track = t;
		t.height = 6;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_sb_png";
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.source = "thumb_png";
		t.verticalCenter = 0;
		return t;
	};
	return HSliderSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ItemRendererSkin.exml'] = window.skins.ItemRendererSkin = (function (_super) {
	__extends(ItemRendererSkin, _super);
	function ItemRendererSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
		
		eui.Binding.$bindProperties(this, ["hostComponent.data"],[0],this.labelDisplay,"text");
	}
	var _proto = ItemRendererSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.bottom = 8;
		t.fontFamily = "Tahoma";
		t.left = 8;
		t.right = 8;
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.top = 8;
		t.verticalAlign = "middle";
		return t;
	};
	return ItemRendererSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/PanelSkin.exml'] = window.skins.PanelSkin = (function (_super) {
	__extends(PanelSkin, _super);
	function PanelSkin() {
		_super.call(this);
		this.skinParts = ["titleDisplay","moveArea","closeButton"];
		
		this.minHeight = 230;
		this.minWidth = 450;
		this.elementsContent = [this._Image1_i(),this.moveArea_i(),this.closeButton_i()];
	}
	var _proto = PanelSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.bottom = 0;
		t.left = 0;
		t.right = 0;
		t.scale9Grid = new egret.Rectangle(2,2,12,12);
		t.source = "border_png";
		t.top = 0;
		return t;
	};
	_proto.moveArea_i = function () {
		var t = new eui.Group();
		this.moveArea = t;
		t.height = 45;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		t.elementsContent = [this._Image2_i(),this.titleDisplay_i()];
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.bottom = 0;
		t.left = 0;
		t.right = 0;
		t.source = "header_png";
		t.top = 0;
		return t;
	};
	_proto.titleDisplay_i = function () {
		var t = new eui.Label();
		this.titleDisplay = t;
		t.fontFamily = "Tahoma";
		t.left = 15;
		t.right = 5;
		t.size = 20;
		t.textColor = 0xFFFFFF;
		t.verticalCenter = 0;
		t.wordWrap = false;
		return t;
	};
	_proto.closeButton_i = function () {
		var t = new eui.Button();
		this.closeButton = t;
		t.bottom = 5;
		t.horizontalCenter = 0;
		t.label = "close";
		return t;
	};
	return PanelSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ProgressBarSkin.exml'] = window.skins.ProgressBarSkin = (function (_super) {
	__extends(ProgressBarSkin, _super);
	function ProgressBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb","labelDisplay"];
		
		this.minHeight = 18;
		this.minWidth = 30;
		this.elementsContent = [this._Image1_i(),this.thumb_i(),this.labelDisplay_i()];
	}
	var _proto = ProgressBarSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_pb_png";
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.percentHeight = 100;
		t.source = "thumb_pb_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.horizontalCenter = 0;
		t.size = 15;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		t.verticalCenter = 0;
		return t;
	};
	return ProgressBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/RadioButtonSkin.exml'] = window.skins.RadioButtonSkin = (function (_super) {
	__extends(RadioButtonSkin, _super);
	function RadioButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.elementsContent = [this._Group1_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","alpha",0.7)
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_up_png")
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_down_png")
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_disabled_png")
				])
		];
	}
	var _proto = RadioButtonSkin.prototype;

	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.percentHeight = 100;
		t.percentWidth = 100;
		t.layout = this._HorizontalLayout1_i();
		t.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		return t;
	};
	_proto._HorizontalLayout1_i = function () {
		var t = new eui.HorizontalLayout();
		t.verticalAlign = "middle";
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.alpha = 1;
		t.fillMode = "scale";
		t.source = "radiobutton_unselect_png";
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		return t;
	};
	return RadioButtonSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ScrollerSkin.exml'] = window.skins.ScrollerSkin = (function (_super) {
	__extends(ScrollerSkin, _super);
	function ScrollerSkin() {
		_super.call(this);
		this.skinParts = ["horizontalScrollBar","verticalScrollBar"];
		
		this.minHeight = 20;
		this.minWidth = 20;
		this.elementsContent = [this.horizontalScrollBar_i(),this.verticalScrollBar_i()];
	}
	var _proto = ScrollerSkin.prototype;

	_proto.horizontalScrollBar_i = function () {
		var t = new eui.HScrollBar();
		this.horizontalScrollBar = t;
		t.bottom = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.verticalScrollBar_i = function () {
		var t = new eui.VScrollBar();
		this.verticalScrollBar = t;
		t.percentHeight = 100;
		t.right = 0;
		return t;
	};
	return ScrollerSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/TextInputSkin.exml'] = window.skins.TextInputSkin = (function (_super) {
	__extends(TextInputSkin, _super);
	function TextInputSkin() {
		_super.call(this);
		this.skinParts = ["textDisplay","promptDisplay"];
		
		this.minHeight = 40;
		this.minWidth = 300;
		this.elementsContent = [this._Image1_i(),this._Rect1_i(),this.textDisplay_i()];
		this.promptDisplay_i();
		
		this.states = [
			new eui.State ("normal",
				[
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("textDisplay","textColor",0xff0000)
				])
			,
			new eui.State ("normalWithPrompt",
				[
					new eui.AddItems("promptDisplay","",1,"")
				])
			,
			new eui.State ("disabledWithPrompt",
				[
					new eui.AddItems("promptDisplay","",1,"")
				])
		];
	}
	var _proto = TextInputSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.fillColor = 0xffffff;
		t.percentHeight = 100;
		t.percentWidth = 100;
		return t;
	};
	_proto.textDisplay_i = function () {
		var t = new eui.EditableText();
		this.textDisplay = t;
		t.height = 24;
		t.left = "10";
		t.right = "10";
		t.size = 20;
		t.textColor = 0x000000;
		t.verticalCenter = "0";
		t.percentWidth = 100;
		return t;
	};
	_proto.promptDisplay_i = function () {
		var t = new eui.Label();
		this.promptDisplay = t;
		t.height = 24;
		t.left = 10;
		t.right = 10;
		t.size = 20;
		t.textColor = 0xa9a9a9;
		t.touchEnabled = false;
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	return TextInputSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ToggleSwitchSkin.exml'] = window.skins.ToggleSwitchSkin = (function (_super) {
	__extends(ToggleSwitchSkin, _super);
	function ToggleSwitchSkin() {
		_super.call(this);
		this.skinParts = [];
		
		this.elementsContent = [this._Image1_i(),this._Image2_i()];
		this.states = [
			new eui.State ("up",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
		];
	}
	var _proto = ToggleSwitchSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.source = "on_png";
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		this._Image2 = t;
		t.horizontalCenter = -18;
		t.source = "handle_png";
		t.verticalCenter = 0;
		return t;
	};
	return ToggleSwitchSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/VScrollBarSkin.exml'] = window.skins.VScrollBarSkin = (function (_super) {
	__extends(VScrollBarSkin, _super);
	function VScrollBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb"];
		
		this.minHeight = 20;
		this.minWidth = 8;
		this.elementsContent = [this.thumb_i()];
	}
	var _proto = VScrollBarSkin.prototype;

	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.height = 30;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(3,3,2,2);
		t.source = "roundthumb_png";
		t.width = 8;
		return t;
	};
	return VScrollBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/VSliderSkin.exml'] = window.skins.VSliderSkin = (function (_super) {
	__extends(VSliderSkin, _super);
	function VSliderSkin() {
		_super.call(this);
		this.skinParts = ["track","thumb"];
		
		this.minHeight = 30;
		this.minWidth = 25;
		this.elementsContent = [this.track_i(),this.thumb_i()];
	}
	var _proto = VSliderSkin.prototype;

	_proto.track_i = function () {
		var t = new eui.Image();
		this.track = t;
		t.percentHeight = 100;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_png";
		t.width = 7;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.horizontalCenter = 0;
		t.source = "thumb_png";
		return t;
	};
	return VSliderSkin;
})(eui.Skin);generateEUI.paths['resource/scene/game/GameExplainSkin.exml'] = window.GameExplainSkin = (function (_super) {
	__extends(GameExplainSkin, _super);
	function GameExplainSkin() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Label4_i(),this._Image1_i(),this._Label5_i(),this._Image2_i(),this._Image3_i(),this._Image4_i()];
	}
	var _proto = GameExplainSkin.prototype;

	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 0;
		t.text = "将底部的卡牌拖动到这里";
		t.y = 376.78;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 0;
		t.text = "相同数值的卡牌可以合成为更大数值的卡牌";
		t.y = 423.15;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.text = "点击这里可以返回到你操作的上一步";
		t.x = 135;
		t.y = 886.44;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.text = "不想要的卡牌可以丢弃到这里";
		t.x = 218.15;
		t.y = 1038.02;
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64.98;
		t.rotation = 350.39;
		t.source = "arrow_png";
		t.width = 64.98;
		t.x = 112.08;
		t.y = 936.51;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.text = "点击这里可以换一批卡牌";
		t.x = 22;
		t.y = 763.08;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64.98;
		t.rotation = 321.02;
		t.source = "arrow_png";
		t.width = 64.98;
		t.x = 22;
		t.y = 843.15;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64.98;
		t.rotation = 136.71;
		t.source = "arrow_png";
		t.width = 64.98;
		t.x = 413.15;
		t.y = 323.13;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64.98;
		t.rotation = 179.11;
		t.source = "arrow_png";
		t.width = 64.98;
		t.x = 595.6;
		t.y = 1019.84;
		return t;
	};
	return GameExplainSkin;
})(eui.Skin);generateEUI.paths['resource/scene/game/GameSkin.exml'] = window.GameSkin = (function (_super) {
	__extends(GameSkin, _super);
	function GameSkin() {
		_super.call(this);
		this.skinParts = ["g_cardsGroup4","g_cardsGroup3","g_cardsGroup2","g_cardsGroup1","g_cardsPool","l_score","l_x2","l_x1","m_dustBinAd1","m_dustBinAd2","g_dustBin","l_recallX","l_recallTime","m_recallAd","g_recall","m_pause","l_highScore","g_change","g_explain","g_level"];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this._Rect1_i(),this._Rect2_i(),this._Rect3_i(),this._Rect4_i(),this._Rect5_i(),this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this.g_cardsGroup4_i(),this.g_cardsGroup3_i(),this.g_cardsGroup2_i(),this.g_cardsGroup1_i(),this.g_cardsPool_i(),this.l_score_i(),this.g_dustBin_i(),this._Rect9_i(),this.g_recall_i(),this.m_pause_i(),this._Group1_i(),this.g_change_i(),this.g_explain_i(),this._Label3_i(),this.g_level_i()];
		
		eui.Binding.$bindProperties(this, ["hostComponent.recallTime"],[0],this.l_recallTime,"text");
	}
	var _proto = GameSkin.prototype;

	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.bottom = 0;
		t.fillColor = 0xffffff;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		return t;
	};
	_proto._Rect2_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.ellipseHeight = 50;
		t.ellipseWidth = 50;
		t.fillColor = 0xf4f4f4;
		t.height = 231.98;
		t.width = 163.09;
		t.x = 383.3;
		t.y = 164.21;
		return t;
	};
	_proto._Rect3_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.ellipseHeight = 50;
		t.ellipseWidth = 50;
		t.fillColor = 0xf4f4f4;
		t.height = 231.76;
		t.width = 164.45;
		t.x = 21.73;
		t.y = 163.61;
		return t;
	};
	_proto._Rect4_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.ellipseHeight = 50;
		t.ellipseWidth = 50;
		t.fillColor = 0xf4f4f4;
		t.height = 231.76;
		t.width = 164.45;
		t.x = 202.73;
		t.y = 164.21;
		return t;
	};
	_proto._Rect5_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.ellipseHeight = 50;
		t.ellipseWidth = 50;
		t.fillColor = 0xf4f4f4;
		t.height = 231.74;
		t.width = 164.45;
		t.x = 563.03;
		t.y = 164.62;
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.alpha = .5;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 119.08;
		t.source = "upload_png";
		t.width = 119.08;
		t.x = 44.14;
		t.y = 218.61;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.alpha = 0.5;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 119.08;
		t.source = "upload_png";
		t.width = 119.08;
		t.x = 224.99;
		t.y = 218.4;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.alpha = 0.5;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 119.08;
		t.source = "upload_png";
		t.width = 119.08;
		t.x = 406.98;
		t.y = 214.4;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.alpha = 0.5;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 119.08;
		t.source = "upload_png";
		t.width = 119.08;
		t.x = 586.32;
		t.y = 214.4;
		return t;
	};
	_proto.g_cardsGroup4_i = function () {
		var t = new eui.Group();
		this.g_cardsGroup4 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 239.59;
		t.minHeight = 210;
		t.width = 171.15;
		t.x = 560.03;
		t.y = 160.61;
		return t;
	};
	_proto.g_cardsGroup3_i = function () {
		var t = new eui.Group();
		this.g_cardsGroup3 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 239.59;
		t.minHeight = 210;
		t.width = 171.15;
		t.x = 379.03;
		t.y = 160.61;
		return t;
	};
	_proto.g_cardsGroup2_i = function () {
		var t = new eui.Group();
		this.g_cardsGroup2 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 239.59;
		t.minHeight = 210;
		t.width = 171.15;
		t.x = 199.73;
		t.y = 160.61;
		return t;
	};
	_proto.g_cardsGroup1_i = function () {
		var t = new eui.Group();
		this.g_cardsGroup1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 239.59;
		t.minHeight = 210;
		t.width = 171.15;
		t.x = 18.73;
		t.y = 160.61;
		return t;
	};
	_proto.g_cardsPool_i = function () {
		var t = new eui.Group();
		this.g_cardsPool = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.x = 317.79;
		t.y = 962.92;
		return t;
	};
	_proto.l_score_i = function () {
		var t = new eui.Label();
		this.l_score = t;
		t.bold = true;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0.5;
		t.size = 38;
		t.text = "0";
		t.textColor = 0x212121;
		t.y = 34;
		return t;
	};
	_proto.g_dustBin_i = function () {
		var t = new eui.Group();
		this.g_dustBin = t;
		t.height = 184;
		t.width = 132;
		t.x = 588.85;
		t.y = 870.49;
		t.elementsContent = [this._Rect6_i(),this._Rect7_i(),this._Image5_i(),this._Rect8_i(),this.l_x2_i(),this.l_x1_i(),this._Label1_i(),this.m_dustBinAd1_i(),this.m_dustBinAd2_i()];
		return t;
	};
	_proto._Rect6_i = function () {
		var t = new eui.Rect();
		t.alpha = 0;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xff3535;
		t.height = 90;
		t.left = 4;
		t.name = "1";
		t.right = 4;
		t.y = 91;
		return t;
	};
	_proto._Rect7_i = function () {
		var t = new eui.Rect();
		t.alpha = 0;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.ellipseHeight = 0;
		t.ellipseWidth = 0;
		t.fillColor = 0xff3535;
		t.height = 88;
		t.left = 3;
		t.name = "2";
		t.right = 4;
		t.y = 3;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0.01;
		t.height = 184.2;
		t.scale9Grid = new egret.Rectangle(46,62,33,35);
		t.source = "dust_png";
		t.width = 131.8;
		t.x = 0;
		t.y = 0.01;
		return t;
	};
	_proto._Rect8_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 66;
		t.bottom = 0;
		t.ellipseHeight = 10;
		t.ellipseWidth = 10;
		t.fillAlpha = 0;
		t.fillColor = 0xc6c6c6;
		t.left = 0;
		t.right = 0;
		t.strokeAlpha = 1;
		t.strokeWeight = 4;
		t.top = 0;
		t.visible = false;
		return t;
	};
	_proto.l_x2_i = function () {
		var t = new eui.Label();
		this.l_x2 = t;
		t.horizontalCenter = 0;
		t.size = 36;
		t.text = "x";
		t.textColor = 0x515151;
		t.y = 24;
		return t;
	};
	_proto.l_x1_i = function () {
		var t = new eui.Label();
		this.l_x1 = t;
		t.horizontalCenter = 0;
		t.size = 36;
		t.text = "x";
		t.textColor = 0x515151;
		t.y = 125;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.text = "丢弃";
		t.textColor = 0x515151;
		t.verticalCenter = 0;
		return t;
	};
	_proto.m_dustBinAd1_i = function () {
		var t = new eui.Image();
		this.m_dustBinAd1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 73;
		t.horizontalCenter = 0;
		t.source = "ad_png";
		t.visible = false;
		t.width = 73;
		t.y = 108;
		return t;
	};
	_proto.m_dustBinAd2_i = function () {
		var t = new eui.Image();
		this.m_dustBinAd2 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 73;
		t.horizontalCenter = 0;
		t.source = "ad_png";
		t.visible = false;
		t.width = 73;
		t.y = 7.5;
		return t;
	};
	_proto._Rect9_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.ellipseHeight = 5;
		t.ellipseWidth = 5;
		t.fillColor = 0x7a7a7a;
		t.height = 8;
		t.left = 23;
		t.right = 21;
		t.visible = false;
		t.y = 822.24;
		return t;
	};
	_proto.g_recall_i = function () {
		var t = new eui.Group();
		this.g_recall = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 54.63;
		t.touchEnabled = false;
		t.width = 114.85;
		t.x = 37;
		t.y = 997.99;
		t.elementsContent = [this._Rect10_i(),this.l_recallX_i(),this._Image6_i(),this.l_recallTime_i(),this.m_recallAd_i()];
		return t;
	};
	_proto._Rect10_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bottom = 0;
		t.ellipseHeight = 20;
		t.ellipseWidth = 20;
		t.fillColor = 0x686767;
		t.left = 0;
		t.name = "rect";
		t.right = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.top = 0;
		return t;
	};
	_proto.l_recallX_i = function () {
		var t = new eui.Label();
		this.l_recallX = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 30.12;
		t.name = "count";
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 24;
		t.text = "x";
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.verticalCenter = 1.6849999999999987;
		t.width = 29.88;
		t.x = 59.43;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 43;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "recall_png";
		t.verticalCenter = -0.8150000000000013;
		t.width = 43;
		t.x = 10.03;
		return t;
	};
	_proto.l_recallTime_i = function () {
		var t = new eui.Label();
		this.l_recallTime = t;
		t.size = 26;
		t.x = 84;
		t.y = 16;
		return t;
	};
	_proto.m_recallAd_i = function () {
		var t = new eui.Image();
		this.m_recallAd = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 50;
		t.source = "adw_png";
		t.visible = false;
		t.width = 50;
		t.x = 54.73;
		t.y = 3.33;
		return t;
	};
	_proto.m_pause_i = function () {
		var t = new eui.Group();
		this.m_pause = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 60.61;
		t.width = 66.68;
		t.x = 0.26;
		t.y = 23.02;
		t.elementsContent = [this._Image7_i()];
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 36.55;
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "pause_png";
		t.verticalCenter = 0;
		t.width = 36.55;
		return t;
	};
	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 42;
		t.width = 169;
		t.x = 82;
		t.y = 33.06;
		t.elementsContent = [this._Label2_i(),this.l_highScore_i()];
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.bottom = 0;
		t.fontFamily = "Microsoft YaHei";
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 30;
		t.text = "最高分：";
		t.textColor = 0x707070;
		t.top = 0;
		t.verticalAlign = "middle";
		t.x = 1.73;
		return t;
	};
	_proto.l_highScore_i = function () {
		var t = new eui.Label();
		this.l_highScore = t;
		t.anchorOffsetY = 0;
		t.bottom = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 30;
		t.text = "";
		t.textColor = 0x707070;
		t.top = 5;
		t.verticalAlign = "middle";
		t.x = 118.93;
		return t;
	};
	_proto.g_change_i = function () {
		var t = new eui.Group();
		this.g_change = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 86;
		t.width = 113.33;
		t.x = 37;
		t.y = 889.18;
		return t;
	};
	_proto.g_explain_i = function () {
		var t = new eui.Group();
		this.g_explain = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 51;
		t.width = 59;
		t.x = 489.88;
		t.y = 29.09;
		t.elementsContent = [this._Image8_i()];
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 42.43;
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "explain_png";
		t.verticalCenter = 0;
		t.width = 42.43;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.size = 22;
		t.text = "即将超越:";
		t.textColor = 0x707070;
		t.x = 18;
		t.y = 109.12;
		return t;
	};
	_proto.g_level_i = function () {
		var t = new eui.Group();
		this.g_level = t;
		t.x = 22.48;
		t.y = 821;
		return t;
	};
	return GameSkin;
})(eui.Skin);generateEUI.paths['resource/scene/game/HitTextSkin.exml'] = window.HitTextSkin = (function (_super) {
	__extends(HitTextSkin, _super);
	function HitTextSkin() {
		_super.call(this);
		this.skinParts = ["m_text"];
		
		this.width = 750;
		this.elementsContent = [this.m_text_i()];
	}
	var _proto = HitTextSkin.prototype;

	_proto.m_text_i = function () {
		var t = new eui.Image();
		this.m_text = t;
		t.horizontalCenter = 0;
		t.source = "";
		t.y = 0;
		return t;
	};
	return HitTextSkin;
})(eui.Skin);generateEUI.paths['resource/scene/game/ResurgenceSkin.exml'] = window.ResurgenceSkin = (function (_super) {
	__extends(ResurgenceSkin, _super);
	function ResurgenceSkin() {
		_super.call(this);
		this.skinParts = ["g_resurgence2","l_skip","g_resurgence"];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this._Rect1_i(),this._Label1_i(),this.g_resurgence2_i(),this.l_skip_i(),this.g_resurgence_i(),this._Label4_i()];
	}
	var _proto = ResurgenceSkin.prototype;

	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.alpha = 0.6;
		t.bottom = 0;
		t.fillColor = 0xFFFFFF;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.bold = true;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 100;
		t.text = "游戏结束";
		t.textColor = 0x424242;
		t.x = 15;
		t.y = 332;
		return t;
	};
	_proto.g_resurgence2_i = function () {
		var t = new eui.Group();
		this.g_resurgence2 = t;
		t.horizontalCenter = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.visible = false;
		t.y = 735.15;
		t.elementsContent = [this._Image1_i(),this._Label2_i(),this._Image2_i()];
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 138;
		t.anchorOffsetY = 54;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "btn_green_png";
		t.x = 138;
		t.y = 54;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 46.5;
		t.size = 40;
		t.text = "复活";
		t.textColor = 0x424242;
		t.verticalCenter = 0.5;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 86.24;
		t.source = "ad_png";
		t.verticalCenter = -0.5;
		t.width = 86.24;
		t.x = 27.68;
		return t;
	};
	_proto.l_skip_i = function () {
		var t = new eui.Label();
		this.l_skip = t;
		t.horizontalCenter = 0;
		t.size = 30;
		t.text = "跳过";
		t.textColor = 0x2b2b2b;
		t.y = 821.82;
		return t;
	};
	_proto.g_resurgence_i = function () {
		var t = new eui.Group();
		this.g_resurgence = t;
		t.anchorOffsetX = 131;
		t.anchorOffsetY = 133;
		t.horizontalCenter = 0;
		t.scaleX = 0.8;
		t.scaleY = 0.8;
		t.y = 668;
		t.elementsContent = [this._Image3_i(),this._Label3_i(),this._Image4_i(),this._Image5_i()];
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "resurgenceBtn2_png";
		t.x = 3.680000000000007;
		t.y = 8.149999999999977;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.bold = true;
		t.horizontalCenter = 2;
		t.size = 40;
		t.text = "复活";
		t.textColor = 0x424242;
		t.verticalCenter = 68.5;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 128.33;
		t.horizontalCenter = 0;
		t.source = "ad_png";
		t.width = 128.33;
		t.y = 48.32;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 103.62;
		t.horizontalCenter = 0.5;
		t.source = "movie_png";
		t.visible = false;
		t.width = 118.5;
		t.y = 58.38;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.text = "看视频复活";
		t.textColor = 0x424242;
		t.x = 300;
		t.y = 490.12;
		return t;
	};
	return ResurgenceSkin;
})(eui.Skin);generateEUI.paths['resource/scene/test/TestSkin.exml'] = window.TestSkin = (function (_super) {
	__extends(TestSkin, _super);
	function TestSkin() {
		_super.call(this);
		this.skinParts = ["r_frame","g_awards","l_start"];
		
		this.height = 1334;
		this.width = 750;
		this.elementsContent = [this.g_awards_i(),this._Rect16_i(),this._Rect17_i(),this.l_start_i()];
	}
	var _proto = TestSkin.prototype;

	_proto.g_awards_i = function () {
		var t = new eui.Group();
		this.g_awards = t;
		t.x = 68;
		t.y = 39;
		t.elementsContent = [this._Rect1_i(),this._Rect2_i(),this._Rect3_i(),this._Rect4_i(),this._Rect5_i(),this._Rect6_i(),this._Rect7_i(),this._Rect8_i(),this._Rect9_i(),this._Rect10_i(),this._Rect11_i(),this._Rect12_i(),this._Rect13_i(),this._Rect14_i(),this._Rect15_i(),this.r_frame_i()];
		return t;
	};
	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xe8e8e8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 1;
		t.y = 1;
		return t;
	};
	_proto._Rect2_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 53;
		t.y = 1;
		return t;
	};
	_proto._Rect3_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 103.5;
		t.y = 1;
		return t;
	};
	_proto._Rect4_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 153.5;
		t.y = 1;
		return t;
	};
	_proto._Rect5_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 205.5;
		t.y = 1;
		return t;
	};
	_proto._Rect6_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 259.5;
		t.y = 1;
		return t;
	};
	_proto._Rect7_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 259.5;
		t.y = 52;
		return t;
	};
	_proto._Rect8_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 259.5;
		t.y = 97;
		return t;
	};
	_proto._Rect9_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 205.5;
		t.y = 143;
		return t;
	};
	_proto._Rect10_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 153.5;
		t.y = 143;
		return t;
	};
	_proto._Rect11_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 103.5;
		t.y = 143;
		return t;
	};
	_proto._Rect12_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 53;
		t.y = 143;
		return t;
	};
	_proto._Rect13_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 1;
		t.y = 143;
		return t;
	};
	_proto._Rect14_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 1;
		t.y = 97;
		return t;
	};
	_proto._Rect15_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 1;
		t.y = 52;
		return t;
	};
	_proto.r_frame_i = function () {
		var t = new eui.Rect();
		this.r_frame = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillAlpha = 0;
		t.fillColor = 0xffffff;
		t.height = 36.33;
		t.strokeColor = 0xcc0202;
		t.strokeWeight = 2;
		t.width = 39;
		t.x = 1;
		t.y = 1;
		return t;
	};
	_proto._Rect16_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xE8E8E8;
		t.height = 36;
		t.scaleX = 1;
		t.scaleY = 1;
		t.width = 39;
		t.x = 328;
		t.y = 182;
		return t;
	};
	_proto._Rect17_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xf4c76e;
		t.height = 32;
		t.width = 73;
		t.x = 181;
		t.y = 247;
		return t;
	};
	_proto.l_start_i = function () {
		var t = new eui.Label();
		this.l_start = t;
		t.anchorOffsetY = 0;
		t.height = 23;
		t.size = 20;
		t.text = "开始";
		t.x = 200;
		t.y = 251.5;
		return t;
	};
	return TestSkin;
})(eui.Skin);generateEUI.paths['resource/scene/card/CardBombSkin.exml'] = window.CardBombSkin = (function (_super) {
	__extends(CardBombSkin, _super);
	function CardBombSkin() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 240;
		this.width = 170;
		this.elementsContent = [this._Image1_i(),this._Label1_i(),this._Label2_i()];
	}
	var _proto = CardBombSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.source = "bomb_png";
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.bold = true;
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = 0;
		t.text = "炸弹牌";
		t.y = 167;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.horizontalCenter = 0;
		t.size = 14;
		t.text = "可以消除一整列的卡牌";
		t.textColor = 0xc9c7c7;
		t.y = 211;
		return t;
	};
	return CardBombSkin;
})(eui.Skin);generateEUI.paths['resource/scene/com/Button.exml'] = window.Button = (function (_super) {
	__extends(Button, _super);
	function Button() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 300;
		this.width = 400;
	}
	var _proto = Button.prototype;

	return Button;
})(eui.Skin);generateEUI.paths['resource/scene/com/RectButtonSkin.exml'] = window.RectButtonSkin = (function (_super) {
	__extends(RectButtonSkin, _super);
	function RectButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.height = 96;
		this.width = 225;
		this.elementsContent = [this._Rect1_i(),this.labelDisplay_i()];
	}
	var _proto = RectButtonSkin.prototype;

	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bottom = 0;
		t.ellipseHeight = 30;
		t.ellipseWidth = 30;
		t.fillColor = 0x4FD6C7;
		t.left = 0;
		t.right = 0;
		t.scaleX = 1;
		t.scaleY = 1;
		t.top = 0;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.horizontalCenter = 0;
		t.text = "";
		t.verticalCenter = 0;
		return t;
	};
	return RectButtonSkin;
})(eui.Skin);generateEUI.paths['resource/scene/game/component/CardChangeSkin.exml'] = window.CardChangeSkin = (function (_super) {
	__extends(CardChangeSkin, _super);
	function CardChangeSkin() {
		_super.call(this);
		this.skinParts = ["g_change"];
		
		this.elementsContent = [this.g_change_i()];
		this.states = [
			new eui.State ("state0",
				[
					new eui.SetProperty("_Image2","visible",false)
				])
			,
			new eui.State ("state1",
				[
					new eui.SetProperty("_Rect1","fillColor",0x5cac86),
					new eui.SetProperty("_Image2","visible",false)
				])
			,
			new eui.State ("state2",
				[
					new eui.SetProperty("_Image1","visible",false)
				])
		];
	}
	var _proto = CardChangeSkin.prototype;

	_proto.g_change_i = function () {
		var t = new eui.Group();
		this.g_change = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 86;
		t.width = 113.33;
		t.y = 0;
		t.elementsContent = [this._Rect1_i(),this._Image1_i(),this._Label1_i(),this._Image2_i()];
		return t;
	};
	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		this._Rect1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bottom = 0;
		t.ellipseHeight = 20;
		t.ellipseWidth = 20;
		t.fillColor = 0x686767;
		t.left = 0;
		t.name = "rect";
		t.right = 0;
		t.strokeColor = 0x444444;
		t.top = 0;
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 37.33;
		t.horizontalCenter = 0;
		t.name = "change";
		t.source = "change_png";
		t.width = 37.33;
		t.y = 9.64;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.fontFamily = "Microsoft YaHei";
		t.horizontalCenter = -0.16499999999999915;
		t.size = 24;
		t.text = "换一批";
		t.y = 53.97;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		this._Image2 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64;
		t.horizontalCenter = 0;
		t.name = "ad";
		t.source = "adw_png";
		t.width = 64;
		t.y = -3.69;
		return t;
	};
	return CardChangeSkin;
})(eui.Skin);generateEUI.paths['resource/scene/game/component/LevelProgressSkin.exml'] = window.LevelProgressSkin = (function (_super) {
	__extends(LevelProgressSkin, _super);
	function LevelProgressSkin() {
		_super.call(this);
		this.skinParts = ["r_bg","r_progress","r_currentLevelRect","r_nextLevelRect"];
		
		this.width = 706;
		this.elementsContent = [this.r_bg_i(),this.r_progress_i(),this.r_currentLevelRect_i(),this.r_nextLevelRect_i(),this._Label1_i(),this._Label2_i()];
		
		eui.Binding.$bindProperties(this, ["hostComponent.nextLevel"],[0],this._Label1,"text");
		eui.Binding.$bindProperties(this, ["hostComponent.currentLevel"],[0],this._Label2,"text");
	}
	var _proto = LevelProgressSkin.prototype;

	_proto.r_bg_i = function () {
		var t = new eui.Rect();
		this.r_bg = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0x7a7a7a;
		t.height = 9.2;
		t.left = 30;
		t.right = 30;
		return t;
	};
	_proto.r_progress_i = function () {
		var t = new eui.Rect();
		this.r_progress = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillAlpha = 0.5;
		t.fillColor = 0xffffff;
		t.height = 9.3;
		t.x = 30;
		return t;
	};
	_proto.r_currentLevelRect_i = function () {
		var t = new eui.Rect();
		this.r_currentLevelRect = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0x7a7a7a;
		t.height = 30;
		t.width = 30;
		return t;
	};
	_proto.r_nextLevelRect_i = function () {
		var t = new eui.Rect();
		this.r_nextLevelRect = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.fillColor = 0xf75151;
		t.height = 30;
		t.right = 0;
		t.width = 30;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		this._Label1 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26.8;
		t.right = 0;
		t.size = 20;
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.width = 30.55;
		t.y = 2.26;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		this._Label2 = t;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26.8;
		t.size = 20;
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.width = 30.55;
		t.x = 0;
		t.y = 2.59;
		return t;
	};
	return LevelProgressSkin;
})(eui.Skin);