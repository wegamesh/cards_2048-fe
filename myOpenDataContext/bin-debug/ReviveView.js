var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * 复活界面
 */
var ReviveView = (function (_super) {
    __extends(ReviveView, _super);
    /**
     * @param 传入当前的分数
     */
    function ReviveView(score) {
        var _this = _super.call(this) || this;
        _this.initHisScore();
        _this.initExceed(score);
        return _this;
    }
    /**
     * 初始化历史最高分
     */
    ReviveView.prototype.initHisScore = function () {
        return __awaiter(this, void 0, void 0, function () {
            var kvData, info, text;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, WXUtils.getUserKVData()];
                    case 1:
                        kvData = _a.sent();
                        info = WXUtils.getInfoByKVData(kvData);
                        text = new egret.TextField;
                        text.size = 38;
                        text.y = 324;
                        text.textColor = 0xfbf0b0;
                        text.text = '历史最高分：' + info.score;
                        text.x = 750 - text.width >> 1;
                        this.addChild(text);
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 初始化即将超过谁
     */
    ReviveView.prototype.initExceed = function (score) {
        return __awaiter(this, void 0, void 0, function () {
            var clzz, infos, i, len, info;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        clzz = WXUtils;
                        return [4 /*yield*/, clzz.getFriendInfos()];
                    case 1:
                        infos = _a.sent();
                        infos.sort(function (a, b) {
                            return a.score - b.score;
                        });
                        for (i = 0, len = infos.length; i < len; i++) {
                            info = infos[i];
                            if (!clzz.isMyInfo(info) && info.score > score) {
                                // 创建即将超过谁的文本
                                this.initBotView(info);
                                break;
                            }
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 创建底部界面
     */
    ReviveView.prototype.initBotView = function (info) {
        var view = new egret.DisplayObjectContainer;
        var height = view.height = 96;
        // 左边文本
        var text0 = new egret.TextField;
        text0.size = 38;
        text0.text = '下个即将超过的好友：';
        // 中间头像
        var head = new egret.Bitmap;
        head.height = head.width = height;
        Main.setTexture(head, info.avatarUrl);
        // 右边分数
        var text1 = new egret.TextField;
        text1.size = 38;
        text1.text = info.score + '';
        // 加入view
        view.addChild(text0);
        view.addChild(head);
        view.addChild(text1);
        // 设置坐标
        text1.y = text0.y = height - text0.height >> 1; // 文字尺寸一样可以这样处理
        head.x = text0.width - 4;
        text1.x = head.x + height + 14;
        view.x = 750 - view.width >> 1;
        view.y = 1180;
        this.addChild(view);
    };
    return ReviveView;
}(egret.DisplayObjectContainer));
__reflect(ReviveView.prototype, "ReviveView");
