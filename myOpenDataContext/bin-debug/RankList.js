var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/**
 * 排行榜界面
 */
var RankList = (function (_super) {
    __extends(RankList, _super);
    function RankList() {
        var _this = _super.call(this) || this;
        // var infos = this.$infos;
        _this.initView();
        // 列表
        // var list = this.m_pList = new eui.List;
        // var layout = list.layout = new eui.VerticalLayout;
        // var width = list.width = 508;
        // // list.height = 760;
        // list.height = 1000;
        // layout.gap = 14;
        // list.itemRenderer = RankItem;
        // list.x = 750 - width >> 1;
        // list.y = 240;
        // this.addChild(list);
        //在这里定义自己的单独列表
        WXUtils.getFriendInfos().then(function (infos) {
            // 排名初始化
            infos.sort(function (a, b) { return b.score - a.score; });
            infos.map(function (v, i) { v.rank = i + 1; });
            // 更新页数
            _this.$infos = infos;
            _this.setListData(_this.$infos);
        });
        return _this;
    }
    RankList.prototype.initView = function () {
        var self = this;
        var scroller = new eui.Scroller;
        var content = self.m_pList = new eui.List;
        var width = scroller.width = 574;
        scroller.height = 560;
        scroller.x = 750 - width >> 1;
        scroller.y = 240;
        self.addChild(scroller);
        scroller.viewport = content;
        content.itemRenderer = RankItem;
        scroller.bounces = false;
        scroller.scrollPolicyV = eui.ScrollPolicy.ON;
        // 个人信息
        var myItem = this.m_pMyItem = new MyRankItem;
        myItem.height = 208;
        myItem.width = width;
        myItem.x = 750 - width >> 1;
        myItem.y = 962;
        this.addChild(myItem);
    };
    /**
     * 设置列表数据类型
     */
    // public setType(type: number = 0): void {
    // 	var self = this;
    // 	var utils = WXUtils;
    // 	var infos = self.m_pCurInfos;
    // 	// var filterInfo=utils.sortInfo(infos, type);
    // 	self.m_pList.dataProvider = new eui.ArrayCollection(infos);	// 用的静态数据，只能new
    // 	//找出自己的信息
    // 	var myInfo;
    // 	for (let i = 0, len = infos.length; i < len; i++) {
    // 		let info = infos[i];
    // 		if (WXUtils.isMyInfo(info)) {
    // 			myInfo = info;
    // 			break;
    // 		}
    // 	}
    // 	// 初始化我的信息
    // 	// console.log(WXUtils.$userInfo);
    // 	this.m_pMyItem.setData(myInfo);
    // }
    /**
     * 设置列表数据
     */
    RankList.prototype.setListData = function (list) {
        var self = this;
        self.m_pCurInfos = list;
        var utils = WXUtils;
        var infos = self.m_pCurInfos;
        // var filterInfo=utils.sortInfo(infos, type);
        self.m_pList.dataProvider = new eui.ArrayCollection(infos); // 用的静态数据，只能new
        //找出自己的信息
        var myInfo;
        for (var i = 0, len = infos.length; i < len; i++) {
            var info = infos[i];
            if (WXUtils.isMyInfo(info)) {
                myInfo = info;
                break;
            }
        }
        // 初始化我的信息
        // console.log(WXUtils.$userInfo);
        this.m_pMyItem.setData(myInfo);
    };
    return RankList;
}(egret.DisplayObjectContainer));
__reflect(RankList.prototype, "RankList");
