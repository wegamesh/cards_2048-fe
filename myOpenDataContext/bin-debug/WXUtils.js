var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var WXUtils = (function () {
    function WXUtils() {
    }
    // public static userInfo: UserGameData;					// 玩家信息数据
    /**
     * 获取玩家的信息
     */
    WXUtils.getUserInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self;
            return __generator(this, function (_a) {
                self = WXUtils;
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        var info = self.$userInfo;
                        info ? resolve(info) :
                            wx.getUserInfo({
                                openIdList: ['selfOpenId'],
                                lang: 'zh_CN',
                                success: function (res) {
                                    // 注意这里的openid假的，不知为何
                                    info = self.$userInfo = res.data[0];
                                    info.nickname = info.nickName; // 注意个人信息这个坑
                                    resolve(info);
                                },
                                fail: function (res) {
                                    reject();
                                }
                            });
                    })];
            });
        });
    };
    /**
     * 获取玩家游戏数据
     */
    WXUtils.getUserKVData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self;
            return __generator(this, function (_a) {
                self = WXUtils;
                return [2 /*return*/, new Promise(function (resolve) {
                        var data = self.$userKVData;
                        data ? resolve(data) :
                            wx.getUserCloudStorage({
                                keyList: self.keyList,
                                success: function (res) {
                                    resolve(res.KVDataList);
                                },
                                fail: function (res) {
                                    resolve();
                                }
                            });
                    })];
            });
        });
    };
    /**
     * 更新玩家游戏数据
     */
    WXUtils.updateUserKVData = function (nInfo) {
        var self = WXUtils;
        var oData = self.$userKVData;
        var notice = Dispatch.notice;
        var message = 'updateSocre';
        if (nInfo && oData) {
            var oInfo = self.getInfoByKVData(oData);
            // 存入新数据
            if (oInfo.score < nInfo.score) {
                var kvData = self.getKVDataByInfo(nInfo);
                ;
                self.setUserKVData(kvData);
                oInfo = nInfo;
            }
            notice(message, oInfo);
        }
        else {
            self.getUserKVData().then(function (data) {
                var info = data && self.getInfoByKVData(data);
                if (nInfo) {
                    if (!info || info.score < nInfo.score) {
                        self.setUserKVData(self.getKVDataByInfo(nInfo));
                        info = nInfo;
                    }
                }
                notice(message, info);
            });
        }
    };
    /**
     * 获取玩家游戏数据
     */
    WXUtils.getGameInfos = function (attr, keyList, resolve, shareTicket) {
        var self = this;
        var call = wx[attr];
        if (call) {
            // 获取缓存内的数据
            var infos_1 = WXUtils.m_pInfos;
            var cache = infos_1[attr];
            if (cache)
                resolve(cache);
            else {
                var obj = {
                    keyList: keyList,
                    success: function (res) {
                        console.log(res);
                        var nData = infos_1[attr] = self.initGameDatas(res.data);
                        resolve(nData);
                    },
                    fail: function (res) {
                        self.$getUserInfo(resolve);
                    }
                };
                shareTicket && (obj.shareTicket = shareTicket);
                call(obj);
            }
        }
        else
            call([]);
    };
    /**
     * 获取好友数据
     */
    WXUtils.getFriendInfos = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self;
            return __generator(this, function (_a) {
                self = WXUtils;
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        self.getGameInfos('getFriendCloudStorage', self.keyList, resolve);
                    })];
            });
        });
    };
    /**
     * 获取自己的数据
     */
    WXUtils.$getUserInfo = function (resolve) {
        var self = WXUtils;
        Promise.all([self.getUserInfo(), self.getUserKVData()]).then(function (array) {
            var infos = [];
            if (array) {
                var info = array[0];
                info.KVDataList = array[1];
                infos[0] = info;
            }
            resolve(infos);
        });
    };
    /**
     * 保存玩家游戏数据
     */
    WXUtils.setUserKVData = function (data) {
        return new Promise(function (resolve) {
            WXUtils.$userKVData = data;
            wx.setUserCloudStorage({
                KVDataList: data,
                success: function () {
                    resolve(true);
                },
                fail: function () {
                    resolve(false);
                }
            });
        });
    };
    /**
     * 获取KV数据
     */
    WXUtils.getInfoByKVData = function (datas) {
        var info = { score: 0 };
        for (var i in datas) {
            var data = datas[i];
            info[data.key] = data.value;
        }
        return info;
    };
    /**
     * 获取游戏数据
     */
    WXUtils.getKVDataByInfo = function (info) {
        var datas = [];
        var getStr = function (v) {
            if (typeof v === 'object')
                v = JSON.parse(v);
            return v + '';
        };
        for (var i in info) {
            var data = {};
            data.key = i;
            data.value = getStr(info[i]);
            datas.push(data);
        }
        return datas;
    };
    /**
     * 检测info是否是我的数据
     */
    WXUtils.isMyInfo = function (info) {
        var myInfo = WXUtils.$userInfo;
        return myInfo.avatarUrl == info.avatarUrl
            && myInfo.nickName == info.nickname; // 小心大小写
    };
    /**
     * 清除数据
     */
    WXUtils.clearData = function () {
        WXUtils.m_pInfos = {};
    };
    /**
     * 获取相近的数据（返回的列表会加上rank字段）
     * @param infos 列表
     * @param myInfo 我的数据，保证myInfo在列表中
     * @param maxNum 最多找几个人，奇数
     */
    WXUtils.getNearInfos = function (infos, myInfo, maxNum) {
        if (maxNum === void 0) { maxNum = 3; }
        // 根据自己的位置找出附近三人
        var myIndex = infos.indexOf(myInfo), len = infos.length;
        var startI;
        // 自己排名最前
        if (myIndex == 0)
            startI = myIndex;
        else if (myIndex == len - 1)
            startI = myIndex - maxNum + 1;
        else
            startI = myIndex - (maxNum / 2 | 0);
        if (startI < 0)
            startI = 0;
        // 限制最多拿三个
        var shows = [];
        for (var i = startI, len_1 = infos.length, j = 0; i < len_1 && j < maxNum; i++, j++) {
            var info = infos[i];
            info.rank = i + 1;
            shows.push(info);
        }
        return shows;
    };
    // 根据游戏自己的字段进行修改
    /**
     * 初始化需要的数据
     */
    WXUtils.initGameDatas = function (datas) {
        var self = WXUtils;
        var nData = [];
        for (var i in datas) {
            var item = datas[i];
            if (item.openid) {
                var info = self.getInfoByKVData(item.KVDataList);
                // 初始化分数字段
                item.score = Number(info.score);
                item.time = Number(info.time);
                nData.push(item);
            }
        }
        return nData;
    };
    WXUtils.keyList = ['score', 'time']; // 游戏存放数据Key值
    WXUtils.m_pInfos = {}; // 存放玩家好友、群数据
    return WXUtils;
}());
__reflect(WXUtils.prototype, "WXUtils");
