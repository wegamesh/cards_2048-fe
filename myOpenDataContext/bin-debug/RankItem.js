var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * 排行榜子项
 */
var RankItem = (function (_super) {
    __extends(RankItem, _super);
    function RankItem() {
        var _this = _super.call(this) || this;
        _this.initView();
        return _this;
    }
    RankItem.prototype.onDestroy = function () {
        this.data = null;
    };
    /**
     * 初始化界面
     */
    RankItem.prototype.initView = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self, clzz, bg, width, height, rank, head, hSize, shape, rank1, name, score;
            return __generator(this, function (_a) {
                self = this, clzz = Main;
                bg = self.m_imgBg = new eui.Image;
                width = self.width = 508;
                height = self.height = 105;
                self.addChild(bg);
                rank = self.m_lblRank = new eui.Label;
                rank.size = 38;
                rank.verticalCenter = 0;
                rank.horizontalCenter = -242;
                self.addChild(rank);
                head = self.m_imgHead = new eui.Image;
                hSize = head.height = head.width = 60;
                shape = clzz.getCircleShape(hSize);
                head.verticalCenter = shape.verticalCenter = 0;
                head.horizontalCenter = shape.horizontalCenter = -182;
                head.mask = shape;
                self.addChild(head);
                self.addChild(shape);
                rank1 = self.m_imgRank = new eui.Image;
                rank1.y = 3;
                rank1.horizontalCenter = -152;
                self.addChild(rank1);
                name = self.m_lblName = new eui.Label;
                // name.x = 210;
                // name.y = 22;
                name.verticalCenter = 0;
                name.horizontalCenter = 16;
                name.size = 30;
                // name.textAlign = name.left;
                self.addChild(name);
                score = self.m_lblScore = new eui.Label;
                // score.x = 500;
                // score.y = 22;
                score.verticalCenter = 0;
                score.horizontalCenter = 200;
                score.size = 38;
                self.addChild(score);
                return [2 /*return*/];
            });
        });
    };
    /**
     * 数据变化
     */
    RankItem.prototype.dataChanged = function () {
        var self = this;
        var clzz = Main;
        var setTexture = clzz.setTexture;
        var data = self.data;
        var rank = data.rank;
        var rankImg = self.m_imgRank;
        var headImg = self.m_imgHead;
        var rankLbl = self.m_lblRank;
        // 排名
        rankLbl.text = '' + rank;
        rankLbl.textColor = self.getRankColor(rank);
        if (rankImg.visible = rank < 4) {
            setTexture(rankImg, 'resource/asserts/settle/icon_no' + rank + '_report.png');
        }
        // 头像
        setTexture(headImg, data.avatarUrl);
        setTexture(self.m_imgBg, self.getItemBg(rank));
        self.m_lblName.text = Main.getLimitStr(data.nickname, 27);
        self.m_lblScore.text = '' + data.score;
    };
    /**
     * 获取背景
     */
    RankItem.prototype.getItemBg = function (rank) {
        // var idx = rank < 4 ? rank : '';
        // return 'resource/rank/kuabg' + idx + '.png'
        var idx = rank % 2;
        if (idx == 1) {
            return 'resource/asserts/rankPict/othersRank.png';
        }
        // else{
        // 	return 'resource/asserts/rankPict/myRank.png'
        // }	
    };
    /**
     * 获取排名颜色
     */
    RankItem.prototype.getRankColor = function (rank) {
        var colors = [0xffdb00, 0xfd973d, 0xd59f7b];
        return colors[rank - 1] || 0xa5a5a5;
    };
    return RankItem;
}(eui.ItemRenderer));
__reflect(RankItem.prototype, "RankItem");
/**
 * 我的排名
 */
var MyRankItem = (function (_super) {
    __extends(MyRankItem, _super);
    function MyRankItem() {
        var _this = _super.call(this) || this;
        _this.initView();
        return _this;
    }
    /**
     * 初始化界面
     */
    MyRankItem.prototype.initView = function () {
        var self = this;
        // 排名
        var rank = self.m_lblRank = new eui.Label;
        rank.size = 40;
        // rank.verticalCenter = 0;
        // rank.horizontalCenter = -206;
        // rank.verticalCenter = 0;
        // rank.horizontalCenter = -230;
        rank.x = 28;
        rank.y = 12;
        self.addChild(rank);
        // 头像
        var head = self.m_imgHead = new eui.Image;
        var hSize = head.height = head.width = 60;
        var shape = Main.getCircleShape(hSize);
        // head.verticalCenter = shape.verticalCenter = 0;
        // head.horizontalCenter = shape.horizontalCenter = -82;
        head.x = shape.x = 75;
        head.y = shape.y = -3;
        head.mask = shape;
        self.addChild(head);
        self.addChild(shape);
        // 排名皇冠
        var rank1 = self.m_imgRank = new eui.Image;
        rank1.x = 115;
        rank1.y = -16;
        // rank1.horizontalCenter = -82;
        self.addChild(rank1);
        // 微信名字
        var name = self.m_lblName = new eui.Label;
        name.x = 240;
        name.y = 12;
        name.size = 36;
        // name.textAlign = name.left;
        self.addChild(name);
        // 分数
        var score = self.m_lblScore = new eui.Label;
        score.x = 450;
        score.y = 12;
        score.size = 40;
        self.addChild(score);
    };
    /**
     * 设置界面
     */
    MyRankItem.prototype.setData = function (data) {
        var self = this;
        self.m_lblRank.text = data.rank;
        // 头像
        Main.setTexture(self.m_imgHead, data.avatarUrl);
        // setTexture(data.avatarUrl, self.onLoadHead, self);
        self.m_lblName.text = data.nickname;
        self.m_lblScore.text = '';
        self.m_lblScore.text = data.score;
        // console.log("走到这里了吗");
        console.log(data.nickname, "昵称");
        console.log(data.score, "昵称fenshu");
        console.log(data.rank, "rank");
        console.log(data.avatarUrl, "touxiang");
        var rankImg = self.m_imgRank;
        if (rankImg.visible = data.rank < 4) {
            Main.setTexture(rankImg, 'resource/asserts/settle/icon_no' + data.rank + '_report.png');
        }
    };
    /**
     * 加载头像回调
     */
    MyRankItem.prototype.onLoadHead = function (texture) {
        this.m_imgHead.texture = texture;
    };
    return MyRankItem;
}(egret.DisplayObjectContainer));
__reflect(MyRankItem.prototype, "MyRankItem");
/**
 * 结算页子项
 */
var SettleItem = (function (_super) {
    __extends(SettleItem, _super);
    function SettleItem(data) {
        var _this = _super.call(this) || this;
        var width = _this.width = 110;
        var height = _this.height = 240;
        // 排名
        var rank = new egret.TextField;
        rank.textColor = data.isMe ? 0x58D334 : 0xffffff;
        rank.text = data.rank + '';
        rank.x = width - rank.width >> 1;
        // rank.size = 28;
        rank.size = 32;
        // 头像
        var head = new egret.Bitmap;
        head.width = head.height = 75;
        head.x = width - head.width >> 1;
        // head.y = 36;
        head.y = 50;
        Main.setTexture(head, data.avatarUrl);
        // 名称
        var name = new egret.TextField;
        name.text = Main.getLimitStr(data.nickname, 9);
        name.size = 30;
        name.x = width - name.width >> 1;
        name.y = 150;
        // 分数
        var score = new egret.TextField;
        score.text = data.score + '';
        score.size = 34;
        score.x = width - score.width >> 1;
        score.y = 200;
        _this.addChild(rank);
        _this.addChild(head);
        _this.addChild(name);
        _this.addChild(score);
        return _this;
    }
    return SettleItem;
}(egret.DisplayObjectContainer));
__reflect(SettleItem.prototype, "SettleItem");
