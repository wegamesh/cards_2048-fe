var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/**
 * 结算页——锚点在中心，请居中显示该控件
 */
var SettleList = (function (_super) {
    __extends(SettleList, _super);
    function SettleList() {
        return _super.call(this) || this;
    }
    /**
     * 初始化UI界面
     */
    SettleList.prototype.initUI = function () {
        return __awaiter(this, void 0, void 0, function () {
            var self, utils, infos, user, data, uInfo, text, myInfo, i, len, info, shows, length, startX, i, item;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        self = this;
                        utils = WXUtils;
                        return [4 /*yield*/, utils.getFriendInfos()];
                    case 1:
                        infos = _a.sent();
                        return [4 /*yield*/, utils.getUserInfo()];
                    case 2:
                        user = _a.sent();
                        return [4 /*yield*/, utils.getUserKVData()];
                    case 3:
                        data = _a.sent();
                        uInfo = utils.getInfoByKVData(data);
                        text = new egret.TextField;
                        text.size = 34;
                        text.textColor = 0xfbf0b0;
                        text.text = '历史最高分：' + uInfo.score;
                        text.x = -text.width / 2;
                        self.addChild(text);
                        for (i = 0, len = infos.length; i < len; i++) {
                            info = infos[i];
                            if (utils.isMyInfo(info)) {
                                info.score = Number(uInfo.score);
                                info.isMe = true;
                                myInfo = info;
                                break;
                            }
                        }
                        // 新用户数据来不及存进去的时候
                        if (!myInfo) {
                            user.score = uInfo.score;
                            user.isMe = true;
                            infos.push(user);
                        }
                        // 按分数从大到小排序
                        infos.sort(function (a, b) {
                            return b.score - a.score;
                        });
                        shows = utils.getNearInfos(infos, myInfo);
                        length = shows.length - 1;
                        startX = [-10, -130, -220][length];
                        for (i in shows) {
                            item = new SettleItem(shows[i]);
                            item.x = startX + Number(i) * 170;
                            item.y = 66;
                            self.addChild(item);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    return SettleList;
}(egret.DisplayObjectContainer));
__reflect(SettleList.prototype, "SettleList");
