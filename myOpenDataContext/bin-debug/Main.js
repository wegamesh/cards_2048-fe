var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var Dispatch = lie.Dispatch;
var Main = (function (_super) {
    __extends(Main, _super);
    function Main() {
        var _this = this;
        console.log("发放域");
        _this = _super.call(this) || this;
        // 注册
        Dispatch.register('updateSocre', _this.updateSocre, _this);
        // 初始化
        _this.width = 750;
        _this.height = 1334;
        WXUtils.getUserInfo(); // 自己的信息先拿
        // this.onExit();  // 刚进来没东西给你看
        wx.onMessage(_this.onMessage.bind(_this));
        // 主菜单分数文本
        var label = _this.m_lblScore = new egret.TextField;
        label.size = 80;
        label.y = 280;
        label.textColor = 0xE87A20;
        _this.addChild(label);
        _this.updateSocre();
        _this.updateUser();
        return _this;
    }
    /**
     * 清除多余的控件
     */
    Main.prototype.clearChild = function (clear) {
        var self = this;
        // 除了主界面的分数，其他的都删了
        for (var i = 1, num = self.numChildren; i < num; i++) {
            clear ? self.removeChildAt(1) : (self.getChildAt(i).visible = false);
        }
    };
    /**
     * 接收消息
     */
    Main.prototype.onMessage = function (msg) {
        var self = this;
        var action = msg.action;
        var data = msg.data;
        console.log('onMessage', msg);
        switch (action) {
            case 'score':
                console.log("score");
                self.m_lblScore.visible = true;
                break;
            case 'update':
                self.updateUser(data);
                break;
            // 结算界面
            case 'settle':
                self.initView(0);
                break;
            // 复活界面
            case 'revive':
                self.initView(1, data);
                break;
            // 排行榜界面
            case 'rank':
                self.initView(2, data);
                break;
            // case 'last':
            //     self.setRankPage(-1);
            //     break;
            // case 'next':
            //     self.setRankPage(1);
            //     break;
            case 'exit':
                self.onExit(data);
            case 'pause':
                egret.ticker.pause();
                break;
            case 'resume':
                egret.ticker.resume();
                break;
        }
    };
    /**
     * 更新玩家分数
     */
    Main.prototype.updateSocre = function (data) {
        var self = this;
        var label = self.m_lblScore;
        label.text = '' + (data ? (data.score || 0) : 0);
        label.x = self.width - label.width >> 1;
        // 更新列表
        var settle = self.$children[1];
        if (settle instanceof SettleList)
            settle.initUI();
    };
    /**
     * 更新用户信息
     */
    Main.prototype.updateUser = function (newInfo) {
        var self = this;
        var clzz = WXUtils;
        clzz.updateUserKVData(newInfo);
    };
    /**
     * 初始化界面
     */
    Main.prototype.initView = function (type, data) {
        var self = this, target;
        var clear = true;
        self.m_lblScore.visible = false;
        switch (type) {
            case 0:
                target = new SettleList;
                target.x = 375;
                target.y = 424;
                break;
            case 1:
                target = new ReviveView(data);
                break;
            case 2:
                target = new RankList;
                clear = data;
                break;
        }
        self.clearChild(clear);
        self.addChild(target);
    };
    /**
     * 设置排行榜页数
     */
    // public setRankPage(change: number): void {
    //     var children = this.$children;
    //     var rank = children[children.length - 1];
    //     if (rank instanceof RankList) {
    //         rank.page += change;
    //     }
    // }
    /**
     * 移除结算页
     * @param type 对应initView
     */
    Main.prototype.onExit = function (type) {
        var self = this;
        var clzz;
        switch (type) {
            case 0:
                clzz = SettleList;
                break;
            case 1:
                clzz = ReviveView;
                break;
            case 2:
                clzz = RankList;
                break;
        }
        if (clzz)
            for (var i = self.numChildren - 1; i > 0; i--) {
                var child = self.getChildAt(i);
                if (child instanceof clzz) {
                    self.removeChildAt(i);
                    self.getChildAt(i - 1).visible = true;
                    return;
                }
            }
        self.clearChild(true);
    };
    /**
     * 加载资源
     * @param url 资源地址
     * @param call 回调
     * @param thisObj 回调所属对象
     * @param param 回调参数，注意第一个默认是纹理，因此param会在第二个参数
     */
    Main.loadUrl = function (url) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve) {
                        var self = Main;
                        var texture = self.m_pCache[url];
                        var endCall = function () {
                            resolve(texture);
                        };
                        if (texture)
                            endCall();
                        else {
                            var loader = new egret.ImageLoader();
                            loader.addEventListener(egret.Event.COMPLETE, function (event) {
                                var imageLoader = event.currentTarget;
                                texture = new egret.Texture();
                                texture._setBitmapData(imageLoader.data);
                                self.m_pCache[url] = texture;
                                endCall();
                            }, null);
                            loader.load(url);
                        }
                    })];
            });
        });
    };
    /**
     * 设置纹理
     */
    Main.setTexture = function (bitmap, url) {
        return __awaiter(this, void 0, void 0, function () {
            var t;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, Main.loadUrl(url)];
                    case 1:
                        t = _a.sent();
                        bitmap.texture = t;
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * 移除缓存
     */
    Main.clearCache = function () {
        Main.m_pCache = null;
    };
    /**
     * 添加圆形遮罩
     */
    Main.getCircleShape = function (size) {
        var shape = new eui.Rect;
        shape.width = shape.height = size;
        // shape.ellipseWidth = size;
        // shape.ellipseHeight = size;
        return shape;
    };
    /**
     * 获取编码所占字节数
     */
    Main.getCodeLen = function (c) {
        var len = 0;
        if (c < 0x007f) {
            len = 1;
        }
        else if ((0x0080 <= c) && (c <= 0x07ff)) {
            len = 2;
        }
        else if ((0x0800 <= c) && (c <= 0xffff)) {
            len = 3;
        }
        return len;
    };
    /**
     * 字符串接省略号
     * @param name 字符串
     * @param max 限制字节数
     */
    Main.getLimitStr = function (name, max) {
        var str = '';
        var total = 0;
        for (var i = 0, len = name.length; i < len; i++) {
            var c = name.charCodeAt(i);
            total += Main.getCodeLen(c);
            if (total > max && i < len - 1) {
                str += '..';
                break;
            }
            str += name[i];
        }
        return str;
    };
    // 控件工具方法
    Main.m_pCache = {}; // 存放url资源
    return Main;
}(egret.DisplayObjectContainer));
__reflect(Main.prototype, "Main");
