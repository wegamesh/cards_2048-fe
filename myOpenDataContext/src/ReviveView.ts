/**
 * 复活界面
 */
class ReviveView extends egret.DisplayObjectContainer {

	/**
	 * @param 传入当前的分数
	 */
	public constructor(score: number) {
		super();
		this.initHisScore();
		this.initExceed(score);
	}

	/**
	 * 初始化历史最高分
	 */
	protected async initHisScore(): Promise<void> {
		var kvData = await WXUtils.getUserKVData();
		var info = WXUtils.getInfoByKVData(kvData);
		var text = new egret.TextField;
		text.size = 38;
		text.y = 324;
		text.textColor = 0xfbf0b0;
		text.text = '历史最高分：' + info.score;
		text.x = 750 - text.width >> 1;
		this.addChild(text);
	}

	/**
	 * 初始化即将超过谁
	 */
	protected async initExceed(score: number): Promise<void> {
		var clzz = WXUtils;
		var infos = await clzz.getFriendInfos();
		infos.sort(function (a, b) {
			return a.score - b.score;
		});
		for (let i = 0, len = infos.length; i < len; i++) {
			let info = infos[i];
			if (!clzz.isMyInfo(info) && info.score > score) {
				// 创建即将超过谁的文本
				this.initBotView(info);
				break;
			}
		}
	}

	/**
	 * 创建底部界面
	 */
	protected initBotView(info: UserGameData): void {
		var view = new egret.DisplayObjectContainer;
		var height = view.height = 96;
		// 左边文本
		var text0 = new egret.TextField;
		text0.size = 38;
		text0.text = '下个即将超过的好友：';
		// 中间头像
		var head = new egret.Bitmap;
		head.height = head.width = height;
		Main.setTexture(head, info.avatarUrl);
		// 右边分数
		var text1 = new egret.TextField;
		text1.size = 38;
		text1.text = info.score + '';
		// 加入view
		view.addChild(text0);
		view.addChild(head);
		view.addChild(text1);
		// 设置坐标
		text1.y = text0.y = height - text0.height >> 1;	// 文字尺寸一样可以这样处理
		head.x = text0.width - 4;
		text1.x = head.x + height + 14;
		view.x = 750 - view.width >> 1;
		view.y = 1180;
		this.addChild(view);
	}
}