/**
 * 排行榜子项
 */
class RankItem extends eui.ItemRenderer {

	private m_lblRank: egret.TextField;
	private m_imgRank: egret.Bitmap;
	private m_imgHead: egret.Bitmap;
	private m_imgBg: egret.Bitmap;
	private m_lblName: egret.TextField;
	private m_lblScore: egret.TextField;
	public data: UserGameData;

	public constructor() {
		super();
		this.initView();
	}

	protected onDestroy(): void {
		this.data = null;
	}

	/**
	 * 初始化界面
	 */
	private async initView(): Promise<void> {
		var self = this, clzz = Main;
		// 背景
		var bg = self.m_imgBg = new eui.Image;
		var width = self.width = 508;
		var height = self.height = 105;
		self.addChild(bg);

		// // 背景
		// var bg1 =self.m_imgBg= new egret.Bitmap;
		// var width = self.width = 512;
		// var height = self.height = 110;
		
		// bg1.x = 0;
		// bg1.width =484 ;
		// bg1.height = height;
		// self.addChild(bg1);
		// // var bg = Main.showFriends ? "ph_tm.png" : "group_ranking_bg.png";
		// Main.setTexture(bg1, 'resource/assets/rankPict/rankBg.png' );
		// 排名
		var rank = self.m_lblRank = new eui.Label;
		rank.size = 38;
		rank.verticalCenter = 0;
		rank.horizontalCenter = -242;
		self.addChild(rank);
		// 头像
		var head = self.m_imgHead = new eui.Image;
		var hSize = head.height = head.width = 60;
		var shape = clzz.getCircleShape(hSize);
		head.verticalCenter = shape.verticalCenter = 0;
		head.horizontalCenter = shape.horizontalCenter = -182;
		head.mask = shape;
		self.addChild(head);
		self.addChild(shape);
		// 排名皇冠
		var rank1 = self.m_imgRank = new eui.Image;
		rank1.y = 3;
		rank1.horizontalCenter = -152;
		self.addChild(rank1);
		// 微信名字
		var name = self.m_lblName = new eui.Label;
		// name.x = 210;
		// name.y = 22;
		name.verticalCenter = 0;
		name.horizontalCenter = 16;
		name.size = 30;
		// name.textAlign = name.left;
		self.addChild(name);
		// 分数
		var score = self.m_lblScore = new eui.Label;
		// score.x = 500;
		// score.y = 22;
		score.verticalCenter = 0;
		score.horizontalCenter = 200;
		score.size = 38;
		self.addChild(score);
	}

	/**
	 * 数据变化
	 */
	public dataChanged(): void {
		var self = this;
		var clzz = Main;
		var setTexture = clzz.setTexture;
		var data = self.data;
		var rank = data.rank;
		var rankImg = self.m_imgRank;
		var headImg = self.m_imgHead;
		var rankLbl = self.m_lblRank;
		// 排名
		rankLbl.text = '' + rank;
		rankLbl.textColor = self.getRankColor(rank);
		if (rankImg.visible = rank < 4) {
			setTexture(rankImg, 'resource/assets/rank/settle/icon_no' + rank + '_report.png');
		}
		// 头像
		setTexture(headImg, data.avatarUrl);
		setTexture(self.m_imgBg, self.getItemBg(rank));
		self.m_lblName.text = Main.getLimitStr(data.nickname, 27);
		self.m_lblScore.text = self.setScoreDigit(data.score);
	}

	/**
	 * 获取背景
	 */
	public getItemBg(rank: number): string {
		// var idx = rank < 4 ? rank : '';
		// return 'resource/rank/kuabg' + idx + '.png'
		var idx = rank%2;
		if(idx == 1){
			return 'resource/assets/rank/rankPict/othersRank.png'
		}
		// else{
		// 	return 'resource/asserts/rankPict/myRank.png'
		// }	
	}

	/**
	 * 获取排名颜色
	 */
	public getRankColor(rank: number): number {
		var colors = [0xffdb00, 0xfd973d, 0xd59f7b];
		return colors[rank - 1] || 0xa5a5a5;
	}

	/**
     * 分数保留5位数
     */
    private setScoreDigit(score: number): string {
        var string = score.toString()

        if (score > 10000) {
            let num = (score / 1000).toFixed(2)
            string = num + 'k'
        }
        if (score > 1000000) {
            let num = (score / 1000000).toFixed(2)
            string = num + 'm'
        }
        return string
    }
}
/**
 * 我的排名
 */
class MyRankItem extends egret.DisplayObjectContainer {

	private m_lblRank: egret.TextField;
	private m_imgHead: egret.Bitmap;
	private m_lblName: egret.TextField;
	private m_lblScore: egret.TextField;
	private m_imgRank: egret.Bitmap;
	private m_imgBg: egret.Bitmap;

	public constructor() {
		super();
		this.initView();
	}

	/**
	 * 初始化界面
	 */
	private initView(): void {
		var self = this;
		// 排名
		var rank = self.m_lblRank = new eui.Label;
		rank.size = 40;
		// rank.verticalCenter = 0;
		// rank.horizontalCenter = -206;
		// rank.verticalCenter = 0;
		// rank.horizontalCenter = -230;
		rank.x = 28;
		rank.y = 12;
		self.addChild(rank);
		// 头像
		var head = self.m_imgHead = new eui.Image;
		var hSize = head.height = head.width = 60;
		var shape = Main.getCircleShape(hSize);
		// head.verticalCenter = shape.verticalCenter = 0;
		// head.horizontalCenter = shape.horizontalCenter = -82;
		head.x = shape.x = 75;
		head.y = shape.y = -3;
		head.mask = shape;
		self.addChild(head);
		self.addChild(shape);
		// 排名皇冠
		var rank1 = self.m_imgRank = new eui.Image;
		rank1.x = 115;
		rank1.y = -16;
		// rank1.horizontalCenter = -82;
		self.addChild(rank1);
		// 微信名字
		var name = self.m_lblName = new eui.Label;
		name.x = 240;
		name.y = 12;
		name.size = 36;
		// name.textAlign = name.left;

		self.addChild(name);
		// 分数
		var score = self.m_lblScore = new eui.Label;
		score.x = 450;
		score.y = 12;
		score.size = 40;
		self.addChild(score);
	}

	/**
	 * 设置界面
	 */
	public setData(data: UserGameData): void {
		var self = this;
		self.m_lblRank.text = <any>data.rank;
		// 头像
		Main.setTexture(self.m_imgHead, data.avatarUrl);
		// setTexture(data.avatarUrl, self.onLoadHead, self);
		self.m_lblName.text = data.nickname;
		self.m_lblScore.text = '';
		self.m_lblScore.text = this.setScoreDigit(data.score)
		// console.log("走到这里了吗");
		console.log(data.nickname,"昵称");
		console.log(data.score,"昵称fenshu");
		console.log(data.rank,"rank");
		console.log(data.avatarUrl,"touxiang");
		var rankImg = self.m_imgRank;
		if (rankImg.visible = data.rank < 4) {
			Main.setTexture(rankImg, 'resource/assets/rank/settle/icon_no' + data.rank + '_report.png');
		}
	}

	/**
	 * 加载头像回调
	 */
	private onLoadHead(texture: egret.Texture): void {
		this.m_imgHead.texture = texture;
	}

	/**
     * 分数保留5位数
     */
    private setScoreDigit(score: number): string {
        var string = score.toString()

        if (score > 10000) {
            let num = (score / 1000).toFixed(2)
            string = num + 'k'
        }
        if (score > 1000000) {
            let num = (score / 1000000).toFixed(2)
            string = num + 'm'
        }
        return string
    }
}




/**
 * 结算页子项
 */
class SettleItem extends egret.DisplayObjectContainer {

	public constructor(data: UserGameData) {
		super();
		var width = this.width = 110;
		var height = this.height = 240;
		// 排名
		var rank = new egret.TextField;
		rank.textColor = data.isMe ? 0x58D334 : 0xffffff;
		rank.text = data.rank + '';
		rank.x = width - rank.width >> 1;
		// rank.size = 28;
		rank.size = 32;
		// 头像
		var head = new egret.Bitmap;
		head.width = head.height = 75;
		head.x = width - head.width >> 1;
		// head.y = 36;
		head.y = 50;
		Main.setTexture(head, data.avatarUrl);
		// 名称
		var name = new egret.TextField;
		name.text = Main.getLimitStr(data.nickname, 9);
		name.size = 30;
		name.x = width - name.width >> 1;
		name.y = 150;
		// 分数
		var score = new egret.TextField;
		score.text = data.score + '';
		score.size = 34;
		score.x = width - score.width >> 1;
		score.y = 200;
		this.addChild(rank);
		this.addChild(head);
		this.addChild(name);
		this.addChild(score);
	}
}