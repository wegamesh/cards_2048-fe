declare var wx;

/**
 * 存储数据类型
 */
interface KVData {
    key: string;
    value: string;
}

/**
 * 分数
 */
interface ITFScore {
    score: number;
    timer: number;
}

/**
 * 玩家云数据
 */
interface UserGameData {
    avatarUrl: string;  // 微信头像
    nickname: string;   // 昵称
    openid: string;     // 玩家openid
    KVDataList: KVData[];// 玩家数据
    score: number;      // 这个是自己加的字段
    time: number;      // 这个是自己加的字段
    rank: number;       // 这个是自己加的字段
    isMe: boolean;      // 这个是自己加的字段
}

/**
 * 微信通讯格式
 */
interface ITFWXMessage {
    action: string;
    data: any;
}

const Dispatch = lie.Dispatch;

class Main extends egret.DisplayObjectContainer {

    private m_lblScore: egret.TextField;

    public constructor() {
        console.log("发放域")
        super();
        // 注册
        Dispatch.register('updateSocre', this.updateSocre, this);
        // 初始化
        this.width = 750;
        this.height = 1334;
        WXUtils.getUserInfo(); // 自己的信息先拿
        // this.onExit();  // 刚进来没东西给你看
        wx.onMessage(this.onMessage.bind(this));
        // 主菜单分数文本
        var label = this.m_lblScore = new egret.TextField;
        label.size = 80;
        label.y = 280;
        label.textColor = 0xE87A20;
        // this.addChild(label);
        this.updateSocre();
        this.updateUser();
    }

    /**
     * 清除多余的控件
     */
    public clearChild(clear?: boolean): void {
        var self = this;
        // 除了主界面的分数，其他的都删了
        for (let i = 1, num = self.numChildren; i < num; i++) {
            clear ? self.removeChildAt(1) : (self.getChildAt(i).visible = false);
        }
    }

    /**
     * 接收消息
     */
    protected onMessage(msg: ITFWXMessage): void {
        var self = this;
        var action = msg.action;
        var data = msg.data;
        console.log('onMessage', msg);
        switch (action) {
            case 'score':
                console.log("score")
                self.m_lblScore.visible = true;
                break;
            case 'update':
                self.updateUser(data);
                break;
            // 结算界面
            case 'settle':
                self.initView(0);
                break;
            // 复活界面
            case 'revive':
                self.initView(1, data);
                break;
            // 排行榜界面
            case 'rank':
                console.log(data)
                self.initView(2, data);
                break;
            // case 'last':
            //     self.setRankPage(-1);
            //     break;
            // case 'next':
            //     self.setRankPage(1);
            //     break;
            case 'beyond':
                self.initView(3, data)
                break
            case 'exit':
                self.onExit(data);
                break
            case 'clear':
                self.removeChildren();
                break
            case 'pause':
                egret.ticker.pause();
                break;
            case 'resume':
                egret.ticker.resume();
                break;
        }
    }

    /**
     * 更新玩家分数
     */
    public updateSocre(data?: ITFScore): void {
        var self = this;
        var label = self.m_lblScore;
        label.text = '' + (data ? (data.score || 0) : 0);
        label.x = self.width - label.width >> 1;
        // 更新列表
        var settle = self.$children[1];
        if (settle instanceof SettleList)
            settle.initUI();
    }

    /**
     * 即将超越好友
     */
    private initBeyondView(){

    }

    /**
     * 更新用户信息
     */
    public updateUser(newInfo?: ITFScore): void {
        var self = this;
        var clzz = WXUtils;
        clzz.updateUserKVData(newInfo);
    }

    /**
     * 初始化界面
     */
    private initView(type: number, data?: any): void {
        var self = this, target;
        var clear = true;
        self.m_lblScore.visible = false;
        switch (type) {
            case 0:
                target = new SettleList;
                target.x = 375;
                target.y = 424;
                break;
            case 1:
                target = new ReviveView(data);
                break;
            case 2:
                target = new RankList;
                clear = data;
                break;
            case 3:
                target = BeyondView.getInstance()
                target.setScore(data)
                break
        }
        self.clearChild(clear);
        self.addChild(target);
        console.log(self.numChildren)
    }

    /**
     * 设置排行榜页数
     */
    // public setRankPage(change: number): void {
    //     var children = this.$children;
    //     var rank = children[children.length - 1];
    //     if (rank instanceof RankList) {
    //         rank.page += change;
    //     }
    // }

    /**
     * 移除结算页
     * @param type 对应initView
     */
    public onExit(type?: number): void {
        var self = this;
        var clzz;
        switch (type) {
            case 0:
                clzz = SettleList;
                break;
            case 1:
                clzz = ReviveView;
                break;
            case 2:
                clzz = RankList;
                break;
        }
        if (clzz)
            for (let i = self.numChildren - 1; i > 0; i--) {
                let child = self.getChildAt(i);
                if (child instanceof clzz) {
                    self.removeChildAt(i);
                    self.getChildAt(i - 1).visible = true;
                    return;
                }
            }
        self.clearChild(true);
    }

    // 控件工具方法

    private static m_pCache: { [key: string]: egret.Texture } = {};  // 存放url资源

    /**
     * 加载资源
     * @param url 资源地址
     * @param call 回调
     * @param thisObj 回调所属对象
     * @param param 回调参数，注意第一个默认是纹理，因此param会在第二个参数
     */
    private static async loadUrl(url: string): Promise<egret.Texture> {
        return new Promise<egret.Texture>(function (resolve) {
            let self = Main;
            let texture = self.m_pCache[url];
            let endCall = function () {
                resolve(texture);
            };
            if (texture)
                endCall();
            else {
                let loader = new egret.ImageLoader();
                loader.addEventListener(egret.Event.COMPLETE, function (event: egret.Event) {
                    let imageLoader = <egret.ImageLoader>event.currentTarget;
                    texture = new egret.Texture();
                    texture._setBitmapData(imageLoader.data);
                    self.m_pCache[url] = texture;
                    endCall();
                }, null);
                loader.load(url);
            }
        })
    }

    /**
     * 设置纹理
     */
    public static async setTexture(bitmap: egret.Bitmap, url: string): Promise<void> {
        var t = await Main.loadUrl(url);
        bitmap.texture = t;
    }

    /**
     * 移除缓存
     */
    public static clearCache(): void {
        Main.m_pCache = null;
    }

    /**
     * 添加圆形遮罩
     */
    public static getCircleShape(size: number): eui.Rect {
        var shape = new eui.Rect;
        shape.width = shape.height = size;
        // shape.ellipseWidth = size;
        // shape.ellipseHeight = size;
        return shape;
    }

    /**
     * 获取编码所占字节数
     */
    public static getCodeLen(c: number): number {
        var len = 0;
        if (c < 0x007f) {
            len = 1;
        } else if ((0x0080 <= c) && (c <= 0x07ff)) {
            len = 2;
        } else if ((0x0800 <= c) && (c <= 0xffff)) {
            len = 3;
        }
        return len;
    }

    /**
     * 字符串接省略号
     * @param name 字符串
     * @param max 限制字节数
     */
    public static getLimitStr(name: string, max: number): string {
        var str = '';
        var total = 0;
        for (let i = 0, len = name.length; i < len; i++) {
            let c = name.charCodeAt(i);
            total += Main.getCodeLen(c);
            if (total > max && i < len - 1) {
                str += '..';
                break;
            }
            str += name[i];
        }
        return str;
    }
}