/**
 * 结算页——锚点在中心，请居中显示该控件
 */
class SettleList extends egret.DisplayObjectContainer {

	public constructor() {
		super();
	}

	/**
	 * 初始化UI界面
	 */
	public async initUI(): Promise<void> {
		var self = this;
		var utils = WXUtils;
		// 获取列表
		var infos = await utils.getFriendInfos();
		var user = await utils.getUserInfo();
		// 获取个人数据
		var data = await utils.getUserKVData();
		var uInfo = utils.getInfoByKVData(data);
		// 历史最高分
		var text = new egret.TextField;
		text.size = 34;
		text.textColor = 0xfbf0b0;
		text.text = '历史最高分：' + uInfo.score;
		text.x = -text.width / 2;
		self.addChild(text);
		// 找出个人分数，并更新列表中个人的分数
		var myInfo;
		for (let i = 0, len = infos.length; i < len; i++) {
			let info = infos[i];
			if (utils.isMyInfo(info)) {
				info.score = Number(uInfo.score);
				info.isMe = true;
				myInfo = info;
				break;
			}
		}
		// 新用户数据来不及存进去的时候
		if (!myInfo) {
			user.score = uInfo.score;
			user.isMe = true;
			infos.push(user);
		}
		// 按分数从大到小排序
		infos.sort(function (a, b) {
			return b.score - a.score;
		});
		// 开始显示
		var shows = utils.getNearInfos(infos, myInfo);
		var length = shows.length - 1;
		// var startX = [0, -110, -205][length];
		var startX = [-10, -130, -220][length];
		for (let i in shows) {
			let item = new SettleItem(shows[i]);
			item.x = startX + Number(i) * 170;
			item.y = 66;
			self.addChild(item);
		}
	}
}