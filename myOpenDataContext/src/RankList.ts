/**
 * 排行榜界面
 */
class RankList extends egret.DisplayObjectContainer {

	// private $page: number = 0;
	// private $size: number = 7;	// 一页显示的数量
	private $infos: UserGameData[];
	private m_pMyItem: MyRankItem;

	private m_pList: eui.List;
	private m_pCurInfos: UserGameData[];

	public constructor() {
		super();
		// var infos = this.$infos;
		this.initView();
		
		// 列表
		// var list = this.m_pList = new eui.List;
		// var layout = list.layout = new eui.VerticalLayout;
		// var width = list.width = 508;
		// // list.height = 760;
		// list.height = 1000;
		// layout.gap = 14;
		// list.itemRenderer = RankItem;
		// list.x = 750 - width >> 1;
		// list.y = 240;
		// this.addChild(list);
		//在这里定义自己的单独列表



		WXUtils.getFriendInfos().then((infos) => {
			// 排名初始化
			infos.sort(function (a, b) { return b.score - a.score });
			infos.map(function (v, i) { v.rank = i + 1 });
			// 更新页数
			this.$infos = infos;
			this.setListData(this.$infos);
		});
	}

	public initView(){
		var self = this;
		var scroller = new eui.Scroller;
		var content = self.m_pList = new eui.List;
		var width = scroller.width = 574;
		scroller.height = 560;
		scroller.x = 750 - width >> 1;
		scroller.y = 240;
		self.addChild(scroller);
		scroller.viewport = content;
		content.itemRenderer = RankItem;
		scroller.bounces = false;
		scroller.scrollPolicyV = eui.ScrollPolicy.ON;

		// 个人信息
		var myItem = this.m_pMyItem = new MyRankItem;
		myItem.height = 208;
		myItem.width = width;
		myItem.x = 750 - width >> 1;
		myItem.y = 962;
		this.addChild(myItem);
	}

	/**
     * 设置列表数据类型
     */
	// public setType(type: number = 0): void {
	// 	var self = this;
	// 	var utils = WXUtils;
	// 	var infos = self.m_pCurInfos;
	// 	// var filterInfo=utils.sortInfo(infos, type);
	// 	self.m_pList.dataProvider = new eui.ArrayCollection(infos);	// 用的静态数据，只能new

	// 	//找出自己的信息
	// 	var myInfo;
	// 	for (let i = 0, len = infos.length; i < len; i++) {
	// 		let info = infos[i];
	// 		if (WXUtils.isMyInfo(info)) {
	// 			myInfo = info;
	// 			break;
	// 		}
	// 	}
	// 	// 初始化我的信息
	// 	// console.log(WXUtils.$userInfo);
	// 	this.m_pMyItem.setData(myInfo);
	// }

	/**
	 * 设置列表数据
	 */
	public setListData(list: UserGameData[]): void {
		var self = this;
		self.m_pCurInfos = list;
		var utils = WXUtils;
		var infos = self.m_pCurInfos;
		// var filterInfo=utils.sortInfo(infos, type);
		self.m_pList.dataProvider = new eui.ArrayCollection(infos);	// 用的静态数据，只能new

		//找出自己的信息
		var myInfo;
		for (let i = 0, len = infos.length; i < len; i++) {
			let info = infos[i];
			if (WXUtils.isMyInfo(info)) {
				myInfo = info;
				break;
			}
		}
		// 初始化我的信息
		// console.log(WXUtils.$userInfo);
		this.m_pMyItem.setData(myInfo);
	}




	// public $onRemoveFromStage(): void {
	// 	var self = this;
	// 	self.m_pList = self.$infos = null;
	// 	super.$onRemoveFromStage();
	// }

	// /**
	//  * 设置页数
	//  * @param page 页数
	//  */
	// public set page(page: number) {
	// 	var self = this;
	// 	if (self.$page != page) {
	// 		// 数据出来时才刷新
	// 		if (self.$infos)
	// 			self.$updatePage(page);
	// 	}
	// }

	// /**
	//  * 获取当前页数
	//  */
	// public get page(): number {
	// 	return this.$page;
	// }

	// /**
	//  * 刷新页数
	//  */
	// private $updatePage(page: number): void {
	// 	var self = this;
	// 	var infos = self.$infos;
	// 	var size = self.$size;
	// 	var max = infos.length;
	// 	var maxp = Math.ceil(max / size) - 1;
	// 	if (page < 0)
	// 		page = 0;
	// 	else if (page > maxp)
	// 		page = maxp;
	// 	else {
	// 		let datas = [];
	// 		let start = page * size;
	// 		let end = start + size;
	// 		for (let i = start; i < max && i < end; i++) {
	// 			datas.push(infos[i]);
	// 		}
	// 		self.updateListData(datas);
	// 	}
	// 	self.$page = page;
	// }

	// /**
	//  * 更新列表数据
	//  */
	// public updateListData(datas: UserGameData[]): void {
	// 	var list = this.m_pList;
	// 	var oldd = <eui.ArrayCollection>list.dataProvider;
	// 	if (oldd)
	// 		oldd.replaceAll(datas);
	// 	else
	// 		list.dataProvider = new eui.ArrayCollection(datas);
	// 	//找出自己的信息
	// 	var myInfo;
	// 	for (let i = 0, len = datas.length; i < len; i++) {
	// 		let info = datas[i];
	// 		if (WXUtils.isMyInfo(info)) {
	// 			// info.score = Number(uInfo.score);
	// 			// info.isMe = true;
	// 			myInfo = info;
	// 			break;
	// 		}
	// 	}
	// 	// if(datas.isMe)

	// 	// 初始化我的信息
	// 	// console.log(WXUtils.$userInfo);
	// 	this.m_pMyItem.setData(myInfo);
	// }
}