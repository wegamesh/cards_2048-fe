class WXUtils {

	public static keyList: string[] = ['score', 'time'];	// 游戏存放数据Key值

	public static $userInfo: any;			// 玩家自己的信息
	private static $userKVData: KVData[];	// 玩家自己的游戏数据
	private static m_pInfos: { [key: string]: UserGameData[] } = {}; // 存放玩家好友、群数据
	// public static userInfo: UserGameData;					// 玩家信息数据

	/**
	 * 获取玩家的信息
	 */
	public static async getUserInfo(): Promise<any> {
		var self = WXUtils;
		return new Promise<any>(function (resolve, reject) {
			let info = self.$userInfo;
			info ? resolve(info) :
				wx.getUserInfo({
					openIdList: ['selfOpenId'],
					lang: 'zh_CN',
					success: function (res) {
						// 注意这里的openid假的，不知为何
						info = self.$userInfo = res.data[0];
						info.nickname = info.nickName;	// 注意个人信息这个坑
						resolve(info);
					},
					fail: function (res) {
						reject();
					}
				});
		});
	}

	/**
	 * 获取玩家游戏数据
	 */
	public static async getUserKVData(): Promise<KVData[]> {
		var self = WXUtils;
		return new Promise<KVData[]>(function (resolve) {
			let data = self.$userKVData;
			data ? resolve(data) :
				wx.getUserCloudStorage({
					keyList: self.keyList,
					success: function (res: any) {
						resolve(res.KVDataList)
					},
					fail: function (res) {
						resolve();
					}
				});
		});
	}

	/**
	 * 更新玩家游戏数据
	 */
	public static updateUserKVData(nInfo: ITFScore): void {
		var self = WXUtils;
		var oData = self.$userKVData;
		var notice = Dispatch.notice;
		var message = 'updateSocre';
		if (nInfo && oData) {
			let oInfo = self.getInfoByKVData(oData);
			// 存入新数据
			if (oInfo.score < nInfo.score) {
				let kvData = self.getKVDataByInfo(nInfo);;
				self.setUserKVData(kvData);
				oInfo = nInfo;
			}
			notice(message, oInfo);
		}
		else {
			self.getUserKVData().then(function (data) {
				let info = data && <ITFScore><any>self.getInfoByKVData(data);
				if (nInfo) {
					if (!info || info.score < nInfo.score) {
						self.setUserKVData(self.getKVDataByInfo(nInfo));
						info = nInfo;
					}
				}
				notice(message, info);
			});
		}
	}

	/**
     * 获取玩家游戏数据
     */
	private static getGameInfos(attr: string, keyList: string[], resolve: (infos: UserGameData[]) => void, shareTicket?: string): void {
		var self = this;
		var call = wx[attr];
		if (call) {
			// 获取缓存内的数据
			let infos = WXUtils.m_pInfos;
			let cache = infos[attr];
			if (cache)
				resolve(cache);
			else {
				let obj = <any>{
					keyList: keyList,
					success: function (res: any) {
						console.log(res);
						let nData = infos[attr] = self.initGameDatas(res.data);
						resolve(nData);
					},
					fail: function (res) {
						self.$getUserInfo(resolve);
					}
				};
				shareTicket && (obj.shareTicket = shareTicket);
				call(obj);
			}
		}
		else
			call([]);
	}

	/**
     * 获取好友数据
     */
	public static async getFriendInfos(): Promise<UserGameData[]> {
		var self = WXUtils;
		return new Promise<UserGameData[]>(function (resolve, reject) {
			self.getGameInfos('getFriendCloudStorage', self.keyList, resolve);
		});
	}

	/**
     * 获取自己的数据
     */
	private static $getUserInfo(resolve: Function): void {
		var self = WXUtils;
		Promise.all([self.getUserInfo(), self.getUserKVData()]).then(function (array) {
			let infos = [];
			if (array) {
				let info = <UserGameData>array[0];
				info.KVDataList = array[1];
				infos[0] = info;
			}
			resolve(infos);
		});
	}

	/**
	 * 保存玩家游戏数据
	 */
	public static setUserKVData(data: KVData[]): Promise<boolean> {
		return new Promise<boolean>(function (resolve) {
			WXUtils.$userKVData = data;
			wx.setUserCloudStorage({
				KVDataList: data,
				success: function () {
					resolve(true);
				},
				fail: function () {
					resolve(false);
				}
			});
		})
	}

	/**
	 * 获取KV数据
	 */
	public static getInfoByKVData(datas: KVData[]): any {
		var info = { score: 0 };
		for (let i in datas) {
			let data = datas[i];
			info[data.key] = data.value;
		}
		return info;
	}

	/**
	 * 获取游戏数据
	 */
	public static getKVDataByInfo(info: { [key: string]: any }): KVData[] {
		var datas = <KVData[]>[];
		var getStr = function (v) {
			if (typeof v === 'object')
				v = JSON.parse(v);
			return v + '';
		};
		for (let i in info) {
			let data = <KVData>{};
			data.key = i;
			data.value = getStr(info[i]);
			datas.push(data);
		}
		return datas;
	}

	/**
	 * 检测info是否是我的数据
	 */
	public static isMyInfo(info: UserGameData): boolean {
		var myInfo = WXUtils.$userInfo;
		return myInfo.avatarUrl == info.avatarUrl
			&& myInfo.nickName == info.nickname;	// 小心大小写
	}

	/**
	 * 清除数据
	 */
	public static clearData(): void {
		WXUtils.m_pInfos = {};
	}

	/**
	 * 获取相近的数据（返回的列表会加上rank字段）
	 * @param infos 列表
	 * @param myInfo 我的数据，保证myInfo在列表中
	 * @param maxNum 最多找几个人，奇数
	 */
	public static getNearInfos(infos: UserGameData[], myInfo: UserGameData, maxNum: number = 3): UserGameData[] {
		// 根据自己的位置找出附近三人
		var myIndex = infos.indexOf(myInfo), len = infos.length;
		var startI;
		// 自己排名最前
		if (myIndex == 0)
			startI = myIndex;
		// 自己最后
		else if (myIndex == len - 1)
			startI = myIndex - maxNum + 1;
		// 自己中间
		else
			startI = myIndex - (maxNum / 2 | 0);
		if (startI < 0)
			startI = 0;
		// 限制最多拿三个
		var shows = [];
		for (let i = startI, len = infos.length, j = 0; i < len && j < maxNum; i++ , j++) {
			let info = infos[i];
			info.rank = i + 1;
			shows.push(info);
		}
		return shows;
	}

	// 根据游戏自己的字段进行修改

	/**
	 * 初始化需要的数据
	 */
	public static initGameDatas(datas: UserGameData[]): UserGameData[] {
		var self = WXUtils;
		var nData = <UserGameData[]>[];
		for (let i in datas) {
			let item = datas[i];
			if (item.openid) {
				let info = <any>self.getInfoByKVData(item.KVDataList);
				// 初始化分数字段
				item.score = Number(info.score);
				item.time = Number(info.time);
				nData.push(item);
			}
		}
		return nData;
	}
}