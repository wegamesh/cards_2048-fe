class BeyondView extends egret.DisplayObjectContainer{
    private score: number
    private m_image: eui.Image
    private l_nickName: eui.Label
    private l_score
    private static beyondView: BeyondView
    public constructor(){
        super()
    }

    public setScore(score: number){
        this.score = score
        this.init()
    }

    private init(){
        var _self = this
        WXUtils.getFriendInfos().then((infos) => {
			// 排名初始化
			infos.sort(function (a, b) { return a.score - b.score });
            // 即将超越的好友
            var item: UserGameData = infos.find((item) => {
                return item.score > this.score
            })
			console.log(item)
            if(item){   
                _self.setBeyondItem(item)
            }else{
                this.m_image && this.removeChild(this.m_image)
                this.l_nickName && this.removeChild(this.l_nickName)
                this.l_score && this.removeChild(this.l_score)
            }
			
		});
    }

    private setBeyondItem(item: UserGameData){
        if(!this.m_image){
            var image = this.m_image = new eui.Image()
            image.width = 60
            image.height = 60
            image.x = 0
            image.y = 0
            image.source = item.avatarUrl
            this.addChild(image)
        }else{
            this.m_image.source = item.avatarUrl
        }

        if(!this.l_nickName){
            var nickName = this.l_nickName = new eui.Label()
            nickName.text = item.nickname
            nickName.textColor = 0x000000
            nickName.x = 70
            nickName.y = 17
            this.addChild(nickName)
        }else{
            this.l_nickName.text = item.nickname
        }

        if(!this.l_score){
            var score = this.l_score = new eui.Label
            score.text = this.setScoreDigit(item.score)
            score.textColor = 0x707070
            score.x = this.l_nickName.x + this.l_nickName.width + 20
            score.y = this.l_nickName.y
            this.addChild(score)
        }else{
            this.l_score.x = this.l_nickName.x + this.l_nickName.width + 20
            this.l_score.text = this.setScoreDigit(item.score)
        }
        
    }

    public static getInstance(){
        var self = BeyondView
        if(!self.beyondView){
            self.beyondView = new BeyondView()
        }
        
        return self.beyondView
    }
    /**
	 * 加载头像回调
	 */
	private onLoadHead(texture: egret.Texture): void {
		// this.m_imgHead.texture = texture;
	}

    /**
     * 分数保留5位数
     */
    private setScoreDigit(score: number): string {
        var string = score.toString()

        if (score > 10000) {
            let num = (score / 1000).toFixed(2)
            string = num + 'k'
        }
        if (score > 1000000) {
            let num = (score / 1000000).toFixed(2)
            string = num + 'm'
        }
        return string
    }
}